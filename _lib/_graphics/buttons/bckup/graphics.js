(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 640,
	height: 480,
	fps: 25,
	color: "#000000",
	manifest: [
		{src:"images/image1.png?1467300991894", id:"image1"},
		{src:"images/image102.png?1467300991894", id:"image102"},
		{src:"images/image109.png?1467300991894", id:"image109"},
		{src:"images/image111.png?1467300991894", id:"image111"},
		{src:"images/image112.png?1467300991894", id:"image112"},
		{src:"images/image113.png?1467300991894", id:"image113"},
		{src:"images/image17.png?1467300991894", id:"image17"},
		{src:"images/image2.png?1467300991894", id:"image2"},
		{src:"images/image20.png?1467300991894", id:"image20"},
		{src:"images/image25.png?1467300991894", id:"image25"},
		{src:"images/image28.png?1467300991894", id:"image28"},
		{src:"images/image33.png?1467300991894", id:"image33"},
		{src:"images/image36.png?1467300991894", id:"image36"},
		{src:"images/image39.png?1467300991894", id:"image39"},
		{src:"images/image42.png?1467300991894", id:"image42"},
		{src:"images/image47.png?1467300991894", id:"image47"},
		{src:"images/image50.png?1467300991894", id:"image50"},
		{src:"images/image55.png?1467300991894", id:"image55"},
		{src:"images/image58.png?1467300991894", id:"image58"},
		{src:"images/image63.png?1467300991894", id:"image63"},
		{src:"images/image66.png?1467300991894", id:"image66"},
		{src:"images/image71.png?1467300991894", id:"image71"},
		{src:"images/image74.png?1467300991894", id:"image74"},
		{src:"images/image79.png?1467300991894", id:"image79"},
		{src:"images/image82.png?1467300991894", id:"image82"},
		{src:"images/image87.png?1467300991894", id:"image87"},
		{src:"images/image90.png?1467300991894", id:"image90"},
		{src:"images/image93.png?1467300991894", id:"image93"},
		{src:"images/image96.png?1467300991894", id:"image96"},
		{src:"images/image99.png?1467300991894", id:"image99"}
	]
};



// symbols:



(lib.image1 = function() {
	this.initialize(img.image1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,952,587);


(lib.image102 = function() {
	this.initialize(img.image102);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,64,36);


(lib.image109 = function() {
	this.initialize(img.image109);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,58,54);


(lib.image111 = function() {
	this.initialize(img.image111);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,9,27);


(lib.image112 = function() {
	this.initialize(img.image112);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,11,38);


(lib.image113 = function() {
	this.initialize(img.image113);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,8,18);


(lib.image17 = function() {
	this.initialize(img.image17);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,60,27);


(lib.image2 = function() {
	this.initialize(img.image2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,952,110);


(lib.image20 = function() {
	this.initialize(img.image20);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,59,24);


(lib.image25 = function() {
	this.initialize(img.image25);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,59,26);


(lib.image28 = function() {
	this.initialize(img.image28);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,58,23);


(lib.image33 = function() {
	this.initialize(img.image33);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,66,40);


(lib.image36 = function() {
	this.initialize(img.image36);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,63,37);


(lib.image39 = function() {
	this.initialize(img.image39);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,62,41);


(lib.image42 = function() {
	this.initialize(img.image42);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,62,38);


(lib.image47 = function() {
	this.initialize(img.image47);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,45,38);


(lib.image50 = function() {
	this.initialize(img.image50);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,44,35);


(lib.image55 = function() {
	this.initialize(img.image55);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,44,38);


(lib.image58 = function() {
	this.initialize(img.image58);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,44,35);


(lib.image63 = function() {
	this.initialize(img.image63);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,43,39);


(lib.image66 = function() {
	this.initialize(img.image66);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,43,36);


(lib.image71 = function() {
	this.initialize(img.image71);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,43,38);


(lib.image74 = function() {
	this.initialize(img.image74);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,43,35);


(lib.image79 = function() {
	this.initialize(img.image79);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,44,37);


(lib.image82 = function() {
	this.initialize(img.image82);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,44,34);


(lib.image87 = function() {
	this.initialize(img.image87);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,58,37);


(lib.image90 = function() {
	this.initialize(img.image90);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,56,35);


(lib.image93 = function() {
	this.initialize(img.image93);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,62,37);


(lib.image96 = function() {
	this.initialize(img.image96);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,60,34);


(lib.image99 = function() {
	this.initialize(img.image99);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,65,38);


(lib.shape115 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#993300").s().p("AhoBpQgsgsgBg9QABg9AsgrQArgsA9gBQA9ABAsAsQAtArgBA9QABA9gtAsQgsAtg9gBQg9ABgrgtgAhTBjQAjAeAwAAQA2ABAmgmQAmgmgBg2QAAgwgegjgAhbhbQglAngBA0QABAwAfAkIC1i1QgkgfgwgBQg0ABgnAlg");
	this.shape.setTransform(15,15);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,30,30);


(lib.shape114 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image112, null, new cjs.Matrix2D(0.472,0,0,0.472,-2.6,-8.9)).s().p("AgYBZIAAixIAxAAIAACxg");
	this.shape.setTransform(25.6,16);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(img.image111, null, new cjs.Matrix2D(0.472,0,0,0.472,-2.1,-6.4)).s().p("AgUA/IAAh9IApAAIAAB9g");
	this.shape_1.setTransform(22.4,15.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(img.image113, null, new cjs.Matrix2D(0.472,0,0,0.472,-1.9,-4.2)).s().p("AgRAqIAAhTIAjAAIAABTg");
	this.shape_2.setTransform(18.9,15.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(17,7,11.2,17.9);


(lib.shape110 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image109, null, new cjs.Matrix2D(1,0,0,1,-29,-27)).s().p("AkhEOIAAobIJCAAIAAIbg");
	this.shape.setTransform(29,27);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,58,54);


(lib.shape108 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.298)").s().p("AicCPIAAkYQAAgTATAAIEYAAQAKAAAEAFIgKAJIgEgBIkYAAQgGAAAAAGIAAEYQAAABAAAAQAAABAAABQABAAAAABQAAAAAAAAIgJAKQgFgEAAgKg");
	this.shape.setTransform(15.8,15.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(0,0,0,0.298)").s().p("AiOCdQgKAAgEgFIAJgKIAFACIEYAAQAGAAAAgGIAAkYIgCgFIAKgJQAFAEAAAKIAAEYQAAATgTAAg");
	this.shape_1.setTransform(16.3,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("AiMCgQgSAAgBgUIAAkYQABgSASgBIEYAAQATABABASIAAEYQgBAUgTAAg");
	this.shape_2.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,32,32);


(lib.shape106 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AApCLIAbgbIArAAIAAgrIAcgcIAABigAiKCLIAAhiIAbAaIAAAtIAsAAIAbAbgABvhEIAAgrIgrAAIgbgbIBiAAIAABigAiKiKIBiAAIgbAbIgtAAIAAAtIgaAag");
	this.shape.setTransform(16,15.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.298)").s().p("AicCPIAAkYQAAgTATAAIEYAAQAKAAAEAFIgKAJIgEgBIkYAAQgGAAAAAGIAAEYQAAABAAAAQAAABAAABQABAAAAABQAAAAAAAAIgJAKQgFgEAAgKg");
	this.shape_1.setTransform(15.8,15.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.298)").s().p("AiOCdQgKAAgEgFIAJgKIAFACIEYAAQAGAAAAgGIAAkYIgCgFIAKgJQAFAEAAAKIAAEYQAAATgTAAg");
	this.shape_2.setTransform(16.3,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#333333").s().p("AhiCgQg9gBAAg7IAAjGQAAg9A9AAIDGAAQA7AAABA9IAADGQgBA7g7ABg");
	this.shape_3.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,32,32);


(lib.shape105 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#999999").s().p("AAcBfIAVgVIgpgpIAZgZIApApIAVgVIAABDgAhfBfIAAhDIAVAVIAqgpIAaAZIgqApIAVAVgAAIggIApgqIgVgVIBDAAIAABEIgVgVIgpAqgAhKgwIgVAVIAAhEIBEAAIgVAVIAqAqIgaAag");
	this.shape.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.298)").s().p("AicCPIAAkYQAAgTATAAIEYAAQAKAAAEAFIgKAJIgEgBIkYAAQgGAAAAAGIAAEYQAAABAAAAQAAABAAABQABAAAAABQAAAAAAAAIgJAKQgFgEAAgKg");
	this.shape_1.setTransform(15.8,15.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.298)").s().p("AiOCdQgKAAgEgFIAJgKIAFACIEYAAQAGAAAAgGIAAkYIgCgFIAKgJQAFAEAAAKIAAEYQAAATgTAAg");
	this.shape_2.setTransform(16.3,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#333333").s().p("AhiCgQg9gBAAg7IAAjGQAAg9A9AAIDGAAQA7AAABA9IAADGQgBA7g7ABg");
	this.shape_3.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,32,32);


(lib.shape103 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image102, null, new cjs.Matrix2D(1,0,0,1,-32,-18)).s().p("Ak/CzIAAlmIJ+AAIAAFmg");
	this.shape.setTransform(33,20);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1,2,64,36);


(lib.shape100 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image99, null, new cjs.Matrix2D(1,0,0,1,-32.5,-19)).s().p("AlEC9IAAl6IKIAAIAAF6g");
	this.shape.setTransform(32.5,19);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,65,38);


(lib.shape97 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image96, null, new cjs.Matrix2D(1,0,0,1,-30,-17)).s().p("AkrCpIAAlSIJXAAIAAFSg");
	this.shape.setTransform(31,20);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1,3,60,34);


(lib.shape94 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image93, null, new cjs.Matrix2D(1,0,0,1,-31,-18.5)).s().p("Ak1C4IAAlwIJrAAIAAFwg");
	this.shape.setTransform(31,18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,62,37);


(lib.shape91 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image90, null, new cjs.Matrix2D(1,0,0,1,-28,-17.5)).s().p("AkXCuIAAlcIIuAAIAAFcg");
	this.shape.setTransform(28,20.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,3,56,35);


(lib.shape88 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image87, null, new cjs.Matrix2D(1,0,0,1,-29,-18.5)).s().p("AkhC4IAAlwIJCAAIAAFwg");
	this.shape.setTransform(29,18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,58,37);


(lib.shape84 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#000000","rgba(255,255,255,0)","#000000"],[0,0.506,1],-33.8,-5.8,33.2,-10).s().p("AiGCMIgdkWIEnAAIAgEWg");
	this.shape.setTransform(-15.3,-7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.8,-21,33,28);


(lib.shape83 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image82, null, new cjs.Matrix2D(1,0,0,1,-22,-17)).s().p("AjbCpIAAlSIG3AAIAAFSg");
	this.shape.setTransform(22,20);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,3,44,34);


(lib.shape80 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image79, null, new cjs.Matrix2D(1,0,0,1,-22,-18.5)).s().p("AjbC4IAAlwIG3AAIAAFwg");
	this.shape.setTransform(22,18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,44,37);


(lib.shape76 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.8)","rgba(255,255,255,0)","rgba(0,0,0,0.8)"],[0,0.506,1],-39.5,-5.9,39.8,-5.8).s().p("AiaCRIgJkgIE9AAIALEgg");
	this.shape.setTransform(-16.4,-6.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-32.9,-21,33,29);


(lib.shape75 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image74, null, new cjs.Matrix2D(1,0,0,1,-21.5,-17.5)).s().p("AjWCuIAAlcIGtAAIAAFcg");
	this.shape.setTransform(21.5,20.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,3,43,35);


(lib.shape72 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image71, null, new cjs.Matrix2D(1,0,0,1,-21.5,-19)).s().p("AjWC9IAAl6IGtAAIAAF6g");
	this.shape.setTransform(21.5,19);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,43,38);


(lib.shape68 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.8)","rgba(255,255,255,0)","rgba(0,0,0,0.8)"],[0,0.506,1],-38.8,-13.5,40.8,-3.1).s().p("AiVCUIAAknIEqAAIAAEng");
	this.shape.setTransform(-16.5,-5.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.5,-20,30,29.7);


(lib.shape67 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image66, null, new cjs.Matrix2D(1,0,0,1,-21.5,-18)).s().p("AjWCzIAAlmIGtAAIAAFmg");
	this.shape.setTransform(21.5,21);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,3,43,36);


(lib.shape64 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image63, null, new cjs.Matrix2D(1,0,0,1,-21.5,-19.5)).s().p("AjWDCIAAmEIGtAAIAAGEg");
	this.shape.setTransform(21.5,19.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,43,39);


(lib.shape60 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.8)","rgba(255,255,255,0)","rgba(0,0,0,0.8)"],[0,0.506,1],-32.6,-6.9,34.9,-6.8).s().p("AikCRIAdkgIEsAAIAAEgg");
	this.shape.setTransform(-16.5,-5.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-33,-20.1,33,29);


(lib.shape59 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image58, null, new cjs.Matrix2D(1,0,0,1,-22,-17.5)).s().p("AjbCuIAAlcIG3AAIAAFcg");
	this.shape.setTransform(22,20.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,3,44,35);


(lib.shape56 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image55, null, new cjs.Matrix2D(1,0,0,1,-22,-19)).s().p("AjbC9IAAl6IG3AAIAAF6g");
	this.shape.setTransform(22,19);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,44,38);


(lib.shape52 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.8)","rgba(255,255,255,0)","rgba(0,0,0,0.8)"],[0,0.506,1],-33.7,-17,34.6,2).s().p("AisCWIAxkqIEoAAIgmEqg");
	this.shape.setTransform(-18.6,-6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36,-21,34.8,30);


(lib.shape51 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image50, null, new cjs.Matrix2D(1,0,0,1,-22,-17.5)).s().p("AjbCuIAAlcIG3AAIAAFcg");
	this.shape.setTransform(22,20.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,3,44,35);


(lib.shape48 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image47, null, new cjs.Matrix2D(1,0,0,1,-22.5,-19)).s().p("AjgC9IAAl6IHBAAIAAF6g");
	this.shape.setTransform(22.5,19);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,45,38);


(lib.shape44 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.8)","rgba(255,255,255,0)","rgba(0,0,0,0.8)"],[0,0.506,1],-40.6,-19.4,35.2,24.2).s().p("Aj2CHIBMkMIGhAAIg5EMg");
	this.shape.setTransform(-11.6,-6.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.4,-20,49.5,27);


(lib.shape43 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image42, null, new cjs.Matrix2D(1,0,0,1,-31,-19)).s().p("Ak1C+IAAl7IJrAAIAAF7g");
	this.shape.setTransform(31,22);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,3,62,38);


(lib.shape40 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image39, null, new cjs.Matrix2D(1,0,0,1,-31,-20.5)).s().p("Ak1DNIAAmZIJrAAIAAGZg");
	this.shape.setTransform(31,20.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,62,41);


(lib.shape37 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image36, null, new cjs.Matrix2D(1,0,0,1,-31.5,-18.5)).s().p("Ak6C4IAAlwIJ0AAIAAFwg");
	this.shape.setTransform(33.5,21.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(2,3,63,37);


(lib.shape34 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image33, null, new cjs.Matrix2D(1,0,0,1,-33,-20)).s().p("AlJDHIAAmOIKSAAIAAGOg");
	this.shape.setTransform(33,20);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,66,40);


(lib.shape30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.8)","rgba(255,255,255,0)","rgba(0,0,0,0.8)"],[0,0.506,1],-67.8,-10,66.6,8.4).s().p("AmlCBIA+kBIMNAAIgsEBg");
	this.shape.setTransform(2.9,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-39.3,-13,84.4,26);


(lib.shape29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image28, null, new cjs.Matrix2D(1,0,0,1,-29,-11.5)).s().p("AkhBzIAAjlIJDAAIAADlg");
	this.shape.setTransform(30,14.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1,3,58,23);


(lib.shape26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image25, null, new cjs.Matrix2D(1,0,0,1,-29.5,-13)).s().p("AkmCCIAAkDIJNAAIAAEDg");
	this.shape.setTransform(29.5,13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,59,26);


(lib.shape22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.8)","rgba(255,255,255,0)","rgba(0,0,0,0.8)"],[0,0.506,1],-43.6,-8.9,42.1,14).s().p("AjrBRIBBihIGWAAIgxChg");
	this.shape.setTransform(-11.2,-10.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-34.8,-19.1,47.3,16.3);


(lib.shape21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image20, null, new cjs.Matrix2D(1,0,0,1,-29.5,-12)).s().p("AkmB4IAAjvIJNAAIAADvg");
	this.shape.setTransform(29.5,15);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,3,59,24);


(lib.shape18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image17, null, new cjs.Matrix2D(1,0,0,1,-30,-13.5)).s().p("AkrCHIAAkNIJXAAIAAENg");
	this.shape.setTransform(30,13.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,60,27);


(lib.shape13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1C1C1C").s().p("AgGADQgCgDAAgIIASAAIgDALQgEAGgDgBg");
	this.shape.setTransform(53,72.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#222224").s().p("AgIADQAIAAAEgDQAFgGABgGIAAAPIgGAHIgMADg");
	this.shape_1.setTransform(52,69.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#454545").s().p("AgNAUIAAgSIAAAAIAAgBQgBgHAKgGQAGgHAMAAIAAAKQgIABgFAIQgFAHAAANg");
	this.shape_2.setTransform(54.5,68);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0C0C0D").s().p("AgCAJIAFgRIgBARg");
	this.shape_3.setTransform(46.6,75.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3C3C3E").s().p("AAAAHQgLgNgMgDIAKgUIAlAUIAAAVIgHATg");
	this.shape_4.setTransform(44.5,73.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#29292B").s().p("AAGAiQgFgegHgOQgHgLABgNQAMAIAHAPQAGALAAAMQAAAVgEACg");
	this.shape_5.setTransform(46.4,59.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#262628").s().p("AEXA0IAKgUIAVAjQgQgGgPgJgAEnABQAGgHAIAAIAAAIIgUAUQAAgNAGgIgAk1g4IAAgKIAeAAIAAAKg");
	this.shape_6.setTransform(35.1,56.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#2C2D2E").s().p("AgIACIACgQIAPABIgJAcQgFgBgDgMg");
	this.shape_7.setTransform(68,52.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#404041").s().p("AgyBBIAAgKIAAgKIAOAEIAQABIAAgFIAUAWQAEAHgIAAgAAIg/IAAgKIAKAAIAeAAIADAAQgUAKgSAAIgFAAg");
	this.shape_8.setTransform(71.2,50.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#252525").s().p("AgFADIgEgPIASAAIAAAKQgDAKgDADIgDABg");
	this.shape_9.setTransform(38,76.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#383838").s().p("AgIgIQAIAAAEADQAFADABACQgBAGgJADQgHgCgBgPg");
	this.shape_10.setTransform(16,69);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#343435").s().p("AjMA6QgGgDgIAAIAAgLIAKAAIAOAEQAFACAAAFIgCAGIgHAEQgBgFgFgCgADHgsQABgIAFgFQAGgHAJAAIAAAKIgFAIQgEAIgIAAg");
	this.shape_11.setTransform(37,62.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#323233").s().p("AAAANIgNgEQADgUAHgIIARAcIgCAHQgDADgEABQgBgFgEgCg");
	this.shape_12.setTransform(17.5,66);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#4F4F4F").s().p("AgTgJIALAAQAPACANAHQgSAKgDAAQgPAAgDgTg");
	this.shape_13.setTransform(6,57.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#212122").s().p("ACHAYIAAgKIAxgKQAAANgOAJQgPAJgUgBgAi4gNIAPgFIAPgPQAAAIAEAGQADAFADABQgOAPgKACQgGAAgKgRg");
	this.shape_14.setTransform(27.5,63.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#181819").s().p("AgJAOIAAgcIASAAIAAAcg");
	this.shape_15.setTransform(38,73.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#313132").s().p("AgEAWIgFgmIASgFIAAAFIgJAmg");
	this.shape_16.setTransform(30,73.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#161618").s().p("AAAgTIAKAAQAAARgGAKQgEAMgJgBg");
	this.shape_17.setTransform(31,74);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#606063").s().p("AgQgEIAPAAIASAAIgMAGIgKADQgLgBAAgIg");
	this.shape_18.setTransform(33.3,67.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#18171A").s().p("AgIAJIgUAAQAAgDgGgDQgGgDgJAAIAAgJIAfAAQAYALAQABIAcgDIAAAJg");
	this.shape_19.setTransform(36,66);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#19191A").s().p("AgMgDQAAgGACgFIAGgJQAGAAAGAGQAFAFABAJIgTAbQgIgOABgNg");
	this.shape_20.setTransform(23.6,70.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#171718").s().p("AAAAYIgEgvIAIAAIAAAKIAAAlg");
	this.shape_21.setTransform(28.5,61.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#333335").s().p("AAUgXIAAAJQAAAPgMAMQgKAMgRgBg");
	this.shape_22.setTransform(24,55.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#393939").s().p("AgJAUIAAgKQAAgLAHgJQADgIAIgBIgJAng");
	this.shape_23.setTransform(30,58);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#2A2A2B").s().p("AB9AJQAAgIAFgDQAGgHAJAAQgBALgFAJQgGAJgIgBgAiGgMQAIgHANABQAAAIgJAGQgJAEgMAAQAAgHAJgFg");
	this.shape_24.setTransform(46.5,55);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#1A1A1A").s().p("AgNAFIAAgIIAbAAIAAAIg");
	this.shape_25.setTransform(36.5,52.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#1C1C1E").s().p("AAwgLIAMAAIAAASIgFAFQgFgBgCgWgAg7AHIAAgIIAPABQAHADABACIgDACg");
	this.shape_26.setTransform(44,52.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#6D6D6D").s().p("AgBgNIACAIQADAIgFALg");
	this.shape_27.setTransform(26.2,26.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#1D1D1F").s().p("AgEAJIABgUIAIAAIAAASIAAAFg");
	this.shape_28.setTransform(11.5,46.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#191A1C").s().p("AgXAFIAAgJIAvAEIAAAFg");
	this.shape_29.setTransform(17.5,40.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#1A1A1B").s().p("AgOgIIAcAAIgKASQgMgBgGgRg");
	this.shape_30.setTransform(1.5,42);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#28282A").s().p("ACfCvIgKAAQAAgJANgGQALgFARAAIgKAUgAAxBeIAUAKIAAAKgAi8iuIAEAJQACALgGAKg");
	this.shape_31.setTransform(24,54.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#373738").s().p("AgSAKIgQgKIAdgKQAWgIAHgGIAIAJIADALQAAAGgLAXg");
	this.shape_32.setTransform(2.6,33.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#2A2A2A").s().p("AgTgIIAnAAIgUARg");
	this.shape_33.setTransform(3,27);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#212123").s().p("AghB8IgGAAIgKgUIAAgKQATAQgBAGQABAJgCAAIgBgBgAggAjQgIgKABgLIAKAAIALAeQgHgCgHgHgAAchyIgIgKIAdAAIgNALg");
	this.shape_34.setTransform(16,47.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#303031").s().p("AgYgHIAKAAIAnAIIAAAFIgWACQgPgCgMgNg");
	this.shape_35.setTransform(14.5,34.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#242426").s().p("AgEgOIAIAAIAAAdg");
	this.shape_36.setTransform(11.5,31.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#58595B").s().p("AgGAIQgEgEgDAAQAAgGAFgFQAGgHAHAAQgBAFADADQAEACADAAIgRASg");
	this.shape_37.setTransform(17.5,19.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#222325").s().p("AgOgEIAOgJIAOAJIgKASg");
	this.shape_38.setTransform(10.5,13.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#3D3E3F").s().p("AgTgFIAQgHIAXAHIgUASg");
	this.shape_39.setTransform(6,19.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#2D2D2D").s().p("AgLgHIgCgLQADgBAJAGQAJAHAHAGIgKAAIAAAUQgKgNgGgOg");
	this.shape_40.setTransform(16.5,9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#696A6C").s().p("AgGADIgIgHIAcAAIgOAJg");
	this.shape_41.setTransform(38.5,23.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#5E5F61").s().p("ABrEEIANgCIAMACgAiDjzQAAgHAEgEQAFgFAGAAQAGAAAEAFQAFAEAAAHg");
	this.shape_42.setTransform(32.3,41.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#3A3B3D").s().p("AAGAEQgUgJgKACQgDAAgEgDQgDgDAAgEQAFAAADgDIACgHIAcAAQANAAAJAOQAJANAAAUQgIgJgVgLg");
	this.shape_43.setTransform(21.5,19.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#353536").s().p("AhoC9IAfAUIgfAAIAAAKIgeABgABpjbIAKAAIAKAAQAEAAADADIACAHIgFAAQgTAAgFgKg");
	this.shape_44.setTransform(14.5,29.1);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#3E3E40").s().p("AgVAAIAAgTIAKAAIAcAKIAFAFQgXAGgUARg");
	this.shape_45.setTransform(19.2,11);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#505052").s().p("AgTgNIAPgEIAEgHIAUAdIgKAAQgBAIgFAGQgEAFgIABg");
	this.shape_46.setTransform(24,6.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#2E2E2F").s().p("ACMFZQgFAAgDgDQgCgEAAgDIAOgDIAGgHIAKAAIAAAUgAiflYIAVAKIgGAHIgPAEg");
	this.shape_47.setTransform(38,37.5);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#535354").s().p("AgGACQgDgCgFAAIAAgIIASAAIAKAAIgSASg");
	this.shape_48.setTransform(28.5,7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#626264").s().p("AgdgDIA7gFIAAAFIgeAIQAAAEgKAAQgFAAgOgMg");
	this.shape_49.setTransform(30,11.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#3A3A3B").s().p("AgOAAIAdgJIAAAJIgLAFIgSAEg");
	this.shape_50.setTransform(31.5,12);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#404142").s().p("AgKgYQAMAEAFADQAEAEAAAHQAAAIgDACQAAAJgGAGQgDAGgJAAg");
	this.shape_51.setTransform(31.2,3.5);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#363636").s().p("AgNgDIAbAAQgEADgXAFg");
	this.shape_52.setTransform(35.5,5.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#37383A").s().p("AgJAZIAAgxIASAKIAAAng");
	this.shape_53.setTransform(38,2.5);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#1E1E1E").s().p("AjUBkIAeAAIgDAHQgDADgEAAgADEhYIAAgVIAKAAIAGABIABAVg");
	this.shape_54.setTransform(53.4,60);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#3F3F40").s().p("AgSAFQAAgHAFgGQAGgFAHgBQAIAAAGAEQAGACgBAEIgKAAIAAATg");
	this.shape_55.setTransform(72,49.5);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#282829").s().p("AgJAKIgKgSIAKAAQAaAAACAEIAAAOg");
	this.shape_56.setTransform(74,42);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#232324").s().p("AADAQIgKgoQAJAOAEAJIACAKIgDAKIgFAGg");
	this.shape_57.setTransform(63.7,45.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#676769").s().p("AgTgEIAmAEIAAAEIgIABQgQAAgOgJg");
	this.shape_58.setTransform(74,33.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#27282A").s().p("ACfCGIAAgUQAIgBANAQQgBAGgNAAIgHgBgAizheIAAgoQANAQgDAYg");
	this.shape_59.setTransform(57.1,14.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#717171").s().p("AgbgCIAKgBIgKgBIAnABIARAGIg5ACg");
	this.shape_60.setTransform(58.7,41.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#1E1E20").s().p("Ah2AZIAAgUQAFAOgCAHIgDAJgABtgiIALAAIAAAfg");
	this.shape_61.setTransform(38,50.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#656566").s().p("AgOgBIAdAAIgIACIgFABQgFAAgLgDg");
	this.shape_62.setTransform(58.5,35.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#343436").s().p("AjVgJQADAAAEAEIADAFIgUAUQADgVAHgIgADXAAIgVgTQANAAAJAHQAJAFAAAHg");
	this.shape_63.setTransform(37.5,21);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#383839").s().p("AgNAJQAMgRABAAIAOADIAAAFIgRAJg");
	this.shape_64.setTransform(53.5,19.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#5F5F61").s().p("AgXgEIAAgKIAKAAIAcAKIAJATg");
	this.shape_65.setTransform(52.5,13.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#252628").s().p("AAAAJQgEgRAAgRIAJAAIAAAwIgCADg");
	this.shape_66.setTransform(49.4,26.6);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#404143").s().p("AgRAHQgGgHAEgDQAEgGALgDIAQgCIAKAAIgUAdQgOgDgFgFg");
	this.shape_67.setTransform(50.8,21.5);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#2B2B2D").s().p("AAXCLIAKABIgLAAQAEgFAOAAIAZAFgAhAhiIAAgpIAKAAIADAgIgCAKIgDAEg");
	this.shape_68.setTransform(53.5,27);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#353537").s().p("AAEAEIgSAAIAAgIIAcAIg");
	this.shape_69.setTransform(46.5,9.5);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#616163").s().p("AAoAEIhOAAQgEAAgEgCQgCgCAAgEIBiAEIAAAEg");
	this.shape_70.setTransform(39,9.5);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#666668").s().p("AgNgEIAJAAIASAAIAAAIIgFABQgRAAgFgJg");
	this.shape_71.setTransform(45.5,10.5);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#2C2C2E").s().p("AD4CXQAJgGAMABIgBAJIABABIgJAEIgUAFQAAgJAIgFgAkMClIAKgUIAKAAIAKAUgAA8iaIAAgKIBQAAIAAAKg");
	this.shape_72.setTransform(29,26.5);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#38383A").s().p("ABeAjIgJgVQAMABAJAFQAJAGgBAJgAgNgOIAAgEIhkgGIAAgJIBtAEIAAAFIAAAKg");
	this.shape_73.setTransform(45.5,11.5);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#525254").s().p("AALAPQgLgBgIgFQgJgGAAgGIAAgLIASALQAAAGAFAGQAGAFAGABg");
	this.shape_74.setTransform(46.9,5.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#3F3F41").s().p("AgYgDIAKAAIAnAAQgMAHgIAAQgIAAgVgHg");
	this.shape_75.setTransform(41.5,5.5);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#3B3C3E").s().p("AgFAIIAAAKIgKAAIAAghIAZgCQAKAAgHADIAAAgg");
	this.shape_76.setTransform(45.7,3.1);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#646466").s().p("AAAAOQgDABgCgGQgDgGgBgHIAAgKIATAKIAAASg");
	this.shape_77.setTransform(59,11.5);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#414143").s().p("AgOAEIAAgIIAAgJIALAAIASAJIgKASg");
	this.shape_78.setTransform(59.5,9.5);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#4D4D4F").s().p("AAJAOIgJAAQgMgLgPgBIgJABIgCABIAAgIIAAgKIAKAAQAGAMAWgCQAYgDAGgHIAGADIAEAHIAAAIIgKAAIAAAKg");
	this.shape_79.setTransform(47,11.5);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#39393B").s().p("AhRE/IAAgdIAKAAQAEAcgEAFQgCADgIADgABXDvIAAgKIAAgKIAOAKIAGgJQAFgJAFgCQAKAYAFAbIgBAAQgIAAgkgVgAiDDbIBGAAIAAAKIgcAEQgPgBgbgNgAAlk1IAAgKQANAAAJAHQAIAFAAAIgAgBlJIAIAAIAUAAQgFAHgXADg");
	this.shape_80.setTransform(47.3,43);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#545456").s().p("AASBFIgDgBQgGgFgDgNQgCgNAAgHIAUAAQAFAAACADIADAHQgCATgFAHIgFADgAgNgUQgUgMAAgQIAAgUIAKAAIATAUQAAAIAKAGQAMAFAQAAIAAAKIAAAKQgcAAgTgLg");
	this.shape_81.setTransform(54.5,12);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#373739").s().p("AgLgCIADgGQAGgHAOAAQABAKgGAVg");
	this.shape_82.setTransform(53.3,5.3);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#323334").s().p("AAAAPQgJgLAAgHQAAgHACAAIAHgDIAKAcg");
	this.shape_83.setTransform(67.9,24.5);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#5B5C5E").s().p("AgEAXQgKgHAAgSQAAgOALgKIASAKIgDAHIgHADIgIADQgBABAAAEQAAAKAJAKIAAAFIgBAAQgEAAgEgEg");
	this.shape_84.setTransform(67.5,23.8);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#424242").s().p("AgRAAIAAgJIAjAGIgRANg");
	this.shape_85.setTransform(69.9,20);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#343537").s().p("ABBBTIgEABQgOgOgGgQIAKAAIANAPIAFAIQAAAIgCAAIgCgCgAg8hLIgIgJIAIAAIAAAAIAXAAIgPALg");
	this.shape_86.setTransform(54.9,15.5);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#4B4B4D").s().p("AAgFPQgQAAgMAFQgKAGAAAJIgUAAIgUgUIgeAAIAUAKIAAAKIgUAAIgKAAIAAgGIgUAGQAAgJgMgGQgLgFgRAAQAAgJgFgGQgHgFgIAAIgogLIgUgeQAAgIgMgFQgLgHgRABQgEgBgDgFQgDgGAAgIIAFAAQADABAAgKQAAgFgSgRIAAAKQgLgIgTgCQgMgHgSgDIAAgKIAQgCQANgEgBgIQAAgGgDgJIgFgKIAAgKIgegVIBkAqQABAYAXARQAXARAhAAQARABALgMIACgCIAoARIAAAHIgKAAIAFAxIAFAAIAUAUQAIAAAHAEQAFADAAADIgPAAQAAAKALABIALgDIANgIIA6AAQAUABAPgJQAIgEADgGIBKAfQAAAHgFAFQgHAFgIABQgZAGgKAHQgFAEAAADgAAgE2IAZAAIgMgCgAA+ETIAAgBIAAgJIAKAAIADAAQAEgCABgUQAAgNgHgNQgIgPgNgIQgFgCgTgJIgagIIAAgKIACgCQAAgCgGgEIgOgCIgeAAIAAAKQgNgBgIAHQgJAFAAAIIgDAHIg1gfQAAgGgEgJIAAgVQAAgcgMgVQgLgUgRAAIgUAAIAAgKIAAgFIgygGIAAALQgOAIgNAZIhJgsQAGgKgCgLIgEgJIAAgIQALgWAAgIIgDgLIgIgJIgUgeIAUgTIAbgFQAOgGADgJQADgKgJgJQgJgJgTgDIAUgUIAZAEQAJgDAAgTIgCgPIgCgGIAKgVIAeALIC+g8IAAAJQAAAFADADQADACAEAAIAAAKIAAAKIgUAAIAAgKIAAgEIg8AEQAOAOAFAAQAKAAABgEIAAAKQAAAqAUAdQAVAdAdABIAIAJIAHABIANgKQAVgEAZgbIAYgdIAIAFIADgEIABgKIgCggIAUAAIAyAVQgBAHADANQADANAGAFIACABIgNAAIAAgFIgPgDQgCAAgNASIgSACQgKADgEAGQgFAFAGAHQAGAFAPADIgDAGIgHAEIgLAAQAAAQAEAUIAFAQIACgDQAIAUAeAVQAQALAQAJQAOAGAIgDIAIgDQAVgKANgSQAHgNABgGQAAgHgHgOIgZgqIAFgBQADAEAAgKIgEgJIgOgPQAAgIgJgFQgIgHgNAAIgDAAIAFgDQAFgHADgTIAyATIgKAKIARATIANgTIAUAKIAAAKQgLALAAANQAAAVAKAHQAFAEAGgBIAAgEIAKAAIAoAAIAAATQgMAHgDAWIAAAWIAFAAQAQANAYgDIAFAAQgMAZgUABQgQAAAAAQQAAAJANARIgKAAIAKAUIgKAAIAAAKIgKAAIgBAMIAEAKIAHARQgJABgFAFQgGAGAAAJIgDAGQgDAEgEAAIgSgBIgCASQAEAMAGABQAEAAADACIADAHIAAAGIgQgBIgOgFIAAAKQgIABgGAIQgGAJAAANIAAAKIgKAUIgoATQgFACgFAJIgGAJIgOgKIAAAKQgMAAgJAHQgJAGAAAJIAAABgACBDGQARANAaADIADAGQAIAAAFgIIAEgIQAIABAHgJQAFgJAAgMQAIgbARgjIAHgFIADgKIgCgNQgEgJgLgNIgIgDQgDgDgBgEIgQgIIgZgFQgOAAgEAFIAMAAIgKABQgMgBgJAGQgIAFAAAJIgeAnIgKAAIAKAfIAAAKIgLAAQABAXAGACIAEgFQAMAWAPALgAgFCBIAYAAIgQgCgAhOgDQgIAJAdAlQAeAkAqgWQAsgXgbhBQhEgjgYAoIAAgBgAjYgNIAIAJIAIABIAOgKIApgyIAJgVQAHgLgDgKIgEgIIgKgfQAAgVgJgOQgIgOgNAAQAAgIgFgEQgEgFgGAAQgGAAgFAFQgEAEAAAIIgDAHQgDACgEAAQgJAAgFAHQgGAFAAAIQgGAIgEAXQgSArgCAaIgKAAIgKAAIAKAfIAKAAIAAAKIgKAAQALAPARACIAWgDIAAgEgAhqDaQACgIAEgHQAGgIAIgBIAHgCIAAgBICNBUQAAAGgEAFgAkACcIAAgBQgGgDgHgHQgJgKACgLIAKAAIgKgUIAAAGIgLgEIABgVIAKAAIADgHICNBSIAAAGIgEAJQAAAPgKALgAlkAtIAKAUIAEAaIgDAIIgLAPgAlkAiIAKAAIgKALgAhqkvIAAgzIAKAAIAAAzg");
	this.shape_87.setTransform(39.8,36.5);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#5C5C5E").s().p("AALDhIAPABIgaABgAC9hqIgDgHQgCgDgFAAQABgIgJgGQgJgGgMAAQgBgIgIgGQgJgGgMAAIgEgHIgGgDIgVAAIgdgKIAAgFIhsgFIi+A8IAAgKIAUAAQATgRAZgJIgFgEIAAgKIAVAAQAIAAAGgFQAFgGABgJQAGAMAXgCIAUgUQAJAAAGgFQAGgGAAgJIAUAKIAAAKQAZgEAEgGIATAAQAUAKAKAAQAIAAAMgKIAKAAQgBAJAJAGQAJAFAMAAIAIAJIAIACIAPgLQAAARAUAMQAVALAcAAQABAJADAGQACAFAFAAIAUAUIAUAeg");
	this.shape_88.setTransform(39,26.7);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#5A5A5B").s().p("AgIAPIAIgKIgSgcIAAgKIASAAIATAUIAAAcIgMATg");
	this.shape_89.setTransform(63,17.5);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#333334").s().p("AhOEnIAAgKQADAAAEgDIADgHIATAUgAj/CJQgXgRAAgZQAOAQAdAVQAZARALAFQghAAgXgRgAD5jWIAAgKIALAAIAUAKIgEAHQgDADgDAAgACqkmIAKAAQADABADADIAEAHIAAAKg");
	this.shape_90.setTransform(42,42.5);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#414142").s().p("AgQgBIAOgJQAHgGAGAAIAGAAQAAAIgPAZg");
	this.shape_91.setTransform(64.8,14.3);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#303132").s().p("AiqCxIgYglQARAAAMAVQAMAUAAAdgACvjGQAAgGAEgDIAFgCQALAGAAAPg");
	this.shape_92.setTransform(41.5,27.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.9,0,77.5,77.6);


(lib.shape11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/AlfMAAAhK+MBj+AAAMAAABK+g");
	this.shape.setTransform(320,240);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.shape7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#F06FFF","rgba(9,34,136,0)","#F06FFF"],[0,0.486,1],-5.8,0,5.8,0).s().p("Ag5VoMAAAgrPIBzAAMAAAArPg");
	this.shape.setTransform(5.8,138.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,11.7,277);


(lib.shape5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#6FD5FF","rgba(72,9,136,0)","#6FD5FF"],[0,0.486,1],-5.8,0,5.8,0).s().p("Ag5VoMAAAgrPIBzAAMAAAArPg");
	this.shape.setTransform(5.8,138.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,11.7,277);


(lib.shape3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image1, null, new cjs.Matrix2D(0.914,0,0,0.914,-435,-268.2)).s().p("EhD9Ap6MAAAhTzMCH7AAAMAAABTzg");
	this.shape.setTransform(435,268.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(img.image2, null, new cjs.Matrix2D(0.914,0,0,0.914,-435,-50.3)).s().p("EhD9AH2IAAvrMCH7AAAIAAPrg");
	this.shape_1.setTransform(435,586.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,870,637);


(lib.sprite116 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape115("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,30,30);


(lib.sprite107 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.shape105("synched",0);

	this.instance_1 = new lib.shape106("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,32,32);


(lib.sprite101 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape100("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,65,38);


(lib.sprite95 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape94("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,62,37);


(lib.sprite89 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape88("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,58,37);


(lib.sprite85 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape84("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.8,-21,33,28);


(lib.sprite81 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape80("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,44,37);


(lib.sprite77 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape76("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-32.9,-21,33,29);


(lib.sprite73 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape72("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,43,38);


(lib.sprite69 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape68("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.5,-20,30,29.7);


(lib.sprite65 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape64("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,43,39);


(lib.sprite61 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape60("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-33,-20.1,33,29);


(lib.sprite57 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape56("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,44,38);


(lib.sprite53 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape52("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36,-21,34.8,30);


(lib.sprite49 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape48("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,45,38);


(lib.sprite45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape44("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.4,-20,49.5,27);


(lib.sprite41 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape40("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,62,41);


(lib.sprite35 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape34("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,66,40);


(lib.sprite31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape30("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-39.3,-13,84.4,26);


(lib.sprite27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape26("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,59,26);


(lib.sprite23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape22("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-34.8,-19.1,47.3,16.3);


(lib.sprite19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape18("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,60,27);


(lib.sprite14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape13("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.9,0,77.5,77.6);


(lib.sprite8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape7("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,11.7,277);


(lib.sprite6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape5("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,11.7,277);


(lib.sprite117 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.shape114("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},2).wait(1));

	// Layer 4
	this.instance_1 = new lib.sprite116();
	this.instance_1.setTransform(4,4,0.8,0.8);
	this.instance_1.alpha = 0.852;
	this.instance_1.shadow = new cjs.Shadow("#FF0000",0,0,2);
	this.instance_1._off = true;
	this.instance_1.filters = [new cjs.ColorFilter(1, 0, 0, 1, 160, -255, 10, 0)];
	this.instance_1.cache(-2,-2,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).wait(1));

	// Layer 3
	this.instance_2 = new lib.shape110("synched",0);
	this.instance_2.setTransform(5.6,5.6,0.372,0.385);
	this.instance_2.filters = [new cjs.ColorFilter(0.75, 0.75, 0.75, 1, 0, 0, 0, 0)];
	this.instance_2.cache(-2,-2,62,58);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3));

	// Layer 2
	this.instance_3 = new lib.shape108("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,32,32);


(lib.sprite104 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sprite101();
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance.cache(-2,-2,69,42);

	this.instance_1 = new lib.shape100("synched",0);

	this.instance_2 = new lib.shape103("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,69,42);


(lib.sprite98 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sprite95();
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance.cache(-2,-2,66,41);

	this.instance_1 = new lib.shape94("synched",0);

	this.instance_2 = new lib.shape97("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,66,41);


(lib.sprite92 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sprite89();
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance.cache(-2,-2,62,41);

	this.instance_1 = new lib.shape88("synched",0);

	this.instance_2 = new lib.shape91("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,62,41);


(lib.sprite86 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sprite85();
	this.instance.setTransform(38,25);
	this.instance._off = true;
	this.instance.filters = [new cjs.BlurFilter(10, 10, 1)];
	this.instance.cache(-34,-23,37,32);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite81();
	this.instance_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance_1.cache(-2,-2,48,41);

	this.instance_2 = new lib.shape80("synched",0);

	this.instance_3 = new lib.shape83("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,48,41);


(lib.sprite78 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sprite77();
	this.instance.setTransform(38,25);
	this.instance._off = true;
	this.instance.filters = [new cjs.BlurFilter(5, 5, 1)];
	this.instance.cache(-35,-23,37,33);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite73();
	this.instance_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance_1.cache(-2,-2,47,42);

	this.instance_2 = new lib.shape72("synched",0);

	this.instance_3 = new lib.shape75("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,47,42);


(lib.sprite70 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sprite69();
	this.instance.setTransform(39,25);
	this.instance._off = true;
	this.instance.filters = [new cjs.BlurFilter(10, 10, 1)];
	this.instance.cache(-33,-22,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite65();
	this.instance_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance_1.cache(-2,-2,47,43);

	this.instance_2 = new lib.shape64("synched",0);

	this.instance_3 = new lib.shape67("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,47,43);


(lib.sprite62 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sprite61();
	this.instance.setTransform(39,25);
	this.instance._off = true;
	this.instance.filters = [new cjs.BlurFilter(10, 10, 1)];
	this.instance.cache(-35,-22,37,33);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite57();
	this.instance_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance_1.cache(-2,-2,48,42);

	this.instance_2 = new lib.shape56("synched",0);

	this.instance_3 = new lib.shape59("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,48,42);


(lib.sprite54 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sprite53();
	this.instance.setTransform(41,25);
	this.instance._off = true;
	this.instance.filters = [new cjs.BlurFilter(10, 10, 1)];
	this.instance.cache(-38,-23,39,34);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite49();
	this.instance_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance_1.cache(-2,-2,49,42);

	this.instance_2 = new lib.shape48("synched",0);

	this.instance_3 = new lib.shape51("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,49,42);


(lib.sprite46 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sprite45();
	this.instance.setTransform(41,26);
	this.instance._off = true;
	this.instance.filters = [new cjs.BlurFilter(10, 10, 1)];
	this.instance.cache(-38,-22,54,31);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite41();
	this.instance_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance_1.cache(-2,-2,66,45);

	this.instance_2 = new lib.shape40("synched",0);

	this.instance_3 = new lib.shape43("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,66,45);


(lib.sprite38 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sprite35();
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance.cache(-2,-2,70,44);

	this.instance_1 = new lib.shape34("synched",0);

	this.instance_2 = new lib.shape37("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,70,44);


(lib.sprite32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sprite31();
	this.instance.setTransform(28,13,0.547,0.599);
	this.instance._off = true;
	this.instance.filters = [new cjs.BlurFilter(10, 10, 1)];
	this.instance.cache(-41,-15,88,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite27();
	this.instance_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance_1.cache(-2,-2,63,30);

	this.instance_2 = new lib.shape26("synched",0);

	this.instance_3 = new lib.shape29("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,63,30);


(lib.sprite24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sprite23();
	this.instance.setTransform(40,24);
	this.instance._off = true;
	this.instance.filters = [new cjs.BlurFilter(10, 10, 1)];
	this.instance.cache(-37,-21,51,20);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite19();
	this.instance_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, -50, 0, 0))];
	this.instance_1.cache(-2,-2,64,31);

	this.instance_2 = new lib.shape18("synched",0);

	this.instance_3 = new lib.shape21("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,64,31);


(lib.sprite15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sprite14();
	this.instance.setTransform(-35,-35,0.915,0.908);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.91,scaleY:0.91,rotation:63,guide:{path:[-34.8,-34.9,-16.8,-50.1,2.6,-47.5,8.8,-46.7,14.5,-44.8]}},7).to({scaleX:0.92,scaleY:0.91,rotation:180,guide:{path:[14.6,-44.7,14.6,-44.7,14.6,-44.7,26.2,-40.8,35,-32.2,48,-19.6,50,-1,52,17.9,34.9,35,34.9,35,34.9,35]}},13).to({rotation:189,guide:{path:[35,35,32,37.9,29.1,40.2]}},1).to({scaleX:0.91,scaleY:0.91,rotation:206.8,guide:{path:[29.1,40.1,22.5,45.3,15.7,47.9]}},2).to({scaleX:0.92,scaleY:0.91,rotation:243,guide:{path:[15.7,47.9,15.7,47.9,15.7,47.9,7.5,50.9,-0.8,50,-8.4,49.2,-15,46.6,-15,46.6,-15,46.6]}},4).to({scaleY:0.91,rotation:360,guide:{path:[-15.2,46.6,-24.7,42.7,-32.3,35,-44.9,22.1,-47.5,2.7,-50.1,-16.9,-34.9,-34.9,-34.9,-34.9,-34.9,-34.9]}},13).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-35.8,-35,70.9,70.5);


(lib.sprite9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sprite6();
	this.instance.setTransform(-5.8,-138.4);

	this.instance_1 = new lib.sprite8();
	this.instance_1.setTransform(-5.8,-138.3);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:0.789},5).to({y:-138.3,alpha:0.039},18).to({_off:true},1).wait(50).to({_off:false,y:-138.4,alpha:0},0).to({alpha:0.988},25).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(24).to({_off:false},0).to({alpha:1},25).to({alpha:0.762},6).to({y:-138.4,alpha:0.039},18).to({_off:true},1).wait(26));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.8,-138.4,11.6,277);


(lib.sprite16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sprite15();
	this.instance.setTransform(316,241);
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,1,2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.instance_1 = new lib.shape11("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.sprite10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.sprite9();
	this.instance.setTransform(760.9,0.4);
	this.instance.filters = [new cjs.BlurFilter(10, 10, 3)];
	this.instance.cache(-8,-140,16,281);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite9();
	this.instance_1.setTransform(0,0.4);
	this.instance_1.filters = [new cjs.BlurFilter(10, 10, 3)];
	this.instance_1.cache(-8,-140,16,281);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.8,-152.1,803.8,308);


(lib.sprite12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.instance = new lib.shape11("synched",0);
	this.instance.setTransform(112,47);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 3
	this.instance_1 = new lib.sprite10();
	this.instance_1.setTransform(85,168.2,0.914,0.914);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 2
	this.instance_2 = new lib.shape3("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,870,637);


(lib.assetsboxTplGaminatorBox = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 48
	this.b12 = new lib.sprite117();
	this.b12.setTransform(772.5,0);

	this.timeline.addTween(cjs.Tween.get(this.b12).wait(1));

	// Layer 44
	this.b13 = new lib.sprite107();
	this.b13.setTransform(823,0);

	this.timeline.addTween(cjs.Tween.get(this.b13).wait(1));

	// Layer 42
	this.b11 = new lib.sprite104();
	this.b11.setTransform(719,542);

	this.timeline.addTween(cjs.Tween.get(this.b11).wait(1));

	// Layer 40
	this.b10 = new lib.sprite98();
	this.b10.setTransform(645,542);

	this.timeline.addTween(cjs.Tween.get(this.b10).wait(1));

	// Layer 38
	this.b9 = new lib.sprite92();
	this.b9.setTransform(574,542);

	this.timeline.addTween(cjs.Tween.get(this.b9).wait(1));

	// Layer 35
	this.b8 = new lib.sprite86();
	this.b8.setTransform(503,580);

	this.timeline.addTween(cjs.Tween.get(this.b8).wait(1));

	// Layer 32
	this.b7 = new lib.sprite78();
	this.b7.setTransform(442,580);

	this.timeline.addTween(cjs.Tween.get(this.b7).wait(1));

	// Layer 29
	this.b6 = new lib.sprite70();
	this.b6.setTransform(378,580);

	this.timeline.addTween(cjs.Tween.get(this.b6).wait(1));

	// Layer 26
	this.b5 = new lib.sprite62();
	this.b5.setTransform(316,580);

	this.timeline.addTween(cjs.Tween.get(this.b5).wait(1));

	// Layer 23
	this.b4 = new lib.sprite54();
	this.b4.setTransform(254,580);

	this.timeline.addTween(cjs.Tween.get(this.b4).wait(1));

	// Layer 20
	this.b3 = new lib.sprite46();
	this.b3.setTransform(149,577);

	this.timeline.addTween(cjs.Tween.get(this.b3).wait(1));

	// Layer 18
	this.b2 = new lib.sprite38();
	this.b2.setTransform(77,577);

	this.timeline.addTween(cjs.Tween.get(this.b2).wait(1));

	// Layer 15
	this.b1 = new lib.sprite32();
	this.b1.setTransform(234,545);

	this.timeline.addTween(cjs.Tween.get(this.b1).wait(1));

	// Layer 12
	this.b0 = new lib.sprite24();
	this.b0.setTransform(166,545);

	this.timeline.addTween(cjs.Tween.get(this.b0).wait(1));

	// Layer 9
	this.container = new lib.sprite16();
	this.container.setTransform(112,46);

	this.timeline.addTween(cjs.Tween.get(this.container).wait(1));

	// Layer 1
	this.instance = new lib.sprite12();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,870,637);


// stage content:
(lib.graphics = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 48
	this.b12_1 = new lib.sprite117();
	this.b12_1.setTransform(772.5,0);

	this.timeline.addTween(cjs.Tween.get(this.b12_1).wait(1));

	// Layer 44
	this.b13_1 = new lib.sprite107();
	this.b13_1.setTransform(823,0);

	this.timeline.addTween(cjs.Tween.get(this.b13_1).wait(1));

	// Layer 42
	this.b11_1 = new lib.sprite104();
	this.b11_1.setTransform(719,542);

	this.timeline.addTween(cjs.Tween.get(this.b11_1).wait(1));

	// Layer 40
	this.b10_1 = new lib.sprite98();
	this.b10_1.setTransform(645,542);

	this.timeline.addTween(cjs.Tween.get(this.b10_1).wait(1));

	// Layer 38
	this.b9_1 = new lib.sprite92();
	this.b9_1.setTransform(574,542);

	this.timeline.addTween(cjs.Tween.get(this.b9_1).wait(1));

	// Layer 35
	this.b8_1 = new lib.sprite86();
	this.b8_1.setTransform(503,580);

	this.timeline.addTween(cjs.Tween.get(this.b8_1).wait(1));

	// Layer 32
	this.b7_1 = new lib.sprite78();
	this.b7_1.setTransform(442,580);

	this.timeline.addTween(cjs.Tween.get(this.b7_1).wait(1));

	// Layer 29
	this.b6_1 = new lib.sprite70();
	this.b6_1.setTransform(378,580);

	this.timeline.addTween(cjs.Tween.get(this.b6_1).wait(1));

	// Layer 26
	this.b5_1 = new lib.sprite62();
	this.b5_1.setTransform(316,580);

	this.timeline.addTween(cjs.Tween.get(this.b5_1).wait(1));

	// Layer 23
	this.b4_1 = new lib.sprite54();
	this.b4_1.setTransform(254,580);

	this.timeline.addTween(cjs.Tween.get(this.b4_1).wait(1));

	// Layer 20
	this.b3_1 = new lib.sprite46();
	this.b3_1.setTransform(149,577);

	this.timeline.addTween(cjs.Tween.get(this.b3_1).wait(1));

	// Layer 18
	this.b2_1 = new lib.sprite38();
	this.b2_1.setTransform(77,577);

	this.timeline.addTween(cjs.Tween.get(this.b2_1).wait(1));

	// Layer 15
	this.b1_1 = new lib.sprite32();
	this.b1_1.setTransform(234,545);

	this.timeline.addTween(cjs.Tween.get(this.b1_1).wait(1));

	// Layer 12
	this.b0_1 = new lib.sprite24();
	this.b0_1.setTransform(166,545);

	this.timeline.addTween(cjs.Tween.get(this.b0_1).wait(1));

	// Layer 9
	this.container_1 = new lib.sprite16();
	this.container_1.setTransform(112,46);

	this.timeline.addTween(cjs.Tween.get(this.container_1).wait(1));

	// Layer 1
	this.instance_1 = new lib.sprite12();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(320,240,870,637);

})(but_lib = but_lib || {}, but_images = but_images || {}, createjs = createjs || {}, ss = ss || {});
var but_lib, but_images, createjs, ss;