(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 914,
	height: 747,
	fps: 24,
	color: "#444444",
	manifest: []
};



// symbols:



(lib.sprite24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.text88 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAHA/IgPg6IAAA6IgbAAIAAh9IAbAAIARA5IAAg5IAbAAIAAB9g");
	this.shape.setTransform(75.6,7.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgPA/IAAh9IAfAAIAAB9g");
	this.shape_1.setTransform(69,7.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAJA/QgGgdgDgkIgIBBIgpAAIgPh9IAgAAIAEAsIADApQACgfAHg2IAgAAIAEAqIAEAuQACgsAHgsIAgAAIgPB9g");
	this.shape_2.setTransform(60.1,7.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgbA/IAAh9IA1AAIAAAaIgWAAIAAAYIAUAAIAAAWIgUAAIAAAbIAYAAIAAAag");
	this.shape_3.setTransform(47.7,7.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAGA/IgMg2IAAA2IghAAIAAh9IAhAAIAAAxIANgxIAfAAIgTA5IAVBEg");
	this.shape_4.setTransform(40.2,7.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAHA/IgBgXIgKAAIgCAXIgiAAIARh9IAtAAIATB9gAgFASIAKAAIgFg0IgFA0g");
	this.shape_5.setTransform(31.5,7.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgPA/IAAhjIgUAAIAAgaIBGAAIAAAaIgTAAIAABjg");
	this.shape_6.setTransform(23.7,7.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(18,-4,83.1,43.1);


(lib.text87 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgPA/IAAhjIgUAAIAAgaIBGAAIAAAaIgTAAIAABjg");
	this.shape.setTransform(66.1,7.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAGA/IAAgqQAAgKgCgDQgBgCgFAAIAAA5IghAAIAAh9IAXAAQAVAAAJACQAIACAFAHQAFAIAAAQQAAAPgDAFQgEAFgLABQAKABADADQAEAEAAAEQABADAAAPIAAAigAgCgMQADAAADgBQACgCAAgIIAAgHQAAgGgCgCQgDgCgDAAg");
	this.shape_1.setTransform(58.1,7.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAHA/IgBgXIgKAAIgCAXIgiAAIARh9IAtAAIATB9gAgFASIAKAAIgFg0IgFA0g");
	this.shape_2.setTransform(49.7,7.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgPA/IAAhjIgUAAIAAgaIBGAAIAAAaIgTAAIAABjg");
	this.shape_3.setTransform(42,7.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgUA8QgJgFgDgHQgDgIAAgOIAAgJIAfAAIAAAQQAAAHABACQABACACAAQADAAABgDQACgCgBgFQAAgLgCgEQgDgDgLgIQgMgIgEgCQgEgEgCgGQgDgHAAgKQAAgOAEgHQADgHAJgEQAIgDALAAQALAAAKAEQAIAEADAGQADAGAAAQIAAAFIgeAAIAAgKQAAgGgBgCQgBgBAAAAQAAAAgBAAQAAgBgBAAQAAAAAAAAQAAAAgBAAQgBAAAAABQgBAAAAAAQgBABAAAAQgBADAAAFQAAAGACADQABAEAGAEQAWANAFAGQAGAJAAASQAAAOgDAGQgDAHgJAEQgKAEgLAAQgMAAgJgFg");
	this.shape_4.setTransform(34.2,7.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(28,-4,60.9,43.1);


(lib.text86 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgiA/IAAh9IAhAAQAMAAAHACQAHACAEAEQAEAEABAGQABAGAAAMIAAALQAAAMgCADQgDAGgGADQgIADgKAAIgHAAIAAAzgAgBgIIABAAQAEAAACgCQACgCAAgHIAAgLQAAgGgCgCQgDgCgEAAg");
	this.shape.setTransform(60.7,7.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgPA/IAAh9IAfAAIAAB9g");
	this.shape_1.setTransform(54.2,7.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAGA/IgMg2IAAA2IghAAIAAh9IAhAAIAAAxIANgxIAfAAIgTA5IAVBEg");
	this.shape_2.setTransform(47.9,7.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgUA8QgKgFgDgHQgCgIAAgOIAAgJIAeAAIAAAQQAAAHABACQACACACAAQACAAACgDQACgCAAgFQAAgLgEgEQgCgDgKgIQgNgIgEgCQgEgEgDgGQgCgHAAgKQAAgOADgHQAFgHAIgEQAIgDAMAAQALAAAIAEQAJAEAEAGQADAGAAAQIAAAFIggAAIAAgKQAAgGgBgCQAAgBAAAAQgBAAAAAAQgBgBAAAAQAAAAAAAAQAAAAgBAAQgBAAAAABQgBAAAAAAQgBABAAAAQgBADgBAFQAAAGACADQACAEAFAEQAWANAHAGQAFAJAAASQAAAOgDAGQgDAHgKAEQgIAEgNAAQgKAAgKgFg");
	this.shape_3.setTransform(39.2,7.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(33,-4,49.7,43.1);


(lib.text83 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgWAzIAAhlIArAAIAAAVIgSAAIAAATIARAAIAAASIgRAAIAAAWIATAAIAAAVg");
	this.shape.setTransform(71.9,5.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgUAzIAAhlIAZAAIAABQIAQAAIAAAVg");
	this.shape_1.setTransform(66.8,5.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgeAzIAAhlIAbAAQAKAAAHACQAGACAFAGQADAGABANQgBAKgCADQgDADgJACQAKADADADQAEAEgBALIAAAJQABALgDAEQgCAFgGACQgFACgPAAgAgDAhQADAAACgCQACgBAAgGIAAgKQAAgHgCgBIgFgCgAgDgKIADAAQABAAABgCQACgBAAgJIgBgHIgCgCIgEgBg");
	this.shape_2.setTransform(60.6,5.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAGAzIgCgTIgHAAIgCATIgbAAIANhlIAlAAIAPBlgAgEAOIAIAAIgEgpQgDAbgBAOg");
	this.shape_3.setTransform(53.6,5.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgMAzIAAhQIgQAAIAAgVIA5AAIAAAVIgQAAIAABQg");
	this.shape_4.setTransform(47.3,5.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgLAzIAAglIgThAIAZAAIAFAoQABgQAFgYIAZAAIgTBAIAAAlg");
	this.shape_5.setTransform(41.2,5.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAGAzIgCgTIgHAAIgCATIgbAAIANhlIAlAAIAPBlgAgEAOIAIAAIgEgpQgDAbgBAOg");
	this.shape_6.setTransform(34.9,5.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgbAzIAAhlIAaAAQAJAAAGACQAGACADADQADADABAFIABAOIAAAJQAAAKgCADQgCAEgFACQgGADgJAAIgFAAIAAApgAgBgGIABAAQADAAABgBQACgDAAgFIAAgIQAAgGgCgBQgBgCgEAAg");
	this.shape_7.setTransform(28.4,5.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(23,-4,69.2,35.7);


(lib.text80 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape.setTransform(30.6,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAGAvIgMgrIAAArIgUAAIAAhdIAUAAIANArIAAgrIAUAAIAABdg");
	this.shape_1.setTransform(24.8,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgLAvIAAhdIAXAAIAABdg");
	this.shape_2.setTransform(19.8,5.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgSAvIAAhdIAWAAIAABKIAPAAIAAATg");
	this.shape_3.setTransform(16,5.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgGAcIAAgVIgUAAIAAgNIAUAAIAAgVIANAAIAAAVIAUAAIAAANIgUAAIAAAVg");
	this.shape_4.setTransform(8.2,5.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3,-4,44.7,33.3);


(lib.text79 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape.setTransform(29.7,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAGAvIgMgrIAAArIgUAAIAAhdIAUAAIANArIAAgrIAUAAIAABdg");
	this.shape_1.setTransform(23.9,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgLAvIAAhdIAXAAIAABdg");
	this.shape_2.setTransform(18.9,5.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgSAvIAAhdIAWAAIAABKIAPAAIAAATg");
	this.shape_3.setTransform(15.1,5.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgPAtQgGgDgDgGQgDgGAAgIIAYAAIAAAIQAAAAAAABQAAAAAAABQABAAAAAAQAAAAAAABIACABIADgBIABgDIAAgHIAAgPQgCAEgCACQgCABgFAAQgHAAgFgDQgFgEgBgFQgCgDAAgIIAAgHIABgPQABgFADgEQAEgEAFgDQAGgCAHAAQAIAAAGADQAGACADAFQAEAFABAGIAAAVIAAALIAAAXQgBAFgEAFQgDAGgGACQgHADgHAAQgIAAgHgEgAgCggQgBACAAAGIAAASQAAAGABAAIACABQAAAAABAAQAAAAABgBQAAAAABAAQAAAAABAAQAAgBAAgFIAAgRIAAgIQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAIgCABg");
	this.shape_4.setTransform(7.3,5.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(2,-4,45.9,33.3);


(lib.text76 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape.setTransform(32.2,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFAvIgLgrIAAArIgUAAIAAhdIAUAAIANArIAAgrIAUAAIAABdg");
	this.shape_1.setTransform(26.4,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgLAvIAAhdIAXAAIAABdg");
	this.shape_2.setTransform(21.4,5.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgSAvIAAhdIAWAAIAABKIAPAAIAAATg");
	this.shape_3.setTransform(17.6,5.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgLAvQgGgCgDgFQgEgEgBgEQgBgFgBgKIAAgfQABgNACgHQACgGAHgEQAGgEAJAAQAHAAAGADQAHADADADQACAEABAFIABAQIAAAeIgBAPQgBAFgDAFQgEAEgFACQgGACgGAAQgHAAgFgCgAgCgfQAAACAAAHIAAAsIAAAKQAAABABAAQAAAAAAAAQABABAAAAQAAAAAAAAQAAAAABAAQAAgBABAAQAAAAAAAAQABgBAAAAQABgCAAgHIAAgtQAAgIgBgBQAAgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQAAAAAAAAQAAAAgBAAQAAABAAAAQAAAAgBABg");
	this.shape_4.setTransform(9.8,5.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgCAvIAAgxIAAgNQgBgBAAAAQAAgBAAAAQgBgBAAAAQgBAAgBAAQgCgBgIAAIgCAAIAAgMQARgDAHgMIANAAIAABdg");
	this.shape_5.setTransform(4.1,5.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-4,50.5,33.3);


(lib.text75 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape.setTransform(28.9,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAGAvIgMgrIAAArIgUAAIAAhdIAUAAIANArIAAgrIAUAAIAABdg");
	this.shape_1.setTransform(23.1,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgLAvIAAhdIAXAAIAABdg");
	this.shape_2.setTransform(18.1,5.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgSAvIAAhdIAWAAIAABKIAPAAIAAATg");
	this.shape_3.setTransform(14.3,5.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgOAvIAOhMIgVAAIAAgRIArAAIAAAVIgPBIg");
	this.shape_4.setTransform(7.3,5.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3,-4,43.8,33.3);


(lib.text73 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape.setTransform(29.7,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFAvIgKgrIAAArIgVAAIAAhdIAVAAIALArIAAgrIAVAAIAABdg");
	this.shape_1.setTransform(23.8,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgLAvIAAhdIAXAAIAABdg");
	this.shape_2.setTransform(18.8,5.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgSAvIAAhdIAWAAIAABKIAPAAIAAATg");
	this.shape_3.setTransform(15,5.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgOAsQgHgCgDgHQgDgGAAgNIAAgGIAYAAIAAAHIAAAMQABAEACAAIADgBIABgDIAAgLIAAgSQAAgDgBgDQAAAAgBAAQAAgBgBAAQAAAAgBAAQAAgBAAAAIgBABIgCACIAAAFIgYAAIABgvIAxAAIAAAPIgbAAIAAAQQAEgGAIAAQAKAAAFAFQAFAGAAANIAAAOIgBAPQgBAEgDAEQgDAFgGABQgGADgIAAQgHAAgHgEg");
	this.shape_4.setTransform(7.2,5.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(2,-4,45.8,33.3);


(lib.text71 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape.setTransform(30.5,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAGAvIgMgrIAAArIgUAAIAAhdIAUAAIANArIAAgrIAUAAIAABdg");
	this.shape_1.setTransform(24.7,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgLAvIAAhdIAXAAIAABdg");
	this.shape_2.setTransform(19.7,5.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgSAvIAAhdIAWAAIAABKIAPAAIAAATg");
	this.shape_3.setTransform(15.9,5.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgQAtQgGgEgCgFQgCgGgBgNIAAgIIAYAAIAAAPQAAAHABABQAAABAAAAQABAAAAAAQAAABABAAQAAAAAAAAQAAAAAAAAQABgBAAAAQABAAAAAAQABgBAAAAIABgLIAAgHIgBgIQgBAAAAgBQgBAAAAgBQgBAAAAAAQAAgBAAAAIgJgBIAAgMIAJAAQAAAAABgBQAAAAABAAQAAgBAAAAQABgBAAAAIABgHIAAgFQgBgFAAgCQgBAAAAAAQAAgBgBAAQAAAAgBAAQAAAAAAAAQAAAAAAAAQAAAAgBAAQAAAAAAABQgBAAAAAAIgBAHIAAAIIgYAAIAAgIQABgNAFgFQAHgFANAAQAQAAAGAHQAFAGABAMQgBAIgCAEQgCADgGADQAGACACADQAEADAAAPQAAAMgDAGQgDAGgGADQgHAEgJAAQgJAAgHgEg");
	this.shape_4.setTransform(8.2,5.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3,-4,44.6,33.3);


(lib.text69 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape.setTransform(28.8,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFAvIgKgrIAAArIgVAAIAAhdIAVAAIALArIAAgrIAVAAIAABdg");
	this.shape_1.setTransform(22.9,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgLAvIAAhdIAXAAIAABdg");
	this.shape_2.setTransform(17.9,5.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgSAvIAAhdIAWAAIAABKIAPAAIAAATg");
	this.shape_3.setTransform(14.1,5.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgCAvIAAgxIAAgNQgBgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAAAQgDgBgIAAIgDAAIAAgMQASgDAHgMIAOAAIAABdg");
	this.shape_4.setTransform(7.1,5.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3,-4,43.7,33.3);


(lib.text66 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAIA3QgFgRgDgYIgCAXIgDASIgcAAIAKg8IgKgxIAcAAIADAPIACAPIAFgeIAYAAIgKAxIAPA8g");
	this.shape.setTransform(28.6,23.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAHA3IgCgUIgJAAIgBAUIgeAAIAPhtIAnAAIARBtgAgEAPIAJAAIgFgsIgEAsg");
	this.shape_1.setTransform(21.6,23.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AATA3IAAhJIgKBJIgQAAIgLhHIAABHIgZAAIAAhtIAmAAIADAYIACAcIAGg0IAmAAIAABtg");
	this.shape_2.setTransform(13,23.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgNA3IAAhXIgRAAIAAgWIA9AAIAAAWIgRAAIAABXg");
	this.shape_3.setTransform(24.8,6.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgYA3IAAhtIAuAAIAAAWIgTAAIAAAVIASAAIAAATIgSAAIAAAZIAWAAIAAAWg");
	this.shape_4.setTransform(18.8,6.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AggA3IAAhtIAdAAQALAAAHACQAHACAFAHQAEAGAAAPQAAAJgDAEQgDAEgJACQAKACAEADQADAGAAALIAAAKQAAALgCAFQgDAGgFACQgGACgRAAgAgDAkQAEAAACgCQABgCABgHIAAgKQgBgHgBgBQgBgCgFAAgAgDgKIADAAQACAAACgCQABgDAAgJIgBgIIgDgCIgEgBg");
	this.shape_5.setTransform(11.9,6.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(6,-4,40.2,55.2);


(lib.text59 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgYA3IAAhtIAvAAIAAAWIgTAAIAAAVIASAAIAAATIgSAAIAAAZIAUAAIAAAWg");
	this.shape.setTransform(26.3,23.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAHA3IgNgzIAAAzIgZAAIAAhtIAZAAIAOAyIAAgyIAYAAIAABtg");
	this.shape_1.setTransform(19.4,23.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOA2QgIgDgEgGQgEgFgBgHIgBgYIAAgRIABgYQABgHAEgFQAEgGAHgDQAHgDAIAAQAIAAAHADQAIADAEAFQAEAGABAHIABAYIAAARIgBAYQgBAGgEAGQgEAGgHADQgHADgJAAQgHAAgHgDgAgCgkQgBACAAAJIAAAxQAAAKABACQAAABAAAAQABABAAAAQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAgBABAAQAAgBAAAAIABgNIAAgwIgBgKQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAAAQAAAAAAAAQAAAAAAABQgBAAAAAAQAAABgBAAg");
	this.shape_2.setTransform(11.8,23.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgNA3IAAhXIgRAAIAAgWIA9AAIAAAWIgRAAIAABXg");
	this.shape_3.setTransform(24.8,6.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgYA3IAAhtIAuAAIAAAWIgTAAIAAAVIASAAIAAATIgSAAIAAAZIAWAAIAAAWg");
	this.shape_4.setTransform(18.8,6.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AggA3IAAhtIAdAAQALAAAHACQAHACAFAHQAEAGAAAPQAAAJgDAEQgDAEgJACQAKACAEADQADAGAAALIAAAKQAAALgCAFQgDAGgFACQgGACgRAAgAgDAkQAEAAACgCQABgCABgHIAAgKQgBgHgBgBQgBgCgFAAgAgDgKIADAAQACAAACgCQABgDAAgJIgBgIIgDgCIgEgBg");
	this.shape_5.setTransform(11.9,6.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(6,-4,38.6,55.2);


(lib.text51 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgMAzIAAhQIgPAAIAAgVIA4AAIAAAVIgQAAIAABQg");
	this.shape.setTransform(49.3,21.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAEAzIAAgiQAAgIgBgDQgBgCgEAAIAAAvIgaAAIAAhlIASAAQARAAAHABQAHACAEAGQAEAGAAANQAAANgDAEQgDADgIABQAHABADACIAEAHIAAAPIAAAbgAgCgKIAFgBQABgBAAgHIAAgFQAAgFgBgCQgCgBgDAAg");
	this.shape_1.setTransform(42.8,21.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAGAzIgCgTIgHAAIgCATIgbAAIANhlIAlAAIAPBlgAgEAOIAIAAIgEgpQgDAbgBAOg");
	this.shape_2.setTransform(36,21.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgMAzIAAhQIgPAAIAAgVIA3AAIAAAVIgPAAIAABQg");
	this.shape_3.setTransform(29.7,21.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgPAxQgIgEgDgHQgCgGAAgLIAAgHIAZAAIAAANQAAAFABACQAAAAABABQAAAAAAAAQABABAAAAQAAAAAAAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBABgBQABgCAAgDQAAgJgCgEIgLgJIgNgIQgEgCgCgGQgCgFAAgIQAAgMADgGQADgFAHgDQAGgDAKAAQAJAAAHADQAHAEACAFQADAFAAANIAAADIgZAAIAAgHQAAgGgBgBQAAAAgBgBQAAAAAAAAQgBAAAAAAQAAAAAAAAQAAAAAAAAQgBAAAAAAQgBAAAAABQAAAAgBABQgBABAAAFQAAAFABACQACADAEADQASALAEAFQAFAGAAAPQAAALgCAGQgDAFgIADQgHAEgJAAQgJAAgHgEg");
	this.shape_4.setTransform(23.4,21.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgRAxQgIgFgCgHQgDgHAAgQIAAgbIABgRQABgFADgGQAEgFAHgDQAGgDAIAAQALAAAHAEQAJAFACAHQACAHAAAOIAAAKIgaAAIAAgSQAAgHgCgCQAAgBAAAAQAAgBgBAAQAAAAgBAAQAAAAgBAAQgCAAAAACIgBAKIAAAtIABAKQAAABAAABQABAAAAAAQABABAAAAQAAAAAAAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBAAgBQACgCAAgIIAAgNIAaAAIAAAEQAAAPgCAIQgDAGgHAFQgIAFgLAAQgKAAgHgEg");
	this.shape_5.setTransform(76.1,5.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgMAzIAAhlIAZAAIAABlg");
	this.shape_6.setTransform(70.6,5.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgMAzIAAhQIgPAAIAAgVIA3AAIAAAVIgPAAIAABQg");
	this.shape_7.setTransform(65.7,5.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAGAzIgCgTIgHAAIgCATIgbAAIANhlIAlAAIAPBlgAgEAOIAIAAIgEgpQgDAbgBAOg");
	this.shape_8.setTransform(59.4,5.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AARAzIAAhDIgJBDIgPAAIgJhCIgBBCIgXAAIAAhlIAjAAIADAWIACAaIAGgwIAjAAIAABlg");
	this.shape_9.setTransform(51.5,5.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgNAyQgHgCgEgGQgEgFgBgHIgBgVIAAgQIABgWQABgGAEgGQADgFAHgDQAHgDAHAAQAIAAAGADQAHADAEAFQAEAGABAFIAAAXIAAAQIAAAVQgBAHgEAFQgDAFgIADQgGADgIAAQgHAAgGgDgAgCghQgBABAAAJIAAAtIABALQAAABABABQAAAAAAAAQABABAAAAQAAAAAAAAQAAAAABAAQAAAAABgBQAAAAAAAAQABgBAAgBIAAgLIAAgtIAAgJQAAgBgBAAQAAgBAAAAQgBAAAAAAQgBAAAAAAQAAAAAAAAQAAAAAAAAQgBAAAAAAQAAABgBAAg");
	this.shape_10.setTransform(43.3,5.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgMAzIAAhQIgPAAIAAgVIA3AAIAAAVIgPAAIAABQg");
	this.shape_11.setTransform(36.7,5.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgOAxQgHgDgEgFQgDgFAAgFIgBgXIAAg7IAbAAIAABLQAAAHABACQAAABAAAAQAAAAABAAQAAABAAAAQAAAAAAAAQAAAAABAAQAAAAAAgBQABAAAAAAQAAgBAAgBQABgCAAgHIAAhKIAbAAIAABDIgBAQQAAAFgEAFQgEAFgGACQgGADgJAAQgHAAgHgDg");
	this.shape_12.setTransform(30.2,6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAGAzIgCgTIgHAAIgCATIgbAAIANhlIAlAAIAPBlgAgEAOIAIAAIgEgpQgDAbgBAOg");
	this.shape_13.setTransform(23.3,5.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(18,-4,80.3,51.6);


(lib.text43 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape.setTransform(56.9,20);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAQAvIAAg+IgJA+IgNAAIgJg9IAAA9IgWAAIAAhdIAhAAIACAVIACAXIAFgsIAhAAIAABdg");
	this.shape_1.setTransform(50,20);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAGAvIgCgRIgHAAIgBARIgaAAIANhdIAiAAIANBdgAgDANIAGAAIgDglIgDAlg");
	this.shape_2.setTransform(42.7,20);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgPAuQgFgDgDgEQgDgFAAgFIgBgPIAAgaQAAgOABgGQACgFAHgFQAHgGAKAAQAKAAAHAFQAHAEACAGQACAGAAAMIAAAEIgYAAIAAgJQAAgHgBgDQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAAAAAQAAAAAAAAQAAAAAAAAQgBABAAAAQgBABAAAAIAAAJIAAArIAAAJQAAABABAAQAAAAABABQAAAAAAAAQAAAAAAAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBAAgBQABgBAAgIIAAgKIgEAAIAAgNIAcAAIAAAxIgPAAIgCgHQgDAEgEADQgEACgDAAQgGAAgGgDg");
	this.shape_3.setTransform(36.3,20);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgLAvIAAhKIgOAAIAAgTIA0AAIAAATIgPAAIAABKg");
	this.shape_4.setTransform(63.2,5.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgQAtQgHgEgCgHQgDgHAAgOIAAgaIABgPQABgFADgEQAEgGAGgCQAGgDAHAAQAKAAAHAFQAHADADAHQACAGAAANIAAAJIgZAAIAAgQQAAgHgBgCQAAAAAAgBQAAAAgBAAQAAgBgBAAQgBAAAAAAQAAAAAAAAQAAAAgBABQAAAAgBAAQAAABAAAAIgBAJIAAArIABAJQAAABAAAAQABAAAAAAQABABAAAAQAAAAAAAAQAAAAABAAQAAAAABgBQAAAAABAAQAAAAAAgBIABgKIAAgMIAZAAIAAADQAAAPgCAHQgCAGgHAFQgIAEgKAAQgJAAgHgEg");
	this.shape_5.setTransform(57.1,5.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape_6.setTransform(51.4,5.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgSAvIAAhdIAWAAIAABKIAPAAIAAATg");
	this.shape_7.setTransform(46.7,5.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgUAvIAAhdIAnAAIAAATIgQAAIAAASIAPAAIAAAQIgPAAIAAAVIASAAIAAATg");
	this.shape_8.setTransform(41.8,5.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgPAtQgHgEgCgFQgCgGAAgLIAAgGIAXAAIAAAMQAAAFABABQAAABABAAQAAAAAAABQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABgBQAAAAAAAAQABAAAAgBQACgCAAgDQAAgJgDgCQgCgEgIgFIgLgHQgDgDgDgFQgBgFgBgHQABgLACgFQADgFAGgDQAGgDAJAAQAIAAAHAEQAGADADAEQACAFAAALIAAAEIgXAAIAAgHQAAgFgBgCQAAAAgBAAQAAAAAAgBQgBAAAAAAQAAAAAAAAQAAAAAAAAQgBAAAAABQgBAAAAAAQAAABAAAAIgBAGIABAHQABACADADQARALAFADQADAHAAANQAAALgBAEQgDAFgHAEQgGADgKAAQgIAAgHgEg");
	this.shape_9.setTransform(36.1,5.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(31,-4,50.6,47.9);


(lib.shape202 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 9
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#000000","#333333","#666666","#999999","#F9F9F6","#999999","#666666","#333333"],[0,0.086,0.267,0.486,0.631,0.745,0.867,0.961],-647.5,-5.8,397.8,-6.2).s().p("Eg9VAAwIB/hfMB2tAAAIB1BXIAAAGIAIAAIACACg");
	this.shape.setTransform(-53.6,159.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 8
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AgiguIBiAAIh/Bdg");
	this.shape_1.setTransform(332.6,-344.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3C3C3C").s().p("AgwANIAAgZIBiAAIAAAZg");
	this.shape_2.setTransform(334,-350.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer 7
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#222222","#595959"],[0,1],-778,236.5,-778,-234.1).s().p("EgBPgmqIB+hmUABCApEgBCAndIh+ABg");
	this.shape_3.setTransform(334.3,-93.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#000000","#333333","#666666","#999999","#F9F9F6"],[0,0.086,0.345,0.537,1],12.2,252.7,12.2,-363.2).s().p("EgAhAoRUABBgndgBBgpEIBDAAMAAABQhg");
	this.shape_4.setTransform(342.5,-93.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 6
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333333").s().p("Ag/guIBiAAIAcBdg");
	this.shape_5.setTransform(-441.7,-344.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#484848").s().p("AgxANIAAgZIBiAAIAAAZg");
	this.shape_6.setTransform(-443.1,-350.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

	// Layer 5
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#222222","#595959"],[0,1],-0.4,236.5,-0.4,-234.1).s().p("EgAuAoRUgBCgndABCgpEIB+BzMAAABOvg");
	this.shape_7.setTransform(-443.4,-93.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#000000","#333333","#666666","#999999","#F9F9F6"],[0,0.086,0.345,0.537,1],-12.6,252.7,-12.6,-363.2).s().p("EgAdAoRMAAAhQhIA6AAUgBAApEABAAndg");
	this.shape_8.setTransform(-451.1,-93.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7}]}).wait(1));

	// Layer 4
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#303030","#333333","#FFFFFF","#666666","#444444"],[0,0.086,0.447,0.89,1],-394.2,0,394.3,0).s().p("Eg9lAANIAAgZMB7LAAAIAAAZg");
	this.shape_9.setTransform(-54.1,-350.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#333333","#1A1A1A","#333333"],[0,0.541,1],-389.7,1.2,399,1.2).s().p("Eg9lAAjIAAhFMB7LAAAIAABFg");
	this.shape_10.setTransform(-54.1,-345.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9}]}).wait(1));

	// Layer 3
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["rgba(0,0,0,0)","rgba(0,0,0,0.4)"],[0.102,1],-4.2,-21.1,0,-4.2,-21.1,457.7).s().p("Eg9nAEeIgDgBQhvgXAiiIIAAgBIDjtAMB3NAAAIDDNVQAWBxhXAaIgCAAUggQAGnge4AAAQ+4AA9gmmg");
	this.shape_11.setTransform(-53.8,252.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

	// Layer 2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#222222","#111111","#000000","#111111","#222222"],[0,0.086,0.537,0.929,1],-412.3,0,413.5,0).s().p("Eg2PAFCIiZgiQjug1jrg8QhPgYgTg8IACgCIhqr+QAUA8BRAZUA+4APpBEngPpQBDgWAQg7IhBMAQgPA7hDAVQkPBAkPA4IidAfQ7+Ff69ABQ7JAA6Jlkg");
	this.shape_12.setTransform(-58.7,353.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#000000","#666666","#333333","#333333","#666666","#666666","#000000"],[0,0.129,0.318,0.541,0.757,0.871,1],-404.6,1.8,409.1,-5).s().p("Eg9sAExIgCAAQiEgZAmidIAAgBIDntKIADgFIAHgCMB3dAAAIAHACIAAAAIAEAGIDENdQAaCGhrAdIgDAAUggRAGnge5AAAQ+7AA9kmngEg+4AB9IAAABQghCIBvAXIACABUA7FANMBAdgNNIABAAQBYgagYhxIjBtVMh3NAAAg");
	this.shape_13.setTransform(-53.9,252.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#222222","#111111","#000000","#111111","#222222"],[0,0.086,0.537,0.929,1],0,77.5,0,-92.1).s().p("EhBlAFcQhRgZgUg7IgBgEQgNgwAXhDIFTvhMB3/AACIB8AAIAfAAIApAAIB5ABID7P2QAOA3gKAoIgBAEQgPA6hDAWUgiVAH1gg2AAAUgg4AAAgfcgH1gEg6xgMWIgDAFIjnNKIAAABQgmCdCEAZIACAAUA7IANOBAhgNOIADAAQBrgdgaiGIjEtdIgEgGIAAAAIgHgCMh3dAAAg");
	this.shape_14.setTransform(-58.8,258.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CCCCCC").s().p("Eg9oAEeIgCgBQhvgXAhiIIAAgBIDltAMB3NAAAIDBNVQAYBxhYAaIgBAAUggRAGnge4AAAQ+4AA9hmmg");
	this.shape_15.setTransform(-53.9,252.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#222222","#111111","#000000","#111111","#222222"],[0,0.086,0.537,0.929,1],-400.7,162.2,-400.7,-7.4).s().p("AA7ABIh4gBIB5AAIABABg");
	this.shape_16.setTransform(340,174);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12}]}).wait(1));

	// Layer 1
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.lf(["#222222","#111111","#000000","#111111","#222222"],[0,0.086,0.537,0.929,1],-390.9,-7,391,-7).s().p("Eg9EAAUIAAgnMB6JAAAIAAAng");
	this.shape_17.setTransform(-52.2,176.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#222222","#111111","#000000","#111111","#222222"],[0,0.086,0.537,0.929,1],-400,2,400,2).s().p("EA9XABGMh6IAAAIhuAAIAAiLMB8+AAAIAACLg");
	this.shape_18.setTransform(-54.1,167.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-489.3,-351.6,861.1,772.5);


(lib.shape85 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AlsBhIAAjBILZAAIAADBg");
	this.shape.setTransform(0,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AkPFeQgnAAgbgbQgbgZAAglIAAipILZAAIAACpQgBAlgaAZQgbAbgnAAgAlshkIAAigQAAglAbgZQAbgaAngBIIfAAQAnABAbAaQAaAZABAlIAACgg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-35,73,70);


(lib.shape68 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#999999").s().p("AjACnIAAlNIGCAAIAAFNg");
	this.shape.setTransform(0.3,-3.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhkFeQgmAAgbgbQgbgZAAglIAAh+IGCAAIAAB+QgBAlgaAZQgcAbglAAgAjAjGIAAg+QAAglAbgZQAbgaAmgBIDKAAQAlABAcAaQAaAZABAlIAAA+g");
	this.shape_1.setTransform(0.3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.1,-35,38.8,70);


(lib.shape65 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUEcIAAgBQAOgBAGghQgIACgDAFIgFAHQgFAGgIAAQgIAAgGgGQgFgGAAgIQAAgJAFgGQAFgGAJAAQAHAAAGAGIAFAHQADAEAIADQgCgKgDgDIgHgFQgGgGAAgIQAAgJAFgGQAFgGAIAAQAGAAAGAGQAGAGABAJQgBAIgFAGIgHAFQgEADgCALQAKgEACgEIAEgHQAGgGAIAAQAIAAAGAGQAGAGAAAJQAAAIgGAGQgFAGgIAAQgJAAgFgGIgFgHQgDgFgJgCIAAAAQAIAiANAAIABAAIAAABgAgVi2IgBgBQAPAAAHgkIgHAIIgNAHQgRAIgIgOQgLgRAXgWIATgSQAMgKACgGIANAQIAUASQAYAWgLARQgHAOgRgIIgWgPQAJAkAPAAIAAABg");
	this.shape.setTransform(0,-3.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhkFeQgmAAgbgbQgbgZAAglIAAh+IGCAAIAAB+QgBAlgaAZQgcAbglAAgAjAjGIAAg+QAAglAbgZQAbgaAmgBIDKAAQAlABAcAaQAaAZABAlIAAA+g");
	this.shape_1.setTransform(0.3,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AjACnIAAlNIGCAAIAAFNg");
	this.shape_2.setTransform(0.3,-3.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.1,-35,38.8,70);


(lib.shape63 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0)"],[0.235,1],27.8,-1.9,0,27.8,-1.9,63.4).s().p("AhIExQgiAAgXgWQgXgWABgeIAAnNQgBgfAXgVQAXgWAiAAICaAAIAAABQAbACAUATQAXAVAAAfIAAHNQAAAegXAWQgUASgbAEIAAAAg");
	this.shape.setTransform(0.4,-1.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],21,-31.7,21,31.8).s().p("AghE4QglAAgZgYQgagYABgiIAAnMQgBgiAagYQAZgXAlAAICaAAIAAAAIAAAHIiaAAQgiAAgXAWQgXAVABAfIAAHMQgBAeAXAXQAXAWAiAAICaAAIAAAHIAAAAg");
	this.shape_1.setTransform(-3.5,-1.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],27.9,-2.2,0,27.9,-2.2,63.4).s().p("AhSFFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIClAAQAjAAAZAYQAZAXABAiIAAHlQgBAhgZAYQgZAYgjABgAiFkjQgaAYABAhIAAHNQgBAhAaAZQAZAXAkABICbAAIAAAAQAdgEAWgUQAZgYABgiIAAnNQgBghgZgYQgWgVgdgCIAAgBIibAAQgkAAgZAYg");
	this.shape_2.setTransform(0.4,-1.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],-29.2,-31.7,-29.2,31.8).s().p("AglExIAAgBQAagDASgSQAXgWABgfIAAnMQgBgfgXgVQgSgTgagCIAAgBIAAgHQAdACAUAVQAZAYABAiIAAHMQgBAigZAYQgUAUgdAEg");
	this.shape_3.setTransform(12.6,-1.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(0,0,0,0.753)","rgba(0,0,0,0.4)","rgba(0,0,0,0.424)","rgba(0,0,0,0.8)"],[0,0.318,0.522,1],17.1,-32.7,17.1,32.7).s().p("AhSFFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIClAAQAjgBAZAZQAZAXABAiIAAHlQgBAhgZAYQgZAZgjAAg");
	this.shape_4.setTransform(0.4,-1.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#000000","rgba(0,0,0,0.8)","rgba(0,0,0,0.812)","#000000"],[0,0.29,0.486,1],17.1,-35,17.1,35).s().p("AhkFeQgmAAgbgbQgbgZAAglIAAoJQAAglAbgZQAbgaAmgBIDKAAQAlABAcAaQAaAZABAlIAAIJQgBAlgaAZQgcAbglAAgAiOk6QgZAXAAAiIAAHkQAAAiAZAYQAZAYAkAAIClAAQAiAAAagYQAZgYAAgiIAAnkQAAgigZgXQgagZgiAAIilAAIgBAAQgjAAgZAZg");
	this.shape_5.setTransform(0.3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.1,-35,38.8,70);


(lib.shape62 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0)"],[0.235,1],30.6,-2,0,30.6,-2,69.8).s().p("AhQE9QglgBgZgXQgZgXAAgfIAAneQAAggAZgWQAZgXAlABICrAAIAAAAQAdADAWATQAZAWABAgIAAHeQgBAggZAWQgWATgdAEIAAABg");
	this.shape.setTransform(0.5,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],23.1,-32.9,23.1,33).s().p("AglFEQgogBgcgYQgcgZABgjIAAneQgBgjAcgYQAcgZAoAAICrAAIAAAAIAAAIIirAAQglgBgZAXQgZAWAAAgIAAHeQAAAfAZAXQAZAXAlABICrAAIAAAHIAAAAg");
	this.shape_1.setTransform(-3.8,-1.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],30.7,-2.3,0,30.7,-2.3,69.8).s().p("AhaFRQgoAAgbgZQgcgZAAgjIAAn3QAAgjAcgZQAbgZAoAAIC2AAQAnAAAbAZQAbAZABAjIAAH3QgBAjgbAZQgbAYgnABgAiTkuQgdAZACAiIAAHfQgCAiAdAZQAcAZAoABICrAAIAAgBQAggDAYgWQAbgYABgjIAAnfQgBgigbgZQgYgWgggCIAAgBIirAAQgoAAgcAZg");
	this.shape_2.setTransform(0.4,-0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],-32.2,-32.9,-32.2,33).s().p("AgpE9IAAgBQAdgEAUgTQAZgWABggIAAneQgBgggZgWQgUgTgdgDIAAAAIAAgIQAhADAVAWQAcAYABAjIAAHeQgBAjgcAZQgVAVghAEg");
	this.shape_3.setTransform(13.9,-1.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(0,0,0,0.2)","rgba(0,0,0,0.051)","rgba(0,0,0,0.063)","rgba(0,0,0,0.153)"],[0,0.318,0.522,1],18.8,-33.9,18.8,34).s().p("AhaFRQgoAAgcgZQgbgZAAgjIAAn3QAAgjAbgZQAcgZAoAAIC1AAQAnAAAcAZQAbAZABAjIAAH3QgBAjgbAZQgcAZgnAAg");
	this.shape_4.setTransform(0.2,-0.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["rgba(0,0,0,0.4)","rgba(0,0,0,0.2)","rgba(0,0,0,0.212)","rgba(0,0,0,0.4)"],[0,0.29,0.486,1],17.1,-35,17.1,35).s().p("AhkFeQgmAAgbgbQgbgZAAglIAAoJQAAglAbgZQAbgaAmgBIDKAAQAlABAcAaQAaAZABAlIAAIJQgBAlgaAZQgcAbglAAgAifk/QgcAZAAAjIAAH3QAAAjAcAZQAcAaAnAAIC1AAQAoAAAcgaQAbgZABgjIAAn3QgBgjgbgZQgcgZgoAAIi1AAQgnAAgcAZg");
	this.shape_5.setTransform(0.3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.1,-35,38.8,70);


(lib.shape61 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0)"],[0.235,1],27.8,-1.9,0,27.8,-1.9,63.4).s().p("AhIExQgiAAgXgWQgXgWABgeIAAnNQgBgfAXgVQAXgWAiAAICaAAIAAABQAbACAUATQAXAVAAAfIAAHNQAAAegXAWQgUASgbAEIAAAAg");
	this.shape.setTransform(0.4,-1.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],21,-31.7,21,31.8).s().p("AghE4QglAAgZgYQgagYABgiIAAnMQgBgiAagYQAZgXAlAAICaAAIAAAAIAAAHIiaAAQgiAAgXAWQgXAVABAfIAAHMQgBAeAXAXQAXAWAiAAICaAAIAAAHIAAAAg");
	this.shape_1.setTransform(-3.5,-1.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],27.9,-2.2,0,27.9,-2.2,63.4).s().p("AhSFFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIClAAQAjAAAZAYQAZAXABAiIAAHlQgBAhgZAYQgZAYgjABgAiFkjQgaAYABAhIAAHNQgBAhAaAZQAZAXAkABICbAAIAAAAQAdgEAWgUQAZgYABgiIAAnNQgBghgZgYQgWgVgdgCIAAgBIibAAQgkAAgZAYg");
	this.shape_2.setTransform(0.4,-1.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],-29.2,-31.7,-29.2,31.8).s().p("AglExIAAgBQAagDASgSQAXgWABgfIAAnMQgBgfgXgVQgSgTgagCIAAgBIAAgHQAdACAUAVQAZAYABAiIAAHMQgBAigZAYQgUAUgdAEg");
	this.shape_3.setTransform(12.6,-1.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(0,0,0,0.2)","rgba(0,0,0,0.051)","rgba(0,0,0,0.063)","rgba(0,0,0,0.153)"],[0,0.318,0.522,1],17.1,-32.7,17.1,32.7).s().p("AhSFFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIClAAQAjgBAZAZQAZAXABAiIAAHlQgBAhgZAYQgZAZgjAAg");
	this.shape_4.setTransform(0.4,-1.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["rgba(0,0,0,0.4)","rgba(0,0,0,0.2)","rgba(0,0,0,0.212)","rgba(0,0,0,0.4)"],[0,0.29,0.486,1],17.1,-35,17.1,35).s().p("AhkFeQgmAAgbgbQgbgZAAglIAAoJQAAglAbgZQAbgaAmgBIDKAAQAlABAcAaQAaAZABAlIAAIJQgBAlgaAZQgcAbglAAgAiOk6QgZAXAAAiIAAHkQAAAiAZAYQAZAYAkAAIClAAQAiAAAagYQAZgYAAgiIAAnkQAAgigZgXQgagZgiAAIilAAIgBAAQgjAAgZAZg");
	this.shape_5.setTransform(0.3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.1,-35,38.8,70);


(lib.shape60 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],21,-31.7,21,31.8).s().p("AghE4QglAAgZgYQgagYABgiIAAnMQgBgiAagYQAZgXAlAAICaAAIAAAAIAAAHIiaAAQgiAAgXAWQgXAVABAfIAAHMQgBAeAXAXQAXAWAiAAICaAAIAAAHIAAAAg");
	this.shape.setTransform(-3.5,-1.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],27.9,-2.2,0,27.9,-2.2,63.4).s().p("AhSFFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIClAAQAjAAAZAYQAZAXABAiIAAHlQgBAhgZAYQgZAYgjABgAiFkjQgaAYABAhIAAHNQgBAhAaAZQAZAXAkABICbAAIAAAAQAdgEAWgUQAZgYABgiIAAnNQgBghgZgYQgWgVgdgCIAAgBIibAAQgkAAgZAYgAhIEuQghgBgXgWQgXgWAAgeIAAnNQAAgeAXgVQAXgXAhABICbAAIAAAAQAaADAUATQAXAVABAeIAAHNQgBAfgXAVQgUATgaADIAAABg");
	this.shape_1.setTransform(0.4,-1.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],-29.2,-31.7,-29.2,31.8).s().p("AglExIAAgBQAagDASgSQAXgWABgfIAAnMQgBgfgXgVQgSgTgagCIAAgBIAAgHQAdACAUAVQAZAYABAiIAAHMQgBAigZAYQgUAUgdAEg");
	this.shape_2.setTransform(12.6,-1.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.2)","rgba(0,0,0,0.212)","rgba(0,0,0,0.4)"],[0,0.318,0.522,1],17.1,-32.7,17.1,32.7).s().p("AhSFFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIClAAQAjgBAZAZQAZAXABAiIAAHlQgBAhgZAYQgZAZgjAAg");
	this.shape_3.setTransform(0.4,-1.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(0,0,0,0.843)","rgba(0,0,0,0.522)","rgba(0,0,0,0.612)","rgba(0,0,0,0.792)"],[0,0.29,0.486,1],17.1,-35,17.1,35).s().p("AhkFeQgmAAgbgbQgbgZAAglIAAoJQAAglAbgZQAbgaAmgBIDKAAQAlABAcAaQAaAZABAlIAAIJQgBAlgaAZQgcAbglAAgAiOk6QgZAXAAAiIAAHkQAAAiAZAYQAZAYAkAAIClAAQAiAAAagYQAZgYAAgiIAAnkQAAgigZgXQgagZgiAAIilAAIgBAAQgjAAgZAZg");
	this.shape_4.setTransform(0.3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.1,-35,38.8,70);


(lib.shape58 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC0A16").s().p("AgUD9QgVgXgHAAQAGAAAWgWQAUgYAAgGQAAAGAVAYQAWAWAHAAQgHAAgVAXQgWAXAAAIQAAgIgUgXgAgMjRIgUgSQgXgZAJgSQAOgZAgAXQAfgXAPAZQALASgXAZIgUASIgOASQgBgHgLgLg");
	this.shape.setTransform(0,-2.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#990000").s().p("AjACnIAAlNIGCAAIAAFNg");
	this.shape_1.setTransform(0.3,-3.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhkFeQgmAAgbgbQgbgZAAglIAAh+IGCAAIAAB+QgBAlgaAZQgcAbglAAgAjAjGIAAg+QAAglAbgZQAbgaAmgBIDKAAQAlABAcAaQAaAZABAlIAAA+g");
	this.shape_2.setTransform(0.3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.1,-35,38.8,70);


(lib.shape57 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(0.5,1,2).p("ADDkFIAAILQgBAkgaAaQgbAagmAAIjMAAQgnAAgbgaQgbgaAAgkIAAoLQAAgkAbgZQAbgbAnAAIDMAAQAmAAAbAbQAaAZABAkg");
	this.shape.setTransform(0.2,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(255,255,255,0.502)","rgba(255,255,255,0.302)"],[0,1],17,-35.9,17,35.9).s().p("AhlFxQgvAAghgfQghggAAgtIAAoJQAAgtAhggQAhgfAvAAIDMAAQAuAAAhAfQAgAgABAtIAAIJQgBAtggAgQghAfguAAgAinlCQgbAZAAAlIAAIJQAAAlAbAZQAbAbAnAAIDMAAQAmAAAbgbQAagZABglIAAoJQgBglgagZQgbgagmgBIjMAAQgnABgbAag");
	this.shape_1.setTransform(0.2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#5E5E5E","#222222"],[0,1],16.6,-40.3,16.6,40.4).s().p("Ah7GTQgtgBgggfQggggAAguIAApJQAAguAgggQAgggAtAAID3AAQAtAAAgAgQAgAgAAAuIAAJJQAAAuggAgQggAfgtABg");
	this.shape_2.setTransform(0,2);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-23.3,-38.4,46.8,80.7);


(lib.shape55 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.753)","#000000"],[0,1],0,-31.7,0,31.8).s().p("AjzE5QglgBgZgYQgagYABghIAAnNQgBgiAagXQAZgYAlAAIHnAAQAjgBAaAZQAZAXABAiIAAHNQgBAhgZAYQgaAYgjABgAkskaQgXAVABAfIAAHNQgBAeAXAWQAXAWAiAAIHnAAQAgAAAYgWQAXgWAAgeIAAnNQAAgfgXgVQgYgWggAAInnAAIgCAAQggAAgXAWg");
	this.shape.setTransform(0.1,-1.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0)"],[0.235,1],10.7,-1.9,0,10.7,-1.9,63.4).s().p("AjzExQgiAAgXgWQgXgWABgeIAAnNQgBgfAXgVQAXgWAiAAIHnAAQAgAAAYAWQAXAVAAAfIAAHNQAAAegXAWQgYAWggAAg");
	this.shape_1.setTransform(0.1,-1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],10.8,-2.2,0,10.8,-2.2,63.4).s().p("Aj9FFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIH7AAQAjgBAZAZQAZAXABAiIAAHlQgBAhgZAYQgZAZgjAAgAkwkjQgaAYABAhIAAHNQgBAhAaAZQAZAXAkABIHnAAQAkgBAZgXQAZgYABgiIAAnNQgBghgZgYQgZgYgkAAInnAAQgkAAgZAYg");
	this.shape_2.setTransform(0.1,-1.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#000000","rgba(0,0,0,0.8)","rgba(0,0,0,0.812)","#000000"],[0,0.29,0.486,1],0,-35,0,35).s().p("AkPFeQgnAAgbgbQgbgZAAglIAAoJQAAglAbgZQAbgaAngBIIfAAQAnABAbAaQAaAZABAlIAAIJQgBAlgaAZQgbAbgnAAgAk5k6QgZAXAAAiIAAHkQAAAiAZAYQAZAYAkAAIH6AAQAkAAAZgYQAYgYACgiIAAnkQgCgigYgXQgZgZgkAAIn6AAIgBAAQgkAAgYAZg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(0,0,0,0.753)","rgba(0,0,0,0.4)","rgba(0,0,0,0.424)","rgba(0,0,0,0.8)"],[0,0.318,0.522,1],0,-32.7,0,32.7).s().p("Aj9FFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIH7AAQAjgBAZAZQAZAXABAiIAAHlQgBAhgZAYQgZAZgjAAg");
	this.shape_4.setTransform(0.1,-1.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-35,73,70);


(lib.shape54 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0)"],[0.235,1],11.2,-2,0,11.2,-2,66.4).s().p("Aj/FDQgjgBgYgXQgYgXAAggIAAnoQAAggAYgWQAYgYAjABIH+AAQAiAAAZAXQAYAWABAgIAAHoQgBAggYAXQgZAXgiABg");
	this.shape.setTransform(-0.2,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],0,-33.5,0,33.6).s().p("Aj/FLQgmgBgagZQgbgaABgjIAAnoQgBgjAbgZQAagZAmAAIH+AAQAmgBAaAaQAbAZABAjIAAHoQgBAjgbAaQgaAZgmABgAk6kqQgYAWAAAgIAAHoQAAAgAYAXQAYAXAjABIH+AAQAigBAZgXQAYgXABggIAAnoQgBgggYgWQgZgXgiAAIn+AAIgBgBQgiAAgYAYg");
	this.shape_1.setTransform(-0.2,-1.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],11.3,-2.4,0,11.3,-2.4,66.4).s().p("AkJFXQgmAAgagaQgagZAAgkIAAoAQAAgkAagZQAagaAmABIITAAQAlgBAaAaQAaAZABAkIAAIAQgBAkgaAZQgaAaglAAgAk/k0QgbAZABAjIAAHoQgBAjAbAaQAaAZAnABIH+AAQAlgBAbgZQAagaABgjIAAnoQgBgjgagZQgbgaglABIn+AAQgnAAgaAZg");
	this.shape_2.setTransform(-0.2,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.2)","rgba(0,0,0,0.051)","rgba(0,0,0,0.063)","rgba(0,0,0,0.153)"],[0,0.318,0.522,1],0,-34.3,0,34.4).s().p("AkJFVQglAAgbgaQgbgZAAgjIAAn9QAAgkAbgZQAbgZAlAAIIUAAQAkAAAbAZQAaAZABAkIAAH9QgBAjgaAZQgbAagkAAg");
	this.shape_3.setTransform(0,-0.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(0,0,0,0.4)","rgba(0,0,0,0.2)","rgba(0,0,0,0.212)","rgba(0,0,0,0.4)"],[0,0.29,0.486,1],0,-35,0,35).s().p("AkPFeQgnAAgbgbQgbgZAAglIAAoJQAAglAbgZQAbgaAngBIIfAAQAnABAbAaQAaAZABAlIAAIJQgBAlgaAZQgbAbgnAAgAlJk9QgbAZAAAjIAAH9QAAAkAbAZQAbAZAlABIIUAAQAkgBAbgZQAagZABgkIAAn9QgBgjgagZQgbgagkABIoUAAQglgBgbAag");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-35,73,70.1);


(lib.shape53 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0)"],[0.235,1],10.7,-1.9,0,10.7,-1.9,63.4).s().p("AjzExQgiAAgXgWQgXgWABgeIAAnNQgBgfAXgVQAXgWAiAAIHnAAQAgAAAYAWQAXAVAAAfIAAHNQAAAegXAWQgYAWggAAg");
	this.shape.setTransform(0.1,-1.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],0,-31.7,0,31.8).s().p("AjzE5QglgBgZgYQgagYABghIAAnNQgBgiAagXQAZgYAlAAIHnAAQAjgBAaAZQAZAXABAiIAAHNQgBAhgZAYQgaAYgjABgAkskaQgXAVABAfIAAHNQgBAeAXAWQAXAWAiAAIHnAAQAgAAAYgWQAXgWAAgeIAAnNQAAgfgXgVQgYgWggAAInnAAIgCAAQggAAgXAWg");
	this.shape_1.setTransform(0.1,-1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],10.8,-2.2,0,10.8,-2.2,63.4).s().p("Aj9FFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIH7AAQAjgBAZAZQAZAXABAiIAAHlQgBAhgZAYQgZAZgjAAgAkwkjQgaAYABAhIAAHNQgBAhAaAZQAZAXAkABIHnAAQAkgBAZgXQAZgYABgiIAAnNQgBghgZgYQgZgYgkAAInnAAQgkAAgZAYg");
	this.shape_2.setTransform(0.1,-1.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.4)","rgba(0,0,0,0.2)","rgba(0,0,0,0.212)","rgba(0,0,0,0.4)"],[0,0.29,0.486,1],0,-35,0,35).s().p("AkPFeQgnAAgbgbQgbgZAAglIAAoJQAAglAbgZQAbgaAngBIIfAAQAnABAbAaQAaAZABAlIAAIJQgBAlgaAZQgbAbgnAAgAk5k6QgZAXAAAiIAAHkQAAAiAZAYQAZAYAkAAIH6AAQAkAAAZgYQAYgYACgiIAAnkQgCgigYgXQgZgZgkAAIn6AAIgBAAQgkAAgYAZg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(0,0,0,0.2)","rgba(0,0,0,0.051)","rgba(0,0,0,0.063)","rgba(0,0,0,0.153)"],[0,0.318,0.522,1],0,-32.7,0,32.7).s().p("Aj9FFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIH7AAQAjgBAZAZQAZAXABAiIAAHlQgBAhgZAYQgZAZgjAAg");
	this.shape_4.setTransform(0.1,-1.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-35,73,70);


(lib.shape52 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],10.8,-2.2,0,10.8,-2.2,63.4).s().p("Aj9FFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIH7AAQAjgBAZAZQAZAXABAiIAAHlQgBAhgZAYQgZAZgjAAgAkwkjQgaAYABAhIAAHNQgBAhAaAZQAZAXAkABIHnAAQAkgBAZgXQAZgYABgiIAAnNQgBghgZgYQgZgYgkAAInnAAQgkAAgZAYgAjzEuQghgBgXgWQgXgWAAgeIAAnNQAAgeAXgVQAXgXAhABIHnAAQAhAAAXAWQAXAVABAeIAAHNQgBAfgXAVQgXAWghABg");
	this.shape.setTransform(0.1,-1.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],0,-31.7,0,31.8).s().p("AjzE5QglgBgZgYQgagYABghIAAnNQgBgiAagXQAZgYAlAAIHnAAQAjgBAaAZQAZAXABAiIAAHNQgBAhgZAYQgaAYgjABgAkskaQgXAVABAfIAAHNQgBAeAXAWQAXAWAiAAIHnAAQAgAAAYgWQAXgWAAgeIAAnNQAAgfgXgVQgYgWggAAInnAAIgCAAQggAAgXAWg");
	this.shape_1.setTransform(0.1,-1.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.2)","rgba(0,0,0,0.212)","rgba(0,0,0,0.4)"],[0,0.318,0.522,1],0,-32.7,0,32.7).s().p("Aj9FFQgkAAgZgZQgZgYAAghIAAnlQAAgiAZgXQAZgZAkABIH7AAQAjgBAZAZQAZAXABAiIAAHlQgBAhgZAYQgZAZgjAAg");
	this.shape_2.setTransform(0.1,-1.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.843)","rgba(0,0,0,0.522)","rgba(0,0,0,0.612)","rgba(0,0,0,0.792)"],[0,0.29,0.486,1],0,-35,0,35).s().p("AkPFeQgnAAgbgbQgbgZAAglIAAoJQAAglAbgZQAbgaAngBIIfAAQAnABAbAaQAaAZABAlIAAIJQgBAlgaAZQgbAbgnAAgAk5k6QgZAXAAAiIAAHkQAAAiAZAYQAZAYAkAAIH6AAQAkAAAZgYQAYgYACgiIAAnkQgCgigYgXQgZgZgkAAIn6AAIgBAAQgkAAgYAZg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-35,73,70);


(lib.shape50 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AkPFeQgnAAgbgbQgbgZAAglIAAoJQAAglAbgZQAbgaAngBIIfAAQAnABAbAaQAaAZABAlIAAIJQgBAlgaAZQgbAbgnAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-35,73,70);


(lib.shape49 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(0.5,1,2).p("AFtkFIAAILQgBAkgaAaQgbAagmAAIogAAQgnAAgbgaQgbgaAAgkIAAoLQAAgkAbgZQAbgbAnAAIIgAAQAmAAAbAbQAaAZABAkg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(255,255,255,0.502)","rgba(255,255,255,0.302)"],[0,1],0,-35.9,0,35.9).s().p("AkPFxQgvAAgggfQgiggAAgtIAAoJQAAgtAiggQAggfAvAAIIfAAQAvAAAgAfQAhAgABAtIAAIJQgBAtghAgQggAfgvAAgAlRlCQgbAZAAAlIAAIJQAAAlAbAZQAbAbAnAAIIfAAQAnAAAbgbQAagZABglIAAoJQgBglgagZQgbgagngBIofAAQgnABgbAag");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#5E5E5E","#222222"],[0,1],0,-40.3,0,40.4).s().p("AkhGTQgugBgggfQgfggAAguIAApJQAAguAfggQAgggAuAAIJDAAQAuAAAgAgQAfAgAAAuIAAJJQAAAugfAgQggAfguABg");
	this.shape_2.setTransform(0,2);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40,-38.4,80,80.7);


(lib.shape47 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0)"],[0.235,1],8.1,9.7,0,8.1,9.7,51.8).s().p("AjzCNQgiAAgXgWQgXgWABgeIAAiGQgBgeAXgVQAXgXAiABIHnAAQAgAAAYAWQAXAVAAAeIAACGQAAAegXAWQgYAWggAAg");
	this.shape.setTransform(0.1,-3.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],0,-15.1,0,14.6).s().p("AjzCVQglgBgZgYQgagYABghIAAiGQgBghAagYQAZgYAlAAIHnAAQAjAAAaAYQAZAYABAhIAACGQgBAhgZAYQgaAYgjABgAksh2QgXAVABAeIAACGQgBAeAXAWQAXAWAiAAIHnAAQAgAAAYgWQAXgWAAgeIAAiGQAAgegXgVQgYgWggAAInnAAIgCAAQggAAgXAWg");
	this.shape_1.setTransform(0.1,-3.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],8.2,9.3,0,8.2,9.3,51.8).s().p("Aj9ChQgkAAgZgZQgZgYAAghIAAidQAAgiAZgYQAZgYAkAAIH7AAQAjAAAZAYQAZAYABAiIAACdQgBAhgZAYQgZAZgjAAgAkwh/QgaAXABAiIAACFQgBAhAaAZQAZAXAkABIHnAAQAkgBAZgXQAZgYABgiIAAiFQgBgigZgXQgZgZgkABInnAAQgkAAgZAYg");
	this.shape_2.setTransform(0.1,-3.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.753)","rgba(0,0,0,0.4)","rgba(0,0,0,0.424)","rgba(0,0,0,0.8)"],[0,0.318,0.522,1],0,-16.3,0,15.9).s().p("Aj9CeQgkAAgZgYQgZgYAAgiIAAiYQAAgiAZgXQAZgZAkABIH7AAQAjgBAZAZQAZAXABAiIAACYQgBAigZAYQgZAYgjAAg");
	this.shape_3.setTransform(0.1,-3.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#000000","rgba(0,0,0,0.8)","rgba(0,0,0,0.812)","#000000"],[0,0.29,0.486,1],-0.2,-18.5,-0.2,18.1).s().p("AkPC3QgnAAgbgaQgbgaAAgkIAAi9QAAgkAbgaQAbgaAnAAIIfAAQAnAAAbAaQAaAZABAlIAAC9QgBAlgaAZQgbAagnAAgAk5iUQgZAXAAAiIAACYQAAAiAZAYQAZAYAkAAIH6AAQAkAAAZgYQAYgYACgiIAAiYQgCgigYgXQgZgZgkABIn6AAIgBAAQgkAAgYAYg");
	this.shape_4.setTransform(0,-1.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-20.3,73,36.8);


(lib.shape46 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0)"],[0.235,1],8.5,10.4,0,8.5,10.4,54.3).s().p("Aj/CYQgjAAgYgYQgYgYAAggIAAiQQAAggAYgXQAYgYAjAAIH/AAQAiAAAYAYQAYAXABAgIAACQQgBAhgYAXQgYAYgiAAg");
	this.shape.setTransform(0.1,-2.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],0,-16.2,0,15.7).s().p("Aj/CgQgmgBgagZQgcgbABgjIAAiQQgBgkAcgZQAagaAmAAIH/AAQAlAAAbAaQAaAZABAkIAACQQgBAkgaAaQgbAZglABgAk6h/QgYAXAAAgIAACQQAAAgAYAYQAYAYAjAAIH/AAQAiAAAYgYQAYgYABggIAAiQQgBgggYgXQgYgYgiAAIn/AAIgCAAQgiAAgXAYg");
	this.shape_1.setTransform(0.1,-2.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],8.6,10,0,8.6,10,54.3).s().p("AkJCtQgmAAgagbQgagZAAgkIAAipQAAglAagZQAagaAmAAIITAAQAlAAAaAaQAaAZABAlIAACpQgBAkgaAZQgaAbglAAgAk/iJQgbAZABAkIAACQQgBAjAbAbQAaAZAmABIH/AAQAlgBAbgZQAagaABgkIAAiQQgBgkgagZQgbgaglAAIn/AAQgmAAgaAag");
	this.shape_2.setTransform(0,-2.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.2)","rgba(0,0,0,0.051)","rgba(0,0,0,0.063)","rgba(0,0,0,0.153)"],[0,0.318,0.522,1],0,-17.7,0,17.3).s().p("AkLCsQgmAAgagbQgbgZAAglIAAimQAAgkAbgaQAagbAmABIIXAAQAlAAAbAaQAaAaABAkIAACmQgBAlgaAZQgbAbglAAg");
	this.shape_3.setTransform(0.2,-2.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(0,0,0,0.4)","rgba(0,0,0,0.2)","rgba(0,0,0,0.212)","rgba(0,0,0,0.4)"],[0,0.29,0.486,1],-0.2,-18.5,-0.2,18.1).s().p("AkPC3QgnAAgbgaQgbgaAAgkIAAi9QAAgkAbgaQAbgaAnAAIIfAAQAnAAAbAaQAaAZABAlIAAC9QgBAlgaAZQgbAagnAAgAlKiXQgaAaAAAkIAACmQAAAlAaAZQAbAbAmAAIIXAAQAlAAAbgbQAagZAAglIAAimQAAgkgagaQgbgaglAAIoXAAIgBAAQglAAgbAag");
	this.shape_4.setTransform(0,-1.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-20.3,73,36.8);


(lib.shape45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0)"],[0.235,1],8.1,9.7,0,8.1,9.7,51.8).s().p("AjzCNQgiAAgXgWQgXgWABgeIAAiGQgBgeAXgVQAXgXAiABIHnAAQAgAAAYAWQAXAVAAAeIAACGQAAAegXAWQgYAWggAAg");
	this.shape.setTransform(0.1,-3.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],0,-15.1,0,14.6).s().p("AjzCVQglgBgZgYQgagYABghIAAiGQgBghAagYQAZgYAlAAIHnAAQAjAAAaAYQAZAYABAhIAACGQgBAhgZAYQgaAYgjABgAksh2QgXAVABAeIAACGQgBAeAXAWQAXAWAiAAIHnAAQAgAAAYgWQAXgWAAgeIAAiGQAAgegXgVQgYgWggAAInnAAIgCAAQggAAgXAWg");
	this.shape_1.setTransform(0.1,-3.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],8.2,9.3,0,8.2,9.3,51.8).s().p("Aj9ChQgkAAgZgZQgZgYAAghIAAidQAAgiAZgYQAZgYAkAAIH7AAQAjAAAZAYQAZAYABAiIAACdQgBAhgZAYQgZAZgjAAgAkwh/QgaAXABAiIAACFQgBAhAaAZQAZAXAkABIHnAAQAkgBAZgXQAZgYABgiIAAiFQgBgigZgXQgZgZgkABInnAAQgkAAgZAYg");
	this.shape_2.setTransform(0.1,-3.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.2)","rgba(0,0,0,0.051)","rgba(0,0,0,0.063)","rgba(0,0,0,0.153)"],[0,0.318,0.522,1],0,-16.3,0,15.9).s().p("Aj9CeQgkAAgZgYQgZgYAAgiIAAiYQAAgiAZgXQAZgZAkABIH7AAQAjgBAZAZQAZAXABAiIAACYQgBAigZAYQgZAYgjAAg");
	this.shape_3.setTransform(0.1,-3.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(0,0,0,0.4)","rgba(0,0,0,0.2)","rgba(0,0,0,0.212)","rgba(0,0,0,0.4)"],[0,0.29,0.486,1],-0.2,-18.5,-0.2,18.1).s().p("AkPC3QgnAAgbgaQgbgaAAgkIAAi9QAAgkAbgaQAbgaAnAAIIfAAQAnAAAbAaQAaAZABAlIAAC9QgBAlgaAZQgbAagnAAgAk5iUQgZAXAAAiIAACYQAAAiAZAYQAZAYAkAAIH6AAQAkAAAZgYQAYgYACgiIAAiYQgCgigYgXQgZgZgkABIn6AAIgBAAQgkAAgYAYg");
	this.shape_4.setTransform(0,-1.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-20.3,73,36.8);


(lib.shape44 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.753)"],[0,1],0,-15.1,0,14.6).s().p("AjzCVQglgBgZgYQgagYABghIAAiGQgBghAagYQAZgYAlAAIHnAAQAjAAAaAYQAZAYABAhIAACGQgBAhgZAYQgaAYgjABgAksh2QgXAVABAeIAACGQgBAeAXAWQAXAWAiAAIHnAAQAgAAAYgWQAXgWAAgeIAAiGQAAgegXgVQgYgWggAAInnAAIgCAAQggAAgXAWg");
	this.shape.setTransform(0.1,-3.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)"],[0.235,1],8.2,9.3,0,8.2,9.3,51.8).s().p("Aj9ChQgkAAgZgZQgZgYAAghIAAidQAAgiAZgYQAZgYAkAAIH7AAQAjAAAZAYQAZAYABAiIAACdQgBAhgZAYQgZAZgjAAgAkwh/QgaAXABAiIAACFQgBAhAaAZQAZAXAkABIHnAAQAkgBAZgXQAZgYABgiIAAiFQgBgigZgXQgZgZgkABInnAAQgkAAgZAYgAjzCKQghgBgXgWQgXgWAAgeIAAiFQAAgfAXgVQAXgWAhAAIHnAAQAhAAAXAWQAXAVABAfIAACFQgBAfgXAVQgXAWghABg");
	this.shape_1.setTransform(0.1,-3.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["rgba(0,0,0,0.843)","rgba(0,0,0,0.522)","rgba(0,0,0,0.612)","rgba(0,0,0,0.792)"],[0,0.29,0.486,1],-0.2,-18.5,-0.2,18.1).s().p("AkPC3QgnAAgbgaQgbgaAAgkIAAi9QAAgkAbgaQAbgaAnAAIIfAAQAnAAAbAaQAaAZABAlIAAC9QgBAlgaAZQgbAagnAAgAk5iUQgZAXAAAiIAACYQAAAiAZAYQAZAYAkAAIH6AAQAkAAAZgYQAYgYACgiIAAiYQgCgigYgXQgZgZgkABIn6AAIgBAAQgkAAgYAYg");
	this.shape_2.setTransform(0,-1.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(0,0,0,0.502)","rgba(0,0,0,0.2)","rgba(0,0,0,0.212)","rgba(0,0,0,0.4)"],[0,0.318,0.522,1],0,-16.3,0,15.9).s().p("Aj9CeQgkAAgZgYQgZgYAAgiIAAiYQAAgiAZgXQAZgZAkABIH7AAQAjgBAZAZQAZAXABAiIAACYQgBAigZAYQgZAYgjAAg");
	this.shape_3.setTransform(0.1,-3.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-20.3,73,36.8);


(lib.shape41 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AkPC0QgnAAgbgaQgbgaAAgkIAAi3QAAgkAbgZQAbgbAnAAIIfAAQAnAAAbAbQAaAZABAkIAAC3QgBAkgaAaQgbAagnAAg");
	this.shape.setTransform(0,-1.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.5,-19.9,73,36.1);


(lib.shape39 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","rgba(255,255,255,0)"],[0,0.976],0,0,0,0,0,14.7).s().p("AhpBqQgsgsAAg+QAAg9AsgrQAsgtA9AAQA+AAArAtQAtArAAA9QAAA+gtAsQgrAsg+AAQg9AAgsgsg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15,-15,30,30);


(lib.shape38 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(0.5,1,2).p("AFtheIAAC9QgBAlgaAZQgbAagmAAIogAAQgnAAgbgaQgbgaAAgkIAAi9QAAgkAbgaQAbgaAnAAIIgAAQAmAAAbAaQAaAZABAlg");
	this.shape.setTransform(0,-1.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(255,255,255,0.502)","rgba(255,255,255,0.302)"],[0,1],0.4,-19.4,0.4,17.4).s().p("AkPDLQgvAAgggfQgiggAAgtIAAi9QAAgtAigfQAgggAvAAIIfAAQAvAAAgAgQAhAfABAtIAAC9QgBAtghAgQggAfgvAAgAlRicQgbAaAAAkIAAC9QAAAkAbAaQAbAaAnAAIIfAAQAnAAAbgaQAagZABglIAAi9QgBglgagZQgbgagnAAIofAAQgnAAgbAag");
	this.shape_1.setTransform(0,-1.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#5E5E5E","#222222"],[0,1],0,-22.2,0,20.2).s().p("AkhDmQguAAgggjQgfgkAAgxIAAjbQAAgyAfgjQAggjAuAAIJDAAQAuAAAgAjQAfAjAAAyIAADbQAAAxgfAkQggAjguAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40,-23,80,46.1);


(lib.sprite81 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 1
	this.instance = new lib.text79("synched",0);
	this.instance.setTransform(2,2);

	this.instance_1 = new lib.text80("synched",0);
	this.instance_1.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(4,-2,45.9,33.3);


(lib.sprite77 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 1
	this.instance = new lib.text75("synched",0);
	this.instance.setTransform(2,2);

	this.instance_1 = new lib.text76("synched",0);
	this.instance_1.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(5,-2,43.8,33.3);


(lib.sprite40 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape39("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15,-15,30,30);


(lib.sprite54b10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 24
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 31
	this.instance = new lib.sprite40();
	this.instance.setTransform(-33.3,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 29
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-32.2,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 27
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(31.2,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 25
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,1.884,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 23
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-31.6,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 22
	this.instance_5 = new lib.shape52("synched",0);

	this.instance_6 = new lib.shape53("synched",0);

	this.instance_7 = new lib.shape54("synched",0);

	this.instance_8 = new lib.shape55("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 20
	this.instance_9 = new lib.text88("synched",0);
	this.instance_9.setTransform(-50.4,8.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 19
	this.instance_10 = new lib.text87("synched",0);
	this.instance_10.setTransform(-50.2,-30.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 18
	this.instance_11 = new lib.text86("synched",0);
	this.instance_11.setTransform(-48.5,-10.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 17
	this.instance_12 = new lib.shape85("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 15
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(0.5,36.1,0.034,2.031,89.8);
	this.instance_13.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 13
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(37.2,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 11
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(-37.7,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 9
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(0.1,35.5,0.164,2.329,89.8);
	this.instance_16.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_16.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 7
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(0.1,-36.3,0.034,2.031,89.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 5
	this.instance_18 = new lib.sprite40();
	this.instance_18.setTransform(35.6,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

	// Layer 3
	this.instance_19 = new lib.sprite40();
	this.instance_19.setTransform(-35,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(5));

	// Layer 2
	this.instance_20 = new lib.shape49("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-41.1,-39.8,91.8,87.4);


(lib.sprite49b11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* #initclip 13
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 29
	this.instance = new lib.sprite40();
	this.instance.setTransform(-33.3,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 27
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-32.2,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 25
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(31.2,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 23
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,1.884,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 21
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-31.6,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 20
	this.instance_5 = new lib.shape52("synched",0);

	this.instance_6 = new lib.shape53("synched",0);

	this.instance_7 = new lib.shape54("synched",0);

	this.instance_8 = new lib.shape55("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 18
	this.instance_9 = new lib.text83("synched",0);
	this.instance_9.setTransform(-51.5,-9);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 17
	this.instance_10 = new lib.shape50("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.5,36.1,0.034,2.031,89.8);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(37.2,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-37.7,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.1,35.5,0.164,2.329,89.8);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.1,-36.3,0.034,2.031,89.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(35.6,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-35,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape49("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-41.1,-39.8,82.8,82.2);


(lib.sprite47b7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 23
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 30
	this.instance = new lib.sprite40();
	this.instance.setTransform(-17.1,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 28
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-16,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 26
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(14.4,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 24
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,0.835,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 22
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-15.4,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 21
	this.instance_5 = new lib.shape60("synched",0);

	this.instance_6 = new lib.shape61("synched",0);

	this.instance_7 = new lib.shape62("synched",0);

	this.instance_8 = new lib.shape63("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 18
	this.instance_9 = new lib.sprite81();
	this.instance_9.setTransform(-22,-12.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 17
	this.instance_10 = new lib.shape68("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.7,36.1,0.034,0.884,0,89.5,89.9);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(21,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-20.3,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.6,35.5,0.164,1.013,0,89.5,89.9);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.6,-36.3,0.034,0.884,0,89.5,89.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(19.4,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-17.3,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape57("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-39.8,52.3,82.2);


(lib.sprite45b6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 22
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 30
	this.instance = new lib.sprite40();
	this.instance.setTransform(-17.1,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 28
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-16,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 26
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(14.4,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 24
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,0.835,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 22
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-15.4,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 21
	this.instance_5 = new lib.shape60("synched",0);

	this.instance_6 = new lib.shape61("synched",0);

	this.instance_7 = new lib.shape62("synched",0);

	this.instance_8 = new lib.shape63("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 18
	this.instance_9 = new lib.sprite77();
	this.instance_9.setTransform(-22.9,-12.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 17
	this.instance_10 = new lib.shape68("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.7,36.1,0.034,0.884,0,89.5,89.9);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(21,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-20.3,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.6,35.5,0.164,1.013,0,89.5,89.9);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.6,-36.3,0.034,0.884,0,89.5,89.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(19.4,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-17.3,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape57("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-39.8,50.3,82.2);


(lib.sprite43b5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 21
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 29
	this.instance = new lib.sprite40();
	this.instance.setTransform(-17.1,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 27
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-16,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 25
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(14.4,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 23
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,0.835,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 21
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-15.4,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 20
	this.instance_5 = new lib.shape60("synched",0);

	this.instance_6 = new lib.shape61("synched",0);

	this.instance_7 = new lib.shape62("synched",0);

	this.instance_8 = new lib.shape63("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 18
	this.instance_9 = new lib.text73("synched",0);
	this.instance_9.setTransform(-20.1,-10.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 17
	this.instance_10 = new lib.shape68("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.7,36.1,0.034,0.884,0,89.5,89.9);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(21,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-20.3,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.6,35.5,0.164,1.013,0,89.5,89.9);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.6,-36.3,0.034,0.884,0,89.5,89.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(19.4,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-17.3,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape57("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-39.8,52.1,82.2);


(lib.sprite41b4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 20
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 29
	this.instance = new lib.sprite40();
	this.instance.setTransform(-17.1,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 27
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-16,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 25
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(14.4,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 23
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,0.835,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 21
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-15.4,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 20
	this.instance_5 = new lib.shape60("synched",0);

	this.instance_6 = new lib.shape61("synched",0);

	this.instance_7 = new lib.shape62("synched",0);

	this.instance_8 = new lib.shape63("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 18
	this.instance_9 = new lib.text71("synched",0);
	this.instance_9.setTransform(-20.1,-10.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 17
	this.instance_10 = new lib.shape68("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.7,36.1,0.034,0.884,0,89.5,89.9);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(21,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-20.3,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.6,35.5,0.164,1.013,0,89.5,89.9);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.6,-36.3,0.034,0.884,0,89.5,89.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(19.4,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-17.3,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape57("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-39.8,51.9,82.2);


(lib.sprite39b3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 19
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 29
	this.instance = new lib.sprite40();
	this.instance.setTransform(-17.1,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 27
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-16,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 25
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(14.4,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 23
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,0.835,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 21
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-15.4,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 20
	this.instance_5 = new lib.shape60("synched",0);

	this.instance_6 = new lib.shape61("synched",0);

	this.instance_7 = new lib.shape62("synched",0);

	this.instance_8 = new lib.shape63("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 18
	this.instance_9 = new lib.text69("synched",0);
	this.instance_9.setTransform(-21,-10.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 17
	this.instance_10 = new lib.shape68("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.7,36.1,0.034,0.884,0,89.5,89.9);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(21,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-20.3,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.6,35.5,0.164,1.013,0,89.5,89.9);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.6,-36.3,0.034,0.884,0,89.5,89.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(19.4,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-17.3,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape57("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-39.8,50,82.2);


(lib.sprite36b9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 18
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 30
	this.instance = new lib.sprite40();
	this.instance.setTransform(-17.1,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 28
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-16,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 26
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(14.4,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 24
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,0.835,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 22
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-15.4,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 21
	this.instance_5 = new lib.shape60("synched",0);

	this.instance_6 = new lib.shape61("synched",0);

	this.instance_7 = new lib.shape62("synched",0);

	this.instance_8 = new lib.shape63("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 19
	this.instance_9 = new lib.text66("synched",0);
	this.instance_9.setTransform(-19.1,-21.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 18
	this.instance_10 = new lib.shape65("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.7,36.1,0.034,0.884,0,89.5,89.9);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(21,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-20.3,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.6,35.5,0.164,1.013,0,89.5,89.9);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.6,-36.3,0.034,0.884,0,89.5,89.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(19.4,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-17.3,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape57("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-39.8,51.5,82.2);


(lib.sprite33b8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 17
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 30
	this.instance = new lib.sprite40();
	this.instance.setTransform(-17.1,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 28
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-16,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 26
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(14.4,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 24
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,0.835,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 22
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-15.4,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 21
	this.instance_5 = new lib.shape60("synched",0);

	this.instance_6 = new lib.shape61("synched",0);

	this.instance_7 = new lib.shape62("synched",0);

	this.instance_8 = new lib.shape63("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 19
	this.instance_9 = new lib.text59("synched",0);
	this.instance_9.setTransform(-19.9,-21.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 18
	this.instance_10 = new lib.shape58("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.7,36.1,0.034,0.884,0,89.5,89.9);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(21,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-20.3,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.6,35.5,0.164,1.013,0,89.5,89.9);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.6,-36.3,0.034,0.884,0,89.5,89.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(19.4,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-17.3,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape57("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-39.8,49.9,82.2);


(lib.sprite25b1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 16
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 29
	this.instance = new lib.sprite40();
	this.instance.setTransform(-33.3,-2.6,0.034,1.646,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 27
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-32.2,30,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 25
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(31.2,-31,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 23
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-33.6,0.034,1.884,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 21
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-31.6,-30.6,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 20
	this.instance_5 = new lib.shape52("synched",0);

	this.instance_6 = new lib.shape53("synched",0);

	this.instance_7 = new lib.shape54("synched",0);

	this.instance_8 = new lib.shape55("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 18
	this.instance_9 = new lib.text51("synched",0);
	this.instance_9.setTransform(-49.8,-16.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 17
	this.instance_10 = new lib.shape50("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.5,36.1,0.034,2.031,89.8);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(37.2,-0.2,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-37.7,3.1,0.034,2.031,179.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.1,35.5,0.164,2.329,89.8);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.1,-36.3,0.034,2.031,89.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(35.6,-33.7,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-35,-33.7,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape49("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-41.1,-39.8,89.6,82.2);


(lib.sprite17b2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* 
		#initclip 25
		
		#endinitclip
		*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 29
	this.instance = new lib.sprite40();
	this.instance.setTransform(-33.3,-3.8,0.034,0.73,179.8);
	this.instance.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 27
	this.instance_1 = new lib.sprite40();
	this.instance_1.setTransform(-31.6,11.3,0.315,0.465,0,128.2,127.9);
	this.instance_1.alpha = 0.602;
	this.instance_1.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_1.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 25
	this.instance_2 = new lib.sprite40();
	this.instance_2.setTransform(31.2,-16.2,0.176,0.365,127.9);
	this.instance_2.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 23
	this.instance_3 = new lib.sprite40();
	this.instance_3.setTransform(0.1,-18.8,0.034,1.884,89.8);
	this.instance_3.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 21
	this.instance_4 = new lib.sprite40();
	this.instance_4.setTransform(-31.6,-15.8,0.176,0.366,49.7);
	this.instance_4.alpha = 0.59;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5));

	// Layer 20
	this.instance_5 = new lib.shape44("synched",0);

	this.instance_6 = new lib.shape45("synched",0);

	this.instance_7 = new lib.shape46("synched",0);

	this.instance_8 = new lib.shape47("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 18
	this.instance_9 = new lib.text43("synched",0);
	this.instance_9.setTransform(-52.2,-19.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5));

	// Layer 17
	this.instance_10 = new lib.shape41("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5));

	// Layer 15
	this.instance_11 = new lib.sprite40();
	this.instance_11.setTransform(0.5,17.5,0.034,2.031,89.8);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

	// Layer 13
	this.instance_12 = new lib.sprite40();
	this.instance_12.setTransform(37.2,-1,0.034,1.003,0,179.5,179.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5));

	// Layer 11
	this.instance_13 = new lib.sprite40();
	this.instance_13.setTransform(-37.7,-1,0.034,1.003,0,179.5,179.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(5));

	// Layer 9
	this.instance_14 = new lib.sprite40();
	this.instance_14.setTransform(0.1,20.8,0.164,2.329,89.8);
	this.instance_14.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -255, -255, 0)];
	this.instance_14.cache(-17,-17,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5));

	// Layer 7
	this.instance_15 = new lib.sprite40();
	this.instance_15.setTransform(0.1,-21.6,0.034,2.031,89.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5));

	// Layer 5
	this.instance_16 = new lib.sprite40();
	this.instance_16.setTransform(35.6,-17.2,0.168,0.405,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(5));

	// Layer 3
	this.instance_17 = new lib.sprite40();
	this.instance_17.setTransform(-35,-17.3,0.168,0.405,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(5));

	// Layer 2
	this.instance_18 = new lib.shape38("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-41.1,-23.6,82.8,48);


// stage content:
(lib.graphics = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 317
	this.b3 = new lib.sprite17b2();
	this.b3.setTransform(80.9,531,0.836,0.651,0,15,0);
	this.b3.cache(-43,-26,87,52);

	this.timeline.addTween(cjs.Tween.get(this.b3).wait(1));

	// Layer 287
	this.b2 = new lib.sprite25b1();
	this.b2.setTransform(655.2,541.2,0.836,0.643,0,-12.1,0);
	this.b2.cache(-43,-42,94,86);

	this.timeline.addTween(cjs.Tween.get(this.b2).wait(1));

	// Layer 256
	this.b9 = new lib.sprite33b8();
	this.b9.setTransform(472.5,541.2,0.836,0.63,0,-3.5,0);
	this.b9.cache(-26,-42,54,86);

	this.timeline.addTween(cjs.Tween.get(this.b9).wait(1));

	// Layer 225
	this.b10 = new lib.sprite36b9();
	this.b10.setTransform(517.3,541.2,0.836,0.631,0,-5.3,0);
	this.b10.cache(-26,-42,56,86);

	this.timeline.addTween(cjs.Tween.get(this.b10).wait(1));

	// Layer 195
	this.b4 = new lib.sprite39b3();
	this.b4.setTransform(231.9,541.2,0.836,0.634,0,7.5,0);
	this.b4.cache(-26,-42,54,86);

	this.timeline.addTween(cjs.Tween.get(this.b4).wait(1));

	// Layer 165
	this.b5 = new lib.sprite41b4();
	this.b5.setTransform(276.3,541.2,0.836,0.632,0,6.3,0);
	this.b5.cache(-26,-42,56,86);

	this.timeline.addTween(cjs.Tween.get(this.b5).wait(1));

	// Layer 135
	this.b6 = new lib.sprite43b5();
	this.b6.setTransform(320.2,541.2,0.836,0.631,0,5,0);
	this.b6.cache(-26,-42,56,86);

	this.timeline.addTween(cjs.Tween.get(this.b6).wait(1));

	// Layer 104
	this.b7 = new lib.sprite45b6();
	this.b7.setTransform(363.5,541.2,0.836,0.629,0,2.5,0);
	this.b7.cache(-26,-42,54,86);

	this.timeline.addTween(cjs.Tween.get(this.b7).wait(1));

	// Layer 73
	this.b8 = new lib.sprite47b7();
	this.b8.setTransform(406.5,541.2,0.836,0.629,0,0.4,0);
	this.b8.cache(-26,-42,56,86);

	this.timeline.addTween(cjs.Tween.get(this.b8).wait(1));

	// Layer 43
	this.b1 = new lib.sprite49b11();
	this.b1.setTransform(154.6,541.2,0.836,0.638,0,10,0);
	this.b1.cache(-43,-42,87,86);

	this.timeline.addTween(cjs.Tween.get(this.b1).wait(1));

	// Layer 11
	this.b11 = new lib.sprite54b10();
	this.b11.setTransform(578.5,541.2,0.836,0.636,0,-8.5,0);
	this.b11.cache(-43,-42,96,91);

	this.timeline.addTween(cjs.Tween.get(this.b11).wait(1));

	// Layer 10
	this.b0 = new lib.sprite24();
	this.b0.setTransform(914.6,172);

	this.instance = new lib.shape202("synched",0);
	this.instance.setTransform(412.8,340.2,0.843,0.966,0,0,0,-0.4,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.b0}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(457.6,374,726.1,746);

})(but_lib = but_lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var but_lib, images, createjs, ss;