(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 800,
	height: 600,
	fps: 25,
	color: "#000000",
	manifest: [
		{src:"images/image11.jpg?1470327182352", id:"image11"},
		{src:"images/image13.jpg?1470327182352", id:"image13"},
		{src:"images/image15.jpg?1470327182352", id:"image15"},
		{src:"images/image17.jpg?1470327182352", id:"image17"},
		{src:"images/image19.jpg?1470327182352", id:"image19"},
		{src:"images/image21.jpg?1470327182352", id:"image21"},
		{src:"images/image23.jpg?1470327182352", id:"image23"},
		{src:"images/image25.jpg?1470327182352", id:"image25"},
		{src:"images/image27.jpg?1470327182352", id:"image27"},
		{src:"images/image280.jpg?1470327182352", id:"image280"},
		{src:"images/image29.jpg?1470327182352", id:"image29"},
		{src:"images/image31.jpg?1470327182352", id:"image31"},
		{src:"images/image311.png?1470327182352", id:"image311"},
		{src:"images/image329.png?1470327182352", id:"image329"},
		{src:"images/image33.jpg?1470327182352", id:"image33"},
		{src:"images/image334.png?1470327182352", id:"image334"},
		{src:"images/image35.jpg?1470327182352", id:"image35"},
		{src:"images/image37.jpg?1470327182352", id:"image37"},
		{src:"images/image39.jpg?1470327182352", id:"image39"},
		{src:"images/image4.png?1470327182352", id:"image4"},
		{src:"images/image41.jpg?1470327182352", id:"image41"},
		{src:"images/image43.jpg?1470327182352", id:"image43"},
		{src:"images/image45.jpg?1470327182352", id:"image45"},
		{src:"images/image47.jpg?1470327182352", id:"image47"},
		{src:"images/image49.jpg?1470327182352", id:"image49"},
		{src:"images/image51.jpg?1470327182352", id:"image51"},
		{src:"images/image53.jpg?1470327182352", id:"image53"},
		{src:"images/image55.jpg?1470327182352", id:"image55"},
		{src:"images/image57.jpg?1470327182352", id:"image57"},
		{src:"images/image59.jpg?1470327182352", id:"image59"},
		{src:"images/image6.png?1470327182352", id:"image6"},
		{src:"images/image62.jpg?1470327182352", id:"image62"},
		{src:"images/image64.jpg?1470327182352", id:"image64"},
		{src:"images/image66.jpg?1470327182352", id:"image66"},
		{src:"images/image68.jpg?1470327182352", id:"image68"},
		{src:"images/image70.jpg?1470327182352", id:"image70"},
		{src:"images/image9.jpg?1470327182352", id:"image9"},
		{src:"sounds/assetssfxWin_4x9.mp3?1470327182352", id:"assetssfxWin_4x9"},
		{src:"sounds/assetssfxWin_5x9.mp3?1470327182352", id:"assetssfxWin_5x9"},
		{src:"sounds/assetssfxGambleWin_2.mp3?1470327182352", id:"assetssfxGambleWin_2"},
		{src:"sounds/assetssfxGambleWin_1.mp3?1470327182352", id:"assetssfxGambleWin_1"},
		{src:"sounds/assetssfxGambleWin_4.mp3?1470327182352", id:"assetssfxGambleWin_4"},
		{src:"sounds/assetssfxWin_2x567_3x012.mp3?1470327182352", id:"assetssfxWin_2x567_3x012"},
		{src:"sounds/assetssfxGambleWin_3.mp3?1470327182352", id:"assetssfxGambleWin_3"},
		{src:"sounds/assetssfxGambleWin_5.mp3?1470327182352", id:"assetssfxGambleWin_5"},
		{src:"sounds/assetssfxWin_3x9.mp3?1470327182352", id:"assetssfxWin_3x9"},
		{src:"sounds/assetssfxBookSymbolChange.mp3?1470327182352", id:"assetssfxBookSymbolChange"},
		{src:"sounds/assetssfxAutoStop.mp3?1470327182352", id:"assetssfxAutoStop"},
		{src:"sounds/assetssfxWin_2x89_3x34.mp3?1470327182352", id:"assetssfxWin_2x89_3x34"},
		{src:"sounds/assetssfxShowGamble.mp3?1470327182352", id:"assetssfxShowGamble"},
		{src:"sounds/assetssfxFreeStart.mp3?1470327182352", id:"assetssfxFreeStart"},
		{src:"sounds/assetssfxBookRising.mp3?1470327182352", id:"assetssfxBookRising"},
		{src:"sounds/assetssfxFree.mp3?1470327182352", id:"assetssfxFree"},
		{src:"sounds/assetssfxWin_4x34.mp3?1470327182352", id:"assetssfxWin_4x34"},
		{src:"sounds/assetssfxHelp.mp3?1470327182352", id:"assetssfxHelp"},
		{src:"sounds/assetssfxChangeCard.mp3?1470327182352", id:"assetssfxChangeCard"},
		{src:"sounds/assetssfxFreeLineWin.mp3?1470327182352", id:"assetssfxFreeLineWin"},
		{src:"sounds/assetssfxWin_5x012.mp3?1470327182352", id:"assetssfxWin_5x012"},
		{src:"sounds/assetssfxRing_10000.mp3?1470327182352", id:"assetssfxRing_10000"},
		{src:"sounds/assetssfxTakeFast.mp3?1470327182352", id:"assetssfxTakeFast"},
		{src:"sounds/assetssfxTakeSlow.mp3?1470327182352", id:"assetssfxTakeSlow"},
		{src:"sounds/assetssfxSpinStop.mp3?1470327182352", id:"assetssfxSpinStop"},
		{src:"sounds/assetssfxWin_345x8.mp3?1470327182352", id:"assetssfxWin_345x8"},
		{src:"sounds/assetssfxWin_345x7.mp3?1470327182352", id:"assetssfxWin_345x7"},
		{src:"sounds/assetssfxWin_4x012.mp3?1470327182352", id:"assetssfxWin_4x012"},
		{src:"sounds/assetssfxBonusTile_1.mp3?1470327182353", id:"assetssfxBonusTile_1"},
		{src:"sounds/assetssfxWait.mp3?1470327182353", id:"assetssfxWait"},
		{src:"sounds/assetssfxWin_5x34.mp3?1470327182353", id:"assetssfxWin_5x34"},
		{src:"sounds/assetssfxRing_1000.mp3?1470327182353", id:"assetssfxRing_1000"},
		{src:"sounds/assetssfxWin_345x5.mp3?1470327182353", id:"assetssfxWin_345x5"},
		{src:"sounds/assetssfxWin_345x6.mp3?1470327182353", id:"assetssfxWin_345x6"},
		{src:"sounds/assetssfxBetLine.mp3?1470327182353", id:"assetssfxBetLine"},
		{src:"sounds/assetssfxBookOpen.mp3?1470327182353", id:"assetssfxBookOpen"},
		{src:"sounds/assetssfxFreeEnd.mp3?1470327182353", id:"assetssfxFreeEnd"},
		{src:"sounds/assetssfxSpin.mp3?1470327182353", id:"assetssfxSpin"},
		{src:"sounds/assetssfxRing_100.mp3?1470327182353", id:"assetssfxRing_100"},
		{src:"sounds/assetssfxTransformSlot.mp3?1470327182353", id:"assetssfxTransformSlot"},
		{src:"sounds/assetssfxBonusTile_3.mp3?1470327182353", id:"assetssfxBonusTile_3"},
		{src:"sounds/assetssfxBonusTile_2.mp3?1470327182353", id:"assetssfxBonusTile_2"},
		{src:"sounds/assetssfxBonusTile_5.mp3?1470327182353", id:"assetssfxBonusTile_5"},
		{src:"sounds/assetssfxBonusTile_4.mp3?1470327182353", id:"assetssfxBonusTile_4"},
		{src:"sounds/assetssfxAutoStart.mp3?1470327182353", id:"assetssfxAutoStart"}
	]
};



// symbols:



(lib.image1001 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1003 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1005 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1007 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1009 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1011 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1013 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1015 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1017 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1019 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1021 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1023 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1025 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1027 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1029 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1031 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1033 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1035 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1037 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1039 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1041 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1043 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1045 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1047 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1049 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1051 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1053 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1055 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1057 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1059 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image106 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1061 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1063 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1065 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1067 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1069 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1071 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1073 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1075 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1077 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1079 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1081 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1083 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1085 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1087 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1089 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1091 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1093 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1095 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1097 = function() {
	this.spriteSheet = ss["graphics_atlas_9"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1099 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image11 = function() {
	this.initialize(img.image11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image1101 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1103 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1105 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1107 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1109 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1111 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1113 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1115 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1117 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1119 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1121 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1123 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1125 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1127 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1129 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1131 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1133 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1135 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1137 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1139 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image114 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1141 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1143 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1145 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1147 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1149 = function() {
	this.spriteSheet = ss["graphics_atlas_10"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1151 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1153 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1155 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1157 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1159 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1163 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1165 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1169 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1171 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1173 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1176 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1178 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1180 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1182 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1184 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1186 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1188 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1190 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1192 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1194 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1196 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1198 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1200 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1202 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1204 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1206 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1208 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1210 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1212 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1214 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1216 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1218 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1220 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1222 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1224 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1226 = function() {
	this.spriteSheet = ss["graphics_atlas_6"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1228 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1230 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1232 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1234 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1236 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1238 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1240 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1242 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1244 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1246 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1248 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1250 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1252 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1254 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1256 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1258 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image126 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1260 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1262 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1264 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1266 = function() {
	this.spriteSheet = ss["graphics_atlas_7"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1268 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1270 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1272 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1274 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1276 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1278 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1280 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1284 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1286 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1289 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1291 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1293 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1295 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1297 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1299 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image13 = function() {
	this.initialize(img.image13);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image1301 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1303 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1305 = function() {
	this.spriteSheet = ss["graphics_atlas_8"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1307 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1309 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1311 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1313 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1315 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1317 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1319 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1321 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1323 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1327 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1329 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1331 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1333 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1335 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1337 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1339 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1341 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1343 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1345 = function() {
	this.spriteSheet = ss["graphics_atlas_11"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1347 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1349 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1351 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1353 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1355 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1357 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1359 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1361 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1363 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1365 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1367 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1369 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1371 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1373 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1376 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1378 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image138 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1382 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1384 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1386 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1388 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1390 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1392 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1394 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1396 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1398 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1400 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1402 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1404 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1406 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1408 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1410 = function() {
	this.spriteSheet = ss["graphics_atlas_12"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1412 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1414 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1416 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1419 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1421 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1423 = function() {
	this.spriteSheet = ss["graphics_atlas_13"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1425 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1427 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1429 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1431 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1433 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1435 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1437 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1439 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1441 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1443 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1445 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1447 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1449 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1451 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1453 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1455 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1457 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1459 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1461 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1463 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1465 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1467 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1469 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1472 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1474 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1476 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1478 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1480 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1482 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1484 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1486 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1488 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1490 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1492 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1494 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1496 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1498 = function() {
	this.spriteSheet = ss["graphics_atlas_14"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image15 = function() {
	this.initialize(img.image15);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image150 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1500 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1502 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1504 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1506 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1508 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1510 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1512 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1514 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1516 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1518 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1520 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1522 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1524 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1526 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1528 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1530 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1532 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1534 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1536 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1538 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1540 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1543 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1545 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1547 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1549 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1551 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1553 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1555 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1557 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1559 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1561 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1563 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1565 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1567 = function() {
	this.spriteSheet = ss["graphics_atlas_16"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1569 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1571 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1573 = function() {
	this.spriteSheet = ss["graphics_atlas_15"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1575 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1577 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1581 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1583 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1585 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1587 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1589 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1591 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1593 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1595 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1597 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1599 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1601 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1603 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1605 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1607 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1609 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1611 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1613 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1615 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1618 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image162 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1621 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1623 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1625 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1627 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1629 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1631 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1634 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1636 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1638 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1640 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1642 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1644 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1646 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1648 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1650 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1652 = function() {
	this.spriteSheet = ss["graphics_atlas_17"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1654 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1656 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1658 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1660 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1662 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1664 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1666 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1668 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1671 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1673 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1675 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1677 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1679 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1681 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1683 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1685 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1687 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1689 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1693 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1695 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1698 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image17 = function() {
	this.initialize(img.image17);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image1700 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1702 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1704 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1706 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1708 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1710 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1712 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1714 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1716 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1718 = function() {
	this.spriteSheet = ss["graphics_atlas_18"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1720 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1722 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1724 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1726 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1728 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1738 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1740 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1743 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1749 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1755 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1757 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1763 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1765 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1767 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1769 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1771 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1773 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1775 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1777 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1779 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image178 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1781 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1783 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1785 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1787 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1789 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1791 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1793 = function() {
	this.spriteSheet = ss["graphics_atlas_20"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1795 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1797 = function() {
	this.spriteSheet = ss["graphics_atlas_19"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1799 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1803 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1807 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1810 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1813 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1815 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1817 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1819 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1821 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1823 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1825 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1827 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1829 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1831 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image1833 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image1835 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image1837 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image1839 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image1841 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image1843 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image1845 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image1847 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image1849 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image1851 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image1853 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image1855 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image1857 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image1859 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image1861 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image1863 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image1865 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image1867 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image1869 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image1871 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image1873 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image1875 = function() {
	this.spriteSheet = ss["graphics_atlas_21"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image1877 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image1881 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image1883 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image19 = function() {
	this.initialize(img.image19);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image194 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image198 = function() {
	this.spriteSheet = ss["graphics_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image206 = function() {
	this.spriteSheet = ss["graphics_atlas_3"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image208 = function() {
	this.spriteSheet = ss["graphics_atlas_2"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image21 = function() {
	this.initialize(img.image21);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image210 = function() {
	this.spriteSheet = ss["graphics_atlas_3"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image212 = function() {
	this.spriteSheet = ss["graphics_atlas_2"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image214 = function() {
	this.spriteSheet = ss["graphics_atlas_3"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image216 = function() {
	this.spriteSheet = ss["graphics_atlas_3"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image218 = function() {
	this.spriteSheet = ss["graphics_atlas_2"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image220 = function() {
	this.spriteSheet = ss["graphics_atlas_3"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image222 = function() {
	this.spriteSheet = ss["graphics_atlas_3"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image224 = function() {
	this.spriteSheet = ss["graphics_atlas_3"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image226 = function() {
	this.spriteSheet = ss["graphics_atlas_3"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image228 = function() {
	this.spriteSheet = ss["graphics_atlas_2"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image23 = function() {
	this.initialize(img.image23);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image230 = function() {
	this.spriteSheet = ss["graphics_atlas_4"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image232 = function() {
	this.spriteSheet = ss["graphics_atlas_4"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image234 = function() {
	this.spriteSheet = ss["graphics_atlas_4"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image236 = function() {
	this.spriteSheet = ss["graphics_atlas_4"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image238 = function() {
	this.spriteSheet = ss["graphics_atlas_4"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image240 = function() {
	this.spriteSheet = ss["graphics_atlas_4"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image242 = function() {
	this.spriteSheet = ss["graphics_atlas_4"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image244 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image246 = function() {
	this.spriteSheet = ss["graphics_atlas_4"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image248 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image25 = function() {
	this.initialize(img.image25);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image250 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image252 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image254 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image257 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image259 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image261 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image263 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image265 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image267 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image269 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image27 = function() {
	this.initialize(img.image27);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image271 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image273 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image280 = function() {
	this.initialize(img.image280);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,313);


(lib.image282 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image285 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image29 = function() {
	this.initialize(img.image29);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image290 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image292 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image294 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image296 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image298 = function() {
	this.spriteSheet = ss["graphics_atlas_5"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image301 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image303 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image305 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image307 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image31 = function() {
	this.initialize(img.image31);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image311 = function() {
	this.initialize(img.image311);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.image329 = function() {
	this.initialize(img.image329);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.image33 = function() {
	this.initialize(img.image33);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image334 = function() {
	this.initialize(img.image334);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.image337 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image338 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image339 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image340 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image341 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image342 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image343 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image345 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image346 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image347 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image349 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image35 = function() {
	this.initialize(img.image35);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image356 = function() {
	this.spriteSheet = ss["graphics_atlas_2"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image37 = function() {
	this.initialize(img.image37);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image384 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image386 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image388 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image39 = function() {
	this.initialize(img.image39);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image390 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image392 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image394 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image396 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image398 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image4 = function() {
	this.initialize(img.image4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.image400 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image402 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image404 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image406 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image408 = function() {
	this.spriteSheet = ss["graphics_atlas_22"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image41 = function() {
	this.initialize(img.image41);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image410 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image412 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image414 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image416 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image418 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image420 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image422 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image424 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image426 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image428 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image43 = function() {
	this.initialize(img.image43);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image430 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image432 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image434 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image436 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image438 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image440 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image442 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image444 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image446 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image448 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image45 = function() {
	this.initialize(img.image45);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image450 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image452 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image454 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image456 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image458 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image460 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image462 = function() {
	this.spriteSheet = ss["graphics_atlas_23"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image464 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image466 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image468 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image47 = function() {
	this.initialize(img.image47);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image470 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image472 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image474 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image476 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image478 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image480 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image482 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image484 = function() {
	this.spriteSheet = ss["graphics_atlas_24"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image486 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image488 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image49 = function() {
	this.initialize(img.image49);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image490 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image492 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image494 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image496 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image498 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image500 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image502 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image504 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image506 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image508 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image51 = function() {
	this.initialize(img.image51);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image510 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image512 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image514 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image516 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image518 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image520 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image522 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image524 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image527 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image529 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image53 = function() {
	this.initialize(img.image53);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image532 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image534 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image536 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image538 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image540 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image542 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image544 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image546 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image548 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image55 = function() {
	this.initialize(img.image55);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image550 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image552 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image554 = function() {
	this.spriteSheet = ss["graphics_atlas_25"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image556 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image558 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image560 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image562 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image564 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image566 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image568 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image57 = function() {
	this.initialize(img.image57);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image570 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image572 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image574 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image576 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image578 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image580 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image582 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image584 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image586 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image588 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image59 = function() {
	this.initialize(img.image59);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image590 = function() {
	this.spriteSheet = ss["graphics_atlas_26"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image592 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image594 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image596 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image598 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image6 = function() {
	this.initialize(img.image6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.image600 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image602 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image604 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image606 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image608 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image610 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image612 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image614 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image616 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image618 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image62 = function() {
	this.initialize(img.image62);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image620 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image622 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image624 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image626 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image628 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image630 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image632 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image634 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image636 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image638 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image64 = function() {
	this.initialize(img.image64);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image640 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image642 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image644 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image646 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image648 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image650 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image652 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image654 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image656 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image658 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image66 = function() {
	this.initialize(img.image66);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image660 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image662 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image664 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image666 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image668 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image670 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image672 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image674 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image676 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image678 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image68 = function() {
	this.initialize(img.image68);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image680 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image682 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image684 = function() {
	this.spriteSheet = ss["graphics_atlas_27"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image686 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image688 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image690 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image692 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image694 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image696 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image698 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image70 = function() {
	this.initialize(img.image70);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image700 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image702 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image704 = function() {
	this.spriteSheet = ss["graphics_atlas_28"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image706 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image708 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image710 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image712 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image714 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image716 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image718 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image720 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image722 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image724 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image726 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image729 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image731 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image733 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image735 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image737 = function() {
	this.spriteSheet = ss["graphics_atlas_29"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image739 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image741 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image743 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image745 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image747 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image749 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image751 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image753 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image755 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image757 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image759 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image761 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image763 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image765 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image767 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image769 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image771 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image773 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image775 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image777 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image779 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image781 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image783 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image785 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image787 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image789 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image791 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image793 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image795 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image797 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image799 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image802 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image804 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image806 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image808 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image810 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image812 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image814 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image816 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image818 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image820 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image822 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image824 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image826 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image828 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image830 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image832 = function() {
	this.spriteSheet = ss["graphics_atlas_31"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image834 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image836 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image838 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image840 = function() {
	this.spriteSheet = ss["graphics_atlas_30"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image842 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image844 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image846 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image848 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image850 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image852 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image854 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image856 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image858 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image86 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image860 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image862 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image864 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image866 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image868 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image870 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image874 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image876 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image878 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image880 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image882 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image884 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image888 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image890 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image892 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image894 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image896 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image898 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image9 = function() {
	this.initialize(img.image9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.image900 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image902 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image904 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image906 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image908 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image910 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image912 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image914 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image916 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image918 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image920 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image922 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image924 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image926 = function() {
	this.spriteSheet = ss["graphics_atlas_32"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image928 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.image930 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image932 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.image934 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.image936 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.image938 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.image940 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.image942 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.image944 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image946 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image948 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image950 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.image952 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.image954 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image957 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.image959 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.image96 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image961 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.image963 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.image965 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image967 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.image969 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.image971 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.image973 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.image975 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.image977 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.image979 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.image981 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.image983 = function() {
	this.spriteSheet = ss["graphics_atlas_35"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image985 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.image987 = function() {
	this.spriteSheet = ss["graphics_atlas_33"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.image989 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.image993 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.image997 = function() {
	this.spriteSheet = ss["graphics_atlas_34"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.text377 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC33").s().p("ABOCPIgmg8QgRgagKgNQgLgOgGgFQgJgGgJgCIgVgBIgsAAIAAB/IgmAAIAAkdIB9AAQAmAAAUAIQAUAIAMATQAMAUAAAYQAAAegUAVQgTATgqAFQAPAIAIAHQARAPAPAXIAyBOgAhXgPIBSAAQAYAAAOgFQAPgGAHgMQAIgMAAgNQAAgVgPgNQgOgNggAAIhZAAg");
	this.shape.setTransform(147.4,69.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC33").s().p("AhHCAQgggTgQgiQgQgiAAgmQAAhFAmgoQAmgpA7AAQAnAAAgATQAgATARAiQAQAiAAApQAAAsgRAiQgSAiggASQggASglAAQgnAAgggUgAhDhWQgdAbAAA/QAAA0AcAeQAbAdApAAQAqAAAcgeQAbgeAAg3QAAghgLgaQgMgagXgPQgXgOgcAAQgmAAgdAcg");
	this.shape_1.setTransform(116.1,69.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC33").s().p("ABOCPIgmg8QgRgagKgNQgLgOgGgFQgJgGgJgCIgVgBIgsAAIAAB/IgmAAIAAkdIB9AAQAmAAAUAIQAUAIAMATQAMAUAAAYQAAAegUAVQgTATgqAFQAPAIAIAHQARAPAPAXIAyBOgAhXgPIBSAAQAYAAAOgFQAPgGAHgMQAIgMAAgNQAAgVgPgNQgOgNggAAIhZAAg");
	this.shape_2.setTransform(87.4,69.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC33").s().p("ABOCPIgmg8QgRgagKgNQgLgOgGgFQgJgGgJgCIgVgBIgsAAIAAB/IgmAAIAAkdIB9AAQAmAAAUAIQAUAIAMATQAMAUAAAYQAAAegUAVQgTATgqAFQAPAIAIAHQARAPAPAXIAyBOgAhXgPIBSAAQAYAAAOgFQAPgGAHgMQAIgMAAgNQAAgVgPgNQgOgNggAAIhZAAg");
	this.shape_3.setTransform(58.5,69.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFCC33").s().p("AhpCPIAAkdIDNAAIAAAiIioAAIAABYICdAAIAAAgIidAAIAABhICvAAIAAAig");
	this.shape_4.setTransform(29.9,69.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC33").s().p("AhqCPIAAkdIDNAAIAAAiIinAAIAABYICdAAIAAAgIidAAIAABhICvAAIAAAig");
	this.shape_5.setTransform(121,24.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFCC33").s().p("ABkCPIAAjuIhUDuIggAAIhTjyIAADyIgkAAIAAkdIA5AAIBEDJIALArIAQguIBEjGIAzAAIAAEdg");
	this.shape_6.setTransform(90.4,24.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFCC33").s().p("ABaCPIghhXIh2AAIggBXIgnAAIBtkdIAnAAIB2EdgAgTg5IgfBSIBfAAIgehNQgOgkgEgXQgGAbgKAbg");
	this.shape_7.setTransform(60.5,24.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFCC33").s().p("Ag9CCQgjgTgRgiQgSgiAAgrQAAgnASgkQARglAhgRQAhgRAoAAQAfAAAaAKQAZAKAOASQAOARAIAdIgjAJQgGgVgKgNQgJgMgSgIQgSgHgVgBQgYABgTAHQgTAJgMAMQgMANgGAQQgLAbAAAeQAAAlANAZQANAaAaAMQAZANAaAAQAZAAAXgJQAYgJAMgMIAAg1IhUAAIAAgfIB5AAIAABoQgcAWgeALQgdAMggAAQgoAAgigSg");
	this.shape_8.setTransform(31.4,24.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(14,1,160.9,138.1);


(lib.text375 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC33").s().p("AhHCAQgggTgQgiQgQgiAAgmQAAhFAmgoQAmgpA7AAQAnAAAgATQAgATARAiQAQAiAAApQAAAsgRAiQgSAiggASQggASglAAQgnAAgggUgAhDhWQgdAbAAA/QAAA0AcAeQAbAdApAAQAqAAAcgeQAbgeAAg3QAAghgLgaQgMgagXgPQgXgOgcAAQgmAAgdAcg");
	this.shape.setTransform(96.1,69.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC33").s().p("AhfCPIAAkdIC/AAIAAAiIiZAAIAABZICEAAIAAAgIiEAAIAACCg");
	this.shape_1.setTransform(69,69.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC33").s().p("ABJCPIiUjfIAADfIgkAAIAAkdIAnAAICUDfIAAjfIAkAAIAAEdg");
	this.shape_2.setTransform(41.4,69.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC33").s().p("AgRCPIAAkdIAjAAIAAEdg");
	this.shape_3.setTransform(21.6,69.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFCC33").s().p("AhqCPIAAkdIDNAAIAAAiIinAAIAABYICdAAIAAAgIidAAIAABhICvAAIAAAig");
	this.shape_4.setTransform(121,24.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC33").s().p("ABkCPIAAjuIhUDuIggAAIhTjyIAADyIgkAAIAAkdIA5AAIBEDJIALArIAQguIBEjGIAzAAIAAEdg");
	this.shape_5.setTransform(90.4,24.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFCC33").s().p("ABaCPIghhXIh2AAIggBXIgnAAIBtkdIAnAAIB2EdgAgTg5IgfBSIBfAAIgehNQgOgkgEgXQgGAbgKAbg");
	this.shape_6.setTransform(60.5,24.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFCC33").s().p("Ag9CCQgjgTgRgiQgSgiAAgrQAAgnASgkQARglAhgRQAhgRAoAAQAfAAAaAKQAZAKAOASQAOARAIAdIgjAJQgGgVgKgNQgJgMgSgIQgSgHgVgBQgYABgTAHQgTAJgMAMQgMANgGAQQgLAbAAAeQAAAlANAZQANAaAaAMQAZANAaAAQAZAAAXgJQAYgJAMgMIAAg1IhUAAIAAgfIB5AAIAABoQgcAWgeALQgdAMggAAQgoAAgigSg");
	this.shape_7.setTransform(31.4,24.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(14,1,140.6,138.1);


(lib.text374 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgvFXIAAhgIBfAAIAABggAgZCtIgalrIAAiYIBnAAIAACYIgZFrg");
	this.shape.setTransform(109.2,62.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(93.8,7.5,39,218.5);


(lib.text371 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC33").s().p("AgbBEQgNgGgHgLQgIgMAAgOIASgCQAAALAGAHQAEAHAJAFQALAEAKgBQAKAAAJgDQAIgDAEgFQAEgGAAgHQAAgHgEgEQgEgFgIgEQgHgDgRgEQgUgFgHgCQgLgGgFgHQgFgJAAgJQAAgLAGgJQAGgKAMgFQAMgEAOgBQAOABAMAEQAMAFAHALQAHAJAAANIgSABQgCgNgIgHQgIgHgQAAQgQAAgIAGQgIAGAAAKQAAAHAGAFQAGAFAUAFQAYAGAIADQAMAFAHAJQAFAIABALQAAAMgHAKQgGAKgNAGQgMAFgPABQgSgBgNgFg");
	this.shape.setTransform(51.1,11);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC33").s().p("AgrBHIAAiNIATAAIAAB8IBEAAIAAARg");
	this.shape_1.setTransform(39.3,11);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC33").s().p("AgIBHIAAiNIARAAIAACNg");
	this.shape_2.setTransform(30.6,11);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC33").s().p("AAtBHIgQgrIg7AAIgPArIgVAAIA3iNIATAAIA7CNgAgJgcIgPAoIAuAAIgPglIgIgeQgCAOgGANg");
	this.shape_3.setTransform(21.1,11);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFCC33").s().p("AgIBHIAAh8IgvAAIAAgRIBvAAIAAARIgvAAIAAB8g");
	this.shape_4.setTransform(8.4,11);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC33").s().p("Ag0BHIAAiNIBmAAIAAARIhTAAIAAArIBNAAIAAAPIhNAAIAAAxIBWAAIAAARg");
	this.shape_5.setTransform(-4.2,11);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFCC33").s().p("Ag6BHIAAiNIAxAAQAPAAAJACQAMADAJAHQALAJAGAQQAGAOAAATQAAAPgEANQgEAMgGAIQgGAIgHAGQgHAEgKACQgKADgLAAgAgnA2IAfAAQAMAAAIgDQAIgCAFgFQAGgGAEgMQAEgMAAgOQAAgWgHgMQgIgMgKgEQgIgDgPAAIgeAAg");
	this.shape_6.setTransform(-18.1,11);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.5,-2,116.9,48.7);


(lib.text333 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgEAFIAAgKIAKAAIAAAKg");
	this.shape.setTransform(230.2,48.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgCQABAHAEADQAEADAFAAQAIAAADgDQADgDAAgDQAAgEgCgCIgLgEIgOgEQgFgBgCgEQgDgDABgFQAAgEABgEIAGgGIAGgDIAJgBQAFAAAHACQAFACADADQACAFABAFIgKABQgBgEgEgDQgDgDgFAAQgGAAgDADQgDACAAADIABAFIAEACIAIADIAPAFQAEAAADADQACAEAAAFQAAAFgDAFQgDAFgFACQgGADgHAAQgLAAgGgFg");
	this.shape_1.setTransform(225.4,45.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgWAqIgBgKIAHABIAFgBIAEgEIACgIIABgCIgXg8IAMAAIAMAkIADAMIAEgMIAOgkIAKAAIgYA9IgEAOQgCAFgEADQgEACgFAAIgHgBg");
	this.shape_2.setTransform(219.5,47.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEADgCIAIgCIAJgCQALgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgDADgCAHIgKgCQABgGADgFQAEgEAFgCQAGgCAHAAQAHAAAFACQAFACADACQACAEACAEIAAAKIAAAMIABASQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgEACQgCAAgBADQgCACAAADQAAAEADADQADADAGAAQAFAAAEgDQAFgCACgFQACgEAAgHIAAgDIgPADg");
	this.shape_3.setTransform(213.1,45.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgaArIAAhUIAKAAIAAAIQAEgFADgCQAFgCAEAAQAIAAAGAEQAGAEAEAHQACAIAAAJQAAAJgDAGQgDAIgHAEQgGAEgHAAQgEAAgEgDQgFgCgCgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAFAGAGAAQAGAAAEgGQAGgGAAgKQgBgMgEgGQgFgGgGAAQgFAAgGAGg");
	this.shape_4.setTransform(206.7,47);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgNAnQgFgEgEgHQgDgIAAgJQAAgKADgFQADgIAGgEQAGgEAHAAQAEAAAFADQAEACADAEIAAgfIALAAIAABUIgKAAIAAgIQgGAJgLAAQgGAAgHgEgAgKgFQgFAFABALQAAAMAEAGQAGAFAFAAQAGAAAFgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_5.setTransform(196.3,44.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_6.setTransform(189.8,45.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEAEgCIAHgCIAKgCQAKgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgDADgCAHIgLgCQACgGAEgFQACgEAHgCQAGgCAFAAQAIAAAGACQAFACACACQADAEABAEIAAAKIAAAMIABASQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgDACQgDAAgCADQgBACAAADQAAAEADADQAEADAFAAQAEAAAGgDQAEgCACgFQACgEAAgHIAAgDIgPADg");
	this.shape_7.setTransform(183.2,45.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgQAbQgHgFgCgKIALgCQABAHAEADQAEADAFAAQAHAAAEgDQADgDABgDQAAgEgDgCIgMgEIgNgEQgFgBgCgEQgCgDAAgFQAAgEACgEIAEgGIAHgDIAJgBQAGAAAGACQAFACACADQADAFABAFIgKABQgBgEgEgDQgDgDgFAAQgGAAgDADQgEACABADIABAFIAEACIAHADIAQAFQAEAAADADQADAEgBAFQAAAFgCAFQgEAFgFACQgHADgHAAQgKAAgGgFg");
	this.shape_8.setTransform(173.5,45.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgWAqIgBgKIAHABIAFgBIAEgEIACgIIABgCIgXg8IAMAAIAMAkIADAMIAEgMIAOgkIAKAAIgYA9IgEAOQgCAFgEADQgDACgGAAIgHgBg");
	this.shape_9.setTransform(167.6,47.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEADgCIAIgCIAJgCQALgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgDADgCAHIgKgCQABgGADgFQAEgEAFgCQAHgCAGAAQAHAAAFACQAFACADACQADAEAAAEIABAKIAAAMIAAASQABAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgHABgDACQgCAAgBADQgCACAAADQAAAEADADQAEADAFAAQAEAAAFgDQAFgCACgFQACgEAAgHIAAgDIgPADg");
	this.shape_10.setTransform(161.2,45.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgZArIAAhUIAJAAIAAAIQADgFAFgCQAEgCAEAAQAIAAAGAEQAGAEAEAHQADAIAAAJQgBAJgDAGQgEAIgGAEQgHAEgGAAQgEAAgEgDQgEgCgDgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAEAGAHAAQAFAAAFgGQAFgGABgKQAAgMgFgGQgFgGgGAAQgGAAgFAGg");
	this.shape_11.setTransform(154.7,47);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_12.setTransform(146.5,44.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_13.setTransform(143.8,44.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEADgCIAIgCIAJgCQALgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgDADgCAHIgKgCQABgGADgFQADgEAGgCQAHgCAGAAQAHAAAFACQAFACADACQACAEABAEIABAKIAAAMIABASQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgEACQgCAAgBADQgCACAAADQAAAEADADQAEADAFAAQAFAAAEgDQAFgCACgFQACgEAAgHIAAgDIgPADg");
	this.shape_14.setTransform(139.2,45.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgCQABAHAEADQAEADAFAAQAHAAAEgDQAEgDAAgDQgBgEgDgCIgLgEIgOgEQgEgBgCgEQgCgDgBgFQAAgEACgEIAGgGIAHgDIAIgBQAFAAAGACQAGACACADQADAFABAFIgLABQAAgEgDgDQgDgDgGAAQgGAAgDADQgEACAAADIACAFIAEACIAHADIAQAFQAFAAACADQACAEABAFQAAAFgEAFQgDAFgGACQgFADgIAAQgKAAgGgFg");
	this.shape_15.setTransform(129.5,45.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgNAnQgFgEgEgHQgEgIABgJQAAgKACgFQAEgIAGgEQAGgEAHAAQAFAAAEADQAFACACAEIAAgfIALAAIAABUIgKAAIAAgIQgGAJgLAAQgGAAgHgEgAgKgFQgEAFAAALQAAAMAEAGQAGAFAFAAQAGAAAFgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_16.setTransform(123,44.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_17.setTransform(118.6,44.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_18.setTransform(113.9,45.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgEAeIgXg7IALAAIANAjIADAMIAEgMIAOgjIAKAAIgYA7g");
	this.shape_19.setTransform(107.6,45.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_20.setTransform(97.9,45.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_21.setTransform(91.3,45.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_22.setTransform(86.6,44.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_23.setTransform(83.7,44.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgJAEgHQADgIAHgDQAHgEAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgEgFAAQgGABgFAFQgFAGAAALQAAAMAFAGQAFAFAGAAQAGAAAEgEQAEgEABgHIALABQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_24.setTransform(79.1,45.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_25.setTransform(72.6,45.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgNAdQgEgCgDgDQgCgDgBgFIgBgKIAAgkIALAAIAAAgIABALQABAEADADQADACAFAAQADAAAEgCQAEgDACgEQABgEAAgIIAAgfIALAAIAAA8IgKAAIAAgJQgHAKgKAAQgGAAgFgCg");
	this.shape_26.setTransform(65.9,46);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgHArIAAg0IgKAAIAAgIIAKAAIAAgHIABgJQABgEAEgDQACgCAHAAIAKABIgCAJIgGAAQgFAAgCACQgCACAAAFIAAAGIAMAAIAAAIIgMAAIAAA0g");
	this.shape_27.setTransform(61.2,44.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_28.setTransform(57.9,44.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgVAbQgGgFAAgIQAAgFACgDQACgEAEgCIAHgCIAKgCQAKgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgEADgBAHIgLgCQACgGAEgFQACgEAHgCQAGgCAFAAQAIAAAGACQAEACADACQACAEABAEIABAKIAAAMIAAASQABAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgFgFgAgBADQgGABgDACQgDAAgCADQgBACAAADQAAAEAEADQACADAHAAQAEAAAFgDQAEgCADgFQABgEAAgHIAAgDIgPADg");
	this.shape_29.setTransform(53.3,45.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAdAqIAAhGIgYBGIgJAAIgYhHIAABHIgMAAIAAhTIARAAIAVA7IACAMIAFgNIAUg6IAQAAIAABTg");
	this.shape_30.setTransform(45,44.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgFAFIAAgKIALAAIAAAKg");
	this.shape_31.setTransform(35,48.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgQAbQgHgFgCgKIALgCQABAHAEADQAEADAFAAQAHAAAEgDQADgDABgDQAAgEgEgCIgLgEIgOgEQgEgBgCgEQgDgDAAgFQABgEACgEIAEgGIAHgDIAJgBQAGAAAFACQAGACACADQADAFABAFIgKABQgBgEgEgDQgDgDgFAAQgGAAgDADQgDACAAADIABAFIAEACIAHADIAQAFQAFAAACADQACAEAAAFQAAAFgCAFQgEAFgFACQgGADgIAAQgKAAgGgFg");
	this.shape_32.setTransform(30.3,45.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_33.setTransform(25.7,44.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_34.setTransform(22.6,44.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgMAnQgHgEgDgHQgDgIgBgJQAAgKAEgFQADgIAGgEQAGgEAHAAQAEAAAFADQAEACADAEIAAgfIAKAAIAABUIgJAAIAAgIQgGAJgLAAQgGAAgGgEgAgKgFQgFAFAAALQABAMAEAGQAFAFAFAAQAIAAAEgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_35.setTransform(17.8,44.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_36.setTransform(11.3,45.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_37.setTransform(6.5,45.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgJAEgHQADgIAHgDQAHgEAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgEgFAAQgGABgFAFQgFAGAAALQAAAMAFAGQAFAFAGAAQAGAAAEgEQAEgEABgHIALABQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_38.setTransform(1.2,45.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_39.setTransform(532.8,32.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgEAqIAAg8IAJAAIAAA8gAgEgdIAAgMIAJAAIAAAMg");
	this.shape_40.setTransform(528.2,31.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_41.setTransform(520.2,32.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgaAeIAAgIIAmgsIgMABIgYAAIAAgJIAxAAIAAAHIgfAlIgHAIIANgBIAbAAIAAAJg");
	this.shape_42.setTransform(513.9,32.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgEAqIAAg8IAJAAIAAA8gAgEgdIAAgMIAJAAIAAAMg");
	this.shape_43.setTransform(509.5,31.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_44.setTransform(506.7,32.5);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgaArIAAhUIAKAAIAAAIQAEgFADgCQAFgCAEAAQAIAAAGAEQAGAEAEAHQACAIAAAJQABAJgEAGQgDAIgHAEQgGAEgHAAQgEAAgEgDQgFgCgCgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAFAGAGAAQAGAAAEgGQAGgGgBgKQAAgMgEgGQgFgGgGAAQgFAAgGAGg");
	this.shape_45.setTransform(501.1,33.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_46.setTransform(492.8,31.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_47.setTransform(490.2,31.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AAbAqIgKgaIgiAAIgJAaIgNAAIAhhTIAKAAIAjBTgAgFgQIgJAXIAbAAIgJgVIgEgSQgCAIgDAIg");
	this.shape_48.setTransform(484.9,31.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgFAFIAAgJIAKAAIAAAJg");
	this.shape_49.setTransform(475.9,35.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgMAnQgGgEgEgHQgEgIAAgJQABgKADgFQADgIAGgEQAGgEAHAAQAEAAAFADQAEACADAEIAAgfIAKAAIAABUIgJAAIAAgIQgGAJgLAAQgGAAgGgEgAgKgFQgFAFABALQAAAMAEAGQAGAFAFAAQAGAAAFgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_50.setTransform(470.7,31.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_51.setTransform(464.2,32.6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_52.setTransform(459.4,32.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_53.setTransform(453.6,32.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgRAnQgHgGAAgJIAKABQABAFADACQAEADAGAAQAGAAAEgDQAEgDABgFIABgOQgHAJgJgBQgMAAgHgJQgHgJAAgLQAAgJADgHQAEgIAGgEQAGgEAHAAQAKAAAHAJIAAgIIAKAAIAAA0QAAAPgDAFQgDAHgGADQgHAEgIAAQgKAAgHgFgAgKgcQgFAFAAAMQAAALAFAEQAFAGAFAAQAHAAAFgGQAFgEAAgLQAAgMgFgFQgFgHgHAAQgFAAgFAHg");
	this.shape_54.setTransform(446.7,33.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgRAnQgHgGAAgJIAKABQABAFADACQAEADAGAAQAGAAAEgDQAEgDABgFIABgOQgHAJgJgBQgMAAgHgJQgHgJAAgLQAAgJADgHQAEgIAGgEQAGgEAHAAQAKAAAHAJIAAgIIAKAAIAAA0QAAAPgDAFQgDAHgGADQgHAEgIAAQgKAAgHgFgAgKgcQgFAFAAAMQAAALAFAEQAFAGAFAAQAHAAAFgGQAFgEAAgLQAAgMgFgFQgFgHgHAAQgFAAgFAHg");
	this.shape_55.setTransform(440.1,33.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgEAqIAAg8IAJAAIAAA8gAgEgdIAAgMIAJAAIAAAMg");
	this.shape_56.setTransform(435.6,31.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_57.setTransform(432.8,32.5);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgCAAgKIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAjIAAAFIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_58.setTransform(428.7,31.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_59.setTransform(423.6,32.6);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_60.setTransform(418.8,32.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_61.setTransform(409.6,32.6);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgQAiIAAAIIgKAAIAAhUIALAAIAAAeQAHgIAIAAQAFAAAGACQAFADAEAEQADAEABAGQACAEAAAHQAAAQgHAIQgJAJgKAAQgJAAgHgJgAgLgFQgFAFAAAKQAAALADAFQAFAIAIAAQAFAAAGgFQAEgHAAgLQAAgLgEgFQgFgGgGAAQgGAAgFAGg");
	this.shape_62.setTransform(403.2,31.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_63.setTransform(393,32.5);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgVAbQgGgFAAgIQAAgFACgDQACgEAEgCIAHgCIAKgCQAKgBAGgCIAAgDQAAgGgDgDQgEgDgHgBQgGABgEACQgEADgBAGIgLgBQACgHAEgEQACgDAHgDQAGgCAFAAQAIAAAGABQAEADADADQACACABAFIABAKIAAALIAAATQABAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgFgFgAgBADQgGABgDABQgDABgCADQgBACAAADQAAAEAEADQACACAHAAQAEAAAFgCQAEgDADgFQABgDAAgHIAAgDIgPADg");
	this.shape_64.setTransform(386.3,32.6);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgSAXQgIgIAAgPQAAgIAEgIQADgHAHgFQAHgDAGAAQAKAAAGAFQAHAFACAJIgLACQgBgGgEgEQgEgDgFAAQgGAAgFAHQgFAFAAALQAAAMAFAGQAFAFAGAAQAGAAAEgDQAEgEABgIIALABQgCALgHAGQgHAGgKAAQgLAAgIgJg");
	this.shape_65.setTransform(380.2,32.6);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgBQABAFAEAEQAEADAFAAQAHAAAEgDQADgCAAgFQAAgDgDgCIgKgEIgPgEQgEgBgCgEQgCgEgBgEQAAgFACgDIAGgGIAHgDIAIgBQAGAAAFACQAGACADAEQACAEABAFIgLABQAAgEgDgDQgDgCgGgBQgGABgDACQgDADgBADIACADIAEADIAIADIAPAEQAEABADADQADAEAAAFQAAAFgEAFQgDAFgGACQgGADgGAAQgLAAgGgFg");
	this.shape_66.setTransform(370.6,32.6);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_67.setTransform(364.3,32.6);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AAfAfIAAglIgBgJIgDgFQgDgBgEAAQgGAAgEAEQgFAEAAAKIAAAiIgJAAIAAgmQAAgHgCgEQgDgDgFAAQgFAAgEACQgEACgCAFQgBAEAAAIIAAAfIgKAAIAAg8IAJAAIAAAJQADgFAFgDQAFgCAGAAQAGAAAFACQACADACAGQAIgLALAAQAKAAAFAFQAEAFAAALIAAAog");
	this.shape_68.setTransform(356,32.5);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEADgCIAIgCIAJgCQALgBAGgCIAAgDQAAgGgDgDQgEgDgHgBQgGABgEACQgDADgCAGIgKgBQABgHADgEQAEgDAFgDQAGgCAHAAQAHAAAFABQAFADADADQACACACAFIAAAKIAAALIABATQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgEABQgCABgCADQgBACAAADQAAAEADADQADACAGAAQAFAAAEgCQAFgDACgFQACgDAAgHIAAgDIgPADg");
	this.shape_69.setTransform(347.7,32.6);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgRAnQgHgGAAgJIAKABQABAFADACQAEADAGAAQAGAAAEgDQAEgDABgFIABgOQgHAJgJgBQgMAAgHgJQgHgJAAgLQAAgJADgHQAEgIAGgEQAGgEAHAAQAKAAAHAJIAAgIIAKAAIAAA0QAAAPgDAFQgDAHgGADQgHAEgIAAQgKAAgHgFgAgKgcQgFAFAAAMQAAALAFAEQAFAGAFAAQAHAAAFgGQAFgEAAgLQAAgMgFgFQgFgHgHAAQgFAAgFAHg");
	this.shape_70.setTransform(340.8,33.8);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_71.setTransform(331,32.6);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_72.setTransform(324.4,32.6);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_73.setTransform(319.5,32.5);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AgcAqIAAhTIA5AAIAAAKIguAAIAAAbIAnAAIAAAIIgnAAIAAAmg");
	this.shape_74.setTransform(313.6,31.4);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AgEAFIAAgJIAJAAIAAAJg");
	this.shape_75.setTransform(304.7,35.1);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgBQABAFAEAEQAEADAFAAQAHAAAEgDQADgCAAgFQAAgDgDgCIgKgEIgPgEQgEgBgCgEQgCgEgBgEQAAgFACgDIAGgGIAHgDIAIgBQAGAAAFACQAGACACAEQADAEABAFIgLABQAAgEgDgDQgDgCgGgBQgGABgDACQgDADgBADIACADIAEADIAIADIAPAEQAEABADADQADAEAAAFQAAAFgEAFQgDAFgGACQgGADgGAAQgLAAgGgFg");
	this.shape_76.setTransform(300,32.6);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_77.setTransform(293.7,32.5);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgEAqIAAg8IAJAAIAAA8gAgEgdIAAgMIAJAAIAAAMg");
	this.shape_78.setTransform(289,31.4);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AAMAeIgKgjIgCgKIgLAtIgLAAIgTg8IALAAIAKAiIADAOIAEgOIAKgiIAIAAIAKAiIACANIAFgNIAKgiIAKAAIgTA8g");
	this.shape_79.setTransform(283.4,32.6);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_80.setTransform(272.4,32.6);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_81.setTransform(265.7,32.5);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AgEAqIAAg8IAJAAIAAA8gAgEgdIAAgMIAJAAIAAAMg");
	this.shape_82.setTransform(261.1,31.4);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_83.setTransform(258.4,31.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AgUAXQgIgIAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAIAAAPQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgJgAgMgQQgFAFAAALQAAALAFAHQAGAFAGAAQAHAAAFgFQAGgHAAgLQAAgLgGgFQgFgHgHAAQgGAAgGAHg");
	this.shape_84.setTransform(250.4,32.6);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgCAAgKIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAjIAAAFIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_85.setTransform(245.5,31.5);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AgNAnQgFgEgEgHQgEgIABgJQAAgKACgFQAEgIAGgEQAGgEAHAAQAFAAAEADQAFACACAEIAAgfIALAAIAABUIgKAAIAAgIQgGAJgLAAQgGAAgHgEgAgKgFQgEAFAAALQAAAMAEAGQAFAFAGAAQAGAAAFgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_86.setTransform(236.9,31.4);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_87.setTransform(230.4,32.6);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("AgNAnQgFgEgEgHQgDgIAAgJQAAgKACgFQAEgIAGgEQAGgEAHAAQAFAAAEADQAFACACAEIAAgfIALAAIAABUIgKAAIAAgIQgGAJgLAAQgGAAgHgEgAgKgFQgEAFgBALQAAAMAGAGQAEAFAFAAQAHAAAFgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_88.setTransform(223.6,31.4);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AgNAnQgGgEgDgHQgDgIgBgJQAAgKADgFQAEgIAGgEQAGgEAHAAQAEAAAFADQAEACADAEIAAgfIAKAAIAABUIgJAAIAAgIQgGAJgLAAQgGAAgHgEgAgKgFQgEAFgBALQAAAMAGAGQAFAFAEAAQAIAAAEgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_89.setTransform(216.9,31.4);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AgVAbQgGgFAAgIQAAgFACgDQACgEADgCIAIgCIAKgCQAKgBAGgCIAAgDQAAgGgDgDQgEgDgHgBQgGABgEACQgEADgBAGIgLgBQACgHAEgEQACgDAHgDQAFgCAGAAQAJAAAFABQAEADADADQACACABAFIABAKIAAALIAAATQABAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgFgFgAgBADQgHABgCABQgDABgBADQgCACAAADQAAAEAEADQACACAHAAQAEAAAFgCQAEgDADgFQABgDAAgHIAAgDIgPADg");
	this.shape_90.setTransform(210.5,32.6);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_91.setTransform(200.5,32.6);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_92.setTransform(195.6,32.5);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEAEgCIAHgCIAJgCQALgBAGgCIAAgDQAAgGgDgDQgEgDgHgBQgGABgEACQgDADgCAGIgKgBQABgHADgEQAEgDAFgDQAGgCAGAAQAJAAAEABQAGADACADQADACABAFIAAAKIAAALIABATQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgDABQgDABgCADQgBACAAADQAAAEADADQAEACAFAAQAEAAAGgCQAEgDACgFQACgDAAgHIAAgDIgPADg");
	this.shape_93.setTransform(189.8,32.6);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("AgQAbQgHgFgCgKIALgBQABAFAEAEQAEADAFAAQAHAAAEgDQADgCABgFQAAgDgEgCIgLgEIgOgEQgEgBgCgEQgDgEAAgEQABgFACgDIAEgGIAHgDIAJgBQAGAAAFACQAGACACAEQADAEABAFIgKABQgBgEgEgDQgDgCgFgBQgGABgDACQgDADAAADIABADIAEADIAHADIAQAEQAFABACADQACAEAAAFQAAAFgCAFQgEAFgFACQgHADgHAAQgKAAgGgFg");
	this.shape_94.setTransform(180.1,32.6);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_95.setTransform(173.8,32.5);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("AgEAqIAAg8IAJAAIAAA8gAgEgdIAAgMIAJAAIAAAMg");
	this.shape_96.setTransform(169.2,31.4);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AAMAeIgKgjIgCgKIgLAtIgLAAIgTg8IALAAIAJAiIAFAOIACgOIAKgiIAJAAIAJAiIAEANIADgNIALgiIAKAAIgTA8g");
	this.shape_97.setTransform(163.5,32.6);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_98.setTransform(154.3,32.5);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_99.setTransform(148.5,32.6);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgCAAgKIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAjIAAAFIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_100.setTransform(143.6,31.5);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgCAAgKIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAjIAAAFIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_101.setTransform(140.2,31.5);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEADgCIAIgCIAJgCQALgBAGgCIAAgDQAAgGgDgDQgEgDgHgBQgGABgEACQgEADgBAGIgKgBQABgHADgEQADgDAGgDQAHgCAGAAQAHAAAFABQAFADADADQACACABAFIABAKIAAALIABATQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgEABQgCABgBADQgCACAAADQAAAEADADQAEACAFAAQAFAAAEgCQAFgDACgFQACgDAAgHIAAgDIgPADg");
	this.shape_102.setTransform(135.2,32.6);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFFFFF").s().p("AgSAXQgIgIAAgPQAAgIAEgIQADgHAHgFQAHgDAGAAQAKAAAGAFQAHAFACAJIgLACQgBgGgEgEQgEgDgFAAQgGAAgFAHQgFAFAAALQAAAMAFAGQAFAFAGAAQAGAAAEgDQAEgEABgIIALABQgCALgHAGQgHAGgKAAQgLAAgIgJg");
	this.shape_103.setTransform(129,32.6);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFFFFF").s().p("AgQApQgIgEgEgGQgFgIAAgIIALgBQABAHACADQADAEAGAEQAGACAFAAQAHAAAFgCQAFgCACgEQADgDAAgEQAAgEgDgDQgCgDgGgCIgNgEQgMgDgEgBQgHgCgDgGQgDgEAAgGQAAgHAEgFQAEgGAHgDQAHgDAIAAQAIAAAIADQAHADAEAHQAEAFAAAIIgLAAQgBgHgFgEQgFgFgJAAQgJAAgFAEQgFAEAAAFQAAAFAEADQADADAMADQAOADAFACQAHABAEAGQAEAFAAAHQAAAHgEAGQgEAGgIAEQgHADgJAAQgKAAgIgDg");
	this.shape_104.setTransform(121.8,31.4);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FFFFFF").s().p("AgFAFIAAgJIALAAIAAAJg");
	this.shape_105.setTransform(112.9,35.1);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_106.setTransform(107.8,32.6);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_107.setTransform(101.2,32.5);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FFFFFF").s().p("AgEAqIAAg8IAJAAIAAA8gAgEgdIAAgMIAJAAIAAAMg");
	this.shape_108.setTransform(96.5,31.4);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_109.setTransform(93.8,31.4);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FFFFFF").s().p("AgNAnQgFgEgEgHQgEgIABgJQAAgKACgFQAEgIAGgEQAGgEAHAAQAFAAAEADQAFACACAEIAAgfIALAAIAABUIgKAAIAAgIQgGAJgLAAQgGAAgHgEgAgKgFQgEAFAAALQAAAMAEAGQAFAFAGAAQAGAAAFgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_110.setTransform(85.7,31.4);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_111.setTransform(79.2,32.6);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgCAAgKIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAjIAAAFIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_112.setTransform(74.3,31.5);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFFFFF").s().p("AgSAXQgIgIAAgPQAAgIAEgIQADgHAHgFQAHgDAGAAQAKAAAGAFQAHAFACAJIgLACQgBgGgEgEQgEgDgFAAQgGAAgFAHQgFAFAAALQAAAMAFAGQAFAFAGAAQAGAAAEgDQAEgEABgIIALABQgCALgHAGQgHAGgKAAQgLAAgIgJg");
	this.shape_113.setTransform(69.7,32.6);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_114.setTransform(63.2,32.6);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_115.setTransform(58.5,31.4);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_116.setTransform(53.9,32.6);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgDgKIALgBQABAFAEAEQAEADAFAAQAHAAAEgDQADgCAAgFQABgDgDgCIgMgEIgNgEQgFgBgCgEQgCgEAAgEQAAgFACgDIAEgGIAHgDIAJgBQAGAAAGACQAFACADAEQACAEABAFIgKABQgBgEgEgDQgDgCgFgBQgGABgDACQgEADABADIABADIAEADIAHADIAQAEQAEABADADQADAEgBAFQAAAFgCAFQgEAFgFACQgHADgHAAQgKAAgGgFg");
	this.shape_117.setTransform(47.6,32.6);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_118.setTransform(39.7,32.5);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFFFFF").s().p("AgTAXQgIgIAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAJgHAGQgHAFgLAAQgLAAgIgJgAgKgRQgFAEgBAJIAhAAQgBgJgDgDQgFgHgIAAQgFAAgFAGg");
	this.shape_119.setTransform(33.9,32.6);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FFFFFF").s().p("AgZArIAAhUIAJAAIAAAIQAEgFAEgCQAEgCAEAAQAIAAAGAEQAGAEAEAHQADAIgBAJQAAAJgDAGQgEAIgGAEQgGAEgHAAQgEAAgEgDQgEgCgDgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAEAGAHAAQAFAAAFgGQAGgGAAgKQgBgMgEgGQgFgGgGAAQgFAAgGAGg");
	this.shape_120.setTransform(27.5,33.7);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#FFFFFF").s().p("AgMAnQgGgEgEgHQgEgIAAgJQABgKADgFQADgIAGgEQAGgEAHAAQAFAAAEADQAEACADAEIAAgfIAKAAIAABUIgJAAIAAgIQgGAJgLAAQgGAAgGgEgAgKgFQgFAFABALQAAAMAEAGQAGAFAFAAQAGAAAFgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_121.setTransform(17.1,31.4);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FFFFFF").s().p("AgEAqIAAg8IAJAAIAAA8gAgEgdIAAgMIAJAAIAAAMg");
	this.shape_122.setTransform(12.6,31.4);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FFFFFF").s().p("AgVAbQgGgFAAgIQAAgFACgDQACgEADgCIAIgCIAKgCQAKgBAGgCIAAgDQAAgGgDgDQgEgDgHgBQgGABgEACQgEADgBAGIgLgBQACgHAEgEQACgDAHgDQAFgCAGAAQAJAAAFABQAEADADADQACACABAFIABAKIAAALIAAATQABAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgFgFgAgBADQgHABgDABQgCABgBADQgCACAAADQAAAEAEADQACACAHAAQAEAAAFgCQAEgDADgFQABgDAAgHIAAgDIgPADg");
	this.shape_123.setTransform(8,32.6);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#FFFFFF").s().p("AgZArIAAhUIAJAAIAAAIQADgFAFgCQAEgCAEAAQAIAAAGAEQAGAEADAHQAEAIAAAJQAAAJgEAGQgDAIgHAEQgHAEgGAAQgEAAgEgDQgEgCgDgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAEAGAHAAQAGAAAFgGQAEgGAAgKQABgMgFgGQgFgGgGAAQgFAAgGAGg");
	this.shape_124.setTransform(1.5,33.7);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FFFFFF").s().p("AgVAqIgCgKIAGABIAGgBIADgEIADgIIACgCIgYg8IALAAIAOAkIACAMIAEgMIANgkIALAAIgXA9IgFAOQgCAFgEADQgEACgFAAIgGgBg");
	this.shape_125.setTransform(544.6,20.5);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FFFFFF").s().p("AgEArIAAhVIAJAAIAABVg");
	this.shape_126.setTransform(540.2,18);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_127.setTransform(535.6,19.1);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAJQAJAHAAAPQAAALgEAGQgEAIgHADQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAFQAGAGAGAAQAHAAAFgGQAGgFAAgMQAAgKgGgHQgFgFgHAAQgGAAgGAFg");
	this.shape_128.setTransform(528.9,19.2);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_129.setTransform(518.9,19.1);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FFFFFF").s().p("AgEArIAAg8IAJAAIAAA8gAgEgdIAAgNIAJAAIAAANg");
	this.shape_130.setTransform(514.3,18);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FFFFFF").s().p("AAMAfIgKgkIgCgLIgLAvIgLAAIgTg8IALAAIAKAhIADANIADgMIALgiIAJAAIAJAiIACALIAFgLIAKgiIAKAAIgTA8g");
	this.shape_131.setTransform(508.6,19.2);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FFFFFF").s().p("AAAAoIgFgEQgBgEAAgJIAAgiIgIAAIAAgIIAIAAIAAgPIAJgHIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEgBIACAKIgIABQgGAAgBgCg");
	this.shape_132.setTransform(499.3,18.2);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FFFFFF").s().p("AgQAbQgHgFgCgKIALgBQABAGAEADQAEADAFAAQAIAAADgCQAEgEAAgDQAAgEgEgDIgLgDIgOgEQgEgBgCgEQgDgDAAgFQAAgEADgEIAEgGIAIgDIAIgBQAFAAAGACQAGACACAEQADADABAHIgLABQAAgFgEgDQgCgCgGAAQgGAAgDACQgEADAAADIACAEIAEACIAHADIAQAEQAFABACADQACAEABAFQAAAGgDAEQgEAFgFADQgGACgIAAQgKAAgGgFg");
	this.shape_133.setTransform(494.6,19.2);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_134.setTransform(488.3,19.2);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#FFFFFF").s().p("AAOArIAAgoQAAgGgDgEQgEgDgGAAQgDAAgEADQgEACgCAEQgBACAAAHIAAAjIgLAAIAAhVIALAAIAAAgQAHgJAJAAQAHAAAFACQAFADADAFQACAFAAAHIAAAog");
	this.shape_135.setTransform(481.6,18);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#FFFFFF").s().p("AgRAnQgHgGAAgKIAKACQABAFADACQAEADAGAAQAGAAAEgDQAEgDABgFIABgNQgHAHgJABQgMAAgHgKQgHgJAAgLQAAgJADgIQAEgHAGgEQAGgEAHAAQAKAAAHAJIAAgHIAKAAIAAAzQAAAPgDAFQgDAGgGAEQgHAEgIAAQgKAAgHgFgAgKgdQgFAGAAALQAAAMAFAEQAFAGAFAAQAHAAAFgGQAFgEAAgMQAAgLgFgGQgFgFgHAAQgFAAgFAFg");
	this.shape_136.setTransform(474.8,20.4);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#FFFFFF").s().p("AgEArIAAg8IAJAAIAAA8gAgEgdIAAgNIAJAAIAAANg");
	this.shape_137.setTransform(470.3,18);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#FFFFFF").s().p("AAWArIAAgpIgrAAIAAApIgLAAIAAhVIALAAIAAAkIArAAIAAgkIALAAIAABVg");
	this.shape_138.setTransform(464.7,18);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#FFFFFF").s().p("AgEAGIAAgLIAJAAIAAALg");
	this.shape_139.setTransform(455.4,21.7);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgBQABAGAEADQAEADAFAAQAIAAADgCQAEgEgBgDQAAgEgDgDIgKgDIgOgEQgFgBgCgEQgCgDgBgFQAAgEACgEIAGgGIAHgDIAIgBQAGAAAFACQAGACADAEQACADABAHIgLABQAAgFgDgDQgEgCgFAAQgGAAgDACQgDADgBADIACAEIAEACIAIADIAPAEQAEABADADQADAEAAAFQgBAGgDAEQgDAFgGADQgGACgGAAQgLAAgGgFg");
	this.shape_140.setTransform(450.6,19.2);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_141.setTransform(446.1,19.1);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_142.setTransform(440.3,19.2);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#FFFFFF").s().p("AAAAoIgFgEQgBgEAAgJIAAgiIgIAAIAAgIIAIAAIAAgPIAJgHIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEgBIACAKIgIABQgGAAgBgCg");
	this.shape_143.setTransform(435.4,18.2);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#FFFFFF").s().p("AAAAoIgFgEQgBgEAAgJIAAgiIgIAAIAAgIIAIAAIAAgPIAJgHIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEgBIACAKIgIABQgGAAgBgCg");
	this.shape_144.setTransform(432,18.2);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgEACgEQACgEAEgCIAHgCIAJgBQALgCAGgCIAAgDQAAgGgDgDQgEgEgHABQgGAAgEACQgDADgCAHIgKgCQABgGADgEQAEgEAFgDQAGgCAGAAQAJAAAEACQAGABACADQADAEABADIAAAKIAAANIABASQAAAEACAEIgLAAIgCgIQgGAFgFACQgEACgGAAQgKAAgGgFgAgBAEQgGAAgDABQgDACgCACQgBACAAADQAAAEADADQAEACAFABQAEgBAGgCQAEgDACgEQACgEAAgHIAAgDIgPAEg");
	this.shape_145.setTransform(427,19.2);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgIAEgIQADgIAHgEQAHgDAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgDgFAAQgGAAgFAFQgFAGAAALQAAAMAFAFQAFAGAGAAQAGAAAEgDQAEgFABgIIALACQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_146.setTransform(420.8,19.2);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgBQABAGAEADQAEADAFAAQAIAAADgCQAEgEgBgDQAAgEgDgDIgKgDIgOgEQgFgBgCgEQgCgDgBgFQAAgEACgEIAGgGIAHgDIAIgBQAGAAAFACQAGACADAEQACADABAHIgLABQAAgFgDgDQgEgCgFAAQgGAAgDACQgDADgBADIACAEIAEACIAIADIAPAEQAEABADADQADAEAAAFQgBAGgDAEQgDAFgGADQgGACgGAAQgLAAgGgFg");
	this.shape_147.setTransform(414.6,19.2);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#FFFFFF").s().p("AAAAoIgFgEQgBgEAAgJIAAgiIgIAAIAAgIIAIAAIAAgPIAJgHIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEgBIACAKIgIABQgGAAgBgCg");
	this.shape_148.setTransform(406.7,18.2);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#FFFFFF").s().p("AgaArIAAhUIAKAAIAAAIQADgFAEgCQAFgCAEAAQAIAAAGAEQAGAEADAHQADAIAAAJQAAAJgDAGQgEAIgGAEQgGAEgHAAQgEAAgEgDQgEgCgDgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAFAGAGAAQAFAAAGgGQAEgGAAgKQABgMgFgGQgFgGgGAAQgGAAgFAGg");
	this.shape_149.setTransform(401.8,20.3);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_150.setTransform(395,19.2);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgIAEgIQADgIAHgEQAHgDAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgDgFAAQgGAAgFAFQgFAGAAALQAAAMAFAFQAFAGAGAAQAGAAAEgDQAEgFABgIIALACQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_151.setTransform(388.8,19.2);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#FFFFFF").s().p("AAPAfIgMgUIgDgFIgPAZIgNAAIAXgfIgVgdIANAAIAKAOIADAHIAEgHIAKgOIANAAIgWAcIAYAgg");
	this.shape_152.setTransform(382.7,19.2);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_153.setTransform(376.3,19.2);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgDgKIALgBQABAGAEADQAEADAFAAQAHAAAEgCQADgEABgDQgBgEgCgDIgMgDIgNgEQgFgBgCgEQgCgDAAgFQAAgEACgEIAEgGIAHgDIAJgBQAFAAAHACQAFACADAEQACADABAHIgKABQgBgFgEgDQgDgCgFAAQgGAAgDACQgEADABADIABAEIAEACIAHADIAQAEQAEABADADQADAEgBAFQAAAGgCAEQgEAFgFADQgHACgHAAQgKAAgGgFg");
	this.shape_154.setTransform(366.6,19.2);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_155.setTransform(360.3,19.2);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_156.setTransform(353.7,19.1);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#FFFFFF").s().p("AgEArIAAg8IAJAAIAAA8gAgEgdIAAgNIAJAAIAAANg");
	this.shape_157.setTransform(349,18);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FFFFFF").s().p("AgEArIAAhVIAJAAIAABVg");
	this.shape_158.setTransform(346.3,18);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#FFFFFF").s().p("AgMAnQgHgEgDgHQgDgIgBgJQAAgKAEgFQADgIAGgEQAGgEAHAAQAEAAAFADQAEACADAEIAAgfIAKAAIAABUIgJAAIAAgIQgGAJgLAAQgGAAgGgEgAgKgFQgFAFAAALQABAMAEAGQAFAFAFAAQAIAAAEgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_159.setTransform(338.2,18.1);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_160.setTransform(331.7,19.2);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#FFFFFF").s().p("AAAAoIgFgEQgBgEAAgJIAAgiIgIAAIAAgIIAIAAIAAgPIAJgHIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEgBIACAKIgIABQgGAAgBgCg");
	this.shape_161.setTransform(326.8,18.2);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgIAEgIQADgIAHgEQAHgDAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgDgFAAQgGAAgFAFQgFAGAAALQAAAMAFAFQAFAGAGAAQAGAAAEgDQAEgFABgIIALACQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_162.setTransform(322.2,19.2);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_163.setTransform(315.7,19.2);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#FFFFFF").s().p("AgEArIAAhVIAJAAIAABVg");
	this.shape_164.setTransform(311,18);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_165.setTransform(306.4,19.2);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgDgKIALgBQABAGAEADQAEADAFAAQAHAAAEgCQAEgEAAgDQgBgEgDgDIgLgDIgOgEQgEgBgCgEQgCgDgBgFQAAgEACgEIAGgGIAHgDIAIgBQAFAAAGACQAGACACAEQADADABAHIgLABQAAgFgDgDQgDgCgGAAQgGAAgDACQgEADAAADIACAEIAEACIAHADIAQAEQAFABACADQACAEABAFQAAAGgEAEQgDAFgGADQgFACgIAAQgKAAgGgFg");
	this.shape_166.setTransform(300.1,19.2);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_167.setTransform(290.4,19.1);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAJQAJAHAAAPQAAALgEAGQgEAIgHADQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAFQAGAGAGAAQAHAAAFgGQAGgFAAgMQAAgKgGgHQgFgFgHAAQgGAAgGAFg");
	this.shape_168.setTransform(283.8,19.2);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_169.setTransform(273.8,19.2);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_170.setTransform(268.9,19.1);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#FFFFFF").s().p("AgVAbQgGgFAAgIQAAgEACgEQACgEADgCIAIgCIAKgBQAKgCAGgCIAAgDQAAgGgDgDQgEgEgHABQgGAAgEACQgEADgBAHIgLgCQACgGAEgEQACgEAHgDQAFgCAGAAQAJAAAFACQAEABADADQACAEABADIABAKIAAANIAAASQABAEACAEIgLAAIgCgIQgGAFgFACQgEACgGAAQgKAAgFgFgAgBAEQgHAAgDABQgCACgBACQgCACAAADQAAAEAEADQACACAHABQAEgBAEgCQAFgDADgEQABgEAAgHIAAgDIgPAEg");
	this.shape_171.setTransform(263.1,19.2);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgBQABAGAEADQAEADAFAAQAIAAADgCQAEgEgBgDQAAgEgCgDIgLgDIgOgEQgFgBgCgEQgDgDABgFQAAgEABgEIAGgGIAGgDIAJgBQAFAAAHACQAFACADAEQACADABAHIgLABQAAgFgDgDQgEgCgFAAQgGAAgDACQgDADgBADIACAEIAEACIAIADIAPAEQAEABADADQACAEAAAFQAAAGgDAEQgDAFgGADQgFACgHAAQgLAAgGgFg");
	this.shape_172.setTransform(253.4,19.2);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_173.setTransform(247.1,19.2);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#FFFFFF").s().p("AgaAfIAAgJIAmgrIgMAAIgYAAIAAgIIAxAAIAAAGIgfAlIgHAIIANAAIAbAAIAAAJg");
	this.shape_174.setTransform(240.8,19.2);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#FFFFFF").s().p("AgEArIAAg8IAJAAIAAA8gAgEgdIAAgNIAJAAIAAANg");
	this.shape_175.setTransform(236.5,18);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_176.setTransform(233.6,19.1);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#FFFFFF").s().p("AgaArIAAhUIAKAAIAAAIQADgFAEgCQAFgCAEAAQAIAAAGAEQAGAEADAHQADAIAAAJQABAJgEAGQgDAIgHAEQgGAEgHAAQgEAAgEgDQgFgCgCgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAFAGAGAAQAFAAAGgGQAEgGAAgKQAAgMgEgGQgFgGgGAAQgGAAgFAGg");
	this.shape_177.setTransform(228,20.3);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#FFFFFF").s().p("AgEArIAAhVIAJAAIAABVg");
	this.shape_178.setTransform(219.8,18);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#FFFFFF").s().p("AgEArIAAhVIAJAAIAABVg");
	this.shape_179.setTransform(217.1,18);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#FFFFFF").s().p("AAbArIgKgaIgiAAIgJAaIgNAAIAhhVIAKAAIAjBVgAgFgQIgJAXIAbAAIgJgWIgEgSQgCAJgDAIg");
	this.shape_180.setTransform(211.9,18);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#FFFFFF").s().p("AgFAGIAAgLIALAAIAAALg");
	this.shape_181.setTransform(202.9,21.7);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_182.setTransform(197.8,19.1);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_183.setTransform(191.2,19.2);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgIQAIgJALAAQAMAAAIAJQAIAIAAAOIAAACIgsAAQAAAKAFAGQAGAFAFAAQAGAAAEgDQAEgDADgGIALABQgDAKgHAEQgHAGgLAAQgLAAgIgIgAgKgSQgFAGgBAIIAhAAQgBgIgDgFQgFgFgIAAQgFAAgFAEg");
	this.shape_184.setTransform(184.5,19.2);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_185.setTransform(179.7,19.1);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgIAEgIQADgIAHgEQAHgDAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgDgFAAQgGAAgFAFQgFAGAAALQAAAMAFAFQAFAGAGAAQAGAAAEgDQAEgFABgIIALACQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_186.setTransform(174.4,19.2);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgDgKIALgBQABAGAEADQAEADAFAAQAHAAAEgCQADgEAAgDQAAgEgCgDIgMgDIgNgEQgFgBgCgEQgCgDAAgFQAAgEACgEIAEgGIAHgDIAJgBQAFAAAHACQAFACADAEQACADABAHIgKABQgBgFgEgDQgDgCgFAAQgGAAgDACQgEADABADIABAEIAEACIAHADIAQAEQAEABADADQADAEgBAFQAAAGgCAEQgEAFgFADQgHACgHAAQgKAAgGgFg");
	this.shape_187.setTransform(168.2,19.2);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_188.setTransform(158.5,19.1);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAJQAJAHAAAPQAAALgEAGQgEAIgHADQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAFQAGAGAGAAQAHAAAFgGQAGgFAAgMQAAgKgGgHQgFgFgHAAQgGAAgGAFg");
	this.shape_189.setTransform(151.9,19.2);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_190.setTransform(141.9,19.1);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAJQAJAHAAAPQAAALgEAGQgEAIgHADQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAFQAGAGAGAAQAHAAAFgGQAGgFAAgMQAAgKgGgHQgFgFgHAAQgGAAgGAFg");
	this.shape_191.setTransform(135.2,19.2);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#FFFFFF").s().p("AgEArIAAg8IAJAAIAAA8gAgEgdIAAgNIAJAAIAAANg");
	this.shape_192.setTransform(130.6,18);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#FFFFFF").s().p("AAAAoIgFgEQgBgEAAgJIAAgiIgIAAIAAgIIAIAAIAAgPIAJgHIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEgBIACAKIgIABQgGAAgBgCg");
	this.shape_193.setTransform(127.6,18.2);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#FFFFFF").s().p("AgEArIAAg8IAJAAIAAA8gAgEgdIAAgNIAJAAIAAANg");
	this.shape_194.setTransform(124.6,18);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgBQABAGAEADQAEADAFAAQAIAAADgCQAEgEgBgDQAAgEgCgDIgLgDIgOgEQgFgBgCgEQgDgDABgFQAAgEABgEIAGgGIAGgDIAJgBQAFAAAHACQAFACADAEQACADABAHIgKABQgBgFgDgDQgEgCgFAAQgGAAgDACQgDADgBADIACAEIAEACIAIADIAPAEQAEABADADQACAEAAAFQAAAGgDAEQgDAFgGADQgFACgHAAQgLAAgGgFg");
	this.shape_195.setTransform(120.2,19.2);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAJQAJAHAAAPQAAALgEAGQgEAIgHADQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAFQAGAGAGAAQAHAAAFgGQAGgFAAgMQAAgKgGgHQgFgFgHAAQgGAAgGAFg");
	this.shape_196.setTransform(113.9,19.2);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#FFFFFF").s().p("AgZArIAAhUIAJAAIAAAIQADgFAFgCQAEgCAEAAQAIAAAGAEQAGAEAEAHQADAIAAAJQgBAJgDAGQgEAIgGAEQgHAEgGAAQgEAAgEgDQgEgCgDgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAEAGAHAAQAFAAAFgGQAFgGABgKQAAgMgFgGQgFgGgGAAQgGAAgFAGg");
	this.shape_197.setTransform(107.5,20.3);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#FFFFFF").s().p("AgWAqIgBgKIAGABIAGgBIADgEIADgIIABgCIgXg8IALAAIAOAkIACAMIAEgMIANgkIALAAIgXA9IgFAOQgCAFgEADQgEACgFAAIgHgBg");
	this.shape_198.setTransform(97.7,20.5);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_199.setTransform(91.3,19.1);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgEACgEQACgEAEgCIAHgCIAKgBQAKgCAGgCIAAgDQAAgGgDgDQgEgEgHABQgGAAgEACQgDADgCAHIgKgCQABgGAEgEQACgEAHgDQAGgCAFAAQAIAAAGACQAFABACADQADAEABADIAAAKIAAANIABASQAAAEACAEIgLAAIgCgIQgGAFgFACQgEACgGAAQgKAAgGgFgAgBAEQgGAAgDABQgDACgCACQgBACAAADQAAAEADADQAEACAFABQAEgBAGgCQAEgDACgEQACgEAAgHIAAgDIgPAEg");
	this.shape_200.setTransform(84.6,19.2);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#FFFFFF").s().p("AAAAoIgFgEQgBgEAAgJIAAgiIgIAAIAAgIIAIAAIAAgPIAJgHIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEgBIACAKIgIABQgGAAgBgCg");
	this.shape_201.setTransform(76.3,18.2);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgEACgEQACgEADgCIAIgCIAJgBQALgCAGgCIAAgDQAAgGgDgDQgEgEgHABQgGAAgEACQgEADgBAHIgKgCQABgGADgEQADgEAGgDQAHgCAGAAQAHAAAFACQAFABADADQACAEABADIABAKIAAANIABASQAAAEACAEIgLAAIgCgIQgGAFgFACQgEACgGAAQgKAAgGgFgAgBAEQgGAAgEABQgCACgBACQgCACAAADQAAAEADADQAEACAFABQAFgBAEgCQAFgDACgEQACgEAAgHIAAgDIgPAEg");
	this.shape_202.setTransform(71.3,19.2);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#FFFFFF").s().p("AgWAqIgBgKIAGABIAGgBIAEgEIACgIIABgCIgXg8IALAAIAOAkIACAMIAEgMIAOgkIAKAAIgXA9IgFAOQgCAFgEADQgEACgFAAIgHgBg");
	this.shape_203.setTransform(61.7,20.5);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgEACgEQACgEAEgCIAHgCIAJgBQALgCAGgCIAAgDQAAgGgDgDQgEgEgHABQgGAAgEACQgDADgCAHIgKgCQABgGADgEQAEgEAFgDQAGgCAGAAQAJAAAEACQAGABACADQADAEABADIAAAKIAAANIABASQAAAEACAEIgLAAIgCgIQgGAFgFACQgEACgGAAQgKAAgGgFgAgBAEQgGAAgDABQgDACgCACQgBACAAADQAAAEADADQAEACAFABQAEgBAGgCQAEgDACgEQACgEAAgHIAAgDIgPAEg");
	this.shape_204.setTransform(55.3,19.2);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#FFFFFF").s().p("AgaArIAAhUIAKAAIAAAIQAEgFADgCQAFgCAEAAQAIAAAGAEQAGAEAEAHQACAIAAAJQABAJgEAGQgDAIgHAEQgGAEgHAAQgEAAgEgDQgFgCgCgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAFAGAGAAQAGAAAEgGQAGgGgBgKQAAgMgEgGQgFgGgGAAQgFAAgGAGg");
	this.shape_205.setTransform(48.8,20.3);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#FFFFFF").s().p("AgQAbQgHgFgCgKIALgBQABAGAEADQAEADAFAAQAHAAAEgCQADgEABgDQAAgEgDgDIgMgDIgNgEQgFgBgCgEQgCgDAAgFQAAgEACgEIAEgGIAHgDIAJgBQAGAAAGACQAFACACAEQADADABAHIgKABQgBgFgEgDQgDgCgFAAQgGAAgDACQgEADABADIABAEIAEACIAHADIAQAEQAEABADADQADAEgBAFQAAAGgCAEQgEAFgFADQgHACgHAAQgKAAgGgFg");
	this.shape_206.setTransform(38.9,19.2);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#FFFFFF").s().p("AgEArIAAhVIAJAAIAABVg");
	this.shape_207.setTransform(34.6,18);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAJQAJAHAAAPQAAALgEAGQgEAIgHADQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAFQAGAGAGAAQAHAAAFgGQAGgFAAgMQAAgKgGgHQgFgFgHAAQgGAAgGAFg");
	this.shape_208.setTransform(30,19.2);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#FFFFFF").s().p("AgQAiIAAAIIgKAAIAAhUIALAAIAAAeQAGgIAJAAQAFAAAGACQAFADADAEQAEAEACAGQABAEAAAHQABAQgJAIQgHAJgLAAQgKAAgGgJgAgLgFQgFAFAAAKQAAALADAFQAFAIAIAAQAGAAAEgFQAGgHAAgLQAAgLgGgFQgEgGgGAAQgGAAgFAGg");
	this.shape_209.setTransform(23.5,18.1);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#FFFFFF").s().p("AAfAfIAAglIgBgJIgDgFQgDgBgEAAQgGAAgEAEQgFAEAAAKIAAAiIgJAAIAAgmQABgHgDgEQgDgDgFAAQgFAAgEACQgEACgCAFQgBAEAAAIIAAAfIgKAAIAAg8IAJAAIAAAJQADgFAFgDQAFgCAFAAQAIAAAEACQACADACAGQAIgLALAAQAKAAAFAFQAEAFAAALIAAAog");
	this.shape_210.setTransform(15,19.1);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#FFFFFF").s().p("AgVAqIgBgKIAFABIAGgBIADgEIAEgIIABgCIgYg8IALAAIAOAkIACAMIAEgMIANgkIALAAIgXA9IgFAOQgDAFgDADQgEACgFAAIgGgBg");
	this.shape_211.setTransform(7.1,20.5);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgBQABAGAEADQAEADAFAAQAIAAADgCQAEgEgBgDQAAgEgCgDIgLgDIgOgEQgFgBgCgEQgDgDABgFQAAgEABgEIAGgGIAGgDIAJgBQAFAAAHACQAFACADAEQACADABAHIgKABQgBgFgDgDQgEgCgFAAQgGAAgDACQgDADgBADIACAEIAEACIAIADIAPAEQAEABADADQACAEAAAFQAAAGgDAEQgDAFgGADQgFACgHAAQgLAAgGgFg");
	this.shape_212.setTransform(1,19.2);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_213.setTransform(562.9,5.8);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAFQgHAFgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_214.setTransform(557.1,5.9);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_215.setTransform(552.1,4.8);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_216.setTransform(548.8,4.8);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#FFFFFF").s().p("AgVAbQgGgFAAgIQAAgFACgDQACgEAEgCIAHgCIAKgCQAKgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgDADgCAHIgLgCQACgGAEgFQACgEAHgCQAGgCAFAAQAIAAAGACQAFACACACQACAEACAEIAAAJIAAANIAAASQABAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgFgFgAgBADQgGABgDACQgDAAgCADQgBACAAADQAAAEAEADQACADAHAAQADAAAGgDQAEgCADgFQABgEAAgHIAAgDIgPADg");
	this.shape_217.setTransform(543.7,5.9);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgJAEgHQADgIAHgDQAHgEAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgEgFAAQgGABgFAFQgFAGAAALQAAAMAFAGQAFAFAGAAQAGAAAEgEQAEgEABgHIALABQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_218.setTransform(537.6,5.9);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#FFFFFF").s().p("AgQAoQgIgDgEgHQgFgGAAgJIALgBQABAGACAEQADAFAGACQAGADAFAAQAHAAAFgCQAFgCACgDQADgEAAgEQAAgEgDgDQgCgDgGgCIgNgEQgMgDgEgBQgHgDgDgEQgDgGAAgFQAAgHAEgFQAEgGAHgDQAHgDAIAAQAIAAAIADQAHAEAEAFQAEAGAAAIIgLABQgBgIgFgFQgFgEgJAAQgJAAgFAEQgFADAAAGQAAAEAEADQADADAMAEQAOADAFACQAHABAEAGQAEAFAAAHQAAAHgEAGQgEAGgIADQgHAEgJAAQgKAAgIgEg");
	this.shape_219.setTransform(530.4,4.7);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#FFFFFF").s().p("AgEAFIAAgKIAKAAIAAAKg");
	this.shape_220.setTransform(521.4,8.4);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgCQABAHAEADQAEADAFAAQAIAAADgDQAEgDgBgDQAAgEgCgCIgLgEIgOgEQgFgBgCgEQgDgDABgFQAAgEABgEIAGgGIAGgDIAJgBQAFAAAHACQAFACADADQACAFABAFIgKABQgBgEgDgDQgEgDgFAAQgGAAgDADQgDACAAADIABAFIAEACIAIADIAPAFQAEAAADADQACAEAAAFQAAAGgDAEQgDAFgGACQgFADgHAAQgLAAgGgFg");
	this.shape_221.setTransform(516.7,5.9);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_222.setTransform(512.2,5.8);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAFQgHAFgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_223.setTransform(506.4,5.9);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_224.setTransform(501.4,4.8);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_225.setTransform(498.1,4.8);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEADgCIAIgCIAJgCQALgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgDADgCAHIgKgCQABgGADgFQAEgEAFgCQAGgCAHAAQAHAAAFACQAFACADACQACAEACAEIAAAJIAAANIABASQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgEACQgCAAgCADQgBACAAADQAAAEADADQADADAGAAQAFAAAEgDQAFgCACgFQACgEAAgHIAAgDIgPADg");
	this.shape_226.setTransform(493,5.9);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgJAEgHQADgIAHgDQAHgEAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgEgFAAQgGABgFAFQgFAGAAALQAAAMAFAGQAFAFAGAAQAGAAAEgEQAEgEABgHIALABQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_227.setTransform(486.9,5.9);

	this.shape_228 = new cjs.Shape();
	this.shape_228.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgCQABAHAEADQAEADAFAAQAIAAADgDQAEgDgBgDQAAgEgCgCIgLgEIgOgEQgFgBgCgEQgDgDABgFQAAgEABgEIAGgGIAGgDIAJgBQAFAAAHACQAFACADADQACAFABAFIgLABQAAgEgDgDQgEgDgFAAQgGAAgDADQgDACAAADIABAFIAEACIAIADIAPAFQAEAAADADQACAEAAAFQAAAGgDAEQgDAFgGACQgFADgHAAQgLAAgGgFg");
	this.shape_228.setTransform(480.7,5.9);

	this.shape_229 = new cjs.Shape();
	this.shape_229.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_229.setTransform(472.7,4.8);

	this.shape_230 = new cjs.Shape();
	this.shape_230.graphics.f("#FFFFFF").s().p("AgaArIAAhUIAKAAIAAAIQADgFAEgCQAFgCAEAAQAIAAAGAEQAGAEADAHQADAIAAAJQABAJgEAGQgDAIgHAEQgGAEgHAAQgEAAgEgDQgFgCgCgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAFAGAGAAQAFAAAGgGQAEgGAAgKQAAgMgEgGQgFgGgGAAQgGAAgFAGg");
	this.shape_230.setTransform(467.9,7);

	this.shape_231 = new cjs.Shape();
	this.shape_231.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAFQgHAFgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_231.setTransform(461,5.9);

	this.shape_232 = new cjs.Shape();
	this.shape_232.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgJAEgHQADgIAHgDQAHgEAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgEgFAAQgGABgFAFQgFAGAAALQAAAMAFAGQAFAFAGAAQAGAAAEgEQAEgEABgHIALABQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_232.setTransform(454.9,5.9);

	this.shape_233 = new cjs.Shape();
	this.shape_233.graphics.f("#FFFFFF").s().p("AAPAeIgMgTIgDgFIgPAYIgNAAIAXgeIgVgdIANAAIAKAOIADAHIAEgHIAKgOIANAAIgWAdIAYAeg");
	this.shape_233.setTransform(448.7,5.9);

	this.shape_234 = new cjs.Shape();
	this.shape_234.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAFQgHAFgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_234.setTransform(442.4,5.9);

	this.shape_235 = new cjs.Shape();
	this.shape_235.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_235.setTransform(434.1,4.8);

	this.shape_236 = new cjs.Shape();
	this.shape_236.graphics.f("#FFFFFF").s().p("AAOAqIAAgnQAAgGgDgDQgEgEgGAAQgDAAgEADQgEACgCAEQgBACAAAIIAAAhIgLAAIAAhTIALAAIAAAeQAHgIAJAAQAHAAAFADQAFADADAEQACAFAAAHIAAAng");
	this.shape_236.setTransform(429,4.7);

	this.shape_237 = new cjs.Shape();
	this.shape_237.graphics.f("#FFFFFF").s().p("AgRAnQgHgFAAgLIAKACQABAFADACQAEADAGAAQAGAAAEgDQAEgDABgFIABgNQgHAHgJAAQgMABgHgKQgHgJAAgLQAAgJADgIQAEgHAGgEQAGgEAHAAQAKAAAHAJIAAgHIAKAAIAAAzQAAAOgDAHQgDAFgGAEQgHAEgIAAQgKAAgHgFgAgKgdQgFAGAAAMQAAALAFAEQAFAGAFAAQAHAAAFgGQAFgEAAgLQAAgMgFgGQgFgFgHgBQgFABgFAFg");
	this.shape_237.setTransform(422.2,7.1);

	this.shape_238 = new cjs.Shape();
	this.shape_238.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_238.setTransform(417.7,4.7);

	this.shape_239 = new cjs.Shape();
	this.shape_239.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_239.setTransform(414.9,5.8);

	this.shape_240 = new cjs.Shape();
	this.shape_240.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_240.setTransform(405.7,5.9);

	this.shape_241 = new cjs.Shape();
	this.shape_241.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_241.setTransform(400.8,4.8);

	this.shape_242 = new cjs.Shape();
	this.shape_242.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_242.setTransform(394.1,4.8);

	this.shape_243 = new cjs.Shape();
	this.shape_243.graphics.f("#FFFFFF").s().p("AgHArIAAg0IgKAAIAAgIIAKAAIAAgHIABgJQABgEAEgDQACgCAHAAIAKABIgCAJIgGAAQgFAAgCACQgCACAAAFIAAAGIAMAAIAAAIIgMAAIAAA0g");
	this.shape_243.setTransform(390.9,4.6);

	this.shape_244 = new cjs.Shape();
	this.shape_244.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAFQgHAFgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_244.setTransform(385.7,5.9);

	this.shape_245 = new cjs.Shape();
	this.shape_245.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_245.setTransform(381,4.7);

	this.shape_246 = new cjs.Shape();
	this.shape_246.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgCQABAHAEADQAEADAFAAQAHAAAEgDQADgDAAgDQAAgEgDgCIgKgEIgPgEQgEgBgCgEQgCgDgBgFQAAgEACgEIAGgGIAHgDIAIgBQAGAAAFACQAGACACADQADAFABAFIgLABQAAgEgDgDQgDgDgGAAQgGAAgDADQgDACgBADIACAFIAEACIAIADIAPAFQAEAAADADQADAEAAAFQAAAGgEAEQgDAFgGACQgGADgGAAQgLAAgGgFg");
	this.shape_246.setTransform(373.3,5.9);

	this.shape_247 = new cjs.Shape();
	this.shape_247.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_247.setTransform(367,5.8);

	this.shape_248 = new cjs.Shape();
	this.shape_248.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_248.setTransform(360.4,5.9);

	this.shape_249 = new cjs.Shape();
	this.shape_249.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_249.setTransform(355.7,4.7);

	this.shape_250 = new cjs.Shape();
	this.shape_250.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_250.setTransform(352.8,4.8);

	this.shape_251 = new cjs.Shape();
	this.shape_251.graphics.f("#FFFFFF").s().p("AgVAbQgGgFAAgIQAAgFACgDQACgEAEgCIAHgCIAKgCQAKgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgEADgBAHIgLgCQACgGAEgFQACgEAHgCQAFgCAGAAQAJAAAFACQAEACADACQACAEABAEIABAJIAAANIAAASQABAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgFgFgAgBADQgHABgDACQgCAAgBADQgCACAAADQAAAEAEADQACADAHAAQAEAAAEgDQAFgCADgFQABgEAAgHIAAgDIgPADg");
	this.shape_251.setTransform(347.7,5.9);

	this.shape_252 = new cjs.Shape();
	this.shape_252.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_252.setTransform(341.1,5.8);

	this.shape_253 = new cjs.Shape();
	this.shape_253.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_253.setTransform(336.4,4.7);

	this.shape_254 = new cjs.Shape();
	this.shape_254.graphics.f("#FFFFFF").s().p("AgQAiIAAAIIgKAAIAAhUIALAAIAAAeQAGgIAJAAQAFAAAGACQAFADAEAEQADAEABAGQACAEAAAHQAAAQgIAIQgHAJgLAAQgJAAgHgJgAgLgFQgFAFAAAKQAAALADAFQAFAIAIAAQAFAAAGgFQAEgHAAgLQAAgLgEgFQgFgGgGAAQgGAAgFAGg");
	this.shape_254.setTransform(332,4.7);

	this.shape_255 = new cjs.Shape();
	this.shape_255.graphics.f("#FFFFFF").s().p("AAfAfIAAglIgBgJQgBgDgCgCQgDgBgEAAQgHAAgEAEQgEAEAAAKIAAAiIgJAAIAAgmQAAgHgCgEQgDgDgGAAQgEAAgEACQgEACgBAFQgCAEAAAIIAAAfIgLAAIAAg8IAKAAIAAAJQADgFAEgDQAFgCAGAAQAIAAAEACQACADACAGQAIgLALAAQAKAAAEAFQAFAFABALIAAAog");
	this.shape_255.setTransform(323.5,5.8);

	this.shape_256 = new cjs.Shape();
	this.shape_256.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_256.setTransform(315.1,5.9);

	this.shape_257 = new cjs.Shape();
	this.shape_257.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgJAEgHQADgIAHgDQAHgEAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgEgFAAQgGABgFAFQgFAGAAALQAAAMAFAGQAFAFAGAAQAGAAAEgEQAEgEABgHIALABQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_257.setTransform(309,5.9);

	this.shape_258 = new cjs.Shape();
	this.shape_258.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_258.setTransform(300.9,5.8);

	this.shape_259 = new cjs.Shape();
	this.shape_259.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_259.setTransform(295.1,5.9);

	this.shape_260 = new cjs.Shape();
	this.shape_260.graphics.f("#FFFFFF").s().p("AgHArIAAg0IgKAAIAAgIIAKAAIAAgHIABgJQABgEAEgDQACgCAHAAIAKABIgCAJIgGAAQgFAAgCACQgCACAAAFIAAAGIAMAAIAAAIIgMAAIAAA0g");
	this.shape_260.setTransform(290.4,4.6);

	this.shape_261 = new cjs.Shape();
	this.shape_261.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAFQgHAFgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_261.setTransform(281.8,5.9);

	this.shape_262 = new cjs.Shape();
	this.shape_262.graphics.f("#FFFFFF").s().p("AgQAbQgHgFgCgKIALgCQABAHAEADQAEADAFAAQAIAAADgDQAEgDAAgDQAAgEgEgCIgLgEIgOgEQgEgBgCgEQgDgDAAgFQAAgEADgEIAEgGIAIgDIAIgBQAFAAAGACQAGACACADQADAFABAFIgLABQAAgEgEgDQgCgDgGAAQgGAAgDADQgEACAAADIACAFIAEACIAHADIAQAFQAFAAACADQACAEABAFQAAAGgDAEQgEAFgFACQgGADgIAAQgKAAgGgFg");
	this.shape_262.setTransform(275.4,5.9);

	this.shape_263 = new cjs.Shape();
	this.shape_263.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEAEgCIAHgCIAKgCQAKgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgDADgCAHIgLgCQACgGAEgFQACgEAHgCQAFgCAGAAQAIAAAGACQAFACACACQADAEABAEIAAAJIAAANIABASQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgDACQgDAAgCADQgBACAAADQAAAEADADQAEADAFAAQAEAAAGgDQAEgCACgFQACgEAAgHIAAgDIgPADg");
	this.shape_263.setTransform(269.1,5.9);

	this.shape_264 = new cjs.Shape();
	this.shape_264.graphics.f("#FFFFFF").s().p("AgQAbQgHgFgCgKIALgCQABAHAEADQAEADAFAAQAHAAAEgDQADgDABgDQAAgEgDgCIgMgEIgNgEQgFgBgCgEQgCgDAAgFQAAgEACgEIAEgGIAHgDIAJgBQAGAAAGACQAFACACADQADAFABAFIgKABQgBgEgEgDQgDgDgFAAQgGAAgDADQgEACABADIABAFIAEACIAHADIAQAFQAEAAADADQADAEgBAFQAAAGgCAEQgEAFgFACQgHADgHAAQgKAAgGgFg");
	this.shape_264.setTransform(259.4,5.9);

	this.shape_265 = new cjs.Shape();
	this.shape_265.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAFQgHAFgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_265.setTransform(253.1,5.9);

	this.shape_266 = new cjs.Shape();
	this.shape_266.graphics.f("#FFFFFF").s().p("AgaAeIAAgIIAmgsIgMABIgYAAIAAgIIAxAAIAAAGIggAmIgGAHIANgBIAbAAIAAAJg");
	this.shape_266.setTransform(246.8,5.9);

	this.shape_267 = new cjs.Shape();
	this.shape_267.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_267.setTransform(242.5,4.7);

	this.shape_268 = new cjs.Shape();
	this.shape_268.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_268.setTransform(239.6,5.8);

	this.shape_269 = new cjs.Shape();
	this.shape_269.graphics.f("#FFFFFF").s().p("AgZArIAAhUIAJAAIAAAIQAEgFAEgCQAEgCAEAAQAIAAAGAEQAGAEAEAHQACAIAAAJQAAAJgDAGQgEAIgGAEQgGAEgHAAQgEAAgEgDQgEgCgDgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAEAGAHAAQAFAAAFgGQAGgGAAgKQgBgMgEgGQgFgGgGAAQgFAAgGAGg");
	this.shape_269.setTransform(234,7);

	this.shape_270 = new cjs.Shape();
	this.shape_270.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_270.setTransform(225.8,4.7);

	this.shape_271 = new cjs.Shape();
	this.shape_271.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_271.setTransform(223.1,4.7);

	this.shape_272 = new cjs.Shape();
	this.shape_272.graphics.f("#FFFFFF").s().p("AAbAqIgLgZIghAAIgKAZIgMAAIAihTIAJAAIAjBTgAgEgQIgKAXIAbAAIgJgVIgEgSQgBAIgDAIg");
	this.shape_272.setTransform(217.9,4.7);

	this.shape_273 = new cjs.Shape();
	this.shape_273.graphics.f("#FFFFFF").s().p("AgEAFIAAgKIAJAAIAAAKg");
	this.shape_273.setTransform(208.9,8.4);

	this.shape_274 = new cjs.Shape();
	this.shape_274.graphics.f("#FFFFFF").s().p("AgMAnQgHgEgDgHQgDgIgBgJQAAgKAEgFQADgIAGgEQAGgEAHAAQAEAAAFADQAEACADAEIAAgfIAKAAIAABUIgJAAIAAgIQgGAJgLAAQgGAAgGgEgAgKgFQgFAFAAALQABAMAEAGQAFAFAFAAQAIAAAEgFQAFgGAAgLQAAgMgFgFQgFgGgHAAQgFAAgFAGg");
	this.shape_274.setTransform(203.6,4.7);

	this.shape_275 = new cjs.Shape();
	this.shape_275.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_275.setTransform(197.2,5.8);

	this.shape_276 = new cjs.Shape();
	this.shape_276.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_276.setTransform(192.5,4.7);

	this.shape_277 = new cjs.Shape();
	this.shape_277.graphics.f("#FFFFFF").s().p("AAMAqIgSgfIgIAHIAAAYIgKAAIAAhTIAKAAIAAAuIAXgWIAOAAIgXAUIAZAng");
	this.shape_277.setTransform(188.6,4.7);

	this.shape_278 = new cjs.Shape();
	this.shape_278.graphics.f("#FFFFFF").s().p("AgVAbQgGgFAAgIQAAgFACgDQACgEADgCIAIgCIAKgCQAKgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgEADgBAHIgLgCQACgGAEgFQACgEAHgCQAFgCAGAAQAJAAAFACQAEACADACQACAEABAEIABAJIAAANIAAASQABAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgFgFgAgBADQgHABgCACQgDAAgBADQgCACAAADQAAAEAEADQACADAHAAQAEAAAEgDQAFgCADgFQABgEAAgHIAAgDIgPADg");
	this.shape_278.setTransform(178.5,5.9);

	this.shape_279 = new cjs.Shape();
	this.shape_279.graphics.f("#FFFFFF").s().p("AgHArIAAg0IgKAAIAAgIIAKAAIAAgHIABgJQABgEAEgDQACgCAHAAIAKABIgCAJIgGAAQgFAAgCACQgCACAAAFIAAAGIAMAAIAAAIIgMAAIAAA0g");
	this.shape_279.setTransform(170.4,4.6);

	this.shape_280 = new cjs.Shape();
	this.shape_280.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_280.setTransform(165.2,5.9);

	this.shape_281 = new cjs.Shape();
	this.shape_281.graphics.f("#FFFFFF").s().p("AgQAbQgHgFgCgKIALgCQABAHAEADQAEADAFAAQAHAAAEgDQADgDABgDQAAgEgDgCIgMgEIgNgEQgFgBgCgEQgCgDAAgFQAAgEACgEIAEgGIAHgDIAJgBQAGAAAGACQAFACACADQADAFABAFIgKABQgBgEgEgDQgDgDgFAAQgGAAgDADQgEACABADIABAFIAEACIAHADIAQAFQAEAAADADQADAEgBAFQAAAGgCAEQgEAFgFACQgHADgHAAQgKAAgGgFg");
	this.shape_281.setTransform(155.5,5.9);

	this.shape_282 = new cjs.Shape();
	this.shape_282.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_282.setTransform(149.2,5.8);

	this.shape_283 = new cjs.Shape();
	this.shape_283.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_283.setTransform(142.5,5.9);

	this.shape_284 = new cjs.Shape();
	this.shape_284.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_284.setTransform(137.9,4.7);

	this.shape_285 = new cjs.Shape();
	this.shape_285.graphics.f("#FFFFFF").s().p("AAAAoIgFgFQgBgDAAgJIAAgiIgIAAIAAgIIAIAAIAAgQIAJgGIAAAWIAKAAIAAAIIgKAAIAAAiIAAAGIACACIAEABIAEAAIACAJIgIABQgGAAgBgCg");
	this.shape_285.setTransform(134.9,4.8);

	this.shape_286 = new cjs.Shape();
	this.shape_286.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEADgCIAIgCIAJgCQALgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgDADgCAHIgKgCQABgGADgFQAEgEAFgCQAGgCAHAAQAHAAAFACQAFACADACQACAEACAEIAAAJIAAANIABASQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgEACQgCAAgBADQgCACAAADQAAAEADADQADADAGAAQAFAAAEgDQAFgCACgFQACgEAAgHIAAgDIgPADg");
	this.shape_286.setTransform(129.9,5.9);

	this.shape_287 = new cjs.Shape();
	this.shape_287.graphics.f("#FFFFFF").s().p("AAOAfIAAgkQAAgGgBgDQgBgDgDgCQgEgCgEAAQgFAAgEAEQgFAEAAAMIAAAgIgLAAIAAg8IAKAAIAAAJQAHgKAKAAQAGAAAFACQAFACACADQACADABAFIABAKIAAAkg");
	this.shape_287.setTransform(123.2,5.8);

	this.shape_288 = new cjs.Shape();
	this.shape_288.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_288.setTransform(118.6,4.7);

	this.shape_289 = new cjs.Shape();
	this.shape_289.graphics.f("#FFFFFF").s().p("AgQAiIAAAIIgJAAIAAhUIAKAAIAAAeQAHgIAIAAQAGAAAFACQAFADADAEQADAEADAGQACAEAAAHQgBAQgHAIQgIAJgLAAQgJAAgHgJgAgLgFQgFAFAAAKQAAALADAFQAFAIAIAAQAGAAAEgFQAFgHABgLQgBgLgFgFQgEgGgGAAQgGAAgFAGg");
	this.shape_289.setTransform(114.1,4.7);

	this.shape_290 = new cjs.Shape();
	this.shape_290.graphics.f("#FFFFFF").s().p("AAfAfIAAglIgBgJQgBgDgDgCQgCgBgEAAQgGAAgFAEQgEAEAAAKIAAAiIgIAAIAAgmQgBgHgCgEQgDgDgFAAQgFAAgEACQgDACgDAFQgBAEAAAIIAAAfIgKAAIAAg8IAJAAIAAAJQADgFAFgDQAEgCAHAAQAGAAAFACQADADACAGQAGgLANAAQAJAAAEAFQAGAFgBALIAAAog");
	this.shape_290.setTransform(105.6,5.8);

	this.shape_291 = new cjs.Shape();
	this.shape_291.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_291.setTransform(97.3,5.9);

	this.shape_292 = new cjs.Shape();
	this.shape_292.graphics.f("#FFFFFF").s().p("AgSAYQgIgJAAgPQAAgJAEgHQADgIAHgDQAHgEAGAAQAKAAAGAFQAHAFACAKIgLABQgBgGgEgDQgEgEgFAAQgGABgFAFQgFAGAAALQAAAMAFAGQAFAFAGAAQAGAAAEgEQAEgEABgHIALABQgCALgHAGQgHAGgKAAQgLAAgIgIg");
	this.shape_292.setTransform(91.1,5.9);

	this.shape_293 = new cjs.Shape();
	this.shape_293.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_293.setTransform(83.1,5.8);

	this.shape_294 = new cjs.Shape();
	this.shape_294.graphics.f("#FFFFFF").s().p("AgUAYQgIgJAAgPQAAgQAKgIQAIgHAKAAQAMAAAIAIQAJAJAAAOQAAAKgEAIQgEAGgHAEQgGAEgIAAQgMAAgIgIgAgMgRQgFAGAAALQAAAMAFAGQAGAFAGAAQAHAAAFgFQAGgGAAgMQAAgKgGgHQgFgFgHgBQgGABgGAFg");
	this.shape_294.setTransform(77.3,5.9);

	this.shape_295 = new cjs.Shape();
	this.shape_295.graphics.f("#FFFFFF").s().p("AgHArIAAg0IgKAAIAAgIIAKAAIAAgHIABgJQABgEAEgDQACgCAHAAIAKABIgCAJIgGAAQgFAAgCACQgCACAAAFIAAAGIAMAAIAAAIIgMAAIAAA0g");
	this.shape_295.setTransform(72.5,4.6);

	this.shape_296 = new cjs.Shape();
	this.shape_296.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAFQgHAFgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_296.setTransform(63.9,5.9);

	this.shape_297 = new cjs.Shape();
	this.shape_297.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_297.setTransform(59.1,5.8);

	this.shape_298 = new cjs.Shape();
	this.shape_298.graphics.f("#FFFFFF").s().p("AgWAbQgFgFAAgIQAAgFACgDQACgEADgCIAIgCIAJgCQALgBAGgCIAAgCQAAgHgDgDQgEgEgHAAQgGAAgEADQgDADgCAHIgKgCQABgGADgFQADgEAGgCQAHgCAGAAQAHAAAFACQAFACADACQACAEABAEIABAJIAAANIABASQAAAEACADIgLAAIgCgHQgGAFgFACQgEACgGAAQgKAAgGgFgAgBADQgGABgEACQgCAAgBADQgCACAAADQAAAEADADQAEADAFAAQAFAAAEgDQAFgCACgFQACgEAAgHIAAgDIgPADg");
	this.shape_298.setTransform(53.3,5.9);

	this.shape_299 = new cjs.Shape();
	this.shape_299.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgKIAKgCQABAHAEADQAEADAFAAQAHAAAEgDQAEgDAAgDQgBgEgDgCIgLgEIgOgEQgEgBgCgEQgCgDgBgFQAAgEACgEIAFgGIAIgDIAIgBQAFAAAGACQAGACACADQADAFABAFIgLABQAAgEgDgDQgDgDgGAAQgGAAgDADQgEACAAADIACAFIAEACIAHADIAQAFQAFAAACADQACAEABAFQAAAGgEAEQgDAFgGACQgFADgIAAQgKAAgGgFg");
	this.shape_299.setTransform(43.6,5.9);

	this.shape_300 = new cjs.Shape();
	this.shape_300.graphics.f("#FFFFFF").s().p("AgTAYQgIgJAAgPQAAgOAIgJQAIgIALAAQAMAAAIAIQAIAJAAAOIAAACIgsAAQAAALAFAFQAGAFAFAAQAGAAAEgDQAEgDADgHIALACQgDAKgHAFQgHAFgLAAQgLAAgIgIgAgKgRQgFAFgBAHIAhAAQgBgHgDgFQgFgFgIgBQgFABgFAFg");
	this.shape_300.setTransform(37.3,5.9);

	this.shape_301 = new cjs.Shape();
	this.shape_301.graphics.f("#FFFFFF").s().p("AgaAeIAAgIIAlgsIgLABIgYAAIAAgIIAxAAIAAAGIggAmIgGAHIANgBIAbAAIAAAJg");
	this.shape_301.setTransform(31,5.9);

	this.shape_302 = new cjs.Shape();
	this.shape_302.graphics.f("#FFFFFF").s().p("AgEAqIAAg7IAJAAIAAA7gAgEgeIAAgLIAJAAIAAALg");
	this.shape_302.setTransform(26.6,4.7);

	this.shape_303 = new cjs.Shape();
	this.shape_303.graphics.f("#FFFFFF").s().p("AgPAfIAAg8IAJAAIAAAJQAEgGACgCQACgCAEAAQAFAAAFADIgDAKQgEgCgEAAQgDAAgCACQgCACgBADQgCAGAAAGIAAAfg");
	this.shape_303.setTransform(23.8,5.8);

	this.shape_304 = new cjs.Shape();
	this.shape_304.graphics.f("#FFFFFF").s().p("AgZArIAAhUIAJAAIAAAIQADgFAFgCQAEgCAEAAQAIAAAGAEQAGAEADAHQAEAIAAAJQAAAJgEAGQgDAIgHAEQgHAEgGAAQgEAAgEgDQgEgCgDgDIAAAegAgLgcQgFAGAAAMQAAALAFAFQAEAGAHAAQAGAAAFgGQAEgGAAgKQABgMgFgGQgFgGgGAAQgFAAgGAGg");
	this.shape_304.setTransform(18.2,7);

	this.shape_305 = new cjs.Shape();
	this.shape_305.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_305.setTransform(9.9,4.7);

	this.shape_306 = new cjs.Shape();
	this.shape_306.graphics.f("#FFFFFF").s().p("AgEAqIAAhTIAJAAIAABTg");
	this.shape_306.setTransform(7.3,4.7);

	this.shape_307 = new cjs.Shape();
	this.shape_307.graphics.f("#FFFFFF").s().p("AAbAqIgLgZIghAAIgKAZIgLAAIAghTIALAAIAjBTgAgEgQIgKAXIAbAAIgIgVIgFgSQgCAIgCAIg");
	this.shape_307.setTransform(2,4.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_307},{t:this.shape_306},{t:this.shape_305},{t:this.shape_304},{t:this.shape_303},{t:this.shape_302},{t:this.shape_301},{t:this.shape_300},{t:this.shape_299},{t:this.shape_298},{t:this.shape_297},{t:this.shape_296},{t:this.shape_295},{t:this.shape_294},{t:this.shape_293},{t:this.shape_292},{t:this.shape_291},{t:this.shape_290},{t:this.shape_289},{t:this.shape_288},{t:this.shape_287},{t:this.shape_286},{t:this.shape_285},{t:this.shape_284},{t:this.shape_283},{t:this.shape_282},{t:this.shape_281},{t:this.shape_280},{t:this.shape_279},{t:this.shape_278},{t:this.shape_277},{t:this.shape_276},{t:this.shape_275},{t:this.shape_274},{t:this.shape_273},{t:this.shape_272},{t:this.shape_271},{t:this.shape_270},{t:this.shape_269},{t:this.shape_268},{t:this.shape_267},{t:this.shape_266},{t:this.shape_265},{t:this.shape_264},{t:this.shape_263},{t:this.shape_262},{t:this.shape_261},{t:this.shape_260},{t:this.shape_259},{t:this.shape_258},{t:this.shape_257},{t:this.shape_256},{t:this.shape_255},{t:this.shape_254},{t:this.shape_253},{t:this.shape_252},{t:this.shape_251},{t:this.shape_250},{t:this.shape_249},{t:this.shape_248},{t:this.shape_247},{t:this.shape_246},{t:this.shape_245},{t:this.shape_244},{t:this.shape_243},{t:this.shape_242},{t:this.shape_241},{t:this.shape_240},{t:this.shape_239},{t:this.shape_238},{t:this.shape_237},{t:this.shape_236},{t:this.shape_235},{t:this.shape_234},{t:this.shape_233},{t:this.shape_232},{t:this.shape_231},{t:this.shape_230},{t:this.shape_229},{t:this.shape_228},{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4,-3.8,628.7,70.8);


(lib.shape1884 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1882 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1879 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1878 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1876 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1874 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1872 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1870 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1868 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1866 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1864 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1862 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1860 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1858 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1856 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1854 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1852 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1850 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1848 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1846 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1844 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1842 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1840 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1838 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1836 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1834 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1832 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1830 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1828 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1826 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1824 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1822 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1820 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1818 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1816 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1814 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1811 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1808 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1804 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1800 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1798 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1796 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_21"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1794 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1792 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1790 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1788 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1786 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1784 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1782 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1780 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1778 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1776 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1774 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1772 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1770 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1768 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1766 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1764 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1758 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1756 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1750 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1744 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1741 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1739 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1729 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1727 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1725 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1723 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1721 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1719 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1717 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1715 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1713 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1711 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1709 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1707 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1705 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1703 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1701 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1699 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1696 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1694 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1692 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1690 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1688 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1686 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1684 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1682 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1680 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1678 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1676 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_20"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1674 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1672 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1669 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1667 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1665 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1663 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1661 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1659 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1657 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_19"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1655 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1653 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1651 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1649 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1647 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1645 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1643 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1641 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1639 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1637 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1635 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1632 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1630 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1628 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1626 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1624 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1622 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1619 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1616 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1614 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1612 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1610 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1608 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1606 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1604 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1602 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1600 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1598 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1596 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1594 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1592 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1590 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1588 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1586 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1584 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1582 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1580 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1578 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1576 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_18"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1574 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1572 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1570 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1568 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1566 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1564 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1562 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1560 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1558 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1556 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1554 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1552 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1550 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1548 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1546 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1544 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1541 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1539 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1537 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1535 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1533 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1531 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1529 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1527 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1525 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1523 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1521 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1519 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_17"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1517 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1515 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1513 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1511 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1509 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1507 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1505 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1503 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1501 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_16"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1499 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1497 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1495 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1493 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1491 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1489 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1487 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1485 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1483 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1481 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1479 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1477 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1475 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1473 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1470 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1468 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1466 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1464 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1462 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1460 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1458 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1456 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1454 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1452 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1450 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1448 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1446 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1444 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_15"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1442 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1440 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1438 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1436 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1434 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1432 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1430 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1428 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1426 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1424 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1422 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1420 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1417 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1415 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1413 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1411 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1409 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1407 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1405 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1403 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1401 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1399 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1397 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1395 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1393 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1391 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1389 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1387 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1385 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1383 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1381 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1379 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1377 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1374 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1372 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1370 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1368 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1366 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_14"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1364 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1362 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1360 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1358 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1356 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1354 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1352 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1350 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1348 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_13"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1346 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1344 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1342 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1340 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1338 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1336 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1334 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1332 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1330 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1328 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1326 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFF00").ss(3,0,0,3).p("AHlHlIvJAAIAAvJIPJAAg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1324 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1322 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1320 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1318 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1316 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1314 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1312 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1310 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1308 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_12"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1306 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1304 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1302 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1300 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1298 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1296 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1294 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1292 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1290 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1287 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1285 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1282 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1281 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1279 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1277 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1275 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1273 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1271 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1269 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1267 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1265 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1263 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1261 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1259 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1257 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1255 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1253 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1251 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1249 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1247 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1245 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1243 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1241 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1239 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1237 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1235 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1233 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1231 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1229 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1227 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1225 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1223 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1221 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1219 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1217 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1215 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1213 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1211 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1209 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1207 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1205 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1203 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1201 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1199 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1197 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1195 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_7"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1193 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1191 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1189 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1187 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1185 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1183 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1181 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1179 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1177 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1174 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1172 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1170 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1168 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1166 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1164 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1162 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1160 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1158 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1156 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1154 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1152 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_6"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1150 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1148 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1146 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1144 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1142 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1140 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1138 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1136 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1134 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1132 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1130 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1128 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1126 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1124 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1122 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1120 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1118 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1116 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1114 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1112 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1110 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1108 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1106 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1104 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1102 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1100 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1098 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1096 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1094 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1092 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1090 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1088 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1086 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1084 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1082 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1080 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1078 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1076 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1074 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1072 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1070 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1068 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1066 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1064 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1062 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1060 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1058 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1056 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1054 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1052 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1050 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1048 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1046 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1044 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1042 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1040 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1038 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1036 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1034 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1032 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1030 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1028 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1026 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1024 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1022 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1020 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1018 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_11"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1016 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1014 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1012 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_8"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1010 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1008 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1006 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1004 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_10"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1002 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_9"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape1000 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape998 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape996 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape994 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape992 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape990 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape988 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape986 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape984 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape982 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape980 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape978 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape976 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape974 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape972 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape970 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape968 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape966 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape964 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape962 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape960 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape958 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape955 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape953 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape951 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape949 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape947 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape945 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape943 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape941 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape939 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape937 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape935 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape933 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape931 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape929 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_34"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape927 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape925 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape923 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape921 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape919 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape917 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape915 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape913 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape911 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape909 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape907 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape905 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape903 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape901 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape899 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape897 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape895 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape893 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape891 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape889 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape887 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape885 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape883 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape881 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape879 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape877 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_33"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape875 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape873 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape871 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape869 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape867 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape865 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape863 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape861 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape859 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape857 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape855 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape853 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape851 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape849 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape847 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape845 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape843 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape841 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape839 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape837 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape835 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_32"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape833 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape831 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape829 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape827 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape825 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape823 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape821 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape819 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape817 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape815 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape813 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape811 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape809 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape807 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape805 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape803 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape800 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape798 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape796 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape794 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape792 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape790 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape788 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape786 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape784 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape782 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape780 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape778 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape776 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape774 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape772 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape770 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape768 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape766 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape764 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape762 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape760 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape758 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape756 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape754 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape752 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape750 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_31"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape748 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape746 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape744 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape742 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape740 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape738 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape736 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape734 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape732 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_30"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape730 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape727 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape725 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape723 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape721 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape719 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape717 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape715 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape713 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape711 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape709 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape707 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape705 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape703 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape701 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape699 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape697 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape695 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape693 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape691 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape689 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape687 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape685 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape683 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape681 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape679 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape677 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape675 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape673 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape671 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape669 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape667 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape665 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape663 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape661 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape659 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape657 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape655 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape653 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape651 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape649 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape647 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape645 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape643 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape641 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape639 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape637 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape635 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape633 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape631 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape629 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_29"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape627 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_28"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape625 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape623 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape621 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape619 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape617 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape615 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape613 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape611 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape609 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape607 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape605 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape603 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape601 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape599 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape597 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape595 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape593 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape591 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape589 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape587 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape585 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape583 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape581 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape579 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape577 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape575 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape573 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape571 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape569 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape567 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape565 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape563 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_27"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape561 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape559 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape557 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape555 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape553 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape551 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape549 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape547 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape545 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape543 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape541 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape539 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape537 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape535 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape533 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape530 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape528 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape525 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape523 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape521 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape519 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape517 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape515 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape513 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape511 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape509 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape507 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape505 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape503 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape501 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape499 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape497 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape495 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape493 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape491 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape489 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape487 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_26"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape485 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape483 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape481 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape479 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape477 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape475 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape473 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape471 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape469 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape467 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape465 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape463 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape461 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape459 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape457 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape455 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape453 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape451 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape449 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape447 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape445 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape443 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape441 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape439 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape437 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],17), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape435 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape433 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],16), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape431 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],15), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape429 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],14), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape427 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],13), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape425 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape423 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],12), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape421 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape419 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape417 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape415 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape413 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape411 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],7), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape409 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],24), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape407 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],23), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape405 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],22), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape403 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],6), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape401 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],21), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape399 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],5), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape397 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],20), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape395 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],4), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape393 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],3), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape391 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],2), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape389 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],1), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape387 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_23"],0), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape385 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],19), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape383 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],18), null, new cjs.Matrix2D(1,0,0,1,-50,-50)).s().p("AnzHzIAAvmIPmAAIAAPmg");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape376 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#660000").ss(1,0,0,3).p("AD4FzIj4j3Ij2D3Ih8h7ID5j4Ij4j1IB8h9ID1D4ID4j4IB7B8Ij3D2ID3D3g");
	this.shape.setTransform(76.2,181.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAAB8Ij2D3Ih8h7ID5j4Ij4j1IB8h9ID1D3ID4j3IB7B7Ij3D3ID3D2Ih7B9g");
	this.shape_1.setTransform(76.3,181.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(1,0,0,3).p("AntntQDNjOEgAAQEiAADMDOQDODMAAEhQAAEhjODNQjMDOkiAAQkgAAjNjOQjOjNAAkhQAAkhDOjMg");
	this.shape_2.setTransform(80,185);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#FF0000","#660000"],[0.843,1],-3.5,-3.5,0,-3.5,-3.5,71.6).s().p("AntHuQjOjNAAkhQAAkhDOjMQDNjNEgAAQEiAADMDNQDODMAAEhQAAEhjODNQjMDNkiAAQkgAAjNjNg");
	this.shape_3.setTransform(80,185);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(9,114,142,142);


(lib.shape373 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,0,0,3).p("AAAq7QEiAADMDOQDODMAAEhQAAEhjODNQjMDOkiAAQkgAAjNjOQjOjNAAkhQAAkhDOjMQDNjOEgAAg");
	this.shape.setTransform(80,185);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["#FF0000","#660000"],[0.843,1],-3.5,-3.5,0,-3.5,-3.5,71.6).s().p("AntHuQjOjNAAkhQAAkhDOjMQDNjNEgAAQEiAADMDNQDODMAAEhQAAEhjODNQjMDNkiAAQkgAAjNjNg");
	this.shape_1.setTransform(80,185);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(9,114,142,142);


(lib.shape372 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AzXRVQh4ABABh4IAA+6QgBh4B4AAMAmvAAAQB4AAgBB4IAAe6QABB4h4gBg");
	this.shape.setTransform(405,215);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AzhR+QiWAAAAiWIAA/OQAAiWCWgBMAnDAAAQCWABAACWIAAfOQAACWiWAAg");
	this.shape_1.setTransform(405,215);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(265,100,280,230);


(lib.shape369 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,0,0,3).p("EAlMgWVMhKXAAAQgyAAAAAyMAAAArHQAAAyAyAAMBKXAAAQAyAAAAgyMAAAgrHQAAgygyAAg");
	this.shape.setTransform(320,200);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#666666").s().p("EglLAXcQh4gBABh3MAAAgrGQgBh4B4AAMBKXAAAQB3AAAAB4MAAAArGQAAB3h3ABg");
	this.shape_1.setTransform(320,200);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("EglLAYDQigAAAAifMAAAgrHQAAigCgAAMBKWAAAQChAAAACgMAAAArHQAACfihAAg");
	this.shape_2.setTransform(321,201);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(0,0,0,0.498)").s().p("EglKAYDQihAAAAigMAAAgrFQAAigChAAMBKWAAAQCgAAAACgMAAAArFQAACgigAAg");
	this.shape_3.setTransform(336,216);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(0,0,0,0.498)").s().p("Egx/AlfMAAAhK+MBj+AAAMAAABK+g");
	this.shape_4.setTransform(320,240);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.shape366 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#010101").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape.setTransform(320,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(216,277,208,113);


(lib.shape364 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 9
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape.setTransform(362.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 8
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C5IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(277.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 7
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(235.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 6
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(362.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(362.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C4IAAlwIFwAAIAAFwg");
	this.shape_5.setTransform(277.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 3
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C4IAAlwIFwAAIAAFwg");
	this.shape_6.setTransform(277.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer 2
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_7.setTransform(235.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

	// Layer 1
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_8.setTransform(235.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(217,278,164,111);


(lib.shape363 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 8
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape.setTransform(362.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 7
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C5IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(277.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 6
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(235.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(362.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 4
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C4IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(277.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 3
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C4IAAlwIFwAAIAAFwg");
	this.shape_5.setTransform(277.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_6.setTransform(235.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_7.setTransform(235.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(217,278,164,111);


(lib.shape362 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape.setTransform(362.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 6
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C5IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(277.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(235.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 4
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C4IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(277.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C4IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(277.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 2
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_5.setTransform(235.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_6.setTransform(235.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(217,278,164,111);


(lib.shape361 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape.setTransform(362.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C5IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(277.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 4
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(235.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C4IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(277.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(235.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_5.setTransform(235.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(217,278,164,111);


(lib.shape360 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape.setTransform(362.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C5IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(277.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(235.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(235.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(235.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(217,278,164,111);


(lib.shape359 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape.setTransform(362.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C5IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(277.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(235.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(235.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(217,278,164,111);


(lib.shape358 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape.setTransform(362.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai3C5IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(277.5,296.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(235.5,370.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(217,278,164,111);


(lib.shape357 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_2"],4), null, new cjs.Matrix2D(0.5,0,0,0.5,-103.5,-56.5)).s().p("AwKI0IAAxnMAgVAAAIAARng");
	this.shape.setTransform(319.5,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(216,277,207,113);


(lib.shape355 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],10), null, new cjs.Matrix2D(0.722,0,0,0.717,-19.5,-21.5)).s().p("AjCDXIAAmtIGFAAIAAGtg");
	this.shape.setTransform(284.5,148.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(265,127,39,43);


(lib.shape354 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.004)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape.setTransform(320,333.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(216,277,208,113);


(lib.shape352 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 16
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],16), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape.setTransform(18.5,-18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 15
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],1), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(18.5,18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 14
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(18.5,55.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 13
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],2), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(18.5,92.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 12
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],1), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(18.5,129.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 11
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],2), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlxIFwAAIAAFxg");
	this.shape_5.setTransform(18.5,166.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 10
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_6.setTransform(18.5,203.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer 9
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],17), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_7.setTransform(18.5,240.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

	// Layer 8
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],16), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_8.setTransform(18.5,277.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

	// Layer 7
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],15), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_9.setTransform(18.5,314.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(1));

	// Layer 6
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_10.setTransform(18.5,351.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(1));

	// Layer 5
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],4), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_11.setTransform(18.5,388.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

	// Layer 4
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],2), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_12.setTransform(18.5,425.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(1));

	// Layer 3
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],18), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_13.setTransform(18.5,462.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(1));

	// Layer 2
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],1), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlxIFwAAIAAFxg");
	this.shape_14.setTransform(18.5,499.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(1));

	// Layer 1
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],1), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_15.setTransform(18.5,536.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-37,37,592);


(lib.shape351 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 14
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],16), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape.setTransform(18.5,-18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 13
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],15), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(18.5,18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 12
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(18.5,55.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 11
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],18), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(18.5,92.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 10
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(18.5,129.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 9
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],2), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlxIFwAAIAAFxg");
	this.shape_5.setTransform(18.5,166.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 8
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_6.setTransform(18.5,203.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer 7
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],16), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_7.setTransform(18.5,240.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

	// Layer 6
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],17), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_8.setTransform(18.5,277.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

	// Layer 5
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],18), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_9.setTransform(18.5,314.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(1));

	// Layer 4
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],1), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_10.setTransform(18.5,351.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(1));

	// Layer 3
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_11.setTransform(18.5,388.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

	// Layer 2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],4), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_12.setTransform(18.5,425.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(1));

	// Layer 1
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],15), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_13.setTransform(18.5,462.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-37,37,518);


(lib.shape350 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 12
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape.setTransform(18.5,-18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 11
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],2), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(18.5,18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 10
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],4), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(18.5,55.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 9
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],17), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(18.5,92.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 8
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],18), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(18.5,129.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],4), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlxIFwAAIAAFxg");
	this.shape_5.setTransform(18.5,166.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 6
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],1), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_6.setTransform(18.5,203.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer 5
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],17), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_7.setTransform(18.5,240.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

	// Layer 4
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_8.setTransform(18.5,277.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

	// Layer 3
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],16), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_9.setTransform(18.5,314.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(1));

	// Layer 2
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],2), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_10.setTransform(18.5,351.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(1));

	// Layer 1
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_11.setTransform(18.5,388.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-37,37,444);


(lib.shape348 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],15), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape.setTransform(18.5,92.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 9
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],17), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(18.5,-18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 8
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(18.5,18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 7
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(18.5,55.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 6
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],16), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(18.5,129.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 5
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],15), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlxIFwAAIAAFxg");
	this.shape_5.setTransform(18.5,166.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 4
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_6.setTransform(18.5,203.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer 3
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],2), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_7.setTransform(18.5,240.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

	// Layer 2
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_8.setTransform(18.5,277.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

	// Layer 1
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_22"],17), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_9.setTransform(18.5,314.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-37,37,370);


(lib.shape344 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 8
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],4), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape.setTransform(18.5,-18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 7
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],1), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_1.setTransform(18.5,18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 6
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],3), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_2.setTransform(18.5,55.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_25"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_3.setTransform(18.5,92.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

	// Layer 4
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],2), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C4IAAlwIFwAAIAAFwg");
	this.shape_4.setTransform(18.5,129.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 3
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],1), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlxIFwAAIAAFxg");
	this.shape_5.setTransform(18.5,166.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],0), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_6.setTransform(18.5,203.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_24"],4), null, new cjs.Matrix2D(0.37,0,0,0.37,-18.5,-18.5)).s().p("Ai4C5IAAlwIFwAAIAAFwg");
	this.shape_7.setTransform(18.5,240.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-37,37,296);


(lib.shape335 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],4), null, new cjs.Matrix2D(0.736,0,0,0.723,-74,-23.5)).s().p("ArjDqIAAnTIXHAAIAAHTg");
	this.shape.setTransform(321,148.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Egs+AFPIAAqdMBZ9AAAIAAKdg");
	this.shape_1.setTransform(322,379.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(img.image334, null, new cjs.Matrix2D(1,0,0,1,-320,-240)).s().p("Egx/AlfMAAAhK+MBj+AAAMAAABK+g");
	this.shape_2.setTransform(320,240);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.shape330 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image329, null, new cjs.Matrix2D(1,0,0,1,-320,-240)).s().p("Egx/AlfMAAAhK+MBj+AAAMAAABK+g");
	this.shape.setTransform(320,240);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.shape312 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image311, null, new cjs.Matrix2D(1,0,0,1,-320,-240)).s().p("Egx/AlfMAAAhK+MBj+AAAMAAABK+g");
	this.shape.setTransform(320,240);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.shape308 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],20), null, new cjs.Matrix2D(1,0,0,1,-18,-17.5)).s().p("AizCvIAAldIFnAAIAAFdg");
	this.shape.setTransform(18,17.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,36,35);


(lib.shape306 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],19), null, new cjs.Matrix2D(1,0,0,1,-18,-17.5)).s().p("AizCvIAAldIFnAAIAAFdg");
	this.shape.setTransform(18,17.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,36,35);


(lib.shape304 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],18), null, new cjs.Matrix2D(1,0,0,1,-17.5,-17.5)).s().p("AiuCvIAAldIFdAAIAAFdg");
	this.shape.setTransform(17.5,17.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,35,35);


(lib.shape302 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],17), null, new cjs.Matrix2D(1,0,0,1,-17.5,-17.5)).s().p("AiuCvIAAldIFdAAIAAFdg");
	this.shape.setTransform(17.5,17.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,35,35);


(lib.shape299 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],11), null, new cjs.Matrix2D(1,0,0,1,-50,-73.5)).s().p("AnzLeIAA28IPmAAIAAW8g");
	this.shape.setTransform(50,73.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,147);


(lib.shape297 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],10), null, new cjs.Matrix2D(1,0,0,1,-50,-73.5)).s().p("AnzLeIAA28IPmAAIAAW8g");
	this.shape.setTransform(50,73.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,147);


(lib.shape295 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],9), null, new cjs.Matrix2D(1,0,0,1,-50,-73.5)).s().p("AnzLeIAA28IPmAAIAAW8g");
	this.shape.setTransform(50,73.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,147);


(lib.shape293 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],8), null, new cjs.Matrix2D(1,0,0,1,-50,-73.5)).s().p("AnzLeIAA28IPmAAIAAW8g");
	this.shape.setTransform(50,73.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,147);


(lib.shape291 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],7), null, new cjs.Matrix2D(1,0,0,1,-49.5,-73.5)).s().p("AnuLeIAA28IPdAAIAAW8g");
	this.shape.setTransform(49.5,73.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,99,147);


(lib.shape286 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],6), null, new cjs.Matrix2D(1,0,0,1,-73,-50.5)).s().p("ArZH4IAAvwIWyAAIAAPwg");
	this.shape.setTransform(73,50.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,146,101);


(lib.shape283 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],5), null, new cjs.Matrix2D(1,0,0,1,-73,-50.5)).s().p("ArZH4IAAvwIWyAAIAAPwg");
	this.shape.setTransform(73,50.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,146,101);


(lib.shape281 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image280, null, new cjs.Matrix2D(1,0,0,1,-320,-156.5)).s().p("Egx/AYcMAAAgw3MBj+AAAMAAAAw3g");
	this.shape.setTransform(320,233.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,77,640,313);


(lib.shape274 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],16), null, new cjs.Matrix2D(1,0,0,1,-27,-30)).s().p("AkNEsIAApXIIbAAIAAJXg");
	this.shape.setTransform(27,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.shape272 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],15), null, new cjs.Matrix2D(1,0,0,1,-27,-30)).s().p("AkNEsIAApXIIbAAIAAJXg");
	this.shape.setTransform(27,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.shape270 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],14), null, new cjs.Matrix2D(1,0,0,1,-27,-30)).s().p("AkNEsIAApXIIbAAIAAJXg");
	this.shape.setTransform(27,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.shape268 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],13), null, new cjs.Matrix2D(1,0,0,1,-27,-30)).s().p("AkNEsIAApXIIbAAIAAJXg");
	this.shape.setTransform(27,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.shape266 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],12), null, new cjs.Matrix2D(1,0,0,1,-27,-30)).s().p("AkNEsIAApXIIbAAIAAJXg");
	this.shape.setTransform(27,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.shape264 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],11), null, new cjs.Matrix2D(1,0,0,1,-27,-30)).s().p("AkNEsIAApXIIbAAIAAJXg");
	this.shape.setTransform(27,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.shape262 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],10), null, new cjs.Matrix2D(1,0,0,1,-27,-30)).s().p("AkNEsIAApXIIbAAIAAJXg");
	this.shape.setTransform(27,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.shape260 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],9), null, new cjs.Matrix2D(1,0,0,1,-27,-30)).s().p("AkNEsIAApXIIbAAIAAJXg");
	this.shape.setTransform(27,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.shape258 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],8), null, new cjs.Matrix2D(1,0,0,1,-27,-30)).s().p("AkNEsIAApXIIbAAIAAJXg");
	this.shape.setTransform(27,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.shape255 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],4), null, new cjs.Matrix2D(1,0,0,1,-100.5,-32.5)).s().p("AvsFEIAAqIIfYAAIAAKIg");
	this.shape.setTransform(100.5,32.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,201,65);


(lib.shape253 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],3), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape251 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],2), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape249 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],1), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape247 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_4"],7), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape245 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_5"],0), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape243 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_4"],6), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape241 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_4"],5), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape239 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_4"],4), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape237 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_4"],3), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape235 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_4"],2), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape233 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_4"],1), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape231 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_4"],0), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape229 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_2"],3), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape227 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_3"],7), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape225 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_3"],6), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape223 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_3"],5), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape221 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_3"],4), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape219 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_2"],2), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape217 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_3"],3), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape215 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_3"],2), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape213 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_2"],1), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape211 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_3"],1), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape209 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_2"],0), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape207 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_3"],0), null, new cjs.Matrix2D(1,0,0,1,-116,-53)).s().p("AyHISIAAwjMAkPAAAIAAQjg");
	this.shape.setTransform(116,53);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.shape199 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_"],0), null, new cjs.Matrix2D(1,0,0,1,-250,-141.5)).s().p("EgnCAWHMAAAgsNMBOFAAAMAAAAsNg");
	this.shape.setTransform(320,226.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(70,85,500,283);


(lib.shape195 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],7), null, new cjs.Matrix2D(1,0,0,1,-14.5,-12.5)).s().p("AiPB8IAAj3IEfAAIAAD3g");
	this.shape.setTransform(625.5,-125.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],7), null, new cjs.Matrix2D(-1,0,0,-1,14.5,12.5)).s().p("AiQB9IAAj5IEgAAIAAD5g");
	this.shape_1.setTransform(14.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-138,640,163);


(lib.shape191 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BFFF7").ss(3,0,0,3).p("AnfAAIO/AA");
	this.shape.setTransform(48,80);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,78.5,99,3);


(lib.shape189 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BFFF7").ss(3,0,0,3).p("AnZAMIAUgSIOqAA");
	this.shape.setTransform(47.5,80.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,78.5,99,5);


(lib.shape187 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BFFF7").ss(3,0,0,3).p("AnfHgIO/u/");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape185 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BFFF7").ss(3,0,0,3).p("AnkARIOWAAIAogm");
	this.shape.setTransform(48.5,16.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,12.5,99,7);


(lib.shape183 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BFFF7").ss(3,0,0,3).p("AnfAAIO/AA");
	this.shape.setTransform(48,18);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,16.5,99,3);


(lib.shape182 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BFFF7").ss(3,0,0,3).p("AHgHgIu/AAIAAu/IO/AAg");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape181 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BFFF7").ss(3,0,0,3).p("AnfHWIi0C0AcIqxIi0AAAKUqdIi0C0EArIgKxICWAAA8HKyIC0AAEgtdAKyICWAA");
	this.shape.setTransform(291,-69);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-139.5,585,141);


(lib.shape179 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],6), null, new cjs.Matrix2D(1,0,0,1,-14.5,-12.5)).s().p("AiPB8IAAj4IEfAAIAAD4g");
	this.shape.setTransform(625.5,150.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],6), null, new cjs.Matrix2D(-1,0,0,-1,14.5,12.5)).s().p("AiQB9IAAj5IEgAAIAAD5g");
	this.shape_1.setTransform(14.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,163);


(lib.shape175 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF8600").ss(3,0,0,3).p("AnfAAIO/AA");
	this.shape.setTransform(48,18);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,16.5,99,3);


(lib.shape173 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF8600").ss(3,0,0,3).p("AnZgVIAoAmIOWAA");
	this.shape.setTransform(47.5,16.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,12.5,99,7);


(lib.shape171 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF8600").ss(3,0,0,3).p("AnfnfIO/O/");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape169 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF8600").ss(3,0,0,3).p("AHaAMIgUgSIuqAA");
	this.shape.setTransform(48.5,80.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,78.5,99,5);


(lib.shape167 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF8600").ss(3,0,0,3).p("AnfAAIO/AA");
	this.shape.setTransform(48,80);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,78.5,99,3);


(lib.shape166 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF8600").ss(3,0,0,3).p("AHgHgIu/AAIAAu/IO/AAg");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape165 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF8600").ss(3,0,0,3).p("EArIAKyICWAAAHgHWIC0C0AZUKyIC0AAAqTqdIC0C0A8HqxIC0AAEgtdgKxICWAA");
	this.shape.setTransform(291,69);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,585,141);


(lib.shape163 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],5), null, new cjs.Matrix2D(1,0,0,1,-14.5,-12.5)).s().p("AiPB9IAAj5IEfAAIAAD5g");
	this.shape.setTransform(625.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],5), null, new cjs.Matrix2D(-1,0,0,-1,14.5,12.5)).s().p("AiQB9IAAj5IEgAAIAAD5g");
	this.shape_1.setTransform(14.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,25);


(lib.shape159 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#A4FF84").ss(3,0,0,3).p("AnfAAIO/AA");
	this.shape.setTransform(48,86);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,84.5,99,3);


(lib.shape157 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#A4FF84").ss(3,0,0,3).p("AHlFlIjwAAIrOrO");
	this.shape.setTransform(47.5,50.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,12.5,99,75);


(lib.shape155 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#A4FF84").ss(3,0,0,3).p("AnkmRICWAAIMoMo");
	this.shape.setTransform(48.5,55.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,13.5,99,84);


(lib.shape154 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#A4FF84").ss(3,0,0,3).p("AHgHgIu/AAIAAu/IO/AAg");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape153 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#A4FF84").ss(3,0,0,3).p("EArIgNWICWAAAHgNXIC0AAAcIgsIi0CzAqTNXIC0AAEgtdgNWICWAAA8HgsIC0Cz");
	this.shape.setTransform(291,85.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,585,174);


(lib.shape151 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],4), null, new cjs.Matrix2D(1,0,0,1,-14.5,-12.5)).s().p("AiPB9IAAj5IEfAAIAAD5g");
	this.shape.setTransform(625.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],4), null, new cjs.Matrix2D(-1,0,0,-1,14.5,12.5)).s().p("AiQB9IAAj5IEgAAIAAD5g");
	this.shape_1.setTransform(14.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,25);


(lib.shape147 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFF7").ss(3,0,0,3).p("AnfAAIO/AA");
	this.shape.setTransform(48,11);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,9.5,99,3);


(lib.shape145 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFF7").ss(3,0,0,3).p("AHllfIj6AAIrELE");
	this.shape.setTransform(47.5,46.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,9.5,99,74);


(lib.shape143 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFF7").ss(3,0,0,3).p("AnkGcICCAAIM8s8");
	this.shape.setTransform(48.5,41.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,86);


(lib.shape142 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFF7").ss(3,0,0,3).p("AHgHgIu/AAIAAu/IO/AAg");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape141 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFF7").ss(3,0,0,3).p("A8HAeIC0izAqTtbIC0AAAHgtbIC0AAAcIAeIi0izEAteANcIiWAAEgtdANcICWAA");
	this.shape.setTransform(291,-86);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-173.5,585,175);


(lib.shape139 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],3), null, new cjs.Matrix2D(1,0,0,1,-14.5,-12.5)).s().p("AiPB9IAAj5IEfAAIAAD5g");
	this.shape.setTransform(625.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],3), null, new cjs.Matrix2D(-1,0,0,-1,14.5,12.5)).s().p("AiQB9IAAj5IEgAAIAAD5g");
	this.shape_1.setTransform(14.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,25);


(lib.shape135 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF51F7").ss(3,0,0,3).p("AnfD1IHfneIHgHe");
	this.shape.setTransform(48,58.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,32.9,99,51.6);


(lib.shape133 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF51F7").ss(3,0,0,3).p("AnaHbIO1u1");
	this.shape.setTransform(48.5,48.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-0.5,98,98);


(lib.shape131 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF51F7").ss(3,0,0,3).p("AnkFgID6AAILErE");
	this.shape.setTransform(48.5,50.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,13.5,99,74);


(lib.shape130 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF51F7").ss(3,0,0,3).p("AHgnfIu/AAIAAO/IO/AAg");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape129 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF51F7").ss(3,0,0,3).p("A8CEuIC+i+AqOtEIC0i0AZPBwIC/C/AHlv4IC0C0EArNAP0ICMAAEgtYAP0ICWAA");
	this.shape.setTransform(290.5,-101.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-204.5,584,206);


(lib.shape127 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],2), null, new cjs.Matrix2D(1,0,0,1,-14.5,-12.5)).s().p("AiPB9IAAj5IEfAAIAAD5g");
	this.shape.setTransform(625.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],2), null, new cjs.Matrix2D(-1,0,0,-1,14.5,12.5)).s().p("AiQB9IAAj5IEgAAIAAD5g");
	this.shape_1.setTransform(14.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,25);


(lib.shape123 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFF700").ss(3,0,0,3).p("Anfj0IHfHeIHgne");
	this.shape.setTransform(48,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,12.5,99,51.6);


(lib.shape121 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFF700").ss(3,0,0,3).p("AnfnfIO/O/");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape119 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFF700").ss(3,0,0,3).p("AnklfID6AAILELE");
	this.shape.setTransform(48.5,46.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,9.5,99,74);


(lib.shape118 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFF700").ss(3,0,0,3).p("AHgHgIu/AAIAAu/IO/AAg");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape117 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFF700").ss(3,0,0,3).p("EArIgPzICWAAAZUh5IC0i0AHgP5IC0i0A8HktIC0C0EgtdgPzICWAAAqTNFIC0C0");
	this.shape.setTransform(291,101.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,585,206);


(lib.shape115 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],1), null, new cjs.Matrix2D(1,0,0,1,-14.5,-12.5)).s().p("AiPB9IAAj5IEfAAIAAD5g");
	this.shape.setTransform(625.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],1), null, new cjs.Matrix2D(-1,0,0,-1,14.5,12.5)).s().p("AiQB9IAAj5IEgAAIAAD5g");
	this.shape_1.setTransform(14.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,25);


(lib.shape111 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#20A206").ss(3,0,0,3).p("AHgAAIu/AA");
	this.shape.setTransform(48,52);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,50.5,99,3);


(lib.shape110 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#20A206").ss(3,0,0,3).p("AHgHgIu/AAIAAu/IO/AAg");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape109 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#20A206").ss(3,0,0,3).p("EArNAAAICMAAAZZAAIC0AAAHlAAIC0AAAqOAAIC0AAA8CAAIC0AAEgtYAAAICWAA");
	this.shape.setTransform(290.5,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,584,3);


(lib.shape107 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],0), null, new cjs.Matrix2D(1,0,0,1,-14.5,-12.5)).s().p("AiPB9IAAj5IEfAAIAAD5g");
	this.shape.setTransform(625.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],0), null, new cjs.Matrix2D(-1,0,0,-1,14.5,12.5)).s().p("AiQB9IAAj5IEgAAIAAD5g");
	this.shape_1.setTransform(14.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,25);


(lib.shape103 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF0000").ss(3,0,0,3).p("AnfAAIO/AA");
	this.shape.setTransform(48,45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,43.5,99,3);


(lib.shape102 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF0000").ss(3,0,0,3).p("AHgHgIu/AAIAAu/IO/AAg");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape101 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF0000").ss(3,0,0,3).p("EArIAAAICWAAAZUAAIC0AAAHgAAIC0AAAqTAAIC0AAA8HAAIC0AAEgtdAAAICWAA");
	this.shape.setTransform(291,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,585,3);


(lib.shape97 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],23), null, new cjs.Matrix2D(1,0,0,1,-14.5,-12.5)).s().p("AiPB9IAAj5IEfAAIAAD5g");
	this.shape.setTransform(625.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],23), null, new cjs.Matrix2D(-1,0,0,-1,14.5,12.5)).s().p("AiQB9IAAj5IEgAAIAAD5g");
	this.shape_1.setTransform(14.5,12.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,25);


(lib.shape93 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BAEF7").ss(3,0,0,3).p("AnfAAIO/AA");
	this.shape.setTransform(48,49);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,47.5,99,3);


(lib.shape92 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BAEF7").ss(3,0,0,3).p("AHgHgIu/AAIAAu/IO/AAg");
	this.shape.setTransform(48,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.shape91 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7BAEF7").ss(3,0,0,3).p("EArIAAAICWAAAZUAAIC0AAAHgAAIC0AAAqTAAIC0AAA8HAAIC0AAEgtdAAAICWAA");
	this.shape.setTransform(291,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,585,3);


(lib.shape89 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.749)").s().p("AnzHzIAAvmIPmAAIAAPmgAnAHCIOCAAIAAuCIuCAAg");
	this.shape.setTransform(50,50);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["rgba(153,153,153,0.502)","rgba(0,0,0,0.502)"],[0,1],0,0,0,0,0,63.7).s().p("AnAHCIAAuCIOCAAIAAOCg");
	this.shape_1.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.shape87 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(cjs.SpriteSheetUtils.extractFrame(ss["graphics_atlas_35"],21), null, new cjs.Matrix2D(1,0,0,1,-26,-8)).s().p("AkDBQIAAifIIHAAIAACfg");
	this.shape.setTransform(26,8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52,16);


(lib.shape71 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image70, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape69 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image68, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape67 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image66, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape65 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image64, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape63 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image62, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape61 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image59, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape60 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image59, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape58 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image57, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape56 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image55, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape54 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image53, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape52 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image51, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape50 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image49, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape48 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image47, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape46 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image45, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape44 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image43, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape42 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image41, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape40 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image39, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape38 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image37, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape36 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image35, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape34 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image33, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image31, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image29, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image27, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image25, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image23, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image21, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image19, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image17, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image15, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image13, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image11, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image9, null, new cjs.Matrix2D(1,0,0,1,-320,-38.5)).s().p("Egx/AGBIAAsBMBj+AAAIAAMBg");
	this.shape.setTransform(320,38.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.shape7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image6, null, new cjs.Matrix2D(1,0,0,1,-320,-240)).s().p("Egx/AlfMAAAhK+MBj+AAAMAAABK+g");
	this.shape.setTransform(320,240);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.shape5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image4, null, new cjs.Matrix2D(1,0,0,1,-320,-240)).s().p("Egx/AlfMAAAhK+MBj+AAAMAAABK+g");
	this.shape.setTransform(320,240);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.shape1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8080FF").s().p("AnfHfIAAu+IO/AAIAAO+g");
	this.shape.setTransform(50,250);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#8080FF").s().p("AnfHgIAAu/IO/AAIAAO/g");
	this.shape_1.setTransform(50,150);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#8080FF").s().p("AnfHgIAAu/IO/AAIAAO/g");
	this.shape_2.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF80").s().p("AnzXbMAAAgu2IPmAAMAAAAu2g");
	this.shape_3.setTransform(50,150);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,300);


(lib.assetsgfxT_04B_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{again:6});

	// Layer 1
	this.instance = new lib.shape1744("synched",0);

	this.instance_1 = new lib.shape1882("synched",0);

	this.instance_2 = new lib.shape1884("synched",0);

	this.instance_3 = new lib.shape1387("synched",0);

	this.instance_4 = new lib.shape1389("synched",0);

	this.instance_5 = new lib.shape1391("synched",0);

	this.instance_6 = new lib.shape1393("synched",0);

	this.instance_7 = new lib.shape1395("synched",0);

	this.instance_8 = new lib.shape1397("synched",0);

	this.instance_9 = new lib.shape1399("synched",0);

	this.instance_10 = new lib.shape1401("synched",0);

	this.instance_11 = new lib.shape1403("synched",0);

	this.instance_12 = new lib.shape1405("synched",0);

	this.instance_13 = new lib.shape1407("synched",0);

	this.instance_14 = new lib.shape1409("synched",0);

	this.instance_15 = new lib.shape1411("synched",0);

	this.instance_16 = new lib.shape1413("synched",0);

	this.instance_17 = new lib.shape1415("synched",0);

	this.instance_18 = new lib.shape1417("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_06B_A23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1177("synched",0);

	this.instance_1 = new lib.shape1814("synched",0);

	this.instance_2 = new lib.shape1816("synched",0);

	this.instance_3 = new lib.shape1818("synched",0);

	this.instance_4 = new lib.shape1820("synched",0);

	this.instance_5 = new lib.shape1822("synched",0);

	this.instance_6 = new lib.shape1824("synched",0);

	this.instance_7 = new lib.shape1826("synched",0);

	this.instance_8 = new lib.shape1828("synched",0);

	this.instance_9 = new lib.shape1830("synched",0);

	this.instance_10 = new lib.shape1832("synched",0);

	this.instance_11 = new lib.shape1834("synched",0);

	this.instance_12 = new lib.shape1836("synched",0);

	this.instance_13 = new lib.shape1838("synched",0);

	this.instance_14 = new lib.shape1840("synched",0);

	this.instance_15 = new lib.shape1842("synched",0);

	this.instance_16 = new lib.shape1844("synched",0);

	this.instance_17 = new lib.shape1846("synched",0);

	this.instance_18 = new lib.shape1848("synched",0);

	this.instance_19 = new lib.shape1850("synched",0);

	this.instance_20 = new lib.shape1852("synched",0);

	this.instance_21 = new lib.shape1854("synched",0);

	this.instance_22 = new lib.shape1856("synched",0);

	this.instance_23 = new lib.shape1858("synched",0);

	this.instance_24 = new lib.shape1860("synched",0);

	this.instance_25 = new lib.shape1862("synched",0);

	this.instance_26 = new lib.shape1864("synched",0);

	this.instance_27 = new lib.shape1866("synched",0);

	this.instance_28 = new lib.shape1868("synched",0);

	this.instance_29 = new lib.shape1870("synched",0);

	this.instance_30 = new lib.shape1872("synched",0);

	this.instance_31 = new lib.shape1874("synched",0);

	this.instance_32 = new lib.shape1876("synched",0);

	this.instance_33 = new lib.shape1878("synched",0);

	this.instance_34 = new lib.shape1879("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_03N_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1692("synched",0);

	this.instance_1 = new lib.shape1811("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_04B_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1744("synched",0);

	this.instance_1 = new lib.shape1808("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileN_00 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape996("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_02N_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1580("synched",0);

	this.instance_1 = new lib.shape1804("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileN_02 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1580("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_05N_A23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1168("synched",0);

	this.instance_1 = new lib.shape1764("synched",0);

	this.instance_2 = new lib.shape1766("synched",0);

	this.instance_3 = new lib.shape1768("synched",0);

	this.instance_4 = new lib.shape1770("synched",0);

	this.instance_5 = new lib.shape1772("synched",0);

	this.instance_6 = new lib.shape1774("synched",0);

	this.instance_7 = new lib.shape1776("synched",0);

	this.instance_8 = new lib.shape1778("synched",0);

	this.instance_9 = new lib.shape1780("synched",0);

	this.instance_10 = new lib.shape1782("synched",0);

	this.instance_11 = new lib.shape1784("synched",0);

	this.instance_12 = new lib.shape1786("synched",0);

	this.instance_13 = new lib.shape1788("synched",0);

	this.instance_14 = new lib.shape1790("synched",0);

	this.instance_15 = new lib.shape1792("synched",0);

	this.instance_16 = new lib.shape1794("synched",0);

	this.instance_17 = new lib.shape1796("synched",0);

	this.instance_18 = new lib.shape1798("synched",0);

	this.instance_19 = new lib.shape1800("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileN_01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1162("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileN_04 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1381("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTile_09 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1000("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_02B_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":6});

	// Layer 1
	this.instance = new lib.shape992("synched",0);

	this.instance_1 = new lib.shape1756("synched",0);

	this.instance_2 = new lib.shape1758("synched",0);

	this.instance_3 = new lib.shape1586("synched",0);

	this.instance_4 = new lib.shape1588("synched",0);

	this.instance_5 = new lib.shape1590("synched",0);

	this.instance_6 = new lib.shape1592("synched",0);

	this.instance_7 = new lib.shape1594("synched",0);

	this.instance_8 = new lib.shape1596("synched",0);

	this.instance_9 = new lib.shape1598("synched",0);

	this.instance_10 = new lib.shape1600("synched",0);

	this.instance_11 = new lib.shape1602("synched",0);

	this.instance_12 = new lib.shape1604("synched",0);

	this.instance_13 = new lib.shape1606("synched",0);

	this.instance_14 = new lib.shape1608("synched",0);

	this.instance_15 = new lib.shape1610("synched",0);

	this.instance_16 = new lib.shape1612("synched",0);

	this.instance_17 = new lib.shape1614("synched",0);

	this.instance_18 = new lib.shape1616("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileN_03 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1692("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileN_05 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1168("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileN_06 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape887("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_04N_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1381("synched",0);

	this.instance_1 = new lib.shape1750("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileN_07 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape383("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileN_08 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape873("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileB_00 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1285("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileB_04 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1744("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_00N_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":6});

	// Layer 1
	this.instance = new lib.shape996("synched",0);

	this.instance_1 = new lib.shape1739("synched",0);

	this.instance_2 = new lib.shape1741("synched",0);

	this.instance_3 = new lib.shape1294("synched",0);

	this.instance_4 = new lib.shape1296("synched",0);

	this.instance_5 = new lib.shape1298("synched",0);

	this.instance_6 = new lib.shape1300("synched",0);

	this.instance_7 = new lib.shape1302("synched",0);

	this.instance_8 = new lib.shape1304("synched",0);

	this.instance_9 = new lib.shape1306("synched",0);

	this.instance_10 = new lib.shape1308("synched",0);

	this.instance_11 = new lib.shape1310("synched",0);

	this.instance_12 = new lib.shape1312("synched",0);

	this.instance_13 = new lib.shape1314("synched",0);

	this.instance_14 = new lib.shape1316("synched",0);

	this.instance_15 = new lib.shape1318("synched",0);

	this.instance_16 = new lib.shape1320("synched",0);

	this.instance_17 = new lib.shape1322("synched",0);

	this.instance_18 = new lib.shape1324("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileB_03 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1377("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileB_02 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape992("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileB_01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape528("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileB_08 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape533("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileB_07 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape803("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileB_06 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1177("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxTileB_05 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape730("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_06N_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape887("synched",0);

	this.instance_1 = new lib.shape1699("synched",0);

	this.instance_2 = new lib.shape1701("synched",0);

	this.instance_3 = new lib.shape1703("synched",0);

	this.instance_4 = new lib.shape1705("synched",0);

	this.instance_5 = new lib.shape1707("synched",0);

	this.instance_6 = new lib.shape1709("synched",0);

	this.instance_7 = new lib.shape1711("synched",0);

	this.instance_8 = new lib.shape1713("synched",0);

	this.instance_9 = new lib.shape1715("synched",0);

	this.instance_10 = new lib.shape1717("synched",0);

	this.instance_11 = new lib.shape1199("synched",0);

	this.instance_12 = new lib.shape1201("synched",0);

	this.instance_13 = new lib.shape1203("synched",0);

	this.instance_14 = new lib.shape1205("synched",0);

	this.instance_15 = new lib.shape1207("synched",0);

	this.instance_16 = new lib.shape1209("synched",0);

	this.instance_17 = new lib.shape1211("synched",0);

	this.instance_18 = new lib.shape1213("synched",0);

	this.instance_19 = new lib.shape1215("synched",0);

	this.instance_20 = new lib.shape1217("synched",0);

	this.instance_21 = new lib.shape1219("synched",0);

	this.instance_22 = new lib.shape1221("synched",0);

	this.instance_23 = new lib.shape1223("synched",0);

	this.instance_24 = new lib.shape1225("synched",0);

	this.instance_25 = new lib.shape1227("synched",0);

	this.instance_26 = new lib.shape1229("synched",0);

	this.instance_27 = new lib.shape1231("synched",0);

	this.instance_28 = new lib.shape1233("synched",0);

	this.instance_29 = new lib.shape1235("synched",0);

	this.instance_30 = new lib.shape1237("synched",0);

	this.instance_31 = new lib.shape1239("synched",0);

	this.instance_32 = new lib.shape1241("synched",0);

	this.instance_33 = new lib.shape1243("synched",0);

	this.instance_34 = new lib.shape1245("synched",0);

	this.instance_35 = new lib.shape1247("synched",0);

	this.instance_36 = new lib.shape1249("synched",0);

	this.instance_37 = new lib.shape1251("synched",0);

	this.instance_38 = new lib.shape1253("synched",0);

	this.instance_39 = new lib.shape1255("synched",0);

	this.instance_40 = new lib.shape1257("synched",0);

	this.instance_41 = new lib.shape1259("synched",0);

	this.instance_42 = new lib.shape1261("synched",0);

	this.instance_43 = new lib.shape1263("synched",0);

	this.instance_44 = new lib.shape1265("synched",0);

	this.instance_45 = new lib.shape1267("synched",0);

	this.instance_46 = new lib.shape1269("synched",0);

	this.instance_47 = new lib.shape1271("synched",0);

	this.instance_48 = new lib.shape1719("synched",0);

	this.instance_49 = new lib.shape1721("synched",0);

	this.instance_50 = new lib.shape1723("synched",0);

	this.instance_51 = new lib.shape1725("synched",0);

	this.instance_52 = new lib.shape1727("synched",0);

	this.instance_53 = new lib.shape1729("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).to({state:[{t:this.instance_35}]},2).to({state:[{t:this.instance_36}]},2).to({state:[{t:this.instance_37}]},2).to({state:[{t:this.instance_38}]},2).to({state:[{t:this.instance_39}]},2).to({state:[{t:this.instance_40}]},2).to({state:[{t:this.instance_41}]},2).to({state:[{t:this.instance_42}]},2).to({state:[{t:this.instance_43}]},2).to({state:[{t:this.instance_44}]},2).to({state:[{t:this.instance_45}]},2).to({state:[{t:this.instance_46}]},2).to({state:[{t:this.instance_47}]},2).to({state:[{t:this.instance_48}]},2).to({state:[{t:this.instance_49}]},2).to({state:[{t:this.instance_50}]},2).to({state:[{t:this.instance_51}]},2).to({state:[{t:this.instance_52}]},2).to({state:[{t:this.instance_53}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_03N_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":6});

	// Layer 1
	this.instance = new lib.shape1692("synched",0);

	this.instance_1 = new lib.shape1694("synched",0);

	this.instance_2 = new lib.shape1696("synched",0);

	this.instance_3 = new lib.shape1548("synched",0);

	this.instance_4 = new lib.shape1550("synched",0);

	this.instance_5 = new lib.shape1552("synched",0);

	this.instance_6 = new lib.shape1554("synched",0);

	this.instance_7 = new lib.shape1556("synched",0);

	this.instance_8 = new lib.shape1558("synched",0);

	this.instance_9 = new lib.shape1560("synched",0);

	this.instance_10 = new lib.shape1562("synched",0);

	this.instance_11 = new lib.shape1564("synched",0);

	this.instance_12 = new lib.shape1566("synched",0);

	this.instance_13 = new lib.shape1568("synched",0);

	this.instance_14 = new lib.shape1570("synched",0);

	this.instance_15 = new lib.shape1572("synched",0);

	this.instance_16 = new lib.shape1574("synched",0);

	this.instance_17 = new lib.shape1576("synched",0);

	this.instance_18 = new lib.shape1578("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_07B_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape803("synched",0);

	this.instance_1 = new lib.shape1672("synched",0);

	this.instance_2 = new lib.shape1674("synched",0);

	this.instance_3 = new lib.shape1676("synched",0);

	this.instance_4 = new lib.shape391("synched",0);

	this.instance_5 = new lib.shape393("synched",0);

	this.instance_6 = new lib.shape395("synched",0);

	this.instance_7 = new lib.shape397("synched",0);

	this.instance_8 = new lib.shape399("synched",0);

	this.instance_9 = new lib.shape401("synched",0);

	this.instance_10 = new lib.shape403("synched",0);

	this.instance_11 = new lib.shape405("synched",0);

	this.instance_12 = new lib.shape407("synched",0);

	this.instance_13 = new lib.shape409("synched",0);

	this.instance_14 = new lib.shape411("synched",0);

	this.instance_15 = new lib.shape413("synched",0);

	this.instance_16 = new lib.shape415("synched",0);

	this.instance_17 = new lib.shape417("synched",0);

	this.instance_18 = new lib.shape419("synched",0);

	this.instance_19 = new lib.shape421("synched",0);

	this.instance_20 = new lib.shape423("synched",0);

	this.instance_21 = new lib.shape425("synched",0);

	this.instance_22 = new lib.shape427("synched",0);

	this.instance_23 = new lib.shape429("synched",0);

	this.instance_24 = new lib.shape431("synched",0);

	this.instance_25 = new lib.shape433("synched",0);

	this.instance_26 = new lib.shape435("synched",0);

	this.instance_27 = new lib.shape437("synched",0);

	this.instance_28 = new lib.shape439("synched",0);

	this.instance_29 = new lib.shape441("synched",0);

	this.instance_30 = new lib.shape443("synched",0);

	this.instance_31 = new lib.shape445("synched",0);

	this.instance_32 = new lib.shape447("synched",0);

	this.instance_33 = new lib.shape449("synched",0);

	this.instance_34 = new lib.shape451("synched",0);

	this.instance_35 = new lib.shape453("synched",0);

	this.instance_36 = new lib.shape455("synched",0);

	this.instance_37 = new lib.shape457("synched",0);

	this.instance_38 = new lib.shape459("synched",0);

	this.instance_39 = new lib.shape461("synched",0);

	this.instance_40 = new lib.shape463("synched",0);

	this.instance_41 = new lib.shape465("synched",0);

	this.instance_42 = new lib.shape467("synched",0);

	this.instance_43 = new lib.shape469("synched",0);

	this.instance_44 = new lib.shape471("synched",0);

	this.instance_45 = new lib.shape473("synched",0);

	this.instance_46 = new lib.shape475("synched",0);

	this.instance_47 = new lib.shape477("synched",0);

	this.instance_48 = new lib.shape479("synched",0);

	this.instance_49 = new lib.shape481("synched",0);

	this.instance_50 = new lib.shape483("synched",0);

	this.instance_51 = new lib.shape485("synched",0);

	this.instance_52 = new lib.shape487("synched",0);

	this.instance_53 = new lib.shape489("synched",0);

	this.instance_54 = new lib.shape491("synched",0);

	this.instance_55 = new lib.shape493("synched",0);

	this.instance_56 = new lib.shape495("synched",0);

	this.instance_57 = new lib.shape497("synched",0);

	this.instance_58 = new lib.shape499("synched",0);

	this.instance_59 = new lib.shape501("synched",0);

	this.instance_60 = new lib.shape503("synched",0);

	this.instance_61 = new lib.shape505("synched",0);

	this.instance_62 = new lib.shape507("synched",0);

	this.instance_63 = new lib.shape509("synched",0);

	this.instance_64 = new lib.shape511("synched",0);

	this.instance_65 = new lib.shape1678("synched",0);

	this.instance_66 = new lib.shape1680("synched",0);

	this.instance_67 = new lib.shape1682("synched",0);

	this.instance_68 = new lib.shape1684("synched",0);

	this.instance_69 = new lib.shape1686("synched",0);

	this.instance_70 = new lib.shape1688("synched",0);

	this.instance_71 = new lib.shape1690("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).to({state:[{t:this.instance_35}]},2).to({state:[{t:this.instance_36}]},2).to({state:[{t:this.instance_37}]},2).to({state:[{t:this.instance_38}]},2).to({state:[{t:this.instance_39}]},2).to({state:[{t:this.instance_40}]},2).to({state:[{t:this.instance_41}]},2).to({state:[{t:this.instance_42}]},2).to({state:[{t:this.instance_43}]},2).to({state:[{t:this.instance_44}]},2).to({state:[{t:this.instance_45}]},2).to({state:[{t:this.instance_46}]},2).to({state:[{t:this.instance_47}]},2).to({state:[{t:this.instance_48}]},2).to({state:[{t:this.instance_49}]},2).to({state:[{t:this.instance_50}]},2).to({state:[{t:this.instance_51}]},2).to({state:[{t:this.instance_52}]},2).to({state:[{t:this.instance_53}]},2).to({state:[{t:this.instance_54}]},2).to({state:[{t:this.instance_55}]},2).to({state:[{t:this.instance_56}]},2).to({state:[{t:this.instance_57}]},2).to({state:[{t:this.instance_58}]},2).to({state:[{t:this.instance_59}]},2).to({state:[{t:this.instance_60}]},2).to({state:[{t:this.instance_61}]},2).to({state:[{t:this.instance_62}]},2).to({state:[{t:this.instance_63}]},2).to({state:[{t:this.instance_64}]},2).to({state:[{t:this.instance_65}]},2).to({state:[{t:this.instance_66}]},2).to({state:[{t:this.instance_67}]},2).to({state:[{t:this.instance_68}]},2).to({state:[{t:this.instance_69}]},2).to({state:[{t:this.instance_70}]},2).to({state:[{t:this.instance_71}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_05B_A23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape730("synched",0);

	this.instance_1 = new lib.shape1635("synched",0);

	this.instance_2 = new lib.shape1637("synched",0);

	this.instance_3 = new lib.shape1639("synched",0);

	this.instance_4 = new lib.shape1641("synched",0);

	this.instance_5 = new lib.shape1643("synched",0);

	this.instance_6 = new lib.shape1645("synched",0);

	this.instance_7 = new lib.shape1647("synched",0);

	this.instance_8 = new lib.shape1649("synched",0);

	this.instance_9 = new lib.shape1651("synched",0);

	this.instance_10 = new lib.shape1653("synched",0);

	this.instance_11 = new lib.shape1655("synched",0);

	this.instance_12 = new lib.shape1657("synched",0);

	this.instance_13 = new lib.shape1659("synched",0);

	this.instance_14 = new lib.shape1661("synched",0);

	this.instance_15 = new lib.shape1663("synched",0);

	this.instance_16 = new lib.shape1665("synched",0);

	this.instance_17 = new lib.shape1667("synched",0);

	this.instance_18 = new lib.shape1669("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_08B_A23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape533("synched",0);

	this.instance_1 = new lib.shape1622("synched",0);

	this.instance_2 = new lib.shape1624("synched",0);

	this.instance_3 = new lib.shape1424("synched",0);

	this.instance_4 = new lib.shape1426("synched",0);

	this.instance_5 = new lib.shape1428("synched",0);

	this.instance_6 = new lib.shape1430("synched",0);

	this.instance_7 = new lib.shape1432("synched",0);

	this.instance_8 = new lib.shape1434("synched",0);

	this.instance_9 = new lib.shape1436("synched",0);

	this.instance_10 = new lib.shape1438("synched",0);

	this.instance_11 = new lib.shape1440("synched",0);

	this.instance_12 = new lib.shape1442("synched",0);

	this.instance_13 = new lib.shape1444("synched",0);

	this.instance_14 = new lib.shape1446("synched",0);

	this.instance_15 = new lib.shape1448("synched",0);

	this.instance_16 = new lib.shape1450("synched",0);

	this.instance_17 = new lib.shape1452("synched",0);

	this.instance_18 = new lib.shape1454("synched",0);

	this.instance_19 = new lib.shape1456("synched",0);

	this.instance_20 = new lib.shape1458("synched",0);

	this.instance_21 = new lib.shape1460("synched",0);

	this.instance_22 = new lib.shape1462("synched",0);

	this.instance_23 = new lib.shape1464("synched",0);

	this.instance_24 = new lib.shape1626("synched",0);

	this.instance_25 = new lib.shape1628("synched",0);

	this.instance_26 = new lib.shape1630("synched",0);

	this.instance_27 = new lib.shape1632("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_01N_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1162("synched",0);

	this.instance_1 = new lib.shape1619("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_02N_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":6});

	// Layer 1
	this.instance = new lib.shape1580("synched",0);

	this.instance_1 = new lib.shape1582("synched",0);

	this.instance_2 = new lib.shape1584("synched",0);

	this.instance_3 = new lib.shape1586("synched",0);

	this.instance_4 = new lib.shape1588("synched",0);

	this.instance_5 = new lib.shape1590("synched",0);

	this.instance_6 = new lib.shape1592("synched",0);

	this.instance_7 = new lib.shape1594("synched",0);

	this.instance_8 = new lib.shape1596("synched",0);

	this.instance_9 = new lib.shape1598("synched",0);

	this.instance_10 = new lib.shape1600("synched",0);

	this.instance_11 = new lib.shape1602("synched",0);

	this.instance_12 = new lib.shape1604("synched",0);

	this.instance_13 = new lib.shape1606("synched",0);

	this.instance_14 = new lib.shape1608("synched",0);

	this.instance_15 = new lib.shape1610("synched",0);

	this.instance_16 = new lib.shape1612("synched",0);

	this.instance_17 = new lib.shape1614("synched",0);

	this.instance_18 = new lib.shape1616("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_03B_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":6});

	// Layer 1
	this.instance = new lib.shape1377("synched",0);

	this.instance_1 = new lib.shape1544("synched",0);

	this.instance_2 = new lib.shape1546("synched",0);

	this.instance_3 = new lib.shape1548("synched",0);

	this.instance_4 = new lib.shape1550("synched",0);

	this.instance_5 = new lib.shape1552("synched",0);

	this.instance_6 = new lib.shape1554("synched",0);

	this.instance_7 = new lib.shape1556("synched",0);

	this.instance_8 = new lib.shape1558("synched",0);

	this.instance_9 = new lib.shape1560("synched",0);

	this.instance_10 = new lib.shape1562("synched",0);

	this.instance_11 = new lib.shape1564("synched",0);

	this.instance_12 = new lib.shape1566("synched",0);

	this.instance_13 = new lib.shape1568("synched",0);

	this.instance_14 = new lib.shape1570("synched",0);

	this.instance_15 = new lib.shape1572("synched",0);

	this.instance_16 = new lib.shape1574("synched",0);

	this.instance_17 = new lib.shape1576("synched",0);

	this.instance_18 = new lib.shape1578("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_07N_A23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape383("synched",0);

	this.instance_1 = new lib.shape1473("synched",0);

	this.instance_2 = new lib.shape1475("synched",0);

	this.instance_3 = new lib.shape1477("synched",0);

	this.instance_4 = new lib.shape1479("synched",0);

	this.instance_5 = new lib.shape1481("synched",0);

	this.instance_6 = new lib.shape1483("synched",0);

	this.instance_7 = new lib.shape1485("synched",0);

	this.instance_8 = new lib.shape1487("synched",0);

	this.instance_9 = new lib.shape1489("synched",0);

	this.instance_10 = new lib.shape1491("synched",0);

	this.instance_11 = new lib.shape1493("synched",0);

	this.instance_12 = new lib.shape1495("synched",0);

	this.instance_13 = new lib.shape1497("synched",0);

	this.instance_14 = new lib.shape1499("synched",0);

	this.instance_15 = new lib.shape1501("synched",0);

	this.instance_16 = new lib.shape1503("synched",0);

	this.instance_17 = new lib.shape1505("synched",0);

	this.instance_18 = new lib.shape1507("synched",0);

	this.instance_19 = new lib.shape1509("synched",0);

	this.instance_20 = new lib.shape1511("synched",0);

	this.instance_21 = new lib.shape1513("synched",0);

	this.instance_22 = new lib.shape1515("synched",0);

	this.instance_23 = new lib.shape1517("synched",0);

	this.instance_24 = new lib.shape1519("synched",0);

	this.instance_25 = new lib.shape1521("synched",0);

	this.instance_26 = new lib.shape1523("synched",0);

	this.instance_27 = new lib.shape1525("synched",0);

	this.instance_28 = new lib.shape1527("synched",0);

	this.instance_29 = new lib.shape1529("synched",0);

	this.instance_30 = new lib.shape1531("synched",0);

	this.instance_31 = new lib.shape1533("synched",0);

	this.instance_32 = new lib.shape1535("synched",0);

	this.instance_33 = new lib.shape1537("synched",0);

	this.instance_34 = new lib.shape1539("synched",0);

	this.instance_35 = new lib.shape1541("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).to({state:[{t:this.instance_35}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_08N_A23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape873("synched",0);

	this.instance_1 = new lib.shape1420("synched",0);

	this.instance_2 = new lib.shape1422("synched",0);

	this.instance_3 = new lib.shape1424("synched",0);

	this.instance_4 = new lib.shape1426("synched",0);

	this.instance_5 = new lib.shape1428("synched",0);

	this.instance_6 = new lib.shape1430("synched",0);

	this.instance_7 = new lib.shape1432("synched",0);

	this.instance_8 = new lib.shape1434("synched",0);

	this.instance_9 = new lib.shape1436("synched",0);

	this.instance_10 = new lib.shape1438("synched",0);

	this.instance_11 = new lib.shape1440("synched",0);

	this.instance_12 = new lib.shape1442("synched",0);

	this.instance_13 = new lib.shape1444("synched",0);

	this.instance_14 = new lib.shape1446("synched",0);

	this.instance_15 = new lib.shape1448("synched",0);

	this.instance_16 = new lib.shape1450("synched",0);

	this.instance_17 = new lib.shape1452("synched",0);

	this.instance_18 = new lib.shape1454("synched",0);

	this.instance_19 = new lib.shape1456("synched",0);

	this.instance_20 = new lib.shape1458("synched",0);

	this.instance_21 = new lib.shape1460("synched",0);

	this.instance_22 = new lib.shape1462("synched",0);

	this.instance_23 = new lib.shape1464("synched",0);

	this.instance_24 = new lib.shape1466("synched",0);

	this.instance_25 = new lib.shape1468("synched",0);

	this.instance_26 = new lib.shape1470("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_04N_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":6});

	// Layer 1
	this.instance = new lib.shape1381("synched",0);

	this.instance_1 = new lib.shape1383("synched",0);

	this.instance_2 = new lib.shape1385("synched",0);

	this.instance_3 = new lib.shape1387("synched",0);

	this.instance_4 = new lib.shape1389("synched",0);

	this.instance_5 = new lib.shape1391("synched",0);

	this.instance_6 = new lib.shape1393("synched",0);

	this.instance_7 = new lib.shape1395("synched",0);

	this.instance_8 = new lib.shape1397("synched",0);

	this.instance_9 = new lib.shape1399("synched",0);

	this.instance_10 = new lib.shape1401("synched",0);

	this.instance_11 = new lib.shape1403("synched",0);

	this.instance_12 = new lib.shape1405("synched",0);

	this.instance_13 = new lib.shape1407("synched",0);

	this.instance_14 = new lib.shape1409("synched",0);

	this.instance_15 = new lib.shape1411("synched",0);

	this.instance_16 = new lib.shape1413("synched",0);

	this.instance_17 = new lib.shape1415("synched",0);

	this.instance_18 = new lib.shape1417("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_03B_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1377("synched",0);

	this.instance_1 = new lib.shape1379("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_09N_A = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":1});

	// Layer 2
	this.instance = new lib.shape1326("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(49));

	// Layer 1
	this.instance_1 = new lib.shape1000("synched",0);

	this.instance_2 = new lib.shape1328("synched",0);

	this.instance_3 = new lib.shape1330("synched",0);

	this.instance_4 = new lib.shape1332("synched",0);

	this.instance_5 = new lib.shape1334("synched",0);

	this.instance_6 = new lib.shape1336("synched",0);

	this.instance_7 = new lib.shape1338("synched",0);

	this.instance_8 = new lib.shape1340("synched",0);

	this.instance_9 = new lib.shape1342("synched",0);

	this.instance_10 = new lib.shape1344("synched",0);

	this.instance_11 = new lib.shape1346("synched",0);

	this.instance_12 = new lib.shape1348("synched",0);

	this.instance_13 = new lib.shape1350("synched",0);

	this.instance_14 = new lib.shape1352("synched",0);

	this.instance_15 = new lib.shape1354("synched",0);

	this.instance_16 = new lib.shape1356("synched",0);

	this.instance_17 = new lib.shape1358("synched",0);

	this.instance_18 = new lib.shape1360("synched",0);

	this.instance_19 = new lib.shape1362("synched",0);

	this.instance_20 = new lib.shape1364("synched",0);

	this.instance_21 = new lib.shape1366("synched",0);

	this.instance_22 = new lib.shape1368("synched",0);

	this.instance_23 = new lib.shape1370("synched",0);

	this.instance_24 = new lib.shape1372("synched",0);

	this.instance_25 = new lib.shape1374("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_00B_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":6});

	// Layer 1
	this.instance = new lib.shape1285("synched",0);

	this.instance_1 = new lib.shape1290("synched",0);

	this.instance_2 = new lib.shape1292("synched",0);

	this.instance_3 = new lib.shape1294("synched",0);

	this.instance_4 = new lib.shape1296("synched",0);

	this.instance_5 = new lib.shape1298("synched",0);

	this.instance_6 = new lib.shape1300("synched",0);

	this.instance_7 = new lib.shape1302("synched",0);

	this.instance_8 = new lib.shape1304("synched",0);

	this.instance_9 = new lib.shape1306("synched",0);

	this.instance_10 = new lib.shape1308("synched",0);

	this.instance_11 = new lib.shape1310("synched",0);

	this.instance_12 = new lib.shape1312("synched",0);

	this.instance_13 = new lib.shape1314("synched",0);

	this.instance_14 = new lib.shape1316("synched",0);

	this.instance_15 = new lib.shape1318("synched",0);

	this.instance_16 = new lib.shape1320("synched",0);

	this.instance_17 = new lib.shape1322("synched",0);

	this.instance_18 = new lib.shape1324("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_00B_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1285("synched",0);

	this.instance_1 = new lib.shape1287("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_06B_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1177("synched",0);

	this.instance_1 = new lib.shape1179("synched",0);

	this.instance_2 = new lib.shape1181("synched",0);

	this.instance_3 = new lib.shape1183("synched",0);

	this.instance_4 = new lib.shape1185("synched",0);

	this.instance_5 = new lib.shape1187("synched",0);

	this.instance_6 = new lib.shape1189("synched",0);

	this.instance_7 = new lib.shape1191("synched",0);

	this.instance_8 = new lib.shape1193("synched",0);

	this.instance_9 = new lib.shape1195("synched",0);

	this.instance_10 = new lib.shape1197("synched",0);

	this.instance_11 = new lib.shape1199("synched",0);

	this.instance_12 = new lib.shape1201("synched",0);

	this.instance_13 = new lib.shape1203("synched",0);

	this.instance_14 = new lib.shape1205("synched",0);

	this.instance_15 = new lib.shape1207("synched",0);

	this.instance_16 = new lib.shape1209("synched",0);

	this.instance_17 = new lib.shape1211("synched",0);

	this.instance_18 = new lib.shape1213("synched",0);

	this.instance_19 = new lib.shape1215("synched",0);

	this.instance_20 = new lib.shape1217("synched",0);

	this.instance_21 = new lib.shape1219("synched",0);

	this.instance_22 = new lib.shape1221("synched",0);

	this.instance_23 = new lib.shape1223("synched",0);

	this.instance_24 = new lib.shape1225("synched",0);

	this.instance_25 = new lib.shape1227("synched",0);

	this.instance_26 = new lib.shape1229("synched",0);

	this.instance_27 = new lib.shape1231("synched",0);

	this.instance_28 = new lib.shape1233("synched",0);

	this.instance_29 = new lib.shape1235("synched",0);

	this.instance_30 = new lib.shape1237("synched",0);

	this.instance_31 = new lib.shape1239("synched",0);

	this.instance_32 = new lib.shape1241("synched",0);

	this.instance_33 = new lib.shape1243("synched",0);

	this.instance_34 = new lib.shape1245("synched",0);

	this.instance_35 = new lib.shape1247("synched",0);

	this.instance_36 = new lib.shape1249("synched",0);

	this.instance_37 = new lib.shape1251("synched",0);

	this.instance_38 = new lib.shape1253("synched",0);

	this.instance_39 = new lib.shape1255("synched",0);

	this.instance_40 = new lib.shape1257("synched",0);

	this.instance_41 = new lib.shape1259("synched",0);

	this.instance_42 = new lib.shape1261("synched",0);

	this.instance_43 = new lib.shape1263("synched",0);

	this.instance_44 = new lib.shape1265("synched",0);

	this.instance_45 = new lib.shape1267("synched",0);

	this.instance_46 = new lib.shape1269("synched",0);

	this.instance_47 = new lib.shape1271("synched",0);

	this.instance_48 = new lib.shape1273("synched",0);

	this.instance_49 = new lib.shape1275("synched",0);

	this.instance_50 = new lib.shape1277("synched",0);

	this.instance_51 = new lib.shape1279("synched",0);

	this.instance_52 = new lib.shape1281("synched",0);

	this.instance_53 = new lib.shape1282("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).to({state:[{t:this.instance_35}]},2).to({state:[{t:this.instance_36}]},2).to({state:[{t:this.instance_37}]},2).to({state:[{t:this.instance_38}]},2).to({state:[{t:this.instance_39}]},2).to({state:[{t:this.instance_40}]},2).to({state:[{t:this.instance_41}]},2).to({state:[{t:this.instance_42}]},2).to({state:[{t:this.instance_43}]},2).to({state:[{t:this.instance_44}]},2).to({state:[{t:this.instance_45}]},2).to({state:[{t:this.instance_46}]},2).to({state:[{t:this.instance_47}]},2).to({state:[{t:this.instance_48}]},2).to({state:[{t:this.instance_49}]},2).to({state:[{t:this.instance_50}]},2).to({state:[{t:this.instance_51}]},2).to({state:[{t:this.instance_52}]},2).to({state:[{t:this.instance_53}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_05N_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1168("synched",0);

	this.instance_1 = new lib.shape1170("synched",0);

	this.instance_2 = new lib.shape734("synched",0);

	this.instance_3 = new lib.shape736("synched",0);

	this.instance_4 = new lib.shape738("synched",0);

	this.instance_5 = new lib.shape740("synched",0);

	this.instance_6 = new lib.shape742("synched",0);

	this.instance_7 = new lib.shape744("synched",0);

	this.instance_8 = new lib.shape746("synched",0);

	this.instance_9 = new lib.shape748("synched",0);

	this.instance_10 = new lib.shape750("synched",0);

	this.instance_11 = new lib.shape752("synched",0);

	this.instance_12 = new lib.shape754("synched",0);

	this.instance_13 = new lib.shape756("synched",0);

	this.instance_14 = new lib.shape758("synched",0);

	this.instance_15 = new lib.shape760("synched",0);

	this.instance_16 = new lib.shape762("synched",0);

	this.instance_17 = new lib.shape764("synched",0);

	this.instance_18 = new lib.shape766("synched",0);

	this.instance_19 = new lib.shape768("synched",0);

	this.instance_20 = new lib.shape770("synched",0);

	this.instance_21 = new lib.shape772("synched",0);

	this.instance_22 = new lib.shape774("synched",0);

	this.instance_23 = new lib.shape776("synched",0);

	this.instance_24 = new lib.shape778("synched",0);

	this.instance_25 = new lib.shape780("synched",0);

	this.instance_26 = new lib.shape782("synched",0);

	this.instance_27 = new lib.shape784("synched",0);

	this.instance_28 = new lib.shape786("synched",0);

	this.instance_29 = new lib.shape788("synched",0);

	this.instance_30 = new lib.shape790("synched",0);

	this.instance_31 = new lib.shape792("synched",0);

	this.instance_32 = new lib.shape1172("synched",0);

	this.instance_33 = new lib.shape1174("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_01N_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":6});

	// Layer 1
	this.instance = new lib.shape1162("synched",0);

	this.instance_1 = new lib.shape1164("synched",0);

	this.instance_2 = new lib.shape1166("synched",0);

	this.instance_3 = new lib.shape962("synched",0);

	this.instance_4 = new lib.shape964("synched",0);

	this.instance_5 = new lib.shape966("synched",0);

	this.instance_6 = new lib.shape968("synched",0);

	this.instance_7 = new lib.shape970("synched",0);

	this.instance_8 = new lib.shape972("synched",0);

	this.instance_9 = new lib.shape974("synched",0);

	this.instance_10 = new lib.shape976("synched",0);

	this.instance_11 = new lib.shape978("synched",0);

	this.instance_12 = new lib.shape980("synched",0);

	this.instance_13 = new lib.shape982("synched",0);

	this.instance_14 = new lib.shape984("synched",0);

	this.instance_15 = new lib.shape986("synched",0);

	this.instance_16 = new lib.shape988("synched",0);

	this.instance_17 = new lib.shape990("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},3).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_09B_A = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape1000("synched",0);

	this.instance_1 = new lib.shape1002("synched",0);

	this.instance_2 = new lib.shape1004("synched",0);

	this.instance_3 = new lib.shape1006("synched",0);

	this.instance_4 = new lib.shape1008("synched",0);

	this.instance_5 = new lib.shape1010("synched",0);

	this.instance_6 = new lib.shape1012("synched",0);

	this.instance_7 = new lib.shape1014("synched",0);

	this.instance_8 = new lib.shape1016("synched",0);

	this.instance_9 = new lib.shape1018("synched",0);

	this.instance_10 = new lib.shape1020("synched",0);

	this.instance_11 = new lib.shape1022("synched",0);

	this.instance_12 = new lib.shape1024("synched",0);

	this.instance_13 = new lib.shape1026("synched",0);

	this.instance_14 = new lib.shape1028("synched",0);

	this.instance_15 = new lib.shape1030("synched",0);

	this.instance_16 = new lib.shape1032("synched",0);

	this.instance_17 = new lib.shape1034("synched",0);

	this.instance_18 = new lib.shape1036("synched",0);

	this.instance_19 = new lib.shape1038("synched",0);

	this.instance_20 = new lib.shape1040("synched",0);

	this.instance_21 = new lib.shape1042("synched",0);

	this.instance_22 = new lib.shape1044("synched",0);

	this.instance_23 = new lib.shape1046("synched",0);

	this.instance_24 = new lib.shape1048("synched",0);

	this.instance_25 = new lib.shape1050("synched",0);

	this.instance_26 = new lib.shape1052("synched",0);

	this.instance_27 = new lib.shape1054("synched",0);

	this.instance_28 = new lib.shape1056("synched",0);

	this.instance_29 = new lib.shape1058("synched",0);

	this.instance_30 = new lib.shape1060("synched",0);

	this.instance_31 = new lib.shape1062("synched",0);

	this.instance_32 = new lib.shape1064("synched",0);

	this.instance_33 = new lib.shape1066("synched",0);

	this.instance_34 = new lib.shape1068("synched",0);

	this.instance_35 = new lib.shape1070("synched",0);

	this.instance_36 = new lib.shape1072("synched",0);

	this.instance_37 = new lib.shape1074("synched",0);

	this.instance_38 = new lib.shape1076("synched",0);

	this.instance_39 = new lib.shape1078("synched",0);

	this.instance_40 = new lib.shape1080("synched",0);

	this.instance_41 = new lib.shape1082("synched",0);

	this.instance_42 = new lib.shape1084("synched",0);

	this.instance_43 = new lib.shape1086("synched",0);

	this.instance_44 = new lib.shape1088("synched",0);

	this.instance_45 = new lib.shape1090("synched",0);

	this.instance_46 = new lib.shape1092("synched",0);

	this.instance_47 = new lib.shape1094("synched",0);

	this.instance_48 = new lib.shape1096("synched",0);

	this.instance_49 = new lib.shape1098("synched",0);

	this.instance_50 = new lib.shape1100("synched",0);

	this.instance_51 = new lib.shape1102("synched",0);

	this.instance_52 = new lib.shape1104("synched",0);

	this.instance_53 = new lib.shape1106("synched",0);

	this.instance_54 = new lib.shape1108("synched",0);

	this.instance_55 = new lib.shape1110("synched",0);

	this.instance_56 = new lib.shape1112("synched",0);

	this.instance_57 = new lib.shape1114("synched",0);

	this.instance_58 = new lib.shape1116("synched",0);

	this.instance_59 = new lib.shape1118("synched",0);

	this.instance_60 = new lib.shape1120("synched",0);

	this.instance_61 = new lib.shape1122("synched",0);

	this.instance_62 = new lib.shape1124("synched",0);

	this.instance_63 = new lib.shape1126("synched",0);

	this.instance_64 = new lib.shape1128("synched",0);

	this.instance_65 = new lib.shape1130("synched",0);

	this.instance_66 = new lib.shape1132("synched",0);

	this.instance_67 = new lib.shape1134("synched",0);

	this.instance_68 = new lib.shape1136("synched",0);

	this.instance_69 = new lib.shape1138("synched",0);

	this.instance_70 = new lib.shape1140("synched",0);

	this.instance_71 = new lib.shape1142("synched",0);

	this.instance_72 = new lib.shape1144("synched",0);

	this.instance_73 = new lib.shape1146("synched",0);

	this.instance_74 = new lib.shape1148("synched",0);

	this.instance_75 = new lib.shape1150("synched",0);

	this.instance_76 = new lib.shape1152("synched",0);

	this.instance_77 = new lib.shape1154("synched",0);

	this.instance_78 = new lib.shape1156("synched",0);

	this.instance_79 = new lib.shape1158("synched",0);

	this.instance_80 = new lib.shape1160("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},3).to({state:[{t:this.instance_2}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_6}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_8}]},3).to({state:[{t:this.instance_9}]},3).to({state:[{t:this.instance_10}]},3).to({state:[{t:this.instance_11}]},3).to({state:[{t:this.instance_12}]},3).to({state:[{t:this.instance_13}]},3).to({state:[{t:this.instance_14}]},3).to({state:[{t:this.instance_15}]},3).to({state:[{t:this.instance_16}]},3).to({state:[{t:this.instance_17}]},3).to({state:[{t:this.instance_18}]},3).to({state:[{t:this.instance_19}]},3).to({state:[{t:this.instance_20}]},3).to({state:[{t:this.instance_21}]},3).to({state:[{t:this.instance_22}]},3).to({state:[{t:this.instance_23}]},3).to({state:[{t:this.instance_24}]},3).to({state:[{t:this.instance_25}]},3).to({state:[{t:this.instance_26}]},3).to({state:[{t:this.instance_27}]},3).to({state:[{t:this.instance_28}]},3).to({state:[{t:this.instance_29}]},3).to({state:[{t:this.instance_30}]},3).to({state:[{t:this.instance_31}]},3).to({state:[{t:this.instance_32}]},3).to({state:[{t:this.instance_33}]},3).to({state:[{t:this.instance_34}]},3).to({state:[{t:this.instance_35}]},3).to({state:[{t:this.instance_36}]},3).to({state:[{t:this.instance_37}]},3).to({state:[{t:this.instance_38}]},3).to({state:[{t:this.instance_39}]},3).to({state:[{t:this.instance_40}]},3).to({state:[{t:this.instance_41}]},3).to({state:[{t:this.instance_42}]},3).to({state:[{t:this.instance_43}]},3).to({state:[{t:this.instance_44}]},3).to({state:[{t:this.instance_45}]},3).to({state:[{t:this.instance_46}]},3).to({state:[{t:this.instance_47}]},3).to({state:[{t:this.instance_48}]},3).to({state:[{t:this.instance_49}]},3).to({state:[{t:this.instance_50}]},3).to({state:[{t:this.instance_51}]},3).to({state:[{t:this.instance_52}]},3).to({state:[{t:this.instance_53}]},3).to({state:[{t:this.instance_54}]},3).to({state:[{t:this.instance_55}]},3).to({state:[{t:this.instance_56}]},3).to({state:[{t:this.instance_57}]},3).to({state:[{t:this.instance_58}]},3).to({state:[{t:this.instance_59}]},3).to({state:[{t:this.instance_60}]},3).to({state:[{t:this.instance_61}]},3).to({state:[{t:this.instance_62}]},3).to({state:[{t:this.instance_63}]},3).to({state:[{t:this.instance_64}]},3).to({state:[{t:this.instance_65}]},3).to({state:[{t:this.instance_66}]},3).to({state:[{t:this.instance_67}]},3).to({state:[{t:this.instance_68}]},3).to({state:[{t:this.instance_69}]},3).to({state:[{t:this.instance_70}]},3).to({state:[{t:this.instance_71}]},3).to({state:[{t:this.instance_72}]},3).to({state:[{t:this.instance_73}]},3).to({state:[{t:this.instance_74}]},3).to({state:[{t:this.instance_75}]},3).to({state:[{t:this.instance_76}]},3).to({state:[{t:this.instance_77}]},3).to({state:[{t:this.instance_78}]},3).to({state:[{t:this.instance_79}]},3).to({state:[{t:this.instance_80}]},3).to({state:[{t:this.instance}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_00N_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape996("synched",0);

	this.instance_1 = new lib.shape998("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_02B_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape992("synched",0);

	this.instance_1 = new lib.shape994("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_01B_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"again":6});

	// Layer 1
	this.instance = new lib.shape528("synched",0);

	this.instance_1 = new lib.shape958("synched",0);

	this.instance_2 = new lib.shape960("synched",0);

	this.instance_3 = new lib.shape962("synched",0);

	this.instance_4 = new lib.shape964("synched",0);

	this.instance_5 = new lib.shape966("synched",0);

	this.instance_6 = new lib.shape968("synched",0);

	this.instance_7 = new lib.shape970("synched",0);

	this.instance_8 = new lib.shape972("synched",0);

	this.instance_9 = new lib.shape974("synched",0);

	this.instance_10 = new lib.shape976("synched",0);

	this.instance_11 = new lib.shape978("synched",0);

	this.instance_12 = new lib.shape980("synched",0);

	this.instance_13 = new lib.shape982("synched",0);

	this.instance_14 = new lib.shape984("synched",0);

	this.instance_15 = new lib.shape986("synched",0);

	this.instance_16 = new lib.shape988("synched",0);

	this.instance_17 = new lib.shape990("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},3).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_06N_A23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape887("synched",0);

	this.instance_1 = new lib.shape889("synched",0);

	this.instance_2 = new lib.shape891("synched",0);

	this.instance_3 = new lib.shape893("synched",0);

	this.instance_4 = new lib.shape895("synched",0);

	this.instance_5 = new lib.shape897("synched",0);

	this.instance_6 = new lib.shape899("synched",0);

	this.instance_7 = new lib.shape901("synched",0);

	this.instance_8 = new lib.shape903("synched",0);

	this.instance_9 = new lib.shape905("synched",0);

	this.instance_10 = new lib.shape907("synched",0);

	this.instance_11 = new lib.shape909("synched",0);

	this.instance_12 = new lib.shape911("synched",0);

	this.instance_13 = new lib.shape913("synched",0);

	this.instance_14 = new lib.shape915("synched",0);

	this.instance_15 = new lib.shape917("synched",0);

	this.instance_16 = new lib.shape919("synched",0);

	this.instance_17 = new lib.shape921("synched",0);

	this.instance_18 = new lib.shape923("synched",0);

	this.instance_19 = new lib.shape925("synched",0);

	this.instance_20 = new lib.shape927("synched",0);

	this.instance_21 = new lib.shape929("synched",0);

	this.instance_22 = new lib.shape931("synched",0);

	this.instance_23 = new lib.shape933("synched",0);

	this.instance_24 = new lib.shape935("synched",0);

	this.instance_25 = new lib.shape937("synched",0);

	this.instance_26 = new lib.shape939("synched",0);

	this.instance_27 = new lib.shape941("synched",0);

	this.instance_28 = new lib.shape943("synched",0);

	this.instance_29 = new lib.shape945("synched",0);

	this.instance_30 = new lib.shape947("synched",0);

	this.instance_31 = new lib.shape949("synched",0);

	this.instance_32 = new lib.shape951("synched",0);

	this.instance_33 = new lib.shape953("synched",0);

	this.instance_34 = new lib.shape955("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_08N_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape873("synched",0);

	this.instance_1 = new lib.shape875("synched",0);

	this.instance_2 = new lib.shape877("synched",0);

	this.instance_3 = new lib.shape879("synched",0);

	this.instance_4 = new lib.shape541("synched",0);

	this.instance_5 = new lib.shape543("synched",0);

	this.instance_6 = new lib.shape545("synched",0);

	this.instance_7 = new lib.shape547("synched",0);

	this.instance_8 = new lib.shape549("synched",0);

	this.instance_9 = new lib.shape551("synched",0);

	this.instance_10 = new lib.shape553("synched",0);

	this.instance_11 = new lib.shape555("synched",0);

	this.instance_12 = new lib.shape557("synched",0);

	this.instance_13 = new lib.shape559("synched",0);

	this.instance_14 = new lib.shape561("synched",0);

	this.instance_15 = new lib.shape563("synched",0);

	this.instance_16 = new lib.shape565("synched",0);

	this.instance_17 = new lib.shape567("synched",0);

	this.instance_18 = new lib.shape569("synched",0);

	this.instance_19 = new lib.shape571("synched",0);

	this.instance_20 = new lib.shape573("synched",0);

	this.instance_21 = new lib.shape575("synched",0);

	this.instance_22 = new lib.shape577("synched",0);

	this.instance_23 = new lib.shape579("synched",0);

	this.instance_24 = new lib.shape581("synched",0);

	this.instance_25 = new lib.shape583("synched",0);

	this.instance_26 = new lib.shape585("synched",0);

	this.instance_27 = new lib.shape587("synched",0);

	this.instance_28 = new lib.shape589("synched",0);

	this.instance_29 = new lib.shape591("synched",0);

	this.instance_30 = new lib.shape593("synched",0);

	this.instance_31 = new lib.shape595("synched",0);

	this.instance_32 = new lib.shape597("synched",0);

	this.instance_33 = new lib.shape599("synched",0);

	this.instance_34 = new lib.shape601("synched",0);

	this.instance_35 = new lib.shape603("synched",0);

	this.instance_36 = new lib.shape605("synched",0);

	this.instance_37 = new lib.shape607("synched",0);

	this.instance_38 = new lib.shape609("synched",0);

	this.instance_39 = new lib.shape611("synched",0);

	this.instance_40 = new lib.shape613("synched",0);

	this.instance_41 = new lib.shape615("synched",0);

	this.instance_42 = new lib.shape617("synched",0);

	this.instance_43 = new lib.shape619("synched",0);

	this.instance_44 = new lib.shape621("synched",0);

	this.instance_45 = new lib.shape623("synched",0);

	this.instance_46 = new lib.shape625("synched",0);

	this.instance_47 = new lib.shape627("synched",0);

	this.instance_48 = new lib.shape629("synched",0);

	this.instance_49 = new lib.shape631("synched",0);

	this.instance_50 = new lib.shape633("synched",0);

	this.instance_51 = new lib.shape635("synched",0);

	this.instance_52 = new lib.shape637("synched",0);

	this.instance_53 = new lib.shape639("synched",0);

	this.instance_54 = new lib.shape641("synched",0);

	this.instance_55 = new lib.shape643("synched",0);

	this.instance_56 = new lib.shape645("synched",0);

	this.instance_57 = new lib.shape647("synched",0);

	this.instance_58 = new lib.shape649("synched",0);

	this.instance_59 = new lib.shape651("synched",0);

	this.instance_60 = new lib.shape653("synched",0);

	this.instance_61 = new lib.shape655("synched",0);

	this.instance_62 = new lib.shape657("synched",0);

	this.instance_63 = new lib.shape659("synched",0);

	this.instance_64 = new lib.shape661("synched",0);

	this.instance_65 = new lib.shape663("synched",0);

	this.instance_66 = new lib.shape665("synched",0);

	this.instance_67 = new lib.shape667("synched",0);

	this.instance_68 = new lib.shape669("synched",0);

	this.instance_69 = new lib.shape671("synched",0);

	this.instance_70 = new lib.shape673("synched",0);

	this.instance_71 = new lib.shape675("synched",0);

	this.instance_72 = new lib.shape677("synched",0);

	this.instance_73 = new lib.shape679("synched",0);

	this.instance_74 = new lib.shape681("synched",0);

	this.instance_75 = new lib.shape683("synched",0);

	this.instance_76 = new lib.shape685("synched",0);

	this.instance_77 = new lib.shape687("synched",0);

	this.instance_78 = new lib.shape689("synched",0);

	this.instance_79 = new lib.shape691("synched",0);

	this.instance_80 = new lib.shape693("synched",0);

	this.instance_81 = new lib.shape695("synched",0);

	this.instance_82 = new lib.shape697("synched",0);

	this.instance_83 = new lib.shape699("synched",0);

	this.instance_84 = new lib.shape701("synched",0);

	this.instance_85 = new lib.shape703("synched",0);

	this.instance_86 = new lib.shape705("synched",0);

	this.instance_87 = new lib.shape707("synched",0);

	this.instance_88 = new lib.shape709("synched",0);

	this.instance_89 = new lib.shape711("synched",0);

	this.instance_90 = new lib.shape713("synched",0);

	this.instance_91 = new lib.shape715("synched",0);

	this.instance_92 = new lib.shape717("synched",0);

	this.instance_93 = new lib.shape719("synched",0);

	this.instance_94 = new lib.shape721("synched",0);

	this.instance_95 = new lib.shape881("synched",0);

	this.instance_96 = new lib.shape883("synched",0);

	this.instance_97 = new lib.shape885("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).to({state:[{t:this.instance_35}]},2).to({state:[{t:this.instance_36}]},2).to({state:[{t:this.instance_37}]},2).to({state:[{t:this.instance_38}]},2).to({state:[{t:this.instance_39}]},2).to({state:[{t:this.instance_40}]},2).to({state:[{t:this.instance_41}]},2).to({state:[{t:this.instance_42}]},2).to({state:[{t:this.instance_43}]},2).to({state:[{t:this.instance_44}]},2).to({state:[{t:this.instance_45}]},2).to({state:[{t:this.instance_46}]},2).to({state:[{t:this.instance_47}]},2).to({state:[{t:this.instance_48}]},2).to({state:[{t:this.instance_49}]},2).to({state:[{t:this.instance_50}]},2).to({state:[{t:this.instance_51}]},2).to({state:[{t:this.instance_52}]},2).to({state:[{t:this.instance_53}]},2).to({state:[{t:this.instance_54}]},2).to({state:[{t:this.instance_55}]},2).to({state:[{t:this.instance_56}]},2).to({state:[{t:this.instance_57}]},2).to({state:[{t:this.instance_58}]},2).to({state:[{t:this.instance_59}]},2).to({state:[{t:this.instance_60}]},2).to({state:[{t:this.instance_61}]},2).to({state:[{t:this.instance_62}]},2).to({state:[{t:this.instance_63}]},2).to({state:[{t:this.instance_64}]},2).to({state:[{t:this.instance_65}]},2).to({state:[{t:this.instance_66}]},2).to({state:[{t:this.instance_67}]},2).to({state:[{t:this.instance_68}]},2).to({state:[{t:this.instance_69}]},2).to({state:[{t:this.instance_70}]},2).to({state:[{t:this.instance_71}]},2).to({state:[{t:this.instance_72}]},2).to({state:[{t:this.instance_73}]},2).to({state:[{t:this.instance_74}]},2).to({state:[{t:this.instance_75}]},2).to({state:[{t:this.instance_76}]},2).to({state:[{t:this.instance_77}]},2).to({state:[{t:this.instance_78}]},2).to({state:[{t:this.instance_79}]},2).to({state:[{t:this.instance_80}]},2).to({state:[{t:this.instance_81}]},2).to({state:[{t:this.instance_82}]},2).to({state:[{t:this.instance_83}]},2).to({state:[{t:this.instance_84}]},2).to({state:[{t:this.instance_85}]},2).to({state:[{t:this.instance_86}]},2).to({state:[{t:this.instance_87}]},2).to({state:[{t:this.instance_88}]},2).to({state:[{t:this.instance_89}]},2).to({state:[{t:this.instance_90}]},2).to({state:[{t:this.instance_91}]},2).to({state:[{t:this.instance_92}]},2).to({state:[{t:this.instance_93}]},2).to({state:[{t:this.instance_94}]},2).to({state:[{t:this.instance_95}]},2).to({state:[{t:this.instance_96}]},2).to({state:[{t:this.instance_97}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_07B_A23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape803("synched",0);

	this.instance_1 = new lib.shape805("synched",0);

	this.instance_2 = new lib.shape807("synched",0);

	this.instance_3 = new lib.shape809("synched",0);

	this.instance_4 = new lib.shape811("synched",0);

	this.instance_5 = new lib.shape813("synched",0);

	this.instance_6 = new lib.shape815("synched",0);

	this.instance_7 = new lib.shape817("synched",0);

	this.instance_8 = new lib.shape819("synched",0);

	this.instance_9 = new lib.shape821("synched",0);

	this.instance_10 = new lib.shape823("synched",0);

	this.instance_11 = new lib.shape825("synched",0);

	this.instance_12 = new lib.shape827("synched",0);

	this.instance_13 = new lib.shape829("synched",0);

	this.instance_14 = new lib.shape831("synched",0);

	this.instance_15 = new lib.shape833("synched",0);

	this.instance_16 = new lib.shape835("synched",0);

	this.instance_17 = new lib.shape837("synched",0);

	this.instance_18 = new lib.shape839("synched",0);

	this.instance_19 = new lib.shape841("synched",0);

	this.instance_20 = new lib.shape843("synched",0);

	this.instance_21 = new lib.shape845("synched",0);

	this.instance_22 = new lib.shape847("synched",0);

	this.instance_23 = new lib.shape849("synched",0);

	this.instance_24 = new lib.shape851("synched",0);

	this.instance_25 = new lib.shape853("synched",0);

	this.instance_26 = new lib.shape855("synched",0);

	this.instance_27 = new lib.shape857("synched",0);

	this.instance_28 = new lib.shape859("synched",0);

	this.instance_29 = new lib.shape861("synched",0);

	this.instance_30 = new lib.shape863("synched",0);

	this.instance_31 = new lib.shape865("synched",0);

	this.instance_32 = new lib.shape867("synched",0);

	this.instance_33 = new lib.shape869("synched",0);

	this.instance_34 = new lib.shape871("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_05B_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape730("synched",0);

	this.instance_1 = new lib.shape732("synched",0);

	this.instance_2 = new lib.shape734("synched",0);

	this.instance_3 = new lib.shape736("synched",0);

	this.instance_4 = new lib.shape738("synched",0);

	this.instance_5 = new lib.shape740("synched",0);

	this.instance_6 = new lib.shape742("synched",0);

	this.instance_7 = new lib.shape744("synched",0);

	this.instance_8 = new lib.shape746("synched",0);

	this.instance_9 = new lib.shape748("synched",0);

	this.instance_10 = new lib.shape750("synched",0);

	this.instance_11 = new lib.shape752("synched",0);

	this.instance_12 = new lib.shape754("synched",0);

	this.instance_13 = new lib.shape756("synched",0);

	this.instance_14 = new lib.shape758("synched",0);

	this.instance_15 = new lib.shape760("synched",0);

	this.instance_16 = new lib.shape762("synched",0);

	this.instance_17 = new lib.shape764("synched",0);

	this.instance_18 = new lib.shape766("synched",0);

	this.instance_19 = new lib.shape768("synched",0);

	this.instance_20 = new lib.shape770("synched",0);

	this.instance_21 = new lib.shape772("synched",0);

	this.instance_22 = new lib.shape774("synched",0);

	this.instance_23 = new lib.shape776("synched",0);

	this.instance_24 = new lib.shape778("synched",0);

	this.instance_25 = new lib.shape780("synched",0);

	this.instance_26 = new lib.shape782("synched",0);

	this.instance_27 = new lib.shape784("synched",0);

	this.instance_28 = new lib.shape786("synched",0);

	this.instance_29 = new lib.shape788("synched",0);

	this.instance_30 = new lib.shape790("synched",0);

	this.instance_31 = new lib.shape792("synched",0);

	this.instance_32 = new lib.shape794("synched",0);

	this.instance_33 = new lib.shape796("synched",0);

	this.instance_34 = new lib.shape798("synched",0);

	this.instance_35 = new lib.shape800("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).to({state:[{t:this.instance_35}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_08B_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape533("synched",0);

	this.instance_1 = new lib.shape535("synched",0);

	this.instance_2 = new lib.shape537("synched",0);

	this.instance_3 = new lib.shape539("synched",0);

	this.instance_4 = new lib.shape541("synched",0);

	this.instance_5 = new lib.shape543("synched",0);

	this.instance_6 = new lib.shape545("synched",0);

	this.instance_7 = new lib.shape547("synched",0);

	this.instance_8 = new lib.shape549("synched",0);

	this.instance_9 = new lib.shape551("synched",0);

	this.instance_10 = new lib.shape553("synched",0);

	this.instance_11 = new lib.shape555("synched",0);

	this.instance_12 = new lib.shape557("synched",0);

	this.instance_13 = new lib.shape559("synched",0);

	this.instance_14 = new lib.shape561("synched",0);

	this.instance_15 = new lib.shape563("synched",0);

	this.instance_16 = new lib.shape565("synched",0);

	this.instance_17 = new lib.shape567("synched",0);

	this.instance_18 = new lib.shape569("synched",0);

	this.instance_19 = new lib.shape571("synched",0);

	this.instance_20 = new lib.shape573("synched",0);

	this.instance_21 = new lib.shape575("synched",0);

	this.instance_22 = new lib.shape577("synched",0);

	this.instance_23 = new lib.shape579("synched",0);

	this.instance_24 = new lib.shape581("synched",0);

	this.instance_25 = new lib.shape583("synched",0);

	this.instance_26 = new lib.shape585("synched",0);

	this.instance_27 = new lib.shape587("synched",0);

	this.instance_28 = new lib.shape589("synched",0);

	this.instance_29 = new lib.shape591("synched",0);

	this.instance_30 = new lib.shape593("synched",0);

	this.instance_31 = new lib.shape595("synched",0);

	this.instance_32 = new lib.shape597("synched",0);

	this.instance_33 = new lib.shape599("synched",0);

	this.instance_34 = new lib.shape601("synched",0);

	this.instance_35 = new lib.shape603("synched",0);

	this.instance_36 = new lib.shape605("synched",0);

	this.instance_37 = new lib.shape607("synched",0);

	this.instance_38 = new lib.shape609("synched",0);

	this.instance_39 = new lib.shape611("synched",0);

	this.instance_40 = new lib.shape613("synched",0);

	this.instance_41 = new lib.shape615("synched",0);

	this.instance_42 = new lib.shape617("synched",0);

	this.instance_43 = new lib.shape619("synched",0);

	this.instance_44 = new lib.shape621("synched",0);

	this.instance_45 = new lib.shape623("synched",0);

	this.instance_46 = new lib.shape625("synched",0);

	this.instance_47 = new lib.shape627("synched",0);

	this.instance_48 = new lib.shape629("synched",0);

	this.instance_49 = new lib.shape631("synched",0);

	this.instance_50 = new lib.shape633("synched",0);

	this.instance_51 = new lib.shape635("synched",0);

	this.instance_52 = new lib.shape637("synched",0);

	this.instance_53 = new lib.shape639("synched",0);

	this.instance_54 = new lib.shape641("synched",0);

	this.instance_55 = new lib.shape643("synched",0);

	this.instance_56 = new lib.shape645("synched",0);

	this.instance_57 = new lib.shape647("synched",0);

	this.instance_58 = new lib.shape649("synched",0);

	this.instance_59 = new lib.shape651("synched",0);

	this.instance_60 = new lib.shape653("synched",0);

	this.instance_61 = new lib.shape655("synched",0);

	this.instance_62 = new lib.shape657("synched",0);

	this.instance_63 = new lib.shape659("synched",0);

	this.instance_64 = new lib.shape661("synched",0);

	this.instance_65 = new lib.shape663("synched",0);

	this.instance_66 = new lib.shape665("synched",0);

	this.instance_67 = new lib.shape667("synched",0);

	this.instance_68 = new lib.shape669("synched",0);

	this.instance_69 = new lib.shape671("synched",0);

	this.instance_70 = new lib.shape673("synched",0);

	this.instance_71 = new lib.shape675("synched",0);

	this.instance_72 = new lib.shape677("synched",0);

	this.instance_73 = new lib.shape679("synched",0);

	this.instance_74 = new lib.shape681("synched",0);

	this.instance_75 = new lib.shape683("synched",0);

	this.instance_76 = new lib.shape685("synched",0);

	this.instance_77 = new lib.shape687("synched",0);

	this.instance_78 = new lib.shape689("synched",0);

	this.instance_79 = new lib.shape691("synched",0);

	this.instance_80 = new lib.shape693("synched",0);

	this.instance_81 = new lib.shape695("synched",0);

	this.instance_82 = new lib.shape697("synched",0);

	this.instance_83 = new lib.shape699("synched",0);

	this.instance_84 = new lib.shape701("synched",0);

	this.instance_85 = new lib.shape703("synched",0);

	this.instance_86 = new lib.shape705("synched",0);

	this.instance_87 = new lib.shape707("synched",0);

	this.instance_88 = new lib.shape709("synched",0);

	this.instance_89 = new lib.shape711("synched",0);

	this.instance_90 = new lib.shape713("synched",0);

	this.instance_91 = new lib.shape715("synched",0);

	this.instance_92 = new lib.shape717("synched",0);

	this.instance_93 = new lib.shape719("synched",0);

	this.instance_94 = new lib.shape721("synched",0);

	this.instance_95 = new lib.shape723("synched",0);

	this.instance_96 = new lib.shape725("synched",0);

	this.instance_97 = new lib.shape727("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).to({state:[{t:this.instance_35}]},2).to({state:[{t:this.instance_36}]},2).to({state:[{t:this.instance_37}]},2).to({state:[{t:this.instance_38}]},2).to({state:[{t:this.instance_39}]},2).to({state:[{t:this.instance_40}]},2).to({state:[{t:this.instance_41}]},2).to({state:[{t:this.instance_42}]},2).to({state:[{t:this.instance_43}]},2).to({state:[{t:this.instance_44}]},2).to({state:[{t:this.instance_45}]},2).to({state:[{t:this.instance_46}]},2).to({state:[{t:this.instance_47}]},2).to({state:[{t:this.instance_48}]},2).to({state:[{t:this.instance_49}]},2).to({state:[{t:this.instance_50}]},2).to({state:[{t:this.instance_51}]},2).to({state:[{t:this.instance_52}]},2).to({state:[{t:this.instance_53}]},2).to({state:[{t:this.instance_54}]},2).to({state:[{t:this.instance_55}]},2).to({state:[{t:this.instance_56}]},2).to({state:[{t:this.instance_57}]},2).to({state:[{t:this.instance_58}]},2).to({state:[{t:this.instance_59}]},2).to({state:[{t:this.instance_60}]},2).to({state:[{t:this.instance_61}]},2).to({state:[{t:this.instance_62}]},2).to({state:[{t:this.instance_63}]},2).to({state:[{t:this.instance_64}]},2).to({state:[{t:this.instance_65}]},2).to({state:[{t:this.instance_66}]},2).to({state:[{t:this.instance_67}]},2).to({state:[{t:this.instance_68}]},2).to({state:[{t:this.instance_69}]},2).to({state:[{t:this.instance_70}]},2).to({state:[{t:this.instance_71}]},2).to({state:[{t:this.instance_72}]},2).to({state:[{t:this.instance_73}]},2).to({state:[{t:this.instance_74}]},2).to({state:[{t:this.instance_75}]},2).to({state:[{t:this.instance_76}]},2).to({state:[{t:this.instance_77}]},2).to({state:[{t:this.instance_78}]},2).to({state:[{t:this.instance_79}]},2).to({state:[{t:this.instance_80}]},2).to({state:[{t:this.instance_81}]},2).to({state:[{t:this.instance_82}]},2).to({state:[{t:this.instance_83}]},2).to({state:[{t:this.instance_84}]},2).to({state:[{t:this.instance_85}]},2).to({state:[{t:this.instance_86}]},2).to({state:[{t:this.instance_87}]},2).to({state:[{t:this.instance_88}]},2).to({state:[{t:this.instance_89}]},2).to({state:[{t:this.instance_90}]},2).to({state:[{t:this.instance_91}]},2).to({state:[{t:this.instance_92}]},2).to({state:[{t:this.instance_93}]},2).to({state:[{t:this.instance_94}]},2).to({state:[{t:this.instance_95}]},2).to({state:[{t:this.instance_96}]},2).to({state:[{t:this.instance_97}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_01B_A3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape528("synched",0);

	this.instance_1 = new lib.shape530("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.assetsgfxT_07N_A45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{e:130});

	// Layer 1
	this.instance = new lib.shape383("synched",0);

	this.instance_1 = new lib.shape385("synched",0);

	this.instance_2 = new lib.shape387("synched",0);

	this.instance_3 = new lib.shape389("synched",0);

	this.instance_4 = new lib.shape391("synched",0);

	this.instance_5 = new lib.shape393("synched",0);

	this.instance_6 = new lib.shape395("synched",0);

	this.instance_7 = new lib.shape397("synched",0);

	this.instance_8 = new lib.shape399("synched",0);

	this.instance_9 = new lib.shape401("synched",0);

	this.instance_10 = new lib.shape403("synched",0);

	this.instance_11 = new lib.shape405("synched",0);

	this.instance_12 = new lib.shape407("synched",0);

	this.instance_13 = new lib.shape409("synched",0);

	this.instance_14 = new lib.shape411("synched",0);

	this.instance_15 = new lib.shape413("synched",0);

	this.instance_16 = new lib.shape415("synched",0);

	this.instance_17 = new lib.shape417("synched",0);

	this.instance_18 = new lib.shape419("synched",0);

	this.instance_19 = new lib.shape421("synched",0);

	this.instance_20 = new lib.shape423("synched",0);

	this.instance_21 = new lib.shape425("synched",0);

	this.instance_22 = new lib.shape427("synched",0);

	this.instance_23 = new lib.shape429("synched",0);

	this.instance_24 = new lib.shape431("synched",0);

	this.instance_25 = new lib.shape433("synched",0);

	this.instance_26 = new lib.shape435("synched",0);

	this.instance_27 = new lib.shape437("synched",0);

	this.instance_28 = new lib.shape439("synched",0);

	this.instance_29 = new lib.shape441("synched",0);

	this.instance_30 = new lib.shape443("synched",0);

	this.instance_31 = new lib.shape445("synched",0);

	this.instance_32 = new lib.shape447("synched",0);

	this.instance_33 = new lib.shape449("synched",0);

	this.instance_34 = new lib.shape451("synched",0);

	this.instance_35 = new lib.shape453("synched",0);

	this.instance_36 = new lib.shape455("synched",0);

	this.instance_37 = new lib.shape457("synched",0);

	this.instance_38 = new lib.shape459("synched",0);

	this.instance_39 = new lib.shape461("synched",0);

	this.instance_40 = new lib.shape463("synched",0);

	this.instance_41 = new lib.shape465("synched",0);

	this.instance_42 = new lib.shape467("synched",0);

	this.instance_43 = new lib.shape469("synched",0);

	this.instance_44 = new lib.shape471("synched",0);

	this.instance_45 = new lib.shape473("synched",0);

	this.instance_46 = new lib.shape475("synched",0);

	this.instance_47 = new lib.shape477("synched",0);

	this.instance_48 = new lib.shape479("synched",0);

	this.instance_49 = new lib.shape481("synched",0);

	this.instance_50 = new lib.shape483("synched",0);

	this.instance_51 = new lib.shape485("synched",0);

	this.instance_52 = new lib.shape487("synched",0);

	this.instance_53 = new lib.shape489("synched",0);

	this.instance_54 = new lib.shape491("synched",0);

	this.instance_55 = new lib.shape493("synched",0);

	this.instance_56 = new lib.shape495("synched",0);

	this.instance_57 = new lib.shape497("synched",0);

	this.instance_58 = new lib.shape499("synched",0);

	this.instance_59 = new lib.shape501("synched",0);

	this.instance_60 = new lib.shape503("synched",0);

	this.instance_61 = new lib.shape505("synched",0);

	this.instance_62 = new lib.shape507("synched",0);

	this.instance_63 = new lib.shape509("synched",0);

	this.instance_64 = new lib.shape511("synched",0);

	this.instance_65 = new lib.shape513("synched",0);

	this.instance_66 = new lib.shape515("synched",0);

	this.instance_67 = new lib.shape517("synched",0);

	this.instance_68 = new lib.shape519("synched",0);

	this.instance_69 = new lib.shape521("synched",0);

	this.instance_70 = new lib.shape523("synched",0);

	this.instance_71 = new lib.shape525("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_17}]},2).to({state:[{t:this.instance_18}]},2).to({state:[{t:this.instance_19}]},2).to({state:[{t:this.instance_20}]},2).to({state:[{t:this.instance_21}]},2).to({state:[{t:this.instance_22}]},2).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},2).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},2).to({state:[{t:this.instance_27}]},2).to({state:[{t:this.instance_28}]},2).to({state:[{t:this.instance_29}]},2).to({state:[{t:this.instance_30}]},2).to({state:[{t:this.instance_31}]},2).to({state:[{t:this.instance_32}]},2).to({state:[{t:this.instance_33}]},2).to({state:[{t:this.instance_34}]},2).to({state:[{t:this.instance_35}]},2).to({state:[{t:this.instance_36}]},2).to({state:[{t:this.instance_37}]},2).to({state:[{t:this.instance_38}]},2).to({state:[{t:this.instance_39}]},2).to({state:[{t:this.instance_40}]},2).to({state:[{t:this.instance_41}]},2).to({state:[{t:this.instance_42}]},2).to({state:[{t:this.instance_43}]},2).to({state:[{t:this.instance_44}]},2).to({state:[{t:this.instance_45}]},2).to({state:[{t:this.instance_46}]},2).to({state:[{t:this.instance_47}]},2).to({state:[{t:this.instance_48}]},2).to({state:[{t:this.instance_49}]},2).to({state:[{t:this.instance_50}]},2).to({state:[{t:this.instance_51}]},2).to({state:[{t:this.instance_52}]},2).to({state:[{t:this.instance_53}]},2).to({state:[{t:this.instance_54}]},2).to({state:[{t:this.instance_55}]},2).to({state:[{t:this.instance_56}]},2).to({state:[{t:this.instance_57}]},2).to({state:[{t:this.instance_58}]},2).to({state:[{t:this.instance_59}]},2).to({state:[{t:this.instance_60}]},2).to({state:[{t:this.instance_61}]},2).to({state:[{t:this.instance_62}]},2).to({state:[{t:this.instance_63}]},2).to({state:[{t:this.instance_64}]},2).to({state:[{t:this.instance_65}]},2).to({state:[{t:this.instance_66}]},2).to({state:[{t:this.instance_67}]},2).to({state:[{t:this.instance_68}]},2).to({state:[{t:this.instance_69}]},2).to({state:[{t:this.instance_70}]},2).to({state:[{t:this.instance_71}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,100);


(lib.GameLib_flaWindowIcon_127 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 3
	this.instance = new lib.text375("synched",0);
	this.instance.setTransform(2,2);
	this.instance.shadow = new cjs.Shadow("#000000",4,4,5);

	this.instance_1 = new lib.text377("synched",0);
	this.instance_1.setTransform(2,2);
	this.instance_1.shadow = new cjs.Shadow("#000000",4,4,5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

	// Layer 2
	this.instance_2 = new lib.text374("synched",0);
	this.instance_2.setTransform(-41.7,119);

	this.instance_3 = new lib.shape376("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).wait(1));

	// Layer 1
	this.instance_4 = new lib.shape373("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(9,-2,162,347.1);


(lib.sprite367 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 68
	this.instance = new lib.shape354("synched",0);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,0,0,0)").ss(0.1,1,1).p("AwPo0MAgfAAAIAARpMggfAAAg");
	this.shape.setTransform(320,333.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(1,1,1,0)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_1.setTransform(320,333.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(1,1,1,0.11)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_2.setTransform(320,333.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(1,1,1,0.224)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_3.setTransform(320,333.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(1,1,1,0.333)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_4.setTransform(320,333.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(1,1,1,0.443)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_5.setTransform(320,333.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(1,1,1,0.557)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_6.setTransform(320,333.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(1,1,1,0.667)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_7.setTransform(320,333.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(1,1,1,0.776)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_8.setTransform(320,333.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(1,1,1,0.89)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_9.setTransform(320,333.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#010101").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_10.setTransform(320,333.5);

	this.instance_1 = new lib.shape366("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},9).to({state:[{t:this.shape_1},{t:this.shape}]},160).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).to({state:[{t:this.shape_4},{t:this.shape}]},1).to({state:[{t:this.shape_5},{t:this.shape}]},1).to({state:[{t:this.shape_6},{t:this.shape}]},1).to({state:[{t:this.shape_7},{t:this.shape}]},1).to({state:[{t:this.shape_8},{t:this.shape}]},1).to({state:[{t:this.shape_9},{t:this.shape}]},1).to({state:[{t:this.shape_10},{t:this.shape}]},1).to({state:[{t:this.instance_1}]},1).wait(6));

	// Layer 67
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("rgba(0,0,0,0)").ss(0.1,1,1).p("AwOo0MAgeAAAIAARpMggeAAAg");
	this.shape_11.setTransform(319.9,333.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AwOI0IAAxnMAgdAAAIAARng");
	this.shape_12.setTransform(319.9,333.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("rgba(0,0,0,0.875)").s().p("AwOI0IAAxnMAgdAAAIAARng");
	this.shape_13.setTransform(319.9,333.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("rgba(0,0,0,0)").ss(0.1,1,1).p("AwPo0MAgfAAAIAARpMggfAAAg");
	this.shape_14.setTransform(320,333.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(0,0,0,0.753)").s().p("AwPI0IAAxnMAgfAAAIAARng");
	this.shape_15.setTransform(320,333.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(0,0,0,0.627)").s().p("AwPI0IAAxnMAgfAAAIAARng");
	this.shape_16.setTransform(320,333.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("rgba(0,0,0,0.502)").s().p("AwPI0IAAxnMAgfAAAIAARng");
	this.shape_17.setTransform(320,333.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(0,0,0,0.376)").s().p("AwPI0IAAxnMAgfAAAIAARng");
	this.shape_18.setTransform(320,333.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("rgba(0,0,0,0.255)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_19.setTransform(320,333.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("rgba(0,0,0,0.129)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_20.setTransform(320,333.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(0,0,0,0.004)").s().p("AwPI0IAAxnMAgeAAAIAARng");
	this.shape_21.setTransform(320,333.5);

	this.instance_2 = new lib.shape268("synched",0);
	this.instance_2.setTransform(265,127,0.722,0.717);

	this.instance_3 = new lib.shape272("synched",0);
	this.instance_3.setTransform(265,127,0.722,0.717);

	this.instance_4 = new lib.shape258("synched",0);
	this.instance_4.setTransform(265,127,0.722,0.717);

	this.instance_5 = new lib.shape260("synched",0);
	this.instance_5.setTransform(265,127,0.722,0.717);

	this.instance_6 = new lib.shape274("synched",0);
	this.instance_6.setTransform(265,127,0.722,0.717);

	this.instance_7 = new lib.shape266("synched",0);
	this.instance_7.setTransform(265,127,0.722,0.717);

	this.instance_8 = new lib.shape264("synched",0);
	this.instance_8.setTransform(265,127,0.722,0.717);

	this.instance_9 = new lib.shape270("synched",0);
	this.instance_9.setTransform(265,127,0.722,0.717);

	this.instance_10 = new lib.shape355("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11}]}).to({state:[{t:this.shape_13},{t:this.shape_11}]},1).to({state:[{t:this.shape_15},{t:this.shape_14}]},1).to({state:[{t:this.shape_16},{t:this.shape_14}]},1).to({state:[{t:this.shape_17},{t:this.shape_14}]},1).to({state:[{t:this.shape_18},{t:this.shape_14}]},1).to({state:[{t:this.shape_19},{t:this.shape_14}]},1).to({state:[{t:this.shape_20},{t:this.shape_14}]},1).to({state:[{t:this.shape_21},{t:this.shape_14}]},1).to({state:[]},1).to({state:[{t:this.instance_2}]},7).to({state:[{t:this.instance_3}]},4).to({state:[{t:this.instance_4}]},4).to({state:[{t:this.instance_5}]},4).to({state:[{t:this.instance_6}]},4).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_8}]},4).to({state:[{t:this.instance_9}]},4).to({state:[{t:this.instance_10}]},4).wait(137));

	// Layer 14
	this.instance_11 = new lib.shape364("synched",0);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(117).to({_off:false},0).wait(68));

	// Layer 13
	this.instance_12 = new lib.shape363("synched",0);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(113).to({_off:false},0).to({_off:true},4).wait(68));

	// Layer 12
	this.instance_13 = new lib.shape362("synched",0);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(109).to({_off:false},0).to({_off:true},4).wait(72));

	// Layer 11
	this.instance_14 = new lib.shape361("synched",0);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(105).to({_off:false},0).to({_off:true},4).wait(76));

	// Layer 10
	this.instance_15 = new lib.shape360("synched",0);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(101).to({_off:false},0).to({_off:true},4).wait(80));

	// Layer 9
	this.instance_16 = new lib.shape359("synched",0);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(97).to({_off:false},0).to({_off:true},4).wait(84));

	// Layer 8
	this.instance_17 = new lib.shape358("synched",0);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(93).to({_off:false},0).to({_off:true},4).wait(88));

	// Mask Layer 5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AwFIqIAAxTMAgLAAAIAARTg");
	mask.setTransform(320,333.5);

	// Masked Layer 65 - 5
	this.instance_18 = new lib.shape352("synched",0);
	this.instance_18.setTransform(386,-166);

	this.instance_18.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).to({startPosition:0},48).to({y:293},36).to({x:385.4,y:282.9},2).wait(1).to({x:385,y:278},0).to({_off:true},6).wait(92));

	// Masked Layer 49 - 5
	this.instance_19 = new lib.shape351("synched",0);
	this.instance_19.setTransform(344,-92);

	this.instance_19.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).to({startPosition:0},48).to({y:293},30).to({x:343.4,y:282.9},2).wait(1).to({x:343,y:278},0).to({_off:true},12).wait(92));

	// Masked Layer 35 - 5
	this.instance_20 = new lib.shape350("synched",0);
	this.instance_20.setTransform(301,-18);

	this.instance_20.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).to({startPosition:0},48).to({y:293},24).to({y:282.9},2).wait(1).to({y:278},0).to({_off:true},18).wait(92));

	// Masked Layer 23 - 5
	this.instance_21 = new lib.shape348("synched",0);
	this.instance_21.setTransform(259,56);

	this.instance_21.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).to({startPosition:0},48).to({y:293},18).to({y:282.9},2).wait(1).to({y:278},0).to({_off:true},24).wait(92));

	// Masked Layer 13 - 5
	this.instance_22 = new lib.shape344("synched",0);
	this.instance_22.setTransform(217,130);

	this.instance_22.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).to({startPosition:0},48).to({y:293},12).to({y:281.7},3).wait(1).to({y:278},0).to({_off:true},29).wait(92));

	// Layer 5
	this.instance_23 = new lib.shape357("synched",0);
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(93).to({_off:false},0).wait(92));

	// Layer 4
	this.instance_24 = new lib.shape335("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(185));

	// Layer 1
	this.instance_25 = new lib.text333("synched",0);
	this.instance_25.setTransform(37,417);

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(185));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,661.7,484);


(lib.sprite331 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape330("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.sprite328 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 26
	this.tfScat_9_5 = new cjs.Text("1800", "bold 15px 'Arial'", "#FFFFFF");
	this.tfScat_9_5.name = "tfScat_9_5";
	this.tfScat_9_5.textAlign = "center";
	this.tfScat_9_5.lineHeight = 17;
	this.tfScat_9_5.lineWidth = 75;
	this.tfScat_9_5.setTransform(377.6,104);

	this.timeline.addTween(cjs.Tween.get(this.tfScat_9_5).wait(1));

	// Layer 25
	this.tfScat_9_4 = new cjs.Text("180", "bold 15px 'Arial'", "#FFFFFF");
	this.tfScat_9_4.name = "tfScat_9_4";
	this.tfScat_9_4.textAlign = "center";
	this.tfScat_9_4.lineHeight = 17;
	this.tfScat_9_4.lineWidth = 75;
	this.tfScat_9_4.setTransform(377.6,128);

	this.timeline.addTween(cjs.Tween.get(this.tfScat_9_4).wait(1));

	// Layer 24
	this.tfScat_9_3 = new cjs.Text("18", "bold 15px 'Arial'", "#FFFFFF");
	this.tfScat_9_3.name = "tfScat_9_3";
	this.tfScat_9_3.textAlign = "center";
	this.tfScat_9_3.lineHeight = 17;
	this.tfScat_9_3.lineWidth = 75;
	this.tfScat_9_3.setTransform(377.6,153);

	this.timeline.addTween(cjs.Tween.get(this.tfScat_9_3).wait(1));

	// Layer 23
	this.tfTile_8_5 = new cjs.Text("5000", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_8_5.name = "tfTile_8_5";
	this.tfTile_8_5.textAlign = "center";
	this.tfTile_8_5.lineHeight = 17;
	this.tfTile_8_5.lineWidth = 75;
	this.tfTile_8_5.setTransform(168.6,112);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_8_5).wait(1));

	// Layer 22
	this.tfTile_8_4 = new cjs.Text("1000", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_8_4.name = "tfTile_8_4";
	this.tfTile_8_4.textAlign = "center";
	this.tfTile_8_4.lineHeight = 17;
	this.tfTile_8_4.lineWidth = 75;
	this.tfTile_8_4.setTransform(168.6,131);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_8_4).wait(1));

	// Layer 21
	this.tfTile_8_3 = new cjs.Text("100", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_8_3.name = "tfTile_8_3";
	this.tfTile_8_3.textAlign = "center";
	this.tfTile_8_3.lineHeight = 17;
	this.tfTile_8_3.lineWidth = 75;
	this.tfTile_8_3.setTransform(168.6,150);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_8_3).wait(1));

	// Layer 20
	this.tfTile_8_2 = new cjs.Text("10", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_8_2.name = "tfTile_8_2";
	this.tfTile_8_2.textAlign = "center";
	this.tfTile_8_2.lineHeight = 17;
	this.tfTile_8_2.lineWidth = 75;
	this.tfTile_8_2.setTransform(168.6,169);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_8_2).wait(1));

	// Layer 19
	this.tfTile_7_5 = new cjs.Text("2000", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_7_5.name = "tfTile_7_5";
	this.tfTile_7_5.textAlign = "center";
	this.tfTile_7_5.lineHeight = 17;
	this.tfTile_7_5.lineWidth = 75;
	this.tfTile_7_5.setTransform(582.6,112);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_7_5).wait(1));

	// Layer 18
	this.tfTile_7_4 = new cjs.Text("400", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_7_4.name = "tfTile_7_4";
	this.tfTile_7_4.textAlign = "center";
	this.tfTile_7_4.lineHeight = 17;
	this.tfTile_7_4.lineWidth = 75;
	this.tfTile_7_4.setTransform(582.6,131);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_7_4).wait(1));

	// Layer 17
	this.tfTile_7_3 = new cjs.Text("40", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_7_3.name = "tfTile_7_3";
	this.tfTile_7_3.textAlign = "center";
	this.tfTile_7_3.lineHeight = 17;
	this.tfTile_7_3.lineWidth = 75;
	this.tfTile_7_3.setTransform(582.6,150);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_7_3).wait(1));

	// Layer 16
	this.tfTile_7_2 = new cjs.Text("5", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_7_2.name = "tfTile_7_2";
	this.tfTile_7_2.textAlign = "center";
	this.tfTile_7_2.lineHeight = 17;
	this.tfTile_7_2.lineWidth = 75;
	this.tfTile_7_2.setTransform(582.6,169);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_7_2).wait(1));

	// Layer 15
	this.tfTile_6_5 = new cjs.Text("750", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_6_5.name = "tfTile_6_5";
	this.tfTile_6_5.textAlign = "center";
	this.tfTile_6_5.lineHeight = 17;
	this.tfTile_6_5.lineWidth = 75;
	this.tfTile_6_5.setTransform(167.6,252);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_6_5).wait(1));

	// Layer 14
	this.tfTile_6_4 = new cjs.Text("100", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_6_4.name = "tfTile_6_4";
	this.tfTile_6_4.textAlign = "center";
	this.tfTile_6_4.lineHeight = 17;
	this.tfTile_6_4.lineWidth = 75;
	this.tfTile_6_4.setTransform(167.6,271);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_6_4).wait(1));

	// Layer 13
	this.tfTile_6_3 = new cjs.Text("30", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_6_3.name = "tfTile_6_3";
	this.tfTile_6_3.textAlign = "center";
	this.tfTile_6_3.lineHeight = 17;
	this.tfTile_6_3.lineWidth = 75;
	this.tfTile_6_3.setTransform(167.6,290);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_6_3).wait(1));

	// Layer 12
	this.tfTile_6_2 = new cjs.Text("5", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_6_2.name = "tfTile_6_2";
	this.tfTile_6_2.textAlign = "center";
	this.tfTile_6_2.lineHeight = 17;
	this.tfTile_6_2.lineWidth = 75;
	this.tfTile_6_2.setTransform(167.6,309);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_6_2).wait(1));

	// Layer 11
	this.tfTile_5_5 = new cjs.Text("750", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_5_5.name = "tfTile_5_5";
	this.tfTile_5_5.textAlign = "center";
	this.tfTile_5_5.lineHeight = 17;
	this.tfTile_5_5.lineWidth = 75;
	this.tfTile_5_5.setTransform(582.6,252);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_5_5).wait(1));

	// Layer 10
	this.tfTile_5_4 = new cjs.Text("100", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_5_4.name = "tfTile_5_4";
	this.tfTile_5_4.textAlign = "center";
	this.tfTile_5_4.lineHeight = 17;
	this.tfTile_5_4.lineWidth = 75;
	this.tfTile_5_4.setTransform(582.6,271);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_5_4).wait(1));

	// Layer 9
	this.tfTile_5_3 = new cjs.Text("30", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_5_3.name = "tfTile_5_3";
	this.tfTile_5_3.textAlign = "center";
	this.tfTile_5_3.lineHeight = 17;
	this.tfTile_5_3.lineWidth = 75;
	this.tfTile_5_3.setTransform(582.6,290);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_5_3).wait(1));

	// Layer 8
	this.tfTile_5_2 = new cjs.Text("5", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_5_2.name = "tfTile_5_2";
	this.tfTile_5_2.textAlign = "center";
	this.tfTile_5_2.lineHeight = 17;
	this.tfTile_5_2.lineWidth = 75;
	this.tfTile_5_2.setTransform(582.6,308);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_5_2).wait(1));

	// Layer 7
	this.tfTile_3_5 = new cjs.Text("150", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_3_5.name = "tfTile_3_5";
	this.tfTile_3_5.textAlign = "center";
	this.tfTile_3_5.lineHeight = 17;
	this.tfTile_3_5.lineWidth = 75;
	this.tfTile_3_5.setTransform(189.6,388);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_3_5).wait(1));

	// Layer 6
	this.tfTile_3_4 = new cjs.Text("40", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_3_4.name = "tfTile_3_4";
	this.tfTile_3_4.textAlign = "center";
	this.tfTile_3_4.lineHeight = 17;
	this.tfTile_3_4.lineWidth = 75;
	this.tfTile_3_4.setTransform(189.6,410);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_3_4).wait(1));

	// Layer 5
	this.tfTile_3_3 = new cjs.Text("5", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_3_3.name = "tfTile_3_3";
	this.tfTile_3_3.textAlign = "center";
	this.tfTile_3_3.lineHeight = 17;
	this.tfTile_3_3.lineWidth = 75;
	this.tfTile_3_3.setTransform(189.6,432);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_3_3).wait(1));

	// Layer 4
	this.tfTile_0_5 = new cjs.Text("100", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_0_5.name = "tfTile_0_5";
	this.tfTile_0_5.textAlign = "center";
	this.tfTile_0_5.lineHeight = 17;
	this.tfTile_0_5.lineWidth = 75;
	this.tfTile_0_5.setTransform(495.6,388);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_0_5).wait(1));

	// Layer 3
	this.tfTile_0_4 = new cjs.Text("25", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_0_4.name = "tfTile_0_4";
	this.tfTile_0_4.textAlign = "center";
	this.tfTile_0_4.lineHeight = 17;
	this.tfTile_0_4.lineWidth = 75;
	this.tfTile_0_4.setTransform(495.6,410);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_0_4).wait(1));

	// Layer 2
	this.tfTile_0_3 = new cjs.Text("5", "bold 15px 'Arial'", "#FFFFFF");
	this.tfTile_0_3.name = "tfTile_0_3";
	this.tfTile_0_3.textAlign = "center";
	this.tfTile_0_3.lineHeight = 17;
	this.tfTile_0_3.lineWidth = 75;
	this.tfTile_0_3.setTransform(495.6,432);

	this.timeline.addTween(cjs.Tween.get(this.tfTile_0_3).wait(1));

	// Layer 1
	this.instance = new lib.shape312("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.sprite309 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape302("synched",0);

	this.instance_1 = new lib.shape304("synched",0);

	this.instance_2 = new lib.shape306("synched",0);

	this.instance_3 = new lib.shape308("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,35,35);


(lib.sprite300 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape291("synched",0);

	this.instance_1 = new lib.shape293("synched",0);

	this.instance_2 = new lib.shape295("synched",0);

	this.instance_3 = new lib.shape297("synched",0);

	this.instance_4 = new lib.shape299("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,99,147);


(lib.sprite287 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape286("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,146,101);


(lib.sprite284 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape283("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,146,101);


(lib.sprite275 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{id_4:0,id_3:3,id_2:6,id_1:9,id_0:12,id_8:15,id_7:18,id_6:21,id_5:24});

	// Layer 1
	this.instance = new lib.shape258("synched",0);

	this.instance_1 = new lib.shape260("synched",0);

	this.instance_2 = new lib.shape262("synched",0);

	this.instance_3 = new lib.shape264("synched",0);

	this.instance_4 = new lib.shape266("synched",0);

	this.instance_5 = new lib.shape268("synched",0);

	this.instance_6 = new lib.shape270("synched",0);

	this.instance_7 = new lib.shape272("synched",0);

	this.instance_8 = new lib.shape274("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},3).to({state:[{t:this.instance_2}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_6}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_8}]},3).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,54,60);


(lib.sprite256 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.shape255("synched",0);
	this.instance.setTransform(11,20);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(25).to({_off:false},0).to({alpha:1},9).wait(1));

	// Layer 1
	this.instance_1 = new lib.shape207("synched",0);

	this.instance_2 = new lib.shape209("synched",0);

	this.instance_3 = new lib.shape211("synched",0);

	this.instance_4 = new lib.shape213("synched",0);

	this.instance_5 = new lib.shape215("synched",0);

	this.instance_6 = new lib.shape217("synched",0);

	this.instance_7 = new lib.shape219("synched",0);

	this.instance_8 = new lib.shape221("synched",0);

	this.instance_9 = new lib.shape223("synched",0);

	this.instance_10 = new lib.shape225("synched",0);

	this.instance_11 = new lib.shape227("synched",0);

	this.instance_12 = new lib.shape229("synched",0);

	this.instance_13 = new lib.shape231("synched",0);

	this.instance_14 = new lib.shape233("synched",0);

	this.instance_15 = new lib.shape235("synched",0);

	this.instance_16 = new lib.shape237("synched",0);

	this.instance_17 = new lib.shape239("synched",0);

	this.instance_18 = new lib.shape241("synched",0);

	this.instance_19 = new lib.shape243("synched",0);

	this.instance_20 = new lib.shape245("synched",0);

	this.instance_21 = new lib.shape247("synched",0);

	this.instance_22 = new lib.shape249("synched",0);

	this.instance_23 = new lib.shape251("synched",0);

	this.instance_24 = new lib.shape253("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,106);


(lib.sprite203 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{begin:0,"again":1,end:2});

	// Layer 2
	this.text = new cjs.Text("10 free games\n", "bold 24px 'Arial'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 27;
	this.text.lineWidth = 491;
	this.text.setTransform(340.4,150,1,1.002);
	this.text.shadow = new cjs.Shadow("rgba(70,16,0,1)",0,0,2);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1).to({text:"10 more free games\n"},0).wait(1).to({text:"FEATURE WIN\n"},0).wait(1));

	// Layer 1
	this.instance = new lib.shape199("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(70,85,526,283);


(lib.sprite192 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape182("synched",0);

	this.instance_1 = new lib.shape191("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite190 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape182("synched",0);

	this.instance_1 = new lib.shape189("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite188 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape182("synched",0);

	this.instance_1 = new lib.shape187("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite186 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape182("synched",0);

	this.instance_1 = new lib.shape185("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite184 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape182("synched",0);

	this.instance_1 = new lib.shape183("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite176 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape166("synched",0);

	this.instance_1 = new lib.shape175("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite174 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape166("synched",0);

	this.instance_1 = new lib.shape173("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite172 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape166("synched",0);

	this.instance_1 = new lib.shape171("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite170 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape166("synched",0);

	this.instance_1 = new lib.shape169("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite168 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape166("synched",0);

	this.instance_1 = new lib.shape167("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite160 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape154("synched",0);

	this.instance_1 = new lib.shape159("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite158 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape154("synched",0);

	this.instance_1 = new lib.shape157("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite156 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape154("synched",0);

	this.instance_1 = new lib.shape155("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite148 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape142("synched",0);

	this.instance_1 = new lib.shape147("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite146 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape142("synched",0);

	this.instance_1 = new lib.shape145("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite144 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape142("synched",0);

	this.instance_1 = new lib.shape143("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite136 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape130("synched",0);

	this.instance_1 = new lib.shape135("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite134 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape130("synched",0);

	this.instance_1 = new lib.shape133("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite132 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape130("synched",0);

	this.instance_1 = new lib.shape131("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite124 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape118("synched",0);

	this.instance_1 = new lib.shape123("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite122 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape118("synched",0);

	this.instance_1 = new lib.shape121("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite120 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape118("synched",0);

	this.instance_1 = new lib.shape119("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite112 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape110("synched",0);

	this.instance_1 = new lib.shape111("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite104 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape102("synched",0);

	this.instance_1 = new lib.shape103("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite94 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape92("synched",0);

	this.instance_1 = new lib.shape93("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,99,99);


(lib.sprite90 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.instance = new lib.shape89("synched",0);
	this.instance.setTransform(498,277);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 14
	this.instance_1 = new lib.shape89("synched",0);
	this.instance_1.setTransform(498,177);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 13
	this.instance_2 = new lib.shape89("synched",0);
	this.instance_2.setTransform(498,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer 12
	this.instance_3 = new lib.shape89("synched",0);
	this.instance_3.setTransform(384,277);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Layer 11
	this.instance_4 = new lib.shape89("synched",0);
	this.instance_4.setTransform(384,177);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Layer 10
	this.instance_5 = new lib.shape89("synched",0);
	this.instance_5.setTransform(384,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

	// Layer 9
	this.instance_6 = new lib.shape89("synched",0);
	this.instance_6.setTransform(270,277);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1));

	// Layer 8
	this.instance_7 = new lib.shape89("synched",0);
	this.instance_7.setTransform(270,177);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1));

	// Layer 7
	this.instance_8 = new lib.shape89("synched",0);
	this.instance_8.setTransform(270,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1));

	// Layer 6
	this.instance_9 = new lib.shape89("synched",0);
	this.instance_9.setTransform(156,277);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1));

	// Layer 5
	this.instance_10 = new lib.shape89("synched",0);
	this.instance_10.setTransform(156,177);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(1));

	// Layer 4
	this.instance_11 = new lib.shape89("synched",0);
	this.instance_11.setTransform(156,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(1));

	// Layer 3
	this.instance_12 = new lib.shape89("synched",0);
	this.instance_12.setTransform(42,277);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(1));

	// Layer 2
	this.instance_13 = new lib.shape89("synched",0);
	this.instance_13.setTransform(42,177);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(1));

	// Layer 1
	this.instance_14 = new lib.shape89("synched",0);
	this.instance_14.setTransform(42,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(42,77,556,300);


(lib.sprite88 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape87("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52,16);


(lib.sprite72 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"prepare->start":0,"prepare->end":23,"transform->start":24,"transform->end":33});

	// Layer 1
	this.instance = new lib.shape10("synched",0);

	this.instance_1 = new lib.shape12("synched",0);

	this.instance_2 = new lib.shape14("synched",0);

	this.instance_3 = new lib.shape16("synched",0);

	this.instance_4 = new lib.shape18("synched",0);

	this.instance_5 = new lib.shape20("synched",0);

	this.instance_6 = new lib.shape22("synched",0);

	this.instance_7 = new lib.shape24("synched",0);

	this.instance_8 = new lib.shape26("synched",0);

	this.instance_9 = new lib.shape28("synched",0);

	this.instance_10 = new lib.shape30("synched",0);

	this.instance_11 = new lib.shape32("synched",0);

	this.instance_12 = new lib.shape34("synched",0);

	this.instance_13 = new lib.shape36("synched",0);

	this.instance_14 = new lib.shape38("synched",0);

	this.instance_15 = new lib.shape40("synched",0);

	this.instance_16 = new lib.shape42("synched",0);

	this.instance_17 = new lib.shape44("synched",0);

	this.instance_18 = new lib.shape46("synched",0);

	this.instance_19 = new lib.shape48("synched",0);

	this.instance_20 = new lib.shape50("synched",0);

	this.instance_21 = new lib.shape52("synched",0);

	this.instance_22 = new lib.shape54("synched",0);

	this.instance_23 = new lib.shape56("synched",0);

	this.instance_24 = new lib.shape58("synched",0);

	this.instance_25 = new lib.shape60("synched",0);

	this.instance_26 = new lib.shape61("synched",0);

	this.instance_27 = new lib.shape63("synched",0);

	this.instance_28 = new lib.shape65("synched",0);

	this.instance_29 = new lib.shape67("synched",0);

	this.instance_30 = new lib.shape69("synched",0);

	this.instance_31 = new lib.shape71("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_23}]},2).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).to({state:[{t:this.instance_27}]},1).to({state:[{t:this.instance_28}]},1).to({state:[{t:this.instance_29}]},1).to({state:[{t:this.instance_30}]},1).to({state:[{t:this.instance_31}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,77);


(lib.sprite8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:0,"bonus->start":1,"bonus->end":5});

	// Layer 2
	this.instance = new lib.shape7("synched",0);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({alpha:1},4).wait(1));

	// Layer 1
	this.instance_1 = new lib.shape5("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.sprite2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.instance = new lib.shape1("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,300);


(lib.sprite381 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 13
	this.tfText = new cjs.Text("tfDetails", "bold 18px 'Consolas'", "#FFCC33");
	this.tfText.name = "tfText";
	this.tfText.lineHeight = 21;
	this.tfText.lineWidth = 282;
	this.tfText.setTransform(275,110);
	this.tfText.shadow = new cjs.Shadow("rgba(255,0,0,1)",0,0,2);

	this.timeline.addTween(cjs.Tween.get(this.tfText).wait(1));

	// Layer 9
	this.icon = new lib.GameLib_flaWindowIcon_127();
	this.icon.setTransform(90,70);

	this.timeline.addTween(cjs.Tween.get(this.icon).wait(1));

	// Layer 8
	this.instance = new lib.shape372("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 6
	this.instance_1 = new lib.text371("synched",0);
	this.instance_1.setTransform(295.6,72);
	this.instance_1.shadow = new cjs.Shadow("rgba(0,0,0,1)",1,1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 5
	this.instance_2 = new lib.shape369("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,640,480);


(lib.sprite368 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 30
	this.instance = new lib.sprite367();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 28
	this.instance_1 = new lib.sprite331();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 1
	this.instance_2 = new lib.sprite328();

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-203,661.7,687);


(lib.sprite310 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 20
	this.suite5 = new lib.sprite309();
	this.suite5.setTransform(568,156);

	this.timeline.addTween(cjs.Tween.get(this.suite5).wait(1));

	// Layer 18
	this.suite4 = new lib.sprite309();
	this.suite4.setTransform(515,156);

	this.timeline.addTween(cjs.Tween.get(this.suite4).wait(1));

	// Layer 16
	this.suite3 = new lib.sprite309();
	this.suite3.setTransform(462,156);

	this.timeline.addTween(cjs.Tween.get(this.suite3).wait(1));

	// Layer 14
	this.suite2 = new lib.sprite309();
	this.suite2.setTransform(409,156);

	this.timeline.addTween(cjs.Tween.get(this.suite2).wait(1));

	// Layer 12
	this.suite1 = new lib.sprite309();
	this.suite1.setTransform(356,156);

	this.timeline.addTween(cjs.Tween.get(this.suite1).wait(1));

	// Layer 10
	this.suite0 = new lib.sprite309();
	this.suite0.setTransform(303,156);

	this.timeline.addTween(cjs.Tween.get(this.suite0).wait(1));

	// Layer 8
	this.card = new lib.sprite300();
	this.card.setTransform(271,208);

	this.timeline.addTween(cjs.Tween.get(this.card).wait(1));

	// Layer 7
	this.tfAmount = new cjs.Text("tfAmount", "bold 19px 'Arial'", "#DEDFD6");
	this.tfAmount.name = "tfAmount";
	this.tfAmount.textAlign = "center";
	this.tfAmount.lineHeight = 21;
	this.tfAmount.lineWidth = 227;
	this.tfAmount.setTransform(167.5,120);
	this.tfAmount.shadow = new cjs.Shadow("rgba(0,0,0,0.694)",1,1,2);

	this.timeline.addTween(cjs.Tween.get(this.tfAmount).wait(1));

	// Layer 6
	this.tfWin = new cjs.Text("tfWin", "bold 19px 'Arial'", "#DEDFD6");
	this.tfWin.name = "tfWin";
	this.tfWin.textAlign = "center";
	this.tfWin.lineHeight = 21;
	this.tfWin.lineWidth = 216;
	this.tfWin.setTransform(483,120);
	this.tfWin.shadow = new cjs.Shadow("rgba(0,0,0,1)",1,1,2);

	this.timeline.addTween(cjs.Tween.get(this.tfWin).wait(1));

	// Layer 4
	this.iconBlack = new lib.sprite287();
	this.iconBlack.setTransform(434,233);

	this.timeline.addTween(cjs.Tween.get(this.iconBlack).wait(1));

	// Layer 2
	this.iconRed = new lib.sprite284();
	this.iconRed.setTransform(63,233);

	this.timeline.addTween(cjs.Tween.get(this.iconRed).wait(1));

	// Layer 1
	this.instance = new lib.shape281("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,77,640,313);


(lib.sprite277 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.tf = new cjs.Text("5 - 5000\n4 - 400\n3 - 30\n2 - 2\n", "bold 11px 'Arial'");
	this.tf.name = "tf";
	this.tf.lineHeight = 13;
	this.tf.lineWidth = 79;
	this.tf.setTransform(110,5);

	this.timeline.addTween(cjs.Tween.get(this.tf).wait(1));

	// Layer 2
	this.items = new lib.sprite275();
	this.items.setTransform(25,3);

	this.timeline.addTween(cjs.Tween.get(this.items).wait(1));

	// Layer 1
	this.instance = new lib.shape255("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,201,65);


(lib.sprite205 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.tf = new cjs.Text("0123456789", "bold 24px 'Arial'", "#FFFFFF");
	this.tf.name = "tf";
	this.tf.textAlign = "center";
	this.tf.lineHeight = 27;
	this.tf.lineWidth = 436;
	this.tf.setTransform(337.9,180,1,1.002);
	this.tf.shadow = new cjs.Shadow("rgba(70,16,0,1)",0,0,2);

	this.timeline.addTween(cjs.Tween.get(this.tf).wait(1));

	// Layer 1
	this.background = new lib.sprite203();

	this.timeline.addTween(cjs.Tween.get(this.background).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(70,85,526,283);


(lib.sprite193 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.c4 = new lib.sprite192();
	this.c4.setTransform(471,-218);

	this.timeline.addTween(cjs.Tween.get(this.c4).wait(1));

	// Layer 8
	this.c3 = new lib.sprite190();
	this.c3.setTransform(357,-218);

	this.timeline.addTween(cjs.Tween.get(this.c3).wait(1));

	// Layer 6
	this.c2 = new lib.sprite188();
	this.c2.setTransform(243,-118);

	this.timeline.addTween(cjs.Tween.get(this.c2).wait(1));

	// Layer 4
	this.c1 = new lib.sprite186();
	this.c1.setTransform(129,-18);

	this.timeline.addTween(cjs.Tween.get(this.c1).wait(1));

	// Layer 2
	this.c0 = new lib.sprite184();
	this.c0.setTransform(15,-18);

	this.timeline.addTween(cjs.Tween.get(this.c0).wait(1));

	// Layer 1
	this.instance = new lib.shape181("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-219.5,585,299);


(lib.sprite177 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.c4 = new lib.sprite176();
	this.c4.setTransform(471,120);

	this.timeline.addTween(cjs.Tween.get(this.c4).wait(1));

	// Layer 8
	this.c3 = new lib.sprite174();
	this.c3.setTransform(357,120);

	this.timeline.addTween(cjs.Tween.get(this.c3).wait(1));

	// Layer 6
	this.c2 = new lib.sprite172();
	this.c2.setTransform(243,20);

	this.timeline.addTween(cjs.Tween.get(this.c2).wait(1));

	// Layer 4
	this.c1 = new lib.sprite170();
	this.c1.setTransform(129,-80);

	this.timeline.addTween(cjs.Tween.get(this.c1).wait(1));

	// Layer 2
	this.c0 = new lib.sprite168();
	this.c0.setTransform(15,-80);

	this.timeline.addTween(cjs.Tween.get(this.c0).wait(1));

	// Layer 1
	this.instance = new lib.shape165("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-81.5,585,299);


(lib.sprite161 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.c4 = new lib.sprite156();
	this.c4.setTransform(567,-15,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.c4).wait(1));

	// Layer 8
	this.c3 = new lib.sprite158();
	this.c3.setTransform(453,85,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.c3).wait(1));

	// Layer 6
	this.c2 = new lib.sprite160();
	this.c2.setTransform(243,85);

	this.timeline.addTween(cjs.Tween.get(this.c2).wait(1));

	// Layer 4
	this.c1 = new lib.sprite158();
	this.c1.setTransform(129,85);

	this.timeline.addTween(cjs.Tween.get(this.c1).wait(1));

	// Layer 2
	this.c0 = new lib.sprite156();
	this.c0.setTransform(15,-15);

	this.timeline.addTween(cjs.Tween.get(this.c0).wait(1));

	// Layer 1
	this.instance = new lib.shape153("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-16.5,585,199);


(lib.sprite149 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.c4 = new lib.sprite144();
	this.c4.setTransform(567,-83,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.c4).wait(1));

	// Layer 8
	this.c3 = new lib.sprite146();
	this.c3.setTransform(453,-183,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.c3).wait(1));

	// Layer 6
	this.c2 = new lib.sprite148();
	this.c2.setTransform(243,-183);

	this.timeline.addTween(cjs.Tween.get(this.c2).wait(1));

	// Layer 4
	this.c1 = new lib.sprite146();
	this.c1.setTransform(129,-183);

	this.timeline.addTween(cjs.Tween.get(this.c1).wait(1));

	// Layer 2
	this.c0 = new lib.sprite144();
	this.c0.setTransform(15,-83);

	this.timeline.addTween(cjs.Tween.get(this.c0).wait(1));

	// Layer 1
	this.instance = new lib.shape141("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-184.5,585,199);


(lib.sprite137 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.c4 = new lib.sprite132();
	this.c4.setTransform(567,-86,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.c4).wait(1));

	// Layer 8
	this.c3 = new lib.sprite134();
	this.c3.setTransform(453,-186,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.c3).wait(1));

	// Layer 6
	this.c2 = new lib.sprite136();
	this.c2.setTransform(243,-286);

	this.timeline.addTween(cjs.Tween.get(this.c2).wait(1));

	// Layer 4
	this.c1 = new lib.sprite134();
	this.c1.setTransform(129,-186);

	this.timeline.addTween(cjs.Tween.get(this.c1).wait(1));

	// Layer 2
	this.c0 = new lib.sprite132();
	this.c0.setTransform(15,-86);

	this.timeline.addTween(cjs.Tween.get(this.c0).wait(1));

	// Layer 1
	this.instance = new lib.shape129("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-287.5,584,299);


(lib.sprite125 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.c4 = new lib.sprite120();
	this.c4.setTransform(567,-11,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.c4).wait(1));

	// Layer 8
	this.c3 = new lib.sprite122();
	this.c3.setTransform(453,89,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.c3).wait(1));

	// Layer 6
	this.c2 = new lib.sprite124();
	this.c2.setTransform(243,189);

	this.timeline.addTween(cjs.Tween.get(this.c2).wait(1));

	// Layer 4
	this.c1 = new lib.sprite122();
	this.c1.setTransform(129,89);

	this.timeline.addTween(cjs.Tween.get(this.c1).wait(1));

	// Layer 2
	this.c0 = new lib.sprite120();
	this.c0.setTransform(15,-11);

	this.timeline.addTween(cjs.Tween.get(this.c0).wait(1));

	// Layer 1
	this.instance = new lib.shape117("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-12.5,585,299);


(lib.sprite113 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.c4 = new lib.sprite112();
	this.c4.setTransform(471,-52);

	this.timeline.addTween(cjs.Tween.get(this.c4).wait(1));

	// Layer 8
	this.c3 = new lib.sprite112();
	this.c3.setTransform(357,-52);

	this.timeline.addTween(cjs.Tween.get(this.c3).wait(1));

	// Layer 6
	this.c2 = new lib.sprite112();
	this.c2.setTransform(243,-52);

	this.timeline.addTween(cjs.Tween.get(this.c2).wait(1));

	// Layer 4
	this.c1 = new lib.sprite112();
	this.c1.setTransform(129,-52);

	this.timeline.addTween(cjs.Tween.get(this.c1).wait(1));

	// Layer 2
	this.c0 = new lib.sprite112();
	this.c0.setTransform(15,-52);

	this.timeline.addTween(cjs.Tween.get(this.c0).wait(1));

	// Layer 1
	this.instance = new lib.shape109("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-53.5,584,99);


(lib.sprite105 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.c4 = new lib.sprite104();
	this.c4.setTransform(471,-45);

	this.timeline.addTween(cjs.Tween.get(this.c4).wait(1));

	// Layer 8
	this.c3 = new lib.sprite104();
	this.c3.setTransform(357,-45);

	this.timeline.addTween(cjs.Tween.get(this.c3).wait(1));

	// Layer 6
	this.c2 = new lib.sprite104();
	this.c2.setTransform(243,-45);

	this.timeline.addTween(cjs.Tween.get(this.c2).wait(1));

	// Layer 4
	this.c1 = new lib.sprite104();
	this.c1.setTransform(129,-45);

	this.timeline.addTween(cjs.Tween.get(this.c1).wait(1));

	// Layer 2
	this.c0 = new lib.sprite104();
	this.c0.setTransform(15,-45);

	this.timeline.addTween(cjs.Tween.get(this.c0).wait(1));

	// Layer 1
	this.instance = new lib.shape101("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-46.5,585,99);


(lib.sprite95 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.c4 = new lib.sprite94();
	this.c4.setTransform(471,-49);

	this.timeline.addTween(cjs.Tween.get(this.c4).wait(1));

	// Layer 8
	this.c3 = new lib.sprite94();
	this.c3.setTransform(357,-49);

	this.timeline.addTween(cjs.Tween.get(this.c3).wait(1));

	// Layer 6
	this.c2 = new lib.sprite94();
	this.c2.setTransform(243,-49);

	this.timeline.addTween(cjs.Tween.get(this.c2).wait(1));

	// Layer 4
	this.c1 = new lib.sprite94();
	this.c1.setTransform(129,-49);

	this.timeline.addTween(cjs.Tween.get(this.c1).wait(1));

	// Layer 2
	this.c0 = new lib.sprite94();
	this.c0.setTransform(15,-49);

	this.timeline.addTween(cjs.Tween.get(this.c0).wait(1));

	// Layer 1
	this.instance = new lib.shape91("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-50.5,585,99);


(lib.sprite3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 21
	this.instance = new lib.sprite2();
	this.instance.setTransform(456,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 16
	this.instance_1 = new lib.sprite2();
	this.instance_1.setTransform(342,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 11
	this.instance_2 = new lib.sprite2();
	this.instance_2.setTransform(228,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer 6
	this.instance_3 = new lib.sprite2();
	this.instance_3.setTransform(114,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Layer 1
	this.instance_4 = new lib.sprite2();

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,556,300);


(lib.sprite278 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.book = new lib.sprite277();
	this.book.setTransform(225,247);

	this.timeline.addTween(cjs.Tween.get(this.book).wait(1));

	// Layer 1
	this.anim = new lib.sprite256();
	this.anim.setTransform(214,227);

	this.timeline.addTween(cjs.Tween.get(this.anim).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(214,227,232,106);


(lib.sprite196 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.tfRight = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfRight.name = "tfRight";
	this.tfRight.textAlign = "center";
	this.tfRight.lineHeight = 16;
	this.tfRight.lineWidth = 28;
	this.tfRight.setTransform(625,-136);

	this.timeline.addTween(cjs.Tween.get(this.tfRight).wait(1));

	// Layer 14
	this.tfLeft = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfLeft.name = "tfLeft";
	this.tfLeft.textAlign = "center";
	this.tfLeft.lineHeight = 16;
	this.tfLeft.lineWidth = 28;
	this.tfLeft.setTransform(14,2);

	this.timeline.addTween(cjs.Tween.get(this.tfLeft).wait(1));

	// Layer 13
	this.instance = new lib.shape195("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.body = new lib.sprite193();
	this.body.setTransform(29,13);
	this.body.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.body).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-213.5,642.9,316);


(lib.sprite180 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.tfRight = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfRight.name = "tfRight";
	this.tfRight.textAlign = "center";
	this.tfRight.lineHeight = 16;
	this.tfRight.lineWidth = 28;
	this.tfRight.setTransform(625,140);

	this.timeline.addTween(cjs.Tween.get(this.tfRight).wait(1));

	// Layer 14
	this.tfLeft = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfLeft.name = "tfLeft";
	this.tfLeft.textAlign = "center";
	this.tfLeft.lineHeight = 16;
	this.tfLeft.lineWidth = 28;
	this.tfLeft.setTransform(14,2);

	this.timeline.addTween(cjs.Tween.get(this.tfLeft).wait(1));

	// Layer 13
	this.instance = new lib.shape179("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.body = new lib.sprite177();
	this.body.setTransform(29,13);
	this.body.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.body).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-75.5,642.9,316);


(lib.sprite164 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.tfRight = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfRight.name = "tfRight";
	this.tfRight.textAlign = "center";
	this.tfRight.lineHeight = 16;
	this.tfRight.lineWidth = 28;
	this.tfRight.setTransform(625,2);

	this.timeline.addTween(cjs.Tween.get(this.tfRight).wait(1));

	// Layer 14
	this.tfLeft = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfLeft.name = "tfLeft";
	this.tfLeft.textAlign = "center";
	this.tfLeft.lineHeight = 16;
	this.tfLeft.lineWidth = 28;
	this.tfLeft.setTransform(14,2);

	this.timeline.addTween(cjs.Tween.get(this.tfLeft).wait(1));

	// Layer 13
	this.instance = new lib.shape163("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.body = new lib.sprite161();
	this.body.setTransform(29,13);
	this.body.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.body).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-10.5,642.9,216);


(lib.sprite152 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.tfRight = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfRight.name = "tfRight";
	this.tfRight.textAlign = "center";
	this.tfRight.lineHeight = 16;
	this.tfRight.lineWidth = 28;
	this.tfRight.setTransform(625,2);

	this.timeline.addTween(cjs.Tween.get(this.tfRight).wait(1));

	// Layer 14
	this.tfLeft = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfLeft.name = "tfLeft";
	this.tfLeft.textAlign = "center";
	this.tfLeft.lineHeight = 16;
	this.tfLeft.lineWidth = 28;
	this.tfLeft.setTransform(14,2);

	this.timeline.addTween(cjs.Tween.get(this.tfLeft).wait(1));

	// Layer 13
	this.instance = new lib.shape151("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.body = new lib.sprite149();
	this.body.setTransform(29,13);
	this.body.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.body).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-178.5,642.9,216);


(lib.sprite140 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.tfRight = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfRight.name = "tfRight";
	this.tfRight.textAlign = "center";
	this.tfRight.lineHeight = 16;
	this.tfRight.lineWidth = 28;
	this.tfRight.setTransform(625,2);

	this.timeline.addTween(cjs.Tween.get(this.tfRight).wait(1));

	// Layer 14
	this.tfLeft = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfLeft.name = "tfLeft";
	this.tfLeft.textAlign = "center";
	this.tfLeft.lineHeight = 16;
	this.tfLeft.lineWidth = 28;
	this.tfLeft.setTransform(14,2);

	this.timeline.addTween(cjs.Tween.get(this.tfLeft).wait(1));

	// Layer 13
	this.instance = new lib.shape139("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.body = new lib.sprite137();
	this.body.setTransform(29,13);
	this.body.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.body).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-281.5,642.9,316);


(lib.sprite128 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.tfRight = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfRight.name = "tfRight";
	this.tfRight.textAlign = "center";
	this.tfRight.lineHeight = 16;
	this.tfRight.lineWidth = 28;
	this.tfRight.setTransform(625,2);

	this.timeline.addTween(cjs.Tween.get(this.tfRight).wait(1));

	// Layer 14
	this.tfLeft = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfLeft.name = "tfLeft";
	this.tfLeft.textAlign = "center";
	this.tfLeft.lineHeight = 16;
	this.tfLeft.lineWidth = 28;
	this.tfLeft.setTransform(14,2);

	this.timeline.addTween(cjs.Tween.get(this.tfLeft).wait(1));

	// Layer 13
	this.instance = new lib.shape127("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.body = new lib.sprite125();
	this.body.setTransform(29,13);
	this.body.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.body).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-6.5,642.9,316);


(lib.sprite116 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.tfRight = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfRight.name = "tfRight";
	this.tfRight.textAlign = "center";
	this.tfRight.lineHeight = 16;
	this.tfRight.lineWidth = 28;
	this.tfRight.setTransform(625,2);

	this.timeline.addTween(cjs.Tween.get(this.tfRight).wait(1));

	// Layer 14
	this.tfLeft = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfLeft.name = "tfLeft";
	this.tfLeft.textAlign = "center";
	this.tfLeft.lineHeight = 16;
	this.tfLeft.lineWidth = 28;
	this.tfLeft.setTransform(14,2);

	this.timeline.addTween(cjs.Tween.get(this.tfLeft).wait(1));

	// Layer 13
	this.instance = new lib.shape115("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.body = new lib.sprite113();
	this.body.setTransform(29,13);
	this.body.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.body).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-47.5,642.9,116);


(lib.sprite108 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.tfRight = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfRight.name = "tfRight";
	this.tfRight.textAlign = "center";
	this.tfRight.lineHeight = 16;
	this.tfRight.lineWidth = 28;
	this.tfRight.setTransform(625,2);

	this.timeline.addTween(cjs.Tween.get(this.tfRight).wait(1));

	// Layer 14
	this.tfLeft = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfLeft.name = "tfLeft";
	this.tfLeft.textAlign = "center";
	this.tfLeft.lineHeight = 16;
	this.tfLeft.lineWidth = 28;
	this.tfLeft.setTransform(14,2);

	this.timeline.addTween(cjs.Tween.get(this.tfLeft).wait(1));

	// Layer 13
	this.instance = new lib.shape107("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.body = new lib.sprite105();
	this.body.setTransform(29,13);
	this.body.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.body).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-40.5,642.9,116);


(lib.sprite100 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.tfRight = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfRight.name = "tfRight";
	this.tfRight.textAlign = "center";
	this.tfRight.lineHeight = 16;
	this.tfRight.lineWidth = 28;
	this.tfRight.setTransform(625,2);

	this.timeline.addTween(cjs.Tween.get(this.tfRight).wait(1));

	// Layer 14
	this.tfLeft = new cjs.Text("20", "bold 14px 'Arial'");
	this.tfLeft.name = "tfLeft";
	this.tfLeft.textAlign = "center";
	this.tfLeft.lineHeight = 16;
	this.tfLeft.lineWidth = 28;
	this.tfLeft.setTransform(14,2);

	this.timeline.addTween(cjs.Tween.get(this.tfLeft).wait(1));

	// Layer 13
	this.instance = new lib.shape97("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.body = new lib.sprite95();
	this.body.setTransform(29,13);
	this.body.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.body).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-44.5,642.9,116);


(lib.sprite197 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 129
	this.instance = new lib.sprite196();
	this.instance.setTransform(0,284);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 113
	this.instance_1 = new lib.sprite180();
	this.instance_1.setTransform(0,146);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 97
	this.instance_2 = new lib.sprite164();
	this.instance_2.setTransform(0,181);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer 81
	this.instance_3 = new lib.sprite152();
	this.instance_3.setTransform(0,249);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Layer 65
	this.instance_4 = new lib.sprite140();
	this.instance_4.setTransform(0,352);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Layer 49
	this.instance_5 = new lib.sprite128();
	this.instance_5.setTransform(0,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

	// Layer 33
	this.instance_6 = new lib.sprite116();
	this.instance_6.setTransform(0,318);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1));

	// Layer 17
	this.instance_7 = new lib.sprite108();
	this.instance_7.setTransform(0,111);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1));

	// Layer 1
	this.instance_8 = new lib.sprite100();
	this.instance_8.setTransform(0,215);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,70.5,642.9,317);


(lib.sprite279 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 191
	this.special = new lib.sprite278();

	this.timeline.addTween(cjs.Tween.get(this.special).wait(1));

	// Layer 186
	this.msgFree = new lib.sprite205();

	this.timeline.addTween(cjs.Tween.get(this.msgFree).wait(1));

	// Layer 56
	this.lines = new lib.sprite197();

	this.timeline.addTween(cjs.Tween.get(this.lines).wait(1));

	// Layer 40
	this.anims = new lib.sprite90();

	this.timeline.addTween(cjs.Tween.get(this.anims).wait(1));

	// Layer 38
	this.iconAuto = new lib.sprite88();
	this.iconAuto.setTransform(560,15);

	this.timeline.addTween(cjs.Tween.get(this.iconAuto).wait(1));

	// Layer 37
	this.tfWinSum = new cjs.Text("win sum", "bold 15px 'Arial'", "#ECD502");
	this.tfWinSum.name = "tfWinSum";
	this.tfWinSum.lineHeight = 17;
	this.tfWinSum.lineWidth = 161;
	this.tfWinSum.setTransform(20,27);
	this.tfWinSum.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.tfWinSum).wait(1));

	// Layer 36
	this.tfWinType = new cjs.Text("win type", "bold 15px 'Arial'", "#ECD502");
	this.tfWinType.name = "tfWinType";
	this.tfWinType.lineHeight = 17;
	this.tfWinType.lineWidth = 161;
	this.tfWinType.setTransform(20,10);
	this.tfWinType.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.tfWinType).wait(1));

	// Layer 35
	this.tfWin = new cjs.Text("9999", "bold 16px 'Arial'", "#ECD502");
	this.tfWin.name = "tfWin";
	this.tfWin.textAlign = "center";
	this.tfWin.lineHeight = 18;
	this.tfWin.lineWidth = 277;
	this.tfWin.setTransform(393.3,455);
	this.tfWin.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,3);

	this.timeline.addTween(cjs.Tween.get(this.tfWin).wait(1));

	// Layer 34
	this.tfCurrency = new cjs.Text("$$$", "22px 'Times New Roman'", "#FF0000");
	this.tfCurrency.name = "tfCurrency";
	this.tfCurrency.lineHeight = 24;
	this.tfCurrency.lineWidth = 62;
	this.tfCurrency.setTransform(420,415,1,1.002);
	this.tfCurrency.shadow = new cjs.Shadow("rgba(204,204,0,1)",0,0,2);

	this.timeline.addTween(cjs.Tween.get(this.tfCurrency).wait(1));

	// Layer 33
	this.tfDenom = new cjs.Text("0.1", "22px 'Times New Roman'", "#FF0000");
	this.tfDenom.name = "tfDenom";
	this.tfDenom.textAlign = "right";
	this.tfDenom.lineHeight = 24;
	this.tfDenom.lineWidth = 40;
	this.tfDenom.setTransform(415,415);
	this.tfDenom.shadow = new cjs.Shadow("rgba(204,204,0,1)",0,0,2);

	this.timeline.addTween(cjs.Tween.get(this.tfDenom).wait(1));

	// Layer 32
	this.tfTotal = new cjs.Text("90", "20px 'Arial'", "#DEE300");
	this.tfTotal.name = "tfTotal";
	this.tfTotal.textAlign = "right";
	this.tfTotal.lineHeight = 22;
	this.tfTotal.lineWidth = 59;
	this.tfTotal.setTransform(623.7,454);

	this.timeline.addTween(cjs.Tween.get(this.tfTotal).wait(1));

	// Layer 31
	this.tfLines = new cjs.Text("9", "20px 'Times New Roman'", "#218AD6");
	this.tfLines.name = "tfLines";
	this.tfLines.textAlign = "right";
	this.tfLines.lineHeight = 22;
	this.tfLines.lineWidth = 51;
	this.tfLines.setTransform(623,421);

	this.timeline.addTween(cjs.Tween.get(this.tfLines).wait(1));

	// Layer 30
	this.tfBet = new cjs.Text("10", "20px 'Times New Roman'", "#DEE300");
	this.tfBet.name = "tfBet";
	this.tfBet.textAlign = "right";
	this.tfBet.lineHeight = 22;
	this.tfBet.lineWidth = 51;
	this.tfBet.setTransform(623,401);

	this.timeline.addTween(cjs.Tween.get(this.tfBet).wait(1));

	// Layer 29
	this.tfCredits = new cjs.Text("1000", "20px 'Times New Roman'", "#DEE300");
	this.tfCredits.name = "tfCredits";
	this.tfCredits.textAlign = "right";
	this.tfCredits.lineHeight = 22;
	this.tfCredits.lineWidth = 123;
	this.tfCredits.setTransform(219,454);

	this.timeline.addTween(cjs.Tween.get(this.tfCredits).wait(1));

	// Layer 28
	this.tfStatus = new cjs.Text("status", "bold 20px 'Arial'", "#DEE300");
	this.tfStatus.name = "tfStatus";
	this.tfStatus.textAlign = "center";
	this.tfStatus.lineHeight = 22;
	this.tfStatus.lineWidth = 346;
	this.tfStatus.setTransform(190,409,1,1.002);

	this.timeline.addTween(cjs.Tween.get(this.tfStatus).wait(1));

	// Layer 26
	this.animHeader = new lib.sprite72();

	this.timeline.addTween(cjs.Tween.get(this.animHeader).wait(1));

	// Layer 23
	this.background = new lib.sprite8();

	this.timeline.addTween(cjs.Tween.get(this.background).wait(1));

	// Layer 1
	this.reels = new lib.sprite3();
	this.reels.setTransform(42,77);

	this.timeline.addTween(cjs.Tween.get(this.reels).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,642.9,488.2);


(lib.assetsgfxGfxStructure = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 245
	this.error = new lib.sprite381();

	this.timeline.addTween(cjs.Tween.get(this.error).wait(1));

	// Layer 214
	this.help = new lib.sprite368();

	this.timeline.addTween(cjs.Tween.get(this.help).wait(1));

	// Layer 193
	this.gamble = new lib.sprite310();

	this.timeline.addTween(cjs.Tween.get(this.gamble).wait(1));

	// Layer 1
	this.slots = new lib.sprite279();

	this.timeline.addTween(cjs.Tween.get(this.slots).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-203,661.7,691.2);


// stage content:
(lib.graphics = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance_5 = new lib.text375("synched",0);
	this.instance_5.setTransform(2,2);
	this.instance_5.shadow = new cjs.Shadow("#000000",4,4,5);

	this.instance_6 = new lib.text377("synched",0);
	this.instance_6.setTransform(2,2);
	this.instance_6.shadow = new cjs.Shadow("#000000",4,4,5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).wait(1));

	// Layer 2
	this.instance_7 = new lib.text374("synched",0);
	this.instance_7.setTransform(-41.7,119);

	this.instance_8 = new lib.shape376("synched",0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7}]}).to({state:[{t:this.instance_8}]},1).wait(1));

	// Layer 1
	this.instance_9 = new lib.shape373("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(409,298,162,347.1);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;