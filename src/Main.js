(function (window) {


    Game._CANVAS = null;
    Game.getCanvas = function () { return Game._CANVAS };

    Game._STAGE = null;
    Game.getStage = function () { return Game._STAGE };

    Game.init = function () {

        Game.start_time = new Date().getTime();
        window.getTimer = function() {
            return new Date().getTime() - Game.start_time;
        }
        Game._CANVAS = document.getElementById("stage");

        Game._STAGE = new createjs.Stage(Game._CANVAS);
        Game._STAGE.stageWidth = Game._CANVAS.width;
        Game._STAGE.stageHeight = Game._CANVAS.height;

        Game._STAGE.enableMouseOver();
        //Game._STAGE.setBounds(0, 0, Game._STAGE.stageWidth, Game._STAGE.stageHeight);
        //Game._STAGE.mouseMoveOutside = true;

        createjs.Touch.enable(Game._STAGE);
        createjs.Ticker.setFPS(60);
        createjs.Ticker.addEventListener("tick", Game._tickHandler);

        fpsLabel = new createjs.Text("-- fps", "bold 12px Arial", "#FFF");
        fpsLabel.x = 728;
        fpsLabel.y = 200;
        Game._STAGE.addChild(fpsLabel);



        Game.POOL = Game.Utils.ObjPool.get_inst();
        Game.Gfx = Game.Comps.Gfx.get_inst();
        Game.Cfg = Game.Comps.Cfg.get_inst();
        Game.Sfx = Game.Comps.Sfx.get_inst();
        Game.TXT = Game.Comps.Txt.get_inst();

        Game.box = new Game.GaminatorBox();
        Game._STAGE.addChild(Game.Gfx);
        Game._STAGE.addChild(Game.box);
        Game.Gfx.x = 47;
        Game.Gfx.y = 9;
        Game._STAGE.update();
        document.getElementById("loading").style.display = 'none';
        Game.Gfx.init();
        Game.BookOfRa = new Game.Controller(Game._STAGE);
        Game.box.showContent(Game._STAGE);
        Game.BookOfRa.init();
    }

    Game._tickHandler = function (e) {
        fpsLabel.text = Math.round(createjs.Ticker.getMeasuredFPS()) + " fps"; //TEMP
        Game._STAGE.update(e);
    };

})(window);