(function(){

    function Preloader() { }

    Preloader._loaderButtons = null;
    Preloader.step = 0;
    Preloader.LOADER = document.getElementById("loading");

    Preloader.loadButtons = function(){
        Preloader._loaderButtons = new createjs.LoadQueue(false, '_lib/_graphics/buttons/');
        Preloader._loaderButtons.installPlugin(createjs.Sound);
        Preloader._loaderButtons.addEventListener("fileload", Preloader._handleButtonLoad);
        Preloader._loaderButtons.addEventListener("progress", Preloader._progressGame);
        Preloader._loaderButtons.addEventListener("complete", Preloader._handleButtonComplete);

        Preloader._loaderButtons.loadManifest(but_lib.properties.manifest);
        Preloader._handleButtonComplete() // TODO
    };

    Preloader._progressGame = function (e) {
        var percent = Math.round(e.progress * 50) + 50 * Preloader.step;
        var txt = percent == 100 ? 'Please Wait...' : 'Loading: ' + percent + '%'

        document.getElementById("loading").innerHTML = txt;

    };

    Preloader._handleButtonLoad = function(e) {
        if (e.item.type == "image") { but_images[e.item.id] = e.result; }
    };

    Preloader._handleButtonComplete = function(e) {
        Preloader.step++;
        Preloader.loadGame();
    };

    Preloader._loaderGame = null;

    Preloader.loadGame = function(){
        Preloader._loaderGame = new createjs.LoadQueue(false, '_lib/_graphics/game/');
        Preloader._loaderGame.installPlugin(createjs.Sound);
        Preloader._loaderGame.addEventListener("fileload", Preloader._handleGameLoad);
        Preloader._loaderGame.addEventListener("progress", Preloader._progressGame);
        Preloader._loaderGame.addEventListener("complete", Preloader._handleGameComplete);
        for (var i = 1; i < 36; i++) {
            Preloader._loaderGame.loadFile({ src: "images/graphics_atlas_" + (i == 1 ? '' : i) + ".json?" + new Date().getTime(), type: "spritesheet", id: "graphics_atlas_" + (i == 1 ? '' : i) }, true);
        }
        Preloader._loaderGame.loadManifest(lib.properties.manifest);
    };

    Preloader._handleGameLoad = function(e) {
        if (e.item.type == "image") { images[e.item.id] = e.result; }
    };

    Preloader._handleGameComplete = function (e) {
        for (var i = 1; i < 36; i++) {
            ss["graphics_atlas_" + (i == 1 ? '' : i)] = e.target.getResult("graphics_atlas_" + (i == 1 ? '' : i));
        }
        window.Game.init();
    };

    window.Preloader = Preloader;
})();
