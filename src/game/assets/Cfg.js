﻿(function (window) {
    function Cfg() {
        this.urlServer = "game.php";
        //this.urlExit:String;
        //this.targetExit:String;
        //this.jpComps:String;
        //this.linePoints:Vector.<Vector.<uint>>;
        //this.pays:Vector.<Vector.<uint>>;
        //this.bets:Vector.<uint>;
        //this.lines:Vector.<uint>;
        //this.denoms:Vector.<uint>;
        //this.idBook:uint;
        //this.idSpec:int;
        //this.idWild:uint;
        //this.idScat:uint;
        //this.idTrig:uint;
        //this.minTrigs:uint;
        //this.wildFactors:Vector.<uint>;
        //this.freeCounts:Vector.<uint>;
        //this.freeFactor:uint;
        //this.freeRetrigger:Boolean;
        //this.gambleGames:uint;
        //this.INST:Cfg;

        //p.Cfg= function()
        //{
        if (this.INST) {
            throw new Error("Oops... `Cfg` is a singleton. Use `Cfg.inst` to access an instance.");
        }
        this.init();
    }
    var p = Cfg.prototype;
    p.parseDenoms = function (param1) {
        if (!param1) {
            return;
        }
        var _loc_2 = param1.split(",");
        var _loc_3 = _loc_2.length;
        while (_loc_3--) {

            _loc_2[_loc_3] = parseInt(_loc_2[_loc_3]);
        }
        if (_loc_2.length == 1) {
            _loc_3 = this.denoms.length;
            while (_loc_3--) {

                this.denoms[_loc_3] = this.denoms[_loc_3] * _loc_2[0];
            }
        }
        else {
            this.denoms = Array(_loc_2);
        }
        return;
    }

    p.init = function () {
        this.linePoints = [[1, 1, 1, 1, 1], [0, 0, 0, 0, 0], [2, 2, 2, 2, 2], [0, 1, 2, 1, 0], [2, 1, 0, 1, 2], [1, 0, 0, 0, 1], [1, 2, 2, 2, 1], [0, 0, 1, 2, 2], [2, 2, 1, 0, 0]];
        this.pays = [[0, 0, 5, 25, 100], [0, 0, 5, 25, 100], [0, 0, 5, 25, 100], [0, 0, 5, 40, 150], [0, 0, 5, 40, 150], [0, 5, 30, 100, 750], [0, 5, 30, 100, 750], [0, 5, 40, 400, 2000], [0, 10, 100, 1000, 5000], [0, 0, 2, 20, 200]];
        this.bets = [1, 2, 3, 4, 5, 10, 15, 20, 30, 40, 50, 100];
        this.lines = [1, 3, 5, 7, 9];
        this.denoms = [10, 20, 5];
        this.idSpec = -1;
        this.idBook = 9;
        this.idWild = 9;
        this.idScat = 9;
        this.idTrig = 9;
        this.minTrigs = 3;
        this.wildFactors = [1, 1, 1, 1, 1];
        this.freeCounts = [0, 0, 10, 10, 10];
        this.freeFactor = 1;
        this.freeRetrigger = true;
        this.gambleGames = 5;
        return;
    }

    Cfg.get_inst = function () {
        if (!this.INST) {
            this.INST = new Cfg;
        }
        return this.INST;
    }

    window.Game.Comps.Cfg = Cfg;

})(window);