﻿(function (window) {
    function Gfx() {
        lib.assetsgfxGfxStructure.call(this);

        //this.pSlots:DisplayObject;
        //this.slotsBackground:AnimCollection;
        //this.slotsHeader:AnimCollection;
        //this.slotsSpecial:Special;
        //this.slotsAuto:DisplayObject;
        //this.slotsReels:Reels;
        //this.slotsAnims:ReelsAnimations;
        //this.slotsLines:Vector.<GameLine>;
        //this.slotsMsgFree:MsgFree;
        //this.slotsTfCredits:TextField;
        //this.slotsTfDenom:TextField;
        //this.slotsTfCurrency:TextField;
        //this.slotsTfBet:TextField;
        //this.slotsTfLines:TextField;
        //this.slotsTfTotal:TextField;
        //this.slotsTfWin:TextField;
        //this.slotsTfWinType:TextField;
        //this.slotsTfWinSum:TextField;
        //this.slotsTfWinFree:TextField;
        //this.slotsStatus:TextDisplay;
        //this.pGamble:DisplayObject;
        //this.gambleCard:MovieClip;
        //this.gambleVecIcon:Vector.<DisplayObject>;
        //this.gambleVecSuite:Vector.<MovieClip>;
        //this.gambleTfAmount:TextField;
        //this.gambleTfWin:TextField;
        //this.pHelp:DisplayObject;
        //this.helpVecPage:Vector.<DisplayObject>;
        //this.pError:DisplayObject;
        //this.errIcon:MovieClip;
        //this.errTfText:TextField;
        //this.vecClsTiles:Vector.<Vector.<Class>>;
        //this.vecClsTileAnims:Vector.<Vector.<Vector.<Class>>>;
        //this.jackpotComponents:Object;
        //this._jpLoadStore:Dictionary;
        //this._jpLoadElapsed:uint;
        //this._jpLoadCallback:Function;
        //this.INST:Gfx;

        //this.init();
    }

    var p = createjs.extend(Gfx, lib.assetsgfxGfxStructure);

    p.prepareJackpotComponents = function (param1, param2) {
        var _loc_3 = null;
        var _loc_4 = null;
        var _loc_5 = null;
        var _loc_6 = null;
        this.jackpotComponents = {};
        this._jpLoadCallback = param2;
        this._jpLoadStore = {};
        this._jpLoadElapsed = 0;
        for (_loc_3 in param1) {

            _loc_4 = _loc_3;
            _loc_5 = param1[_loc_3];
            _loc_6 = new Connector();
            _loc_6.addEventListener('Event.COMPLETE', this.handleJPCompLoaded.bind(this));
            _loc_6.load(_loc_5, null, Connector.FORMAT_BIN, true);
            this._jpLoadStore[_loc_6] = _loc_4;
            var _loc_9 = this;
            var _loc_10 = this._jpLoadElapsed + 1;
            _loc_9._jpLoadElapsed = _loc_10;
        }
        if (!this._jpLoadElapsed) {
            this._jpLoadCallback.call();
        }
        return;
    }

    p.handleJPCompLoaded = function (event) {
        var _loc_2 = event.target;
        var _loc_3 = this._jpLoadStore[_loc_2];
        this.jackpotComponents[_loc_3] = _loc_2.data;
        var _loc_4 = this;
        var _loc_5 = this._jpLoadElapsed - 1;
        _loc_4._jpLoadElapsed = _loc_5;
        if (!this._jpLoadElapsed) {
            this._jpLoadCallback.call();
        }
        return;
    }

    p.init = function () {
        var _loc_1 = null;
        var _loc_2 = null;
        this.vecClsTiles = [[lib.assetsgfxTileN_00, lib.assetsgfxTileB_00], [lib.assetsgfxTileN_01, lib.assetsgfxTileB_01], [lib.assetsgfxTileN_02, lib.assetsgfxTileB_02], [lib.assetsgfxTileN_03, lib.assetsgfxTileB_03], [lib.assetsgfxTileN_04, lib.assetsgfxTileB_04], [lib.assetsgfxTileN_05, lib.assetsgfxTileB_05], [lib.assetsgfxTileN_06, lib.assetsgfxTileB_06], [lib.assetsgfxTileN_07, lib.assetsgfxTileB_07], [lib.assetsgfxTileN_08, lib.assetsgfxTileB_08], [lib.assetsgfxTile_09, lib.assetsgfxTile_09]];
        this.vecClsTileAnims = [[null, null, [lib.assetsgfxT_00N_A3, lib.assetsgfxT_00B_A3], [lib.assetsgfxT_00N_A45, lib.assetsgfxT_00B_A45], [lib.assetsgfxT_00N_A45, lib.assetsgfxT_00B_A45]], [null, null, [lib.assetsgfxT_01N_A3, lib.assetsgfxT_01B_A3], [lib.assetsgfxT_01N_A45, lib.assetsgfxT_01B_A45], [lib.assetsgfxT_01N_A45, lib.assetsgfxT_01B_A45]], [null, null, [lib.assetsgfxT_02N_A3, lib.assetsgfxT_02B_A3], [lib.assetsgfxT_02N_A45, lib.assetsgfxT_02B_A45], [lib.assetsgfxT_02N_A45, lib.assetsgfxT_02B_A45]], [null, null, [lib.assetsgfxT_03N_A3, lib.assetsgfxT_03B_A3], [lib.assetsgfxT_03N_A45, lib.assetsgfxT_03B_A45], [lib.assetsgfxT_03N_A45, lib.assetsgfxT_03B_A45]], [null, null, [lib.assetsgfxT_04N_A3, lib.assetsgfxT_04B_A3], [lib.assetsgfxT_04N_A45, lib.assetsgfxT_04B_A45], [lib.assetsgfxT_04N_A45, lib.assetsgfxT_04B_A45]], [null, [lib.assetsgfxT_05N_A23, lib.assetsgfxT_05B_A23], [lib.assetsgfxT_05N_A23, lib.assetsgfxT_05B_A23], [lib.assetsgfxT_05N_A45, lib.assetsgfxT_05B_A45], [lib.assetsgfxT_05N_A45, lib.assetsgfxT_05B_A45]], [null, [lib.assetsgfxT_06N_A23, lib.assetsgfxT_06B_A23], [lib.assetsgfxT_06N_A23, lib.assetsgfxT_06B_A23], [lib.assetsgfxT_06N_A45, lib.assetsgfxT_06B_A45], [lib.assetsgfxT_06N_A45, lib.assetsgfxT_06B_A45]], [null, [lib.assetsgfxT_07N_A23, lib.assetsgfxT_07B_A23], [lib.assetsgfxT_07N_A23, lib.assetsgfxT_07B_A23], [lib.assetsgfxT_07N_A45, lib.assetsgfxT_07B_A45], [lib.assetsgfxT_07N_A45, lib.assetsgfxT_07B_A45]], [null, [lib.assetsgfxT_08N_A23, lib.assetsgfxT_08B_A23], [lib.assetsgfxT_08N_A23, lib.assetsgfxT_08B_A23], [lib.assetsgfxT_08N_A45, lib.assetsgfxT_08B_A45], [lib.assetsgfxT_08N_A45, lib.assetsgfxT_08B_A45]], [null, [lib.assetsgfxT_09N_A, lib.assetsgfxT_09N_A, lib.assetsgfxT_09B_A], [lib.assetsgfxT_09N_A, lib.assetsgfxT_09N_A, lib.assetsgfxT_09B_A], [lib.assetsgfxT_09N_A, lib.assetsgfxT_09N_A, lib.assetsgfxT_09B_A], [lib.assetsgfxT_09N_A, lib.assetsgfxT_09N_A, lib.assetsgfxT_09B_A]]];
        Game.Comps.Tile.DATA = this.vecClsTiles;
        Game.Comps.ReelsAnimations.DATA = this.vecClsTileAnims;
        this.pSlots = this.slots;
        _loc_1 = this.pSlots;
        _loc_2 = _loc_1.background;
        this.slotsBackground = new Game.Comps.AnimCollection(_loc_2);
        _loc_2 = _loc_1.animHeader;
        this.slotsHeader = new Game.Comps.AnimCollection(_loc_2);
        _loc_2 = _loc_1.special;
        this.slotsSpecial = new Game.Comps.Special(_loc_2);
        this.slotsAuto = _loc_1.iconAuto;
        this.slotsReels = new Game.Comps.Reels(_loc_1);
        this.slotsAnims = new Game.Comps.ReelsAnimations(_loc_1.anims);
        this.slotsMsgFree = new Game.Comps.MsgFree(_loc_1);
        this.slotsTfCredits = _loc_1.tfCredits;
        this.slotsTfDenom = _loc_1.tfDenom;
        this.slotsTfCurrency = _loc_1.tfCurrency;
        this.slotsTfBet = _loc_1.tfBet;
        this.slotsTfLines = _loc_1.tfLines;
        this.slotsTfTotal = _loc_1.tfTotal;
        this.slotsTfWin = _loc_1.tfWin;
        this.slotsTfWinType = _loc_1.tfWinType;
        this.slotsTfWinSum = _loc_1.tfWinSum;
        this.slotsTfWinFree = _loc_1.msgFree.tf;
        _loc_2 = _loc_1.tfStatus;
        this.slotsStatus = new Game.Comps.TextDisplay(_loc_2);
        _loc_1 = _loc_1.lines;
        this.slotsLines = [new Game.Comps.GameLine(_loc_1.instance), new Game.Comps.GameLine(_loc_1.instance_1), new Game.Comps.GameLine(_loc_1.instance_2), new Game.Comps.GameLine(_loc_1.instance_3), new Game.Comps.GameLine(_loc_1.instance_4), new Game.Comps.GameLine(_loc_1.instance_5), new Game.Comps.GameLine(_loc_1.instance_6), new Game.Comps.GameLine(_loc_1.instance_7), new Game.Comps.GameLine(_loc_1.instance_8)];
        this.pGamble = this.gamble;
        _loc_1 = this.pGamble;
        this.gambleCard = _loc_1.card;
        this.gambleTfAmount = _loc_1.tfAmount;
        this.gambleTfWin = _loc_1.tfWin;
        this.gambleVecIcon = [_loc_1.iconRed, _loc_1.iconBlack];
        this.gambleVecSuite = [_loc_1.suite0, _loc_1.suite1, _loc_1.suite2, _loc_1.suite3, _loc_1.suite4, _loc_1.suite5];
        this.pHelp = this.help;
        _loc_1 = this.pHelp;
        this.helpVecPage = [_loc_1.instance_2, _loc_1.instance_1, _loc_1.instance];
        this.pError = this.error;
        _loc_1 = this.pError;
        this.errIcon = _loc_1.icon;
        this.errTfText = _loc_1.tfText;
        return;
    }

    Gfx.get_inst = function () {
        if (!this.INST) {
            this.INST = new Gfx;
            //this.INST.init();

        }
        return this.INST;
    }
    window.Game.Comps.Gfx = Gfx;
})(window);