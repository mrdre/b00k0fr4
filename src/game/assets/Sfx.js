﻿(function (window) {

    function Sfx() {
        if (this.INST) {
            throw new Error("Oops... `Sfx` is a singleton. Use `Sfx.inst` to access an instance.");
        }
        this.init();
        return;
    }
    var p = Sfx.prototype;

    function Sound(id) {
        this._id = id;
    }
    var s = Sound.prototype;
    createjs.EventDispatcher.initialize(s);

    s.play = function (startTime, loops) {
        return createjs.Sound.play(this._id, "none", 0, 0, loops == undefined ? 0 : loops, 1, 0, startTime == undefined ? null : undefined);
    }
    s.stop = function () {
        return createjs.Sound.stop(this._id);
    }

    p.init = function () {
        this.autoStart = new Sound('assetssfxAutoStart');
        this.autoStop = new Sound('assetssfxAutoStop');
        this.betLine = new Sound('assetssfxBetLine');
        this.bonusTile_1 = new Sound('assetssfxBonusTile_1');
        this.bonusTile_2 = new Sound('assetssfxBonusTile_2');
        this.bonusTile_3 = new Sound('assetssfxBonusTile_3');
        this.bonusTile_4 = new Sound('assetssfxBonusTile_4');
        this.bonusTile_5 = new Sound('assetssfxBonusTile_5');
        this.changeCard = new Sound('assetssfxChangeCard');
        this.free = new Sound('assetssfxFree');
        this.freeEnd = new Sound('assetssfxFreeEnd');
        this.freeStart = new Sound('assetssfxFreeStart');
        this.gambleWin_1 = new Sound('assetssfxGambleWin_1');
        this.gambleWin_2 = new Sound('assetssfxGambleWin_2');
        this.gambleWin_3 = new Sound('assetssfxGambleWin_3');
        this.gambleWin_4 = new Sound('assetssfxGambleWin_4');
        this.gambleWin_5 = new Sound('assetssfxGambleWin_5');
        this.help = new Sound('assetssfxHelp');
        this.ring_100 = new Sound('assetssfxRing_100');
        this.ring_1000 = new Sound('assetssfxRing_1000');
        this.ring_10000 = new Sound('assetssfxRing_10000');
        this.showGamble = new Sound('assetssfxShowGamble');
        this.spin = new Sound('assetssfxSpin');
        this.spinStop = new Sound('assetssfxSpinStop');
        this.takeFast = new Sound('assetssfxTakeFast');
        this.takeSlow = new Sound('assetssfxTakeSlow');
        this.wait = new Sound('assetssfxWait');
        this.win_2x567_3x012 = new Sound('assetssfxWin_2x567_3x012');
        this.win_2x89_3x34 = new Sound('assetssfxWin_2x89_3x34');
        this.win_345x5 = new Sound('assetssfxWin_345x5');
        this.win_345x6 = new Sound('assetssfxWin_345x6');
        this.win_345x7 = new Sound('assetssfxWin_345x7');
        this.win_345x8 = new Sound('assetssfxWin_345x8');
        this.win_3x9 = new Sound('assetssfxWin_3x9');
        this.win_4x012 = new Sound('assetssfxWin_4x012');
        this.win_4x34 = new Sound('assetssfxWin_4x34');
        this.win_4x9 = new Sound('assetssfxWin_4x9');
        this.win_5x012 = new Sound('assetssfxWin_5x012');
        this.win_5x34 = new Sound('assetssfxWin_5x34');
        this.win_5x9 = new Sound('assetssfxWin_5x9');
        this.bookOpen = new Sound('assetssfxBookOpen');
        this.bookRising = new Sound('assetssfxBookRising');
        this.bookTileChange = new Sound('assetssfxBookSymbolChange');
        this.freeLineWin = new Sound('assetssfxFreeLineWin');
        this.tileChange = new Sound('assetssfxTransformSlot');
        this.vecAuto = [this.autoStop, this.autoStart];
        this.vecBonusTile = [this.bonusTile_1, this.bonusTile_2, this.bonusTile_3, this.bonusTile_4, this.bonusTile_5];
        this.vecGambleWin = [this.gambleWin_1, this.gambleWin_2, this.gambleWin_3, this.gambleWin_4, this.gambleWin_5];
        this.vecRing = [null, null, this.ring_100, this.ring_1000, this.ring_10000];
        this.vecWin = [[null, null, this.win_2x567_3x012, this.win_4x012, this.win_5x012], [null, null, this.win_2x567_3x012, this.win_4x012, this.win_5x012], [null, null, this.win_2x567_3x012, this.win_4x012, this.win_5x012], [null, null, this.win_2x89_3x34, this.win_4x34, this.win_5x34], [null, null, this.win_2x89_3x34, this.win_4x34, this.win_5x34], [null, this.win_2x567_3x012, this.win_345x5, this.win_345x5, this.win_345x5], [null, this.win_2x567_3x012, this.win_345x5, this.win_345x6, this.win_345x6], [null, this.win_2x567_3x012, this.win_345x5, this.win_345x7, this.win_345x7], [null, this.win_2x89_3x34, this.win_345x8, this.win_345x8, this.win_345x8], [null, this.win_2x89_3x34, this.win_3x9, this.win_4x9, this.win_5x9]];
        return;
    }

    Sfx.get_inst = function () {
        if (!this.INST) {
            this.INST = new Sfx;
        }
        return this.INST;
    }
    window.Game.Comps.Sfx = Sfx;


})(window);