﻿(function (window) {



    function Txt() {
        if (this.INST) {
            throw new Error("Oops... `Txt` is a singleton. Use `Txt.inst` to access an instance.");
        }
        this.init();
        return;
    }

    var p = Txt.prototype;

    p.WAIT = "wait";
    p.WAIT_JP = "waitJP";
    p.GAME_OVER = "gameOver";
    p.GAMBLE_OVER = "gambleOver";
    p.FREE_OVER = "freeOver";
    p.COIN_OVER = "coinOver";
    p.START_FREE = "startFree";
    p.FREE_GAME = "freeGame";
    p.PRIZE_FREE = "prizeFree";
    p.PRIZE_SUM = "prizeSum";
    p.PRIZE_LINE = "prizeLine";
    p.PRIZE_SCAT = "prizeScat";
    p.PRIZE_JP = "prizeJP";
    p.SKIP_JP = "skipJP";
    p.INFO_GAMBLE = "infoGamble";
    p.INVALID = "invalid";

    p.getText = function (param1, param2) {
        param2 = param2 == undefined ? null : param2;
        var _loc_3 = new RegExp("%param%");
        var _loc_4 = this._data[param1][this.lng];
        var _loc_5 = param2 ? (param2.length) : (0);
        var _loc_6 = 0;
        while (_loc_6 < _loc_5) {

            _loc_4 = _loc_4.replace(_loc_3, param2[_loc_6]);
            _loc_6 = _loc_6 + 1;
        }
        return _loc_4;
    }

    p.init = function () {
        this.langs = ["en", "ru"];
        this.lng = "ru";
        var _loc_1 = null//Capabilities.language;
        var _loc_2 = this.langs.indexOf(_loc_1);
        if (_loc_2 == -1) {
            this.langs.unshift(_loc_1);
        }
        else {
            this.langs[_loc_2] = this.langs[0];
            this.langs[0] = _loc_1;
        }
        this._data = { invalid: { en: "You run a few games. To return to the game refresh the page.", ru: "У вас запущена другая игра. Для возврата к этой игре обновите страницу." }, wait: { en: "GAMBLE UP TO 5xOR TAKE WIN", ru: "GAMBLE UP TO 5xOR TAKE WIN" }, waitJP: { en: "Press \"TAKE WIN\"", ru: "Press \"TAKE WIN\"" }, gameOver: { en: "GAME OVER,PLACE YOUR BET", ru: "GAME OVER,PLACE YOUR BET" }, gambleOver: { en: "GAME OVER - GAMBLE COMPLETED,PLACE YOUR BET", ru: "GAME OVER - GAMBLE COMPLETED,PLACE YOUR BET" }, freeOver: { en: "GAME OVER - FEATURE COMPLETED,PLACE YOUR BET", ru: "GAME OVER - FEATURE COMPLETED,PLACE YOUR BET" }, coinOver: { en: "GAME OVER,INSERT COIN", ru: "GAME OVER,INSERT COIN" }, startFree: { en: "Press a button to begin", ru: "Press a button to begin" }, freeGame: { en: "%param% OF %param% FREE GAMES", ru: "%param% OF %param% FREE GAMES" }, prizeFree: { en: "BONUS FEATURE WON", ru: "BONUS FEATURE WON" }, prizeSum: { en: "%param% CREDITS WON", ru: "%param% CREDITS WON" }, prizeLine: { en: "LINE PAIS", ru: "LINE PAIS" }, prizeScat: { en: "SCATTER PAIS", ru: "SCATTER PAIS" }, prizeJP: { en: "JACKPOT PAIS", ru: "JACKPOT PAIS" }, skipJP: { en: "Press a button to skip JACKPOT", ru: "Press a button to skip JACKPOT" }, infoGamble: { en: "s|CHOOSE RED OR BLACK OR TAKE WIN", ru: "s|CHOOSE RED OR BLACK OR TAKE WIN" } };
        return;
    }

    Txt.get_inst = function () {
        if (!this.INST) {
            this.INST = new Txt;
        }
        return this.INST;
    }

    window.Game.Comps.Txt = Txt;

})(window);