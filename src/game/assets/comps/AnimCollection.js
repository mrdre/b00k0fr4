﻿(function (window) {
    function AnimCollection(param1) {
        //this._body:MovieClip;
        //this._hash:Object;
        //this._lblEnd:String;
        this.DELIM = "->";
        this.START = "start";
        this.END = "end";

        //p.AnimCollection= function(param1:MovieClip)
        //{
        this._body = param1;

        this._body.gotoAndStop(0); //was 1
        this.parseAnims();

    }

    var p = AnimCollection.prototype;
    createjs.EventDispatcher.initialize(p);

    p.gotoStart = function (param1) {
        if (this._hash[param1]) {
            this._body.gotoAndStop(param1 + this.DELIM + this.START);
        }
        else {
            this._body.gotoAndStop(param1);
        }
        return;
    }

    p.gotoEnd = function (param1) {
        if (this._hash[param1]) {
            this._body.gotoAndStop(param1 + this.DELIM + this.END);
        }
        else {
            this._body.gotoAndStop(param1);
        }
        return;
    }

    p.play = function (param1) {
        if (this._hash[param1]) {
            //console.log('D:\OpenServer\domains\test\src\game\assets\comps\AnimCollection.js 44')
            this._body.addEventListener("tick", this.handleFirstFrame.bind(this));
            this._body.gotoAndStop(param1 + this.DELIM + this.START);
            this._lblEnd = param1 + this.DELIM + this.END;
        }
        else {
            this._body.gotoAndStop(param1);
            this.dispatchEvent('Event.COMPLETE');
        }
        return;
    }

    p.parseAnims = function () {
        var _loc_4 = null;
        var _loc_5 = null;
        var _loc_6 = null;
        var _loc_7 = null;
        this._hash = {};
        var _loc_1 = this._body.getLabels();
        var _loc_2 = [];
        var _loc_3 = _loc_1.length;
        while (_loc_3--) {

            _loc_4 = _loc_1[_loc_3];
            if (_loc_2.indexOf(_loc_4.label) == -1) {
                _loc_2.push(_loc_4.label);
            }
        }
        _loc_3 = _loc_2.length;
        while (_loc_3--) {

            _loc_5 = _loc_2[_loc_3];
            if (_loc_5.indexOf(this.DELIM) == -1) {
                continue;
            }
            _loc_6 = _loc_5.split(this.DELIM, 2);
            if (_loc_6[1] != this.START && _loc_6[1] != this.END) {
                continue;
            }
            _loc_7 = _loc_6[0] + this.DELIM + (_loc_6[1] == this.END ? (this.START) : (this.END));
            if (_loc_2.indexOf(_loc_7) == -1) {
                continue;
            }
            this._hash[_loc_6[0]] = true;
        }
        return;
    }

    p.handleFirstFrame = function (event) {
        this._body.removeAllEventListeners("tick", this.handleFirstFrame);
        //console.log('D:\OpenServer\domains\test\src\game\assets\comps\AnimCollection.js 94')
        this._body.addEventListener("tick", this.handleEnterFrame.bind(this));
        this._body.uncache();
        this._body.play();
        return;
    }

    p.handleEnterFrame = function (event) {
        if (this._body.currentFrameLabel != this._lblEnd) {
            return;
        }
        this._body.stop();
        this._body.removeAllEventListeners("tick", this.handleEnterFrame);
        this.dispatchEvent('Event.COMPLETE');
        return;
    }

    window.Game.Comps.AnimCollection = AnimCollection;
})(window);