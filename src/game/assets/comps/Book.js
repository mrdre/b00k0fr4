﻿(function (window) {
    // this._targ:DisplayObjectContainer;
    // this._items:MovieClip;
    // this._tf:TextField;
    // this._id:uint;
    // this._bet:uint;
    // this._pays:Vector.<uint>;
    // this._chan:SoundChannel;
    // this._cnt:uint;
    // this._time:uint;

    function Book(param1) {
        Game.Comps.Book.SND_CHANGE = null;
        this.SND_FLOW = null;
        this.BEGIN = 247;
        this.END = 3;
        this._targ = param1;
        this._items = this._targ.items;
        this._tf = this._targ.tf;
        this._items.gotoAndStop(1);
        this._tf.text = "";
    }

    var p = Book.prototype;
    createjs.EventDispatcher.initialize(p);

    p.start = function (param1, param2, param3) {
        this._id = param1;
        this._bet = param2;
        this._pays = param3;
        this._cnt = 0;
        this._targ.y = this.BEGIN;
        //this._targ.visible = false;
        return;
    }

    p.execute = function () {
        this._targ.visible = true;
        this._items.play();
        this._items.addEventListener("tick", this.handleEFChange.bind(this));
        this._chan = Game.Comps.Book.SND_CHANGE.play(1, Number.MAX_VALUE);//TODO Sound
        this._chan.addEventListener('complete', this.handleSndFlowComplete.bind(this));//TODO Sound
        //this.handleSndFlowComplete();

        return;
    }

    p.stop = function () {
        this._targ.visible = false;
        this._targ.y = this.BEGIN;
        return;
    }

    p.flow = function () {
        this._chan = this.SND_FLOW.play();
        this._chan.addEventListener('complete', this.handleSndFlowComplete.bind(this));//TODO Sound
        //this.handleSndFlowComplete();
        //console.log('D:\OpenServer\domains\test\src\game\assets\comps\Book.js 57')
        this._targ.addEventListener("tick", this.handleEFFlow.bind(this));
        this._time = getTimer();
        return;
    }

    p.handleEFChange = function (event) {
        var _loc_3 = null;
        var _loc_4 = 0;
        var _loc_5 = 0;
        if (this._items.currentFrame == this._items.totalFrames - 1) {
            var _loc_6 = this;
            var _loc_7 = this._cnt + 1;
            _loc_6._cnt = _loc_7;
        }
        if (this._cnt < 2) {
            return;
        }
        var _loc_2 = parseInt(this._items.currentLabel.split("_")[1]);
        if (this._id == _loc_2) {
            this._items.removeAllEventListeners("tick", this.handleEFChange);
            this._items.stop();
            this._chan.stop();
            _loc_3 = [];
            _loc_4 = this._pays.length;
            while (_loc_4--) {

                if (!this._pays[_loc_4]) {
                    continue;
                }
                _loc_5 = this._pays[_loc_4] * this._bet;
                _loc_3.push((_loc_4 + 1) + " - " + _loc_5);
            }
            this._tf.text = _loc_3.join("\n");
            setTimeout(this.flow.bind(this), 500);
        }
        return;
    }

    p.handleEFFlow = function (event) {
        var _loc_2 = (getTimer() - this._time) / this.SND_FLOW.length;
        this._targ.y = this.BEGIN + _loc_2 * _loc_2 * (this.END - this.BEGIN);
        return;
    }

    p.handleSndFlowComplete = function (event) {
        this._chan.removeAllEventListeners('complete', this.handleSndFlowComplete);
        this._targ.removeAllEventListeners("tick", this.handleEFFlow);
        this._targ.y = this.END;
        this.dispatchEvent('Event.COMPLETE');
        return;
    }
    window.Game.Comps.Book = Book;

})(window);