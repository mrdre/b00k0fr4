﻿(function (window) {
    function GameLine(param1) {
        // this._targ:DisplayObjectContainer;
        // this._body:DisplayObjectContainer;
        // this._vecCell:Vector.<MovieClip>;
        // this._vecTf:Vector.<TextField>;

        // p.GameLine(param1:DisplayObjectContainer)// {
        this._targ = param1;
        this._body = this._targ.body;
        this._vecCell = [this._body.c0, this._body.c1, this._body.c2, this._body.c3, this._body.c4];
        this._vecTf = [this._targ.tfLeft, this._targ.tfRight];
        this._vecTf[0].text = "";
        this._vecTf[1].text = "";
        return;
    }
    var p = GameLine.prototype;
    createjs.EventDispatcher.initialize(p);

    p.set_bet = function (param1) {
        var _loc_2 = param1.toString();
        var _loc_3 = this._vecTf.length;
        while (_loc_3--) {

            this._vecTf[_loc_3].text = _loc_2;
        }
        return;
    }

    p.set_active = function (param1) {
        this._targ.visible = param1;
        return;
    }

    p.cells = function (param1, param2) {
        param2 = param2 == undefined ? 0 : param2
        var _loc_6 = 0;
        this._body.visible = param1 != -1;
        var _loc_3 = param2;
        var _loc_4 = _loc_3 + param1 - 1;
        var _loc_5 = this._vecCell.length;
        while (_loc_5--) {

            _loc_6 = _loc_5 < _loc_3 || _loc_4 < _loc_5 ? (1) : (0); //2 1
            this._vecCell[_loc_5].gotoAndStop(_loc_6);
        }
        return;
    }

    p.cellsSpec = function (param1) {
        var _loc_3 = 0;
        this._body.visible = true;
        var _loc_2 = this._vecCell.length;
        while (_loc_2--) {

            _loc_3 = param1[_loc_2] == -1 ? (1) : (0); //2 1
            this._vecCell[_loc_2].gotoAndStop(_loc_3);
        }
        return;
    }

    window.Game.Comps.GameLine = GameLine;
})(window);