﻿(function(window){
		function MsgFree(param1){
        //this._host:DisplayObjectContainer;
        //this._body:DisplayObjectContainer;
        //this._bgAanim:AnimCollection;
        //this._tf:TextField;
        //this._state:String;
        //this._prize;
        //this._chan:SoundChannel;
        //this.SND_BEGIN:Sound;
        //this.SND_AGAIN:Sound;
        //this.SND_END:Sound;

        //p.MsgFree= function(param1:DisplayObject){
		    this._body = param1.msgFree;
		    this._host = param1;
		    this._bgAanim = new Game.Comps.AnimCollection(this._body.background);
		    this._tf = this._body.tf;
		    this._tf.text = "";
		    this._body.visible = false;
		    this._host.removeChild(this._body);
            return;
		}
		var p = MsgFree.prototype;
		createjs.EventDispatcher.initialize(p);

        p.playBegin= function(){
            this._state = "begin";
            this.playState();
            return;
        }

        p.playAgain= function(){
            this._state = "again";
            this.playState();
            return;
        }

        p.playEnd= function(param1){
            this._state = "end";
            this._prize = param1;
            this.playState();
            return;
        }

        p.stop= function(){
            this._body.visible = false;
            //this._host.removeChild(this._body);
            return;
        }

        p.playState= function(){
            var _loc_1 = null;
            switch(this._state)
            {
                case "begin":
                {
                    _loc_1 = Game.Comps.MsgFree.SND_BEGIN;
                    break;
                }
                case "again":
                {
                    _loc_1 = Game.Comps.MsgFree.SND_AGAIN;
                    break;
                }
                case "end":
                {
                    _loc_1 = Game.Comps.MsgFree.SND_END;
                    break;
                }
                default:
                {
                    break;
                }
            }
            this._tf.text = "";
            this._body.visible = true;
            //this._host.addChild(this._body);
            //console.log('D:\OpenServer\domains\test\src\game\assets\comps\MsgFree.js 76')
            this._bgAanim.addEventListener('Event.COMPLETE', this.handleAnimComplete.bind(this));
            this._bgAanim.play(this._state);
            if (this._state == "end")
            {
                this._chan = _loc_1.play();
                this._chan.addEventListener('complete', this.handleSndComplete.bind(this)); //TODO Sound
                //this.handleSndComplete();
            }
            return;
        }

        p.afterAnim= function(){
            switch(this._state)
            {
                case "begin":
                {
                    break;
                }
                case "continue":
                {
                    break;
                }
                case "end":
                {
                    this._tf.text = this._prize.toString();
                    break;
                }
                default:
                {
                    break;
                }
            }
            return;
        }

        p.handleAnimComplete= function(event){
            this._bgAanim.removeAllEventListeners('Event.COMPLETE', this.handleAnimComplete);
            this.afterAnim();
            if (this._state != "end")
            {
                this.onComplete();
            }
            return;
        }

        p.handleSndComplete= function(event){
            this._chan.removeAllEventListeners('complete', this.handleSndComplete);
            this.onComplete();
            return;
        }

        p.onComplete= function(){
            this.dispatchEvent('Event.COMPLETE');
            return;
        }

        window.Game.Comps.MsgFree = MsgFree;
    
})(window);