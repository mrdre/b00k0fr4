﻿(function (window) {

    Reels.SIZE = 4 * Game.Comps.Tile.SIZE;
    Reels.FULL_SET = [0, 1, 2, 3, 4];
    Reels.ID_TRIG = null;
    Reels.MIN_TRIGS = null;
    Reels.SND_STOP = null;
    Reels.VEC_SND_TRIGS = null;

    function Reels(param1) {
        //this._host:DisplayObjectContainer;
        //this._slots:Vector.<DisplayObjectContainer>;
        //this._tiles;
        //this._items:Vector.<Vector.<Tile>>;
        //this._spinner:Spinner;
        //this._activeReels;
        //this.SIZE = 4 * Game.Comps.Tile.SIZE;
        //this.FULL_SET = [0, 1, 2, 3, 4];
        //this.ID_TRIG;
        //this.MIN_TRIGS;
        //this.SND_STOP:Sound;
        //this.VEC_SND_TRIGS:Vector.<Sound>;

        //p.Reels= function(param1){
        this._host_parent = param1;
        this._host = param1.reels;
        this.init();
        return;
    }
    var p = Reels.prototype;
    createjs.EventDispatcher.initialize(p);

    p.spin = function (param1) {
        if (!this._spinner) {
            this._spinner = new Game.Comps.Spinner(this._host, this._slots, this._items, Game.Comps.Reels.SND_STOP, Game.Comps.Reels.VEC_SND_TRIGS, this.onStop.bind(this));
        }
        if (param1 && param1.length) {
            this._activeReels = param1;
        }
        else {
            this._activeReels = Game.Comps.Reels.FULL_SET;
        }
        this._spinner.start(Game.Comps.Reels.ID_TRIG, Game.Comps.Reels.MIN_TRIGS, this._activeReels);
        return;
    }

    p.setFinite = function (param1) {
        this._spinner.setFin(param1);
        return;
    }

    p.setTiles = function (param1) {
        var _loc_2 = 0;
        var _loc_3 = 0;
        _loc_2 = param1.length;
        while (_loc_2--) {

            this._tiles[_loc_2][0] = param1[_loc_2][(param1[_loc_2].length - 1)];
            _loc_3 = param1[_loc_2].length;
            while (_loc_3--) {

                this._tiles[_loc_2][_loc_3] = param1[_loc_2][_loc_3];
                this._items[_loc_2][_loc_3].set_id(this._tiles[_loc_2][_loc_3]);
            }
        }
        return;
    }

    p.stopReel = function (param1) {
        this._spinner.stopReel(param1);
        return;
    }

    p.stopAllReels = function () {
        this._spinner.stopAllReels();
        return;
    }

    p.init = function () {
        var _loc_5 = null;
        this._items = Array(5);
        this._slots = Array(5);
        var _loc_1 = new createjs.Rectangle(0, Game.Comps.Tile.SIZE, Game.Comps.Tile.SIZE, 3 * Game.Comps.Tile.SIZE);
        var _loc_2 = 5;
        var _loc_3 = 0;
        var _loc_4 = 0;
        //console.log(this)
        while (_loc_2--) {

            _loc_5 = this._host[_loc_4 == 0 ? 'instance' : 'instance_' + _loc_4];//instancegetChildAt(_loc_2);
            _loc_5.scrollRect = _loc_1;
            this._slots[_loc_2] = _loc_5;
            this._items[_loc_2] = Array(4);
            _loc_4++;
        }
        var _loc_4 = XMath.intRnd(Game.Comps.Tile.SET_SIZE);
        _loc_2 = 5;
        while (_loc_2--) {

            _loc_3 = 4;
            while (_loc_3--) {

                this._items[_loc_2][_loc_3] = new Game.Comps.Tile();
                this._items[_loc_2][_loc_3].set_host(this._slots[_loc_2]);
                this._items[_loc_2][_loc_3].set_id((_loc_4 + 4 * _loc_2 + _loc_3) % Game.Comps.Tile.SET_SIZE);
                this._items[_loc_2][_loc_3].set_pos(_loc_3 * Game.Comps.Tile.SIZE);
            }
        }
        //console.log(this._host)
        //this._host.scrollRect = this._host.getRect(this._host);
        //this._host_parent.addChildAt(this._host, 0);
        return;
    }

    p.onStop = function (param1) {
        param1 = param1 == undefined ? -1 : param1
        if (param1 == -1) {
            setTimeout(this.dispatchEvent('Event.COMPLETE'), 1);
        }
        else {
            this.dispatchEvent(new createjs.Event(Event.CHANGE));
        }
        return;
    }

    window.Game.Comps.Reels = Reels;
})(window);

(function (window) {

    Reel.BLUR = [new createjs.BlurFilter(0,20,3)];

    Reel.CLAEN = [];

    function Reel(param1, param2, param3) {
        this._host = param1;
        this._items = param2;
        this._tiles = param3;
    }

    var p = Reel.prototype;

    p.start = function () {
        this._host.filters = Reel.BLUR;
        this._host.cache(0, 0, this._host.nominalBounds.width, this._host.nominalBounds.height);
    }

    p.stop = function () {
        this._host.filters = Reel.CLAEN;
        this._host.uncache();
    }

    p.set_pos = function (param1) {
        if (this._pos == param1) {
            return;
        }
        this._pos = param1;
        this.update();
    }

    p.update = function () {
        var _loc1_ = this._pos % Game.Comps.Tile.SIZE;
        var _loc2_ = 4;
        while (_loc2_--) {
            this._items[_loc2_].set_id(this._tiles[_loc2_]);
            this._items[_loc2_].set_pos(_loc1_ + _loc2_ * Game.Comps.Tile.SIZE);
        }
    }
    window.Game.Comps.Reel = Reel;

})(window);

(function (window) {


    function Spinner(param1, param2, param3, param4, param5, param6) {
        //this._host:DisplayObjectContainer;
        //this._slots:Vector.<DisplayObjectContainer>;
        //this._items:Vector.<Vector.<Tile>>;
        //this._callback:Function;
        //this._fin;
        //this._idTrig;
        //this._minTrigs;
        //this._vecReels:Vector.<Reel>;
        //this._vecState:Vector.<uint>;
        //this._vecPos:Vector.<Number>;
        //this._vecBreak:Vector.<uint>;
        //this._vecBoost:Vector.<uint>;
        //this._vecId;
        //this._vecTrig:Vector.<uint>;
        //this._tick;
        //this._sndSpin:Sound;
        //this._sndStop:Sound;
        //this._vecSndTrig:Vector.<Sound>;
        //this._activeReels;
        this.ACCELERATE_STEPS = 7;
        this.DECELERATE_STEPS = 6;
        this.BREAKING_PATH_PERC = 1;
        this.STEPS_PER_HEIGHT = 2;
        this.SPEED = Game.Comps.Tile.SIZE / this.STEPS_PER_HEIGHT;
        this.DEPTH = this.SPEED * this.BREAKING_PATH_PERC;
        this._host = param1;
        this._slots = param2;
        this._items = param3;
        this._sndStop = param4;
        this._vecSndTrig = param5;
        this._callback = param6;
        this.init();
        return;
    }
    var p = Spinner.prototype;
    createjs.EventDispatcher.initialize(p);

    p.init = function () {
        this._vecState = Array(5);
        this._vecPos = Array(5);
        this._vecBoost = Array(5);
        this._vecBreak = Array(5);
        this._vecReels = Array(5);
        this._vecTrig = Array(5);
        this._vecId = Array(5);
        var _loc_1 = 5;
        while (_loc_1--) {

            this._vecId[_loc_1] = [];
            this._vecReels[_loc_1] = new Game.Comps.Reel(this._slots[_loc_1], this._items[_loc_1], this._vecId[_loc_1]);
        }
        return;
    }

    p.start = function (param1, param2, param3) {
        var _loc_5 = 0;
        var _loc_6 = 0;
        this._idTrig = param1;
        this._minTrigs = param2;
        this._activeReels = param3;
        var _loc_4 = param3.length;
        while (_loc_4--) {

            _loc_5 = param3[_loc_4];
            this._vecState[_loc_5] = 1;
            this._vecBoost[_loc_5] = 0;
            this._vecBreak[_loc_5] = 0;
            this._vecPos[_loc_5] = 0;
            this._vecId[_loc_5].length = 4;
            _loc_6 = 4;
            while (_loc_6--) {

                this._vecId[_loc_5][_loc_6] = this._items[_loc_5][_loc_6].get_id();
            }
            this._vecReels[_loc_5].start();
            this._vecReels[_loc_5].set_pos(0);
        }
        this._tick = 0;
        this._fin = null;
        //console.log('D:\OpenServer\domains\test\src\game\assets\comps\Reels.js 208')
        this._host.on("tick", this.handleEnterFrame.bind(this));
        //this.handleEnterFrame()
        return;
    }

    p.setFin = function (param1) {
        var _loc_3 = 0;
        var _loc_4 = 0;
        var _loc_5 = 0;
        var _loc_6 = 0;
        var _loc_7 = 0;
        this.checkTriggers(param1);
        this._fin = Array(5);
        var _loc_2 = this._activeReels.length;
        while (_loc_2--) {

            _loc_3 = this._activeReels[_loc_2];
            this._fin[_loc_3] = Array(3);
            _loc_4 = 3;
            while (_loc_4--) {

                this._fin[_loc_3][_loc_4] = param1[_loc_2][_loc_4];
            }
            do {

                _loc_5 = XMath.intRnd((Game.Comps.Tile.SET_SIZE - 1));
            } while (this._fin[_loc_3].indexOf(_loc_5) != -1)
            this._fin[_loc_3].unshift(_loc_5);
            _loc_4 = 3 + 4 * _loc_2;
            while (_loc_4--) {

                do {

                    _loc_5 = XMath.intRnd((Game.Comps.Tile.SET_SIZE - 1));
                    _loc_6 = this._fin[_loc_3].length;
                    _loc_7 = this._fin[_loc_3].lastIndexOf(_loc_5);
                } while (_loc_6 - _loc_7 < 3)
                this._fin[_loc_3].push(_loc_5);
            }
        }
        return;
    }

    p.stopReel = function (param1) {
        if (!this._fin) {
            return;
        }
        if (this._fin[param1].length > 4) {
            this._fin[param1].length = 4;
            this._vecBoost[param1] = 1;
        }
        return;
    }

    p.stopAllReels = function () {
        if (!this._fin) {
            return;
        }
        var _loc_1 = 5;
        while (_loc_1--) {

            if (this._fin[_loc_1].length > 4) {
                this._fin[_loc_1].length = 4;
                this._vecBoost[_loc_1] = 1;
            }
        }
        return;
    }

    p.checkTriggers = function (param1) {
        var _loc_6 = false;
        var _loc_2 = this._minTrigs;
        var _loc_3 = 0;
        var _loc_4 = param1.length;
        var _loc_5 = 0;
        while (_loc_5 < _loc_4) {

            _loc_6 = param1[_loc_5].indexOf(this._idTrig) != -1;
            if (_loc_6 && _loc_5 + _loc_2 <= 5) {
                this._vecTrig[_loc_5] = _loc_3 + 1;
                _loc_2 = _loc_2 - 1;
            }
            else {
                this._vecTrig[_loc_5] = 0;
            }
            _loc_5 = _loc_5 + 1;
        }
        return;
    }

    p.update = function () {
        var _loc2_ = NaN;
        var _loc1_ = 5;
        while(_loc1_--)
        {
            switch(this._vecState[_loc1_])
            {
                case 1:
                    if(this._tick > this.ACCELERATE_STEPS)
                    {
                        this._vecPos[_loc1_] = this.SPEED;
                        this._vecState[_loc1_] = 2;
                    }
                    else
                    {
                        this._vecPos[_loc1_] = Game.Comps.Tile.SIZE * this._tick * (1 + this._tick) / (this.ACCELERATE_STEPS * (1 + this.ACCELERATE_STEPS));
                    }
                    break;
                case 2:
                    if(this._vecBoost[_loc1_])
                    {
                        this._vecPos[_loc1_] = Game.Comps.Tile.SIZE;
                    }
                    else
                    {
                        this._vecPos[_loc1_] = this._vecPos[_loc1_] + this.SPEED;
                    }
                    break;
                case 3:
                    if(this._vecBreak[_loc1_]++ == 0)
                    {
                        this._vecReels[_loc1_].stop();
                        this._sndStop.play();
                        if(this._vecTrig[_loc1_])
                        {
                            this._vecSndTrig[this._vecTrig[_loc1_] - 1].play();
                        }
                    }
                    if(this._vecBreak[_loc1_] > this.DECELERATE_STEPS)
                    {
                        this._vecState[_loc1_] = 0;
                        this._callback.call(null,_loc1_);
                        return;
                    }
                    _loc2_ = this._vecBreak[_loc1_] * Math.PI / this.DECELERATE_STEPS;
                    this._vecPos[_loc1_] = this.DEPTH * Math.sin(_loc2_);
                    break;
            }
            if (this._vecPos[_loc1_] >= Game.Comps.Tile.SIZE)
            {
                this._vecPos[_loc1_] = this._vecPos[_loc1_] % Game.Comps.Tile.SIZE;
                this.nextTileId(_loc1_);
            }
        }
    }

    p.nextTileId = function (param1) {
        var _loc_2 = 0;
        var _loc_3 = 0;
        if (this._fin) {
            _loc_2 = this._fin[param1].pop();
            if (this._fin[param1].length == 0) {
                this._vecState[param1] = 3;
            }
        }
        else {
            do {

                _loc_2 = XMath.intRnd((Game.Comps.Tile.SET_SIZE - 1));
                _loc_3 = this._vecId[param1].indexOf(_loc_2);
            } while (_loc_3 < 3)
        }
        this._vecId[param1].pop();
        this._vecId[param1].unshift(_loc_2);
        return;
    }

    p.draw = function () {
        var _loc_1 = 5;
        while (_loc_1--) {

            this._vecReels[_loc_1].set_pos(this._vecPos[_loc_1]);
        }
        return;
    }

    p.checkStates = function () {
        var me = this;
        var _loc_1 = 5;

        while (_loc_1--) {

            if (this._vecState[_loc_1] != 0) {
                //console.log('checkstates')
                return;
            }
        }
        this._host.removeAllEventListeners();
        this._callback.call();
        return;
    }

    p.handleEnterFrame = function (event) {
        for (var i = 0; i < 5; i++) {
            this._host.getChildAt(i).cacheCanvas ? this._host.getChildAt(i).updateCache() : null;
        }
        this._tick++;
        this.update();
        this.draw();
        this.checkStates();
        return;
    }

    window.Game.Comps.Spinner = Spinner;

})(window);