﻿(function (window) {
    function ReelsAnimations(param1) {
        //this._host:DisplayObjectContainer;
        //this._points:Vector.<Pnt>;
        //this._anims:Vector.<MovieClip>;
        //this._shader:TileShader;
        //this._idSpec:int;
        //this._priceSpec:uint;
        //this._changeAnims:Array;
        //this._changePnts:Array;
        //this.Game.POOL:ObjGame.POOL = ObjGame.POOL.inst;
        //this.Game.Comps.ReelsAnimations.DATA:Vector.<Vector.<Vector.<Class>>>;
        //this.SND_CHANGE:Sound;
        //this.CLS_TRIG:Class;
        this._host = param1;
        this.init();
    }

    var p = ReelsAnimations.prototype;
    createjs.EventDispatcher.initialize(p);

    p.play = function (param1, param2) {
        var _loc_6 = 0;
        var _loc_7 = 0;
        var _loc_8 = 0;
        var _loc_9 = null;
        var _loc_10 = null;
        var _loc_11 = null;
        this.clear();
        var _loc_3 = param1.length;
        var _loc_4 = param1[0].length;
        var _loc_5 = 0;
        while (_loc_5 < _loc_3) {

            _loc_6 = 0;
            while (_loc_6 < _loc_4) {

                _loc_7 = param1[_loc_5][_loc_6];
                _loc_8 = param2[_loc_5][_loc_6];
                _loc_9 = this._points[_loc_5 * _loc_4 + _loc_6];
                _loc_10 = this.getAnim(_loc_7, _loc_8);
                if (!_loc_10) {
                }
                else {
                    _loc_11 = new _loc_10;
                    _loc_11.x = _loc_9.x;
                    _loc_11.y = _loc_9.y;
                    //console.log(_loc_11)
                    this._host.addChild(_loc_11);
                    this._anims.push(_loc_11);
                    _loc_11.gotoAndStop(0); //was 1
                }
                _loc_6 = _loc_6 + 1;
            }
            _loc_5 = _loc_5 + 1;
        }
        this.run();
        return;
    }

    p.playSpec = function (param1, param2, param3) {
        var _loc_7 = false;
        var _loc_8 = 0;
        var _loc_9 = 0;
        var _loc_10 = null;
        var _loc_11 = null;
        var _loc_12 = null;
        var _loc_13 = null;
        this.clear();
        this._idSpec = param1;
        this._priceSpec = param3;
        this._changeAnims = [];
        this._changePnts = [];
        var _loc_4 = param2.length;
        var _loc_5 = param2[0].length;
        var _loc_6 = 0;
        while (_loc_6 < _loc_4) {

            _loc_7 = param2[_loc_6].indexOf(this._idSpec) != -1;
            _loc_8 = 0;
            while (_loc_8 < _loc_5) {

                _loc_9 = param2[_loc_6][_loc_8];
                _loc_10 = this._points[_loc_6 * _loc_5 + _loc_8];
                if (_loc_9 == this._idSpec) {
                    _loc_11 = Game.Comps.ReelsAnimations.DATA[_loc_9][(param3 - 1)][1];
                    _loc_12 = Game.POOL.getInst(_loc_11)
                    _loc_12.gotoAndStop(1);
                    _loc_12.x = _loc_10.x;
                    _loc_12.y = _loc_10.y;
                    this._changeAnims.push(null);
                    this._anims.push(_loc_12);
                    this._host.addChild(_loc_12);
                }
                else {
                    _loc_11 = Game.Comps.ReelsAnimations.DATA[_loc_9][4][0];
                    _loc_12 = Game.POOL.getInst(_loc_11)
                    _loc_12.gotoAndStop(1);
                    _loc_12.x = _loc_10.x;
                    _loc_12.y = _loc_10.y;
                    this._changeAnims.push(_loc_12);
                    if (_loc_7) {
                        this._changePnts.push(_loc_10);
                    }
                }
                _loc_8 = _loc_8 + 1;
            }
            _loc_6 = _loc_6 + 1;
        }
        //this._shader.execute(this._changeAnims); //TODO
        _loc_6 = this._changeAnims.length;
        while (_loc_6--) {

            _loc_13 = this._changeAnims.pop();
            if (!_loc_13) {
                continue;
            }
            Game.POOL.giveInst(_loc_13);
        }
        setTimeout(this.changeNext.bind(this), 500);
        return;
    }

    p.playTrigs = function (param1) {
        var _loc_3 = 0;
        var _loc_4 = null;
        var _loc_5 = null;
        this.clear();
        var _loc_2 = param1.length;
        while (_loc_2--) {

            if (param1[_loc_2] == -1) {
                continue;
            }
            _loc_3 = param1[_loc_2];
            _loc_4 = this._points[_loc_2 * 3 + _loc_3];
            _loc_5 = Game.POOL.getInst(Game.Comps.ReelsAnimations.CLS_TRIG)
            _loc_5.gotoAndStop(1);
            _loc_5.x = _loc_4.x;
            _loc_5.y = _loc_4.y;
            this._host.addChild(_loc_5);
            this._anims.push(_loc_5);
        }
        setTimeout(this.run.bind(this), 10);
        return;
    }

    p.clear = function () {
        var _loc_1 = null;
        while (this._anims.length) {

            _loc_1 = this._anims.pop();
            if (_loc_1.parent == this._host) {
                this._host.removeChild(_loc_1);
            }
            _loc_1.gotoAndStop(1);
            Game.POOL.giveInst(_loc_1);
        }
        //this._shader.clear(); //TODO
        return;
    }

    p.changeNext = function () {
        var _loc_1 = null;
        var _loc_2 = null;
        var _loc_3 = null;
        if (this._changePnts.length) {
            _loc_1 = this._changePnts.shift();
            _loc_2 = Game.Comps.ReelsAnimations.DATA[this._idSpec][(this._priceSpec - 1)][1];
            _loc_3 = Game.POOL.getInst(_loc_2)
            _loc_3.gotoAndStop(1);
            _loc_3.x = _loc_1.x;
            _loc_3.y = _loc_1.y;
            this._anims.push(_loc_3);
            this._host.addChild(_loc_3);
            SND_CHANGE.play();
            setTimeout(this.changeNext.bind(this), SND_CHANGE.length);
        }
        else {
            setTimeout(this.run.bind(this), 10);
            this.dispatchEvent(new createjs.Event(Event.CHANGE));
        }
        return;
    }

    p.run = function () {
        var _loc_1 = this._anims.length;
        while (_loc_1--) {
            this._anims[_loc_1].uncache();

            this._anims[_loc_1].play();
        }
        return;
    }

    p.init = function () {
        var _loc_2 = null;
        var _loc_1 = this._host.numChildren;
        //console.log(this._host)
        this._points = Array(_loc_1);
        while (_loc_1--) {

            this._points[_loc_1] = new Game.Comps.Pnt(this._host.getChildAt(_loc_1).x, this._host.getChildAt(_loc_1).y);
            this._host.removeChildAt(_loc_1);
        }
        this._shader = null;//new TileShader(this._host); //TODO
        this._anims = [];
        return;
    }

    p.getAnim = function (param1, param2) {
        var _loc_3 = Number(Game.Comps.Tile.SPEC_ID == param1);
        var _loc_4 = param2 ? (Game.Comps.ReelsAnimations.DATA[param1][(param2 - 1)][_loc_3]) : (null);
        return param2 ? (Game.Comps.ReelsAnimations.DATA[param1][(param2 - 1)][_loc_3]) : (null) ? (Game.POOL.getInst(_loc_4)) : (null);
    }

    window.Game.Comps.ReelsAnimations = ReelsAnimations;
})(window);

(function (window) {
    function Pnt(param1, param2) {
        this.x = param1 == undefined ? 0 : param1;
        this.y = param2 == undefined ? 0 : param2;
    }
    window.Game.Comps.Pnt = Pnt;

})(window);