﻿(function(window){
		function Special(param1){
        //this._host:DisplayObjectContainer;
        //this._anim:MovieClip;
        //this._book:Book;
        //this.OPEN:Sound;

        //p.Special= function(param1:DisplayObjectContainer)
        //{
		    this._host = param1;

		    this._anim = this._host.anim;
		    this._book = new Game.Comps.Book(this._host.book);
		    this.stop();
        }

		var p = Special.prototype;
		createjs.EventDispatcher.initialize(p);

        p.execute= function(param1, param2, param3){
            this._host.parent.addChild(this._host);
            this._host.visible = true;
            this._book.start(param1, param2, param3);
            //console.log('D:\OpenServer\domains\test\src\game\assets\comps\Special.js 24')
            this._anim.addEventListener("tick", this.handleEFOpen.bind(this));
            this._anim.gotoAndPlay(1);
            this._anim.visible = true;
            Game.Comps.Special.OPEN.play();
            return;
        }

        p.stop= function(){
            this._host.visible = false;
            this._anim.visible = false;
            this._anim.gotoAndStop(1);
            this._book.stop();
            return;
        }

        p.handleEFOpen= function(event){
            if (this._anim.currentFrame != this._anim.totalFrames - 1)
            {
                return;
            }
            this._anim.removeAllEventListeners("tick", this.handleEFOpen);
            this._anim.gotoAndStop(1);
            this._anim.visible = false;
            this._book.execute();
            //console.log('D:\OpenServer\domains\test\src\game\assets\comps\Special.js 49')
            this._book.addEventListener('Event.COMPLETE', this.handleBookComplete.bind(this));
            return;
        }

        p.handleBookComplete= function(event){
            this._book.removeAllEventListeners('Event.COMPLETE', this.handleBookComplete);
            this.dispatchEvent('Event.COMPLETE');
            return;
        }

        window.Game.Comps.Special = Special;

})(window);