﻿(function (window) {
    function TextDisplay(param1) {
        //this._rect:Rectangle;
        //this._tf:TextField;
        //this._timer:Timer;
        //this._format:TextFormat;
        this.FONT_SIZE_SMALL = 15;
        this.FONT_SIZE_BIG = 24;
        this._tf = param1;

        //this._rect = this._tf.getRect(this._tf.parent);//TODO
        //this._timer = new Timer(500);
        //this._tf.autoSize = TextFieldAutoSize.LEFT;
        //this._format = this._tf.defaultTextFormat;
        //this._format.align = TextFormatAlign.CENTER;
    }
    var p = TextDisplay.prototype;
    createjs.EventDispatcher.initialize(p);

    p.write = function (param1, param2) {
        param2 = param2 == undefined ? false : param2
        this.clear();
        var _loc_3 = "";
        var _loc_4 = 0;
        var _loc_5 = param1.split("|", 2);
        if (param1.split("|", 2).length == 2) {
            _loc_3 = _loc_5[1];
        }
        else {
            _loc_3 = param1;
        }
        switch (_loc_5[0]) {
            case "s":
                {
                    _loc_4 = this.FONT_SIZE_SMALL;
                    break;
                }
            case "b":
                {
                    _loc_4 = this.FONT_SIZE_BIG;
                    break;
                }
            default:
                {
                    _loc_4 = _loc_3.indexOf("\n") == -1 ? (this.FONT_SIZE_BIG) : (this.FONT_SIZE_SMALL);
                    break;
                    break;
                }
        }
        this._tf.condenseWhite = true;
        this._tf.text = "";
        //this._format.size = _loc_4;//TODO
        this._tf.defaultTextFormat = this._format;
        this._tf.text = _loc_3;
        this.placeTf();
        if (param2) {
            this._timer = setTimeout(this.handleTimer.bind(this), 500);
        }
        return;
    }

    p.clear = function () {

        if (this._timer) {
            clearTimeout(this._timer);
            this._timer = null;
        }
        this._tf.visible = true;
        this._tf.text = "";
        return;
    }

    p.placeTf = function () {
        this._tf.height = this._tf.textHeight + 10;
        //this._tf.x = this._rect.x + 0.5 * (this._rect.width - this._tf.width);//TODO
        //this._tf.y = this._rect.y + 0.5 * (this._rect.height - this._tf.height);
        return;
    }

    p.handleTimer = function (event) {
        this._tf.visible = !this._tf.visible;
        return;
    }

    window.Game.Comps.TextDisplay = TextDisplay
})(window);