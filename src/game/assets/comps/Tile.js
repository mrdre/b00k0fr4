﻿(function (window) {

    Tile.SIZE = 100;
    Tile.SET_SIZE = 10;
    Tile.SPEC_ID = -1;

    function Tile() {
        //this._host:DisplayObjectContainer;
        //this._body:DisplayObject;
        //this._id:uint;
        //this._pos:Number;
        //this.POOL:ObjPool = ObjPool.inst;
        //this.SIZE:uint = 100;
        //this.DATA:Vector.<Vector.<Class>>;
        //this.SET_SIZE:uint = 10;
        //this.SPEC_ID:int = -1;

        //p.Tile = function()
        //{
        //    return;
    }

    var p = Tile.prototype;
    createjs.EventDispatcher.initialize(p);

    p.set_host = function (param1) {
        this._host = param1;
        return;
    }

    p.get_id = function () {
        return this._id;
    }

    p.set_id = function (param1) {
        this._id = param1;
        if (this._body) {
            if (this._body.parent) {
                this._body.parent.removeChild(this._body);
            }
            var _loc_2 = 1;
            this._body.scaleY = 1;
            this._body.scaleX = _loc_2;
            Game.POOL.giveInst(this._body);
        }
        this._body = this.getBody(this._id);
        this._body.x = 0;
        this._body.y = this._pos - 100;
        this._body.height = Tile.SIZE + 0.5;
        this._host.addChild(this._body);
        return;
    }

    p.get_pos = function () {
        return this._pos;
    }

    p.set_pos = function (param1) {
        this._pos = param1;
        if (this._body) {
            this._body.y = this._pos - 100;
        }
        return;
    }

    p.getBody = function (param1) {
        var _loc_2 = Math.floor(Tile.SPEC_ID == param1);
        var _loc_3 = Tile.DATA[param1][_loc_2];
        return Game.POOL.getInst(_loc_3);
    }

    window.Game.Comps.Tile = Tile;
})(window);