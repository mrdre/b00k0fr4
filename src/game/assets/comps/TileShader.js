﻿(function(window){
		// function TileShader(){
        // this._host:DisplayObjectContainer;
        // this._bitmap:Bitmap;
        // this._pnts:Vector.<Game.Comps.Pnt>;
        this.FR = 0.2125;
        this.FG = 0.7154;
        this.FB = 0.0721;
        //this.FILTER = new ColorMatrixFilter([FR, FG, FB, 0, 0, FR, FG, FB, 0, 0, FR, FG, FB, 0, 0, 0, 0, 0, 1, 0]);

        function TileShader(param1)
        {
            this._host = param1;
			this._bitmap = new Bitmap(null, PixelSnapping.NEVER, true);
            this._bitmap.x = 42;
            this._bitmap.y = 77;
            this._pnts = [new Game.Comps.Pnt(0, 0), new Game.Comps.Pnt(0, 100), new Game.Comps.Pnt(0, 200), new Game.Comps.Pnt(114, 0), new Game.Comps.Pnt(114, 100), new Game.Comps.Pnt(114, 200), new Game.Comps.Pnt(228, 0), new Game.Comps.Pnt(228, 100), new Game.Comps.Pnt(228, 200), new Game.Comps.Pnt(342, 0), new Game.Comps.Pnt(342, 100), new Game.Comps.Pnt(342, 200), new Game.Comps.Pnt(456, 0), new Game.Comps.Pnt(456, 100), new Game.Comps.Pnt(456, 200)];
            
            return;
        }

        p.execute= function(param1){
            var _loc_5 = null;
            var _loc_6 = null;
            var _loc_2 = new BitmapData(556, 300, true, 0);
            var _loc_3 = new Matrix();
            var _loc_4 = param1.length;
            while (_loc_4--)
            {
                
                if (!param1[_loc_4])
                {
                    continue;
                }
                _loc_5 = param1[_loc_4];
                _loc_6 = this._pnts[_loc_4];
                _loc_3.identity();
                _loc_3.translate(_loc_6.x, _loc_6.y);
                _loc_2.draw(_loc_5, _loc_3, null, null, null, true);
            }
            _loc_2.applyFilter(_loc_2, _loc_2.rect, new Point(), FILTER);
            this._bitmap.bitmapData = _loc_2;
            this._host.addChildAt(this._bitmap, 0);
            return;
        }

        p.clear= function(){
            this._bitmap.bitmapData = null;
            if (this._bitmap.parent)
            {
                this._host.removeChild(this._bitmap);
            }
            return;
        }

})(window);