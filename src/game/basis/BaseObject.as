package game.basis
{
   import flash.events.EventDispatcher;
   import game.assets.Gfx;
   import game.assets.Sfx;
   import game.assets.Txt;
   import game.assets.Cfg;
   
   public class BaseObject extends EventDispatcher
   {
      
      protected static const REG:game.basis.Register = game.basis.Register.inst;
      
      protected static const GFX:Gfx = Gfx.inst;
      
      protected static const SFX:Sfx = Sfx.inst;
      
      protected static const TXT:Txt = Txt.inst;
      
      protected static const CFG:Cfg = Cfg.inst;
       
      public function BaseObject()
      {
         super();
         this.init();
      }
      
      protected function init() : void
      {
      }
   }
}
