﻿(function (window) {
    function BetParams() {
        // this._vecLines:Vector.<GameLine>;
        // this._tfBet:TextField;
        // this._tfLines:TextField;
        // this._tfTotal:TextField;
        // this._paysHash:Object;
        // this._bet:uint;
        // this._lines:uint;

        // p.BetParams()
        // {
        // return;

        var _loc_3 = null;
        this._vecLines = Game.Gfx.slotsLines;
        this._tfBet = Game.Gfx.slotsTfBet;
        this._tfLines = Game.Gfx.slotsTfLines;
        this._tfTotal = Game.Gfx.slotsTfTotal;
        this._paysHash = {};
        var _loc_1 = Game.Gfx.helpVecPage[0];
        var _loc_2 = _loc_1.timeline._tweens.length;
        while (_loc_2--) {

            _loc_3 = _loc_1.timeline._tweens[_loc_2];
            if (!_loc_3) {
                continue;
            }
            this._paysHash[_loc_3.target.id] = _loc_3;
        }
    }

    var p = BetParams.prototype;

    p.set_bet = function (param1) {
        this._bet = param1;
        this._tfBet.text = this._bet.toString();
        var _loc_2 = this._vecLines.length;
        while (_loc_2--) {

            this._vecLines[_loc_2].set_bet(this._bet);
        }
        return;
    }

    p.set_lines = function (param1) {
        this._lines = param1;
        this._tfLines.text = this._lines.toString();
        var _loc_2 = this._vecLines.length;
        while (_loc_2--) {

            this._vecLines[_loc_2].set_active(_loc_2 < this._lines);
        }
        return;
    }

    p.set_total = function (param1) {
        this._tfTotal.text = param1.toString();
        return;
    }

    p.set_shown = function (param1) {
        var _loc_2 = this._vecLines.length;
        while (_loc_2--) {

            this._vecLines[_loc_2].cells(param1 ? (0) : (-1));
        }
        return;
    }

    p.updatePays = function () {
        var _loc_1 = null;
        var _loc_2 = null;
        var _loc_3 = null;
        var _loc_4 = 0;
        var _loc_5 = 0;
        var _loc_6 = false;
        var _loc_7 = 0;
        var _loc_8 = null;
        for (_loc_1 in this._paysHash) {

            //_loc_2 = _loc_1.split("_"); //TODO WTF??
            //_loc_3 = _loc_2[0];
            //_loc_4 = parseInt(_loc_2[1]);
            //_loc_5 = parseInt(_loc_2[2]);
            //_loc_6 = _loc_2[3] == "d";
            //_loc_7 = Game.Cfg.pays[_loc_4][(_loc_5 - 1)];
            //if (_loc_6) {
            //    _loc_7 = _loc_7 * Game.Cfg.wildFactors[0];
            //}
            //if (_loc_3 == "s" || _loc_3 == "tfScat") {
            //    _loc_7 = _loc_7 * this._lines;
            //}
            //_loc_7 = _loc_7 * this._bet;
            //_loc_8 = this._paysHash[_loc_1];
            //_loc_8.text = _loc_7.toString();
        }
        return;
    }

    p.beep = function () {
        Game.Sfx.betLine.play();
        return;
    }

    window.Game.Basis.BetParams = BetParams;

})(window);