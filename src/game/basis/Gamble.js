﻿(function (window) {
	function Gamble() {
		// this._status:TextDisplay;
		// this._card:MovieClip;
		// this._tfAmount:TextField;
		// this._tfWin:TextField;
		// this._vecIcon:Vector.<DisplayObject>;
		// this._vecSuite:Vector.<MovieClip>;
		// this._chan:SoundChannel;
		// this._prize;
		// this._body:DisplayObject;
		// this._start;

		// p.Gamble()
		// {
		// return;

		this._status = Game.Gfx.slotsStatus;
		this._card = Game.Gfx.gambleCard;
		this._tfAmount = Game.Gfx.gambleTfAmount;
		this._tfWin = Game.Gfx.gambleTfWin;
		this._vecIcon = Game.Gfx.gambleVecIcon;
		this._vecSuite = Game.Gfx.gambleVecSuite;
		this._body = Game.Gfx.pGamble;
		this._card.gotoAndStop(0); //was 1
		this._tfAmount.text = "";
		this._tfWin.text = "";
		this._vecIcon[0].visible = false;
		this._vecIcon[1].visible = false;
	}
	var p = Gamble.prototype;
	createjs.EventDispatcher.initialize(p);

	p.prepare = function (param1, param2) {
		this.set_prize(param1);
		this.set_color(0);
		this.set_queue(param2);
		this.set_color(-1);
		this._body.x = this._body.width;
		this._body.addEventListener("tick", this.handleShow.bind(this));
		this._start = getTimer();
		Game.Sfx.showGamble.play();
		return;
	}

	p.attempt = function () {
	    this.set_color(0);
	    if (!this._chan) {
	        this._chan = Game.Sfx.changeCard.play(0, Number.MAX_VALUE); //TODO
	        //this._chan.addEventListener('complete', this.handleSndComplete.bind(this));
	        this._card.addEventListener("tick", this.handleEnterFrame.bind(this));
        }
	        
		return;
	}

	p.set_choice = function (param1) {
	    this.set_color(param1);
		this._status.clear();
		return;
	}

	p.result = function (param1, param2, param3) {
		var _loc_4 = 0;
		this.set_prize(this.get_prize() * param1);
		this.set_cardColor(param2[0]);
		this.set_queue(param2);
		if (param1) {
			_loc_4 = Game.Cfg.gambleGames - param3;
			if (!this._chan)
			    this._chan = Game.Sfx.vecGambleWin[(_loc_4 - 1)].play();
		    this._chan.addEventListener('complete', this.handleSndComplete.bind(this)); //TODO Sound
			//this.handleSndComplete();
		}
		else {
			setTimeout(this.onResult.bind(this), 1000);
		}
		return;
	}

	p.end = function () {
		this._card.removeAllEventListeners("tick", this.handleEnterFrame);
		this._status.clear();
		if (this._chan) {
			this._chan.removeAllEventListeners('complete', this.handleSndComplete);
			this._chan.stop();
			this._chan = null;
		}
		return;
	}

	p.onPrepare = function () {
		return;
	}

	p.onResult = function () {
		return;
	}

	p.set_queue = function (param1) {
		var _loc_3 = 0;
		var _loc_2 = this._vecSuite.length;
		while (_loc_2--) {

			_loc_3 = param1[_loc_2] + 1;
			this._vecSuite[_loc_2].gotoAndStop(_loc_3 - 1);
		}
		return;
	}

	p.set_color = function (param1) {
		var _loc_2 = null;
		this._vecIcon[0].visible = param1 == -1;
		this._vecIcon[1].visible = param1 == 1;
		if (!param1) {
		    _loc_2 = Game.TXT.getText(Game.TXT.INFO_GAMBLE);
			this._status.write(_loc_2);
			this._card.gotoAndStop(0); // was 1
		}
		else {
			this._status.clear();
		}
		return;
	}

	p.get_prize = function () {
		return this._prize;
	}

	p.set_prize = function (param1) {
		this._prize = param1;
		this._tfAmount.text = this._prize.toString();
		if (this._prize) {
			this._tfWin.text = Number(2 * param1).toString();
		}
		return;
	}

	p.set_cardColor = function (param1) {
		if (this._chan) {
		    this._chan.stop();
		    this._chan = null;
		}
		this._card.removeAllEventListeners("tick", this.handleEnterFrame);
		this._card.gotoAndStop(param1 + 1); //was 2
		this._card.visible = true;
		return;
	}

	p.handleSndComplete = function (event) {
	    this._chan.removeAllEventListeners('complete', this.handleSndComplete);
	    this._chan.stop();
	    this._chan = null;
		this.onResult();
		return;
	}

	p.handleEnterFrame = function (event) {
		this._card.visible = !this._card.visible;
		return;
	}

	p.handleShow = function (event) {
		var _loc_2 = getTimer() - this._start;
		var _loc_3 = _loc_2 / Game.Sfx.showGamble.length;
		_loc_3 = _loc_3 < 1 ? (1 - _loc_3) : (0);
		this._body.x = _loc_3 * this._body.width;
		if (_loc_3 == 0) {
			this._body.removeAllEventListeners("tick", this.handleShow);
			this.onPrepare();
		}
		return;
	}
	window.Game.Basis.Gamble = Gamble;


})(window);