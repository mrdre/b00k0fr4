﻿
(function (window) {

    function Input() {
        this._hash = {};
    }

    var p = Input.prototype;
    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\basis\GameInput.js 13')
        this._model.addEventListener(Game.ModelBox.PRESS, this.handleChange.bind(this));
        return;
    }

    p.defineMode = function (param1, param2) {
        this._hash[param1] = param2;
        return;
    }

    p.setMode = function (param1) {
        this._mode = param1;
        var _loc_2 = this._hash[this._mode];
        if (!_loc_2) {
            console.log("`" + this._mode + "` is unknown mode id.");
        }
        var _loc_3 = [];
        var _loc_5 = _loc_2.length;
        _loc_3.length = _loc_2.length;
        var _loc_4 = _loc_5;
        while (_loc_4--) {

            _loc_3[_loc_4] = _loc_2[_loc_4] ? (1) : (0);
        }
        this._model.setStates(_loc_3);
        return;
    }

    p.disable = function (param1) {
        var _loc_2 = this._hash[this._mode];
        var _loc_3 = _loc_2.length;
        while (_loc_3--) {

            if (param1 == _loc_2[_loc_3]) {
                this._model.disableButtons(_loc_3);
            }
        }
        return;
    }

    p.enable = function (param1) {
        var _loc_2 = this._hash[this._mode];
        var _loc_3 = _loc_2.length;
        while (_loc_3--) {

            if (param1 == _loc_2[_loc_3]) {
                this._model.enableButtons(_loc_3);
            }
        }
        return;
    }

    p.handleChange = function (event) {
        var _loc_2 = this._hash[this._mode];
        this.pressed = _loc_2[this._model.get_pressed()];
        this.dispatchEvent(Game.ModelBox.CHANGE);
        return;
    }
    window.Game.Lib.Input = Input;

})(window);

(function (window) {

    function GameInput() {
        Game.Lib.Input.call(this);

        this.model = null;

        this.defineMode(GameInput.M_BLOCK, [null, null, null, null, null, null, null, null, null, null, null, null]);
        this.defineMode(GameInput.M_GAME_OVER, [GameInput.B_COIN, GameInput.B_HELP, GameInput.B_AUTO, GameInput.B_EXIT, GameInput.B_LINES1, GameInput.B_LINES3, GameInput.B_LINES5, GameInput.B_LINES7, GameInput.B_LINES9, GameInput.B_BET, GameInput.B_MAXBET, GameInput.B_START]);
        this.defineMode(GameInput.M_SPIN, [null, null, GameInput.B_AUTO, null, null, null, null, null, null, null, null, null]);
        this.defineMode(GameInput.M_FREE, [GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE, GameInput.B_FREE]);
        this.defineMode(GameInput.M_JP, [GameInput.B_JP, GameInput.B_JP, GameInput.B_JP, GameInput.B_JP, GameInput.B_JP, GameInput.B_JP, GameInput.B_JP, GameInput.B_JP, GameInput.B_JP, GameInput.B_JP, GameInput.B_JP, GameInput.B_JP]);
        this.defineMode(GameInput.M_CHOICE, [null, null, null, null, null, null, null, null, null, GameInput.B_GAMBLE, GameInput.B_GAMBLE, GameInput.B_TAKE]);
        this.defineMode(GameInput.M_GAMBLE, [null, null, null, null, null, null, null, null, null, GameInput.B_RED, GameInput.B_BLACK, GameInput.B_TAKE]);
        this.defineMode(GameInput.M_HELP, [null, GameInput.B_HELP, null, null, null, null, null, null, null, null, null, null]);
        this.defineMode(GameInput.M_WARNING, [GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY, GameInput.B_ANY]);
        this.defineMode(GameInput.M_ERROR, [null, null, null, GameInput.B_EXIT, null, null, null, null, null, null, null, null]);
    }

    var p = createjs.extend(GameInput, Game.Lib.Input);

    GameInput.B_COIN = "coin";
    GameInput.B_EXIT = "exit";
    GameInput.B_HELP = "help";
    GameInput.B_AUTO = "auto";
    GameInput.B_LINES1 = "lines_1";
    GameInput.B_LINES3 = "lines_3";
    GameInput.B_LINES5 = "lines_5";
    GameInput.B_LINES7 = "lines_7";
    GameInput.B_LINES9 = "lines_9";
    GameInput.B_GAMBLE = "gamble";
    GameInput.B_RED = "red";
    GameInput.B_BLACK = "black";
    GameInput.B_BET = "bet";
    GameInput.B_MAXBET = "maxbet";
    GameInput.B_TAKE = "take";
    GameInput.B_START = "start";
    GameInput.B_FREE = "free";
    GameInput.B_JP = "jp";
    GameInput.B_ANY = "any";
    GameInput.M_BLOCK = "block";
    GameInput.M_GAME_OVER = "gameOver";
    GameInput.M_SPIN = "spin";
    GameInput.M_FREE = "free";
    GameInput.M_JP = "jp";
    GameInput.M_CHOICE = "choice";
    GameInput.M_GAMBLE = "gamble";
    GameInput.M_HELP = "help";
    GameInput.M_WARNING = "warning";
    GameInput.M_ERROR = "error";

    window.Game.Basis.GameInput = GameInput;

})(window);
