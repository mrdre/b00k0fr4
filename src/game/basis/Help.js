﻿(function(window){
	function Help(){
        // this._vecPage:Vector.<DisplayObject>;
        // this._chan:SoundChannel;
        // this._snd:Sound;
        // this._page:uint;
        // this._start:uint;

        // p.Help()
        // {
            // return;
			
            this._vecPage = Game.Gfx.helpVecPage;
            this._snd = Game.Sfx.help;
            var _loc_1 = this._vecPage.length;
            while (_loc_1--)
            {
                
                this._vecPage[_loc_1].y = -480;
            }
        }
	var p = Help.prototype;

        p.show= function(param1){
            this._page = param1;
            var _loc2_ = this._vecPage.length;
            var _loc3_ = 0;
            while(_loc3_ < _loc2_)
            {
                if(_loc3_ >= param1)
                {
                    this._vecPage[_loc3_].y = -480;
                }
                _loc3_++;
            }
            if(this._vecPage[this._page])
            {
                this._vecPage[this._page].gotoAndStop(0);
            }
            this._start = getTimer();
            this._chan = this._snd.play();
            Game.Gfx.addEventListener("tick", this.handleEnterFrame.bind(this));//TODO Sounds
            return;
        }

        p.easePos= function(){
            var _loc_1 = getTimer() - this._start;
            _loc_1 = XMath.clamp(this._chan.duration - _loc_1, 0, this._chan.duration);
            var _loc_2 = _loc_1 / this._chan.duration;
            this._vecPage[this._page].y = -480 * _loc_2;
            //console.log(this._vecPage)
            return;
        }

        p.handleEnterFrame= function(event){
            if (this._chan.position >= this._chan.duration || this._chan.position == 0)
            {
                this._vecPage[this._page].y = 0;
                Game.Gfx.removeAllEventListeners();
                this.onShow();
                if (this._vecPage[this._page])
                {
                    (this._vecPage[this._page]).uncache();
                    (this._vecPage[this._page]).play();
                }
                return;
            }
            this.easePos();
            return;
        }

        p.onShow= function(){
            return;
        }
        window.Game.Basis.Help = Help;

    
})(window);