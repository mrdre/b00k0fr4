﻿(function (window) {
    function MoneyParams() {
        // this._tfCurrency:TextField;
        // this._tfDenom:TextField;
        // this._tfCredits:TextField;
        // this._tfPrize:TextField;
        // this._tfWinFree:TextField;
        // this._tfWinTotal:TextField;
        // this._tdWinCurrent:TextDisplay;
        // this._collector:Collector;

        // p.MoneyParams()
        // {

        this._tfCurrency = Game.Gfx.slotsTfCurrency;
        this._tfDenom = Game.Gfx.slotsTfDenom;
        this._tfCredits = Game.Gfx.slotsTfCredits;
        this._tfPrize = Game.Gfx.slotsTfWinSum;
        this._tfWinFree = Game.Gfx.slotsTfWinFree;
        this._tfWinTotal = Game.Gfx.slotsTfWin;
        this._tdWinCurrent = Game.Gfx.slotsStatus;
        this._collector = new Game.Basis.Collector(this, Game.Sfx.takeSlow, Game.Sfx.takeFast);
        this._tfWinTotal.text = "";
        if (this._tfWinFree) {
            this._tfWinFree.text = "";
        }
        return;
    }
    var p = MoneyParams.prototype;

    p.set_currency = function (param1) {
        this._tfCurrency.text = param1 ? (param1) : ("");
        return;
    }

    p.set_denom = function (param1) {
        this._tfDenom.text = Number(param1 / 10).toString();
        return;
    }

    p.set_credits = function (param1) {
        this._tfCredits.text = param1.toString();
        return;
    }

    p.set_prize = function (param1) {
        this._tfPrize.text = param1.toString();
        return;
    }

    p.set_winTotal = function (param1) {
        this._tfWinTotal.text = param1 ? ("WIN " + param1.toString()) : ("");
        if (this._tfWinFree) {
            this._tfWinFree.text = param1.toString();
        }
        return;
    }

    p.set_winCurrent = function (param1) {
        var _loc_2 = param1 ? (Game.TXT.getText("prizeSum", [param1])) : ("");
        this._tdWinCurrent.write(_loc_2);
        return;
    }

    p.collect = function (param1, param2) {
        this._collector.execute(param1, param2);
        return;
    }

    p.collectResult = function (param1) {
        this._tfWinTotal.text = "WINNER PAID " + param1.toString();
        return;
    }

    p.onCollect = function () {
        return;
    }
    window.Game.Basis.MoneyParams = MoneyParams;

})(window);

(function (window) {
    function Collector(param1, param2, param3) {

        // this._targ:MoneyParams;
        // this._timer:Timer;
        // this._sndSlow:Sound;
        // this._sndFast:Sound;
        // this._chan:SoundChannel;
        // this._start;
        // this._end;
        // this._delta;
        // this._curr;
        this._isSlow = false;

        // function Collector()
        // {
        this._targ = param1;
        this._sndSlow = param2;
        this._sndFast = param3;
        return;
    }
    var p = Collector.prototype;

    p.execute = function (param1, param2) {
        this._start = param2 - param1;
        this._end = param2;
        this._delta = param1;
        this._curr = 0;
        this._isSlow = true;
        this._timer = setInterval(this.handleTimer.bind(this), 50);
        if (!this._chan)
            this._chan = this._sndSlow.play(0, Number.MAX_VALUE);
        this._targ.set_credits(this._start);
        this._targ.set_winTotal(this._delta);
        return;
    }

    p.handleTimer = function (event) {
        var _loc_2 = Math.ceil((this._delta - this._curr) / 9);
        if (_loc_2) {
            if (this._curr + _loc_2 > this._delta) {
                _loc_2 = this._delta - this._curr;
            }
            this._curr = this._curr + _loc_2;
            this._targ.set_credits(this._start + this._curr);
            this._targ.set_winTotal(this._delta - this._curr);
            if (this._isSlow && this._delta <= 1.2 * this._curr) {
                this._isSlow = false;
                this._chan.stop();
                this._chan = null;
                this._chan = this._sndFast.play(0, Number.MAX_VALUE);
            }
        }
        else {
            this._chan.stop();
            this._chan = null;
            this.finish();
        }
        return;
    }

    p.finish = function () {
        this._targ.set_credits(this._end);
        this._targ.collectResult(this._delta);
        if (this._timer) {
            clearInterval(this._timer);
            this._timer = null;
        }
        this._targ.onCollect();
        return;
    }
    window.Game.Basis.Collector = Collector;


})(window);