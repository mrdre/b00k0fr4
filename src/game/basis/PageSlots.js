﻿(function (window) {
	function PageSlots() {
		// this._iconAuto:DisplayObject;
		// this._background:AnimCollection;
		// this._tfWinFree:TextField;
		// this._dspStatus:TextDisplay;
		// this._mode:String;
		// this._chan:SoundChannel;

		// p.PageSlots()
		// {
		// return;

		this._iconAuto = Game.Gfx.slotsAuto;
		this._background = Game.Gfx.slotsBackground;
		this._tfWinFree = Game.Gfx.slotsTfWinFree;
		this._dspStatus = Game.Gfx.slotsStatus;
		this._background.visible = false;
		this._iconAuto.visible = false;
	}
	var p = PageSlots.prototype;

	p.get_auto = function () {
		return this._iconAuto.visible;
	}

	p.set_auto = function (param1) {
		this._iconAuto.visible = param1;
		var _loc_2 = param1 ? (Game.Sfx.autoStart) : (Game.Sfx.autoStop);
		this._chan = _loc_2.play();
	    this._chan.addEventListener('complete', this.handleSndComplete.bind(this)); //TODO Sound
		//this.handleSndComplete();
		return;
	}

	p.get_mode = function () {
		return this._mode;
	}

	p.set_mode = function (param1) {
		if (this._tfWinFree) {
			this._tfWinFree.visible = false;
		}
		this._mode = param1;
		//console.log('D:\OpenServer\domains\test\src\game\basis\PageSlots.js 44')
		this._background.addEventListener('Event.COMPLETE', this.handleModeComplete.bind(this));
		this._background.play(this._mode);
		return;
	}

	p.set_status = function (param1) {
		this._dspStatus.write(param1);
		return;
	}

	p.handleSndComplete = function (event) {
		this._chan.removeAllEventListeners('complete', this.handleSndComplete);
		this.onAuto();
		return;
	}

	p.handleModeComplete = function (event) {
		this._background.removeAllEventListeners('Event.COMPLETE', this.handleModeComplete);
		if (this._tfWinFree) {
			this._tfWinFree.visible = this._mode == "bonus";
		}
		this.onMode();
		return;
	}

	p.onAuto = function () {
		return;
	}

	p.onMode = function () {
		return;
	}

	window.Game.Basis.PageSlots = PageSlots;

})(window);