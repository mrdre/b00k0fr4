﻿(function (window) {
	function PrizeFree() {
		// this._msg:MsgFree;
		// this._status:TextDisplay;
		// this._anims:ReelsAnimations;
		// this._header:AnimCollection;
		// this._special:Special;
		// this._kind;
		// this._points;
		// this._idSpec;
		// this._bet;
		// this._chan:SoundChannel;

		// p.PrizeFree()
		// {

	    Game.Comps.MsgFree.SND_BEGIN = Game.Sfx.freeStart;
	    Game.Comps.MsgFree.SND_AGAIN = Game.Sfx.freeStart;
	    Game.Comps.MsgFree.SND_END = Game.Sfx.freeEnd;
	    Game.Comps.ReelsAnimations.CLS_TRIG = Game.Gfx.vecClsTileAnims[Game.Cfg.idTrig][1][2];
	    Game.Comps.Special.OPEN = Game.Sfx.bookOpen;
	    Game.Comps.Book.SND_CHANGE = Game.Sfx.bookTileChange;
	    Game.Comps.Book.SND_FLOW = Game.Sfx.bookRising;
	    this._msg = Game.Gfx.slotsMsgFree;
	    this._status = Game.Gfx.slotsStatus;
	    this._special = Game.Gfx.slotsSpecial;
	    this._anims = Game.Gfx.slotsAnims;
	    this._header = Game.Gfx.slotsHeader;
	}
	var p = PrizeFree.prototype;

	p.prepare = function (param1, param2, param3, param4, param5, param6) {
		this._kind = param1;
		this._points = param4;
		this._idSpec = param6;
		this._bet = param2;
		if (this._kind == "begin") {
		    //console.log(this._header)
			this._header.play("prepare");
		}
		var _loc_7 = Game.TXT.getText(Game.TXT.PRIZE_FREE);
		this._status.write(_loc_7);
		this._anims.playTrigs(this._points);
		this._chan = Game.Sfx.freeStart.play();//TODO Sound
		this._chan.addEventListener('complete', this.handleSndPrepareComplete.bind(this));
		//this.handleSndPrepareComplete();
		return;
	}

	p.execute = function (param1, param2, param3, param4, param5, param6) {
		var _loc7_ = Game.TXT.getText(Game.TXT.PRIZE_FREE);
		this._status.write(_loc7_);
		switch (param1) {
			case "begin":
				this.doBegin();
				break;
			case "again":
				this.doAgain();
		}
	}

	p.clear = function () {
		this._msg.stop();
		return;
	}

	p.play = function (param1, param2) {
		var _loc_3 = Game.TXT.getText(Game.TXT.FREE_GAME, [param1, param2]);
		this._status.write(_loc_3);
		return;
	}

	p.finish = function (param1, param2) {
	    //console.log('D:\OpenServer\domains\test\src\game\basis\PrizeFree.js 61')
		this._msg.addEventListener('Event.COMPLETE', this.handleFreeFinish.bind(this));
		this._msg.playEnd(param2);
		this._header.play("normal");
		this._special.stop();
		this._status.clear();
		return;
	}

	p.shown = function () {
		this._msg.stop();
		this.onShown();
		return;
	}

	//p.onPrepare = function () {
	//	return;
	//}

	//p.onStart = function () {
	//	return;
	//}

	//p.onShown = function () {
	//	return;
	//}

	//p.onFinish = function () {
	//	return;
	//}

	p.doBegin = function () {
		this._header.play("transform");
		this._msg.playBegin();
		//console.log('D:\OpenServer\domains\test\src\game\basis\PrizeFree.js 95')
		this._special.addEventListener('Event.COMPLETE', this.handleSpecialComplete.bind(this));
		this._special.execute(this._idSpec, this._bet, Game.Cfg.pays[this._idSpec]);
		return;
	}

	p.doAgain = function () {
		this._msg.playAgain();
		var _loc_1 = Game.TXT.getText(Game.TXT.START_FREE);
		this._status.write(_loc_1, true);
		this.onStart();
		return;
	}

	p.handleSndPrepareComplete = function (event) {
		this._chan.removeAllEventListeners('complete', this.handleSndPrepareComplete);
		this.onPrepare();
		return;
	}

	p.handleSndAgainComplete = function (event) {
		this._chan.removeAllEventListeners('complete', this.handleSndPrepareComplete);
		var _loc_2 = Game.TXT.getText(Game.TXT.START_FREE);
		this._status.write(_loc_2, true);
		this.onStart();
		return;
	}

	p.handleSpecialComplete = function (event) {
		this._special.removeAllEventListeners('Event.COMPLETE', this.handleSpecialComplete);
		var _loc_2 = Game.TXT.getText(Game.TXT.START_FREE);
		this._status.write(_loc_2, true);
		this.onStart();
		return;
	}

	p.handleFreeFinish = function (event) {
		this._msg.removeAllEventListeners('Event.COMPLETE', this.handleFreeFinish);
		this._msg.stop();
		this.onFinish();
		return;
	}
	window.Game.Basis.PrizeFree = PrizeFree;


})(window);