﻿(function (window) {
	function PrizeJackpot() {
		// this._host:DisplayObjectContainer;
		// this._body:DisplayObject;
		// this._ldr:Loader;
		// this._sum;
		// this._tfType:TextField;
		// this._tfSum:TextField;
		// this.denom;
		// this.currency;

		// p.PrizeJackpot()
		// {

		this._host = Game.Gfx.pSlots;
		this._tfType = Game.Gfx.slotsTfWinType;
		this._tfSum = Game.Gfx.slotsTfWinSum;
		return;
	}
	var p = PrizeJackpot.prototype;

	p.execute = function (param1, param2) {
		this._tfType.text = "JACKPOT PAYS";
		this._tfSum.text = parseInt(param2 / this.denom).toString();
		var _loc_3 = Game.Gfx.jackpotComponents[param1];
		this._ldr = new Loader();
		this._ldr.loadBytes(_loc_3);
		//console.log('D:\OpenServer\domains\test\src\game\basis\PrizeJackpot.js 61')
		this._ldr.contentLoaderInfo.addEventListener('Event.COMPLETE', this.handleLoader.bind(this));
		this._sum = param2;
		return;
	}

	p.skip = function () {
		var _loc_1 = this._body["stop"];
		_loc_1.call();
		return;
	}

	//p.onComplete = function () {
	//	return;
	//}

	p.handleLoader = function (event) {
		var _loc_2 = event.currentTarget;
		_loc_2.removeAllEventListeners('Event.COMPLETE', this.handleLoader);
		this._body = this._ldr.content;
		this._ldr = null;
		this._host.addChild(this._body);
		var _loc_3 = this._body["execute"];
		_loc_3.call(null, this._sum, this.denom, this.currency, TXT.lng, this.callback);
		return;
	}

	p.callback = function () {
		this._host.removeChild(this._body);
		this._body = null;
		this._tfType.text = "";
		this._tfSum.text = "";
		this.onComplete();
		return;
	}

	window.Game.Basis.PrizeJackpot = PrizeJackpot;

})(window);