﻿(function (window) {
	function PrizeLine() {
		// this._lines:Vector.<GameLine>;
		// this._tfType:TextField;
		// this._tfSum:TextField;
		// this._playlist:PlayList;
		// this._id;

		// p.PrizeLine()
		// {

		this._lines = Game.Gfx.slotsLines;
		this._tfType = Game.Gfx.slotsTfWinType;
		this._tfSum = Game.Gfx.slotsTfWinSum;
	    this._playlist = new PlayList();//TODO Sound
		this._tfType.text = "";
		this._tfSum.text = "";
		return;
	}
	var p = PrizeLine.prototype;

	p.execute = function (param1, param2, param3, param4) {
		var _loc_6 = null;
		var _loc_5 = param4.toString().length;
		this._id = param1;
		this._lines[this._id].cells(param3);
		this._tfType.text = "LINE PAYS";
		this._tfSum.text = param4.toString();
		_loc_6 = Game.Sfx.vecRing[(_loc_5 - 1)];
		if (_loc_6) {
			this._playlist.add(_loc_6); //TODO Sound
		}
		_loc_6 = Game.Sfx.vecWin[param2][(param3 - 1)];
	    this._playlist.add(_loc_6);//TODO Sound
	    this._playlist.addEventListener('Event.COMPLETE', this.handlePlaylistComplete.bind(this));//TODO Sound
	    this._playlist.play();//TODO Sound
		return;
	}

	p.handlePlaylistComplete = function (event) {
	    this._playlist.removeAllEventListeners('Event.COMPLETE', this.handlePlaylistComplete);//TODO Sound
		this._lines[this._id].cells(-1);
		this._tfType.text = "";
		this._tfSum.text = "";
	    this._playlist.clear();//TODO Sound
		this.onComplete();
		return;
	}

	//p.onComplete = function () {
	//	return;
	//}

	window.Game.Basis.PrizeLine = PrizeLine;

})(window);