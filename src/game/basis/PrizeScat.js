﻿(function(window){
	function PrizeScat(){
        // this._tfType:TextField;
        // this._tfSum:TextField;
        // this._playlist:PlayList;

        // p.PrizeScat()
        // {
		
            this._tfType = Game.Gfx.slotsTfWinType;
            this._tfSum = Game.Gfx.slotsTfWinSum;
            this._playlist = new PlayList();
            return;
        }
	var p = PrizeScat.prototype;

        p.execute= function(param1, param2, param3){
            var _loc_5 = null;
            var _loc_4 = param3.toString().length;
            this._tfType.text = "SCATTER PAYS";
            this._tfSum.text = param3.toString();
            _loc_5 = Game.Sfx.vecRing[(_loc_4 - 1)];
            if (_loc_5)
            {
                this._playlist.add(_loc_5);//TODO Sound
            }
            _loc_5 = Game.Sfx.vecWin[Game.Cfg.idScat][(param1 - 1)];
            this._playlist.add(_loc_5);//TODO Sound
            this._playlist.addEventListener('Event.COMPLETE', this.handlePlaylistComplete.bind(this));//TODO Sound
            this._playlist.play();//TODO Sound
            return;
        }

        p.handlePlaylistComplete= function(event){
            //this._playlist.removeAllEventListeners('Event.COMPLETE', this.handlePlaylistComplete);//TODO Sound
            this._tfType.text = "";
            this._tfSum.text = "";
            //this._playlist.clear();//TODO Sound
            this.onComplete();
            return;
        }

        //p.onComplete= function(){
        //    return;
        //}
        window.Game.Basis.PrizeScat = PrizeScat;

    
})(window);