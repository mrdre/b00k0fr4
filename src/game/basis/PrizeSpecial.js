﻿(function (window) {
	function PrizeSpecial() {
		// this._anims:ReelsAnimations;
		// this._lines:Vector.<GameLine>;
		// this._tfType:TextField;
		// this._tfSum:TextField;
		// this._id;
		// this._chan:SoundChannel;

		// p.PrizeSpecial()
		// {

	    Game.Comps.ReelsAnimations.SND_CHANGE = Game.Sfx.bookTileChange;
		this._lines = Game.Gfx.slotsLines;
		this._tfType = Game.Gfx.slotsTfWinType;
		this._tfSum = Game.Gfx.slotsTfWinSum;
		this._anims = Game.Gfx.slotsAnims;
		return;
	}
	var p = PrizeSpecial.prototype;

	p.prepare = function (param1, param2, param3) {
		var _loc_4 = this._lines.length;
		while (_loc_4--) {

			this._lines[_loc_4].cells(-1);
		}
		this._tfType.text = "";
		this._tfSum.text = "";
		//console.log('D:\OpenServer\domains\test\src\game\basis\PrizeSpecial.js 30')
		this._anims.addEventListener(Event.CHANGE, this.handleAnimChange.bind(this));
		this._anims.playSpec(param1, param2, param3);
		return;
	}

	p.execute = function (param1, param2, param3) {
		this._id = param1;
		this._lines[this._id].cellsSpec(param2);
		this._tfType.text = "FREE LINE PAYS";
		this._tfSum.text = param3.toString();
		this._chan = Game.Sfx.freeLineWin.play();
	    this._chan.addEventListener('complete', this.handleSndComplete.bind(this)); //TODO Sound
			//this.handleSndComplete();
		return;
	}

	p.clear = function () {
		this._anims.clear();
		return;
	}

	//p.onPrepare = function () {
	//	return;
	//}

	//p.onComplete = function () {
	//	return;
	//}

	p.handleAnimChange = function (event) {
		this._anims.removeAllEventListeners(Event.CHANGE, this.handleAnimChange);
		this.onPrepare();
		return;
	}

	p.handleSndComplete = function (event) {
		this._chan.removeAllEventListeners('complete', this.handleSndComplete);
		this._lines[this._id].cells(-1);
		this._tfType.text = "";
		this._tfSum.text = "";
		this.onComplete();
		return;
	}
	window.Game.Basis.PrizeSpecial = PrizeSpecial;


})(window);