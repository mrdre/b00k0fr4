﻿(function (window) {
	function Prizes() {
		// this._anims:ReelsAnimations;
		// this._status:TextDisplay;
		// this._items:Array;
		// this._chan:SoundChannel;

		// p.Prizes()
		// {

		this._anims = Game.Gfx.slotsAnims;
		this._status = Game.Gfx.slotsStatus;
		return;
	}
	var p = Prizes.prototype;

	p.animate = function (param1, param2) {
		this._anims.play(param1, param2);
		return;
	}

	p.hide = function () {
		this._anims.clear();
		return;
	}

	p.wait = function (param1) {
	    if(!this._chan)
    	    this._chan = Game.Sfx.wait.play(0, Number.MAX_VALUE);
		var _loc_2 = param1 ? (Game.TXT.WAIT_JP) : (Game.TXT.WAIT);
		var _loc_3 = Game.TXT.getText(_loc_2);
		this._status.write(_loc_3);
		return;
	}

	p.waitEnd = function () {
		if (this._chan) {
		    this._chan.stop();
		    this._chan = null;
		}
		this._status.clear();
		return;
	}

	window.Game.Basis.Prizes = Prizes;

})(window);