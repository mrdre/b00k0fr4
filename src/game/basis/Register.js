﻿(function(window){
	function Register(){
        //this._store:Dictionary;
        //this.INST:Register;

        //p.Register= function()
        //{
            if (this.INST)
            {
                throw new Error("Oops... `Register` is a singleton. Use `Register.inst` to access an instance.");
            }
            this.init();
            return;
        }
	var p = Register.prototype;

        p.getInst= function(param1){
            if (!this._store[param1])
            {
                this._store[param1] = new param1;
            }
            return this._store[param1];
        }

        p.init= function(){
        	this._store = {};
            return;
        }

        Register.inst = function(){
            if (!this.INST)
            {
                this.INST = new Register;
            }
            return this.INST;
        }
        window.Game.Basis.Register = Register;

    
})(window);