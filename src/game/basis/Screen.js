﻿(function (window) {
    function Screen() {
        // this._host:DisplayObjectContainer;
        // this._pSlots:DisplayObject;
        // this._pGamble:DisplayObject;
        // this._pHelp:DisplayObject;
        // this._pError:DisplayObject;

        // p.Screen()
        // {

        this._host = Game.Gfx;
        this._pSlots = Game.Gfx.pSlots;
        this._pGamble = Game.Gfx.pGamble;
        this._pHelp = Game.Gfx.pHelp;
        this._pError = Game.Gfx.pError;
        return;
    }
    var p = Screen.prototype;

    p.doError = function () {
        if (!Game.Gfx.visible) {
            Game.Gfx.visible = true;
        }
        this._host.addChild(this._pError);
        return;
    }

    p.doHelp = function () {
        if (this._pError.parent) {
            this._host.removeChild(this._pError);
        }
        if (!this._pHelp.parent) {
            this._host.addChild(this._pHelp);
        }
        return;
    }

    p.doSlots = function () {
        if (this._pGamble.parent) {
            this._host.removeChild(this._pGamble);
        }
        if (this._pHelp.parent) {
            this._host.removeChild(this._pHelp);
        }
        if (this._pError.parent) {
            this._host.removeChild(this._pError);
        }
        if (!this._pSlots.parent) {
            this._host.addChild(this._pSlots);
        }
        return;
    }

    p.doGamble = function () {
        if (this._pHelp.parent) {
            this._host.removeChild(this._pHelp);
        }
        if (this._pError.parent) {
            this._host.removeChild(this._pError);
        }
        if (!this._pGamble.parent) {
            this._host.addChild(this._pGamble);
        }
        return;
    }
    window.Game.Basis.Screen = Screen;

})(window);