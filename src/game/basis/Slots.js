﻿(function (window) {
    Slots.IS_FREE_SPIN_SOUND = false;

    function Slots() {

        Game.Comps.Reels.ID_TRIG = Game.Cfg.idTrig;
        Game.Comps.Reels.MIN_TRIGS = Game.Cfg.minTrigs;
        Game.Comps.Reels.SND_STOP = Game.Sfx.spinStop;
        Game.Comps.Reels.VEC_SND_TRIGS = Game.Sfx.vecBonusTile;
        this._plNorm = new ThemePlayer(Game.Sfx.spin);
        this._plFree = new ThemePlayer(Game.Sfx.free);
        this._reels = Game.Gfx.slotsReels;
        this._reels.addEventListener('Event.COMPLETE', this.handleReelsComplete.bind(this));
    }
    var p = Slots.prototype;

    p.spin = function (param1, param2) {
        if (this._mode != param1) {
            this._mode = param1;
            this._plFree.stop();
        }
        switch (this._mode) {
            case "norm":
                {
                    this.playNorm(param2);
                    break;
                }
            case "free":
                {
                    this.playFree(param2);
                    break;
                }
            default:
                {
                    break;
                }
        }
        return;
    }

    p.setFin = function (param1) {
        this._reels.setFinite(param1);
        return;
    }

    p.setTiles = function (param1) {
        this._reels.setTiles(param1);
        return;
    }

    p.playNorm = function (param1) {
        this._plNorm.play();
        this._reels.spin(param1);
        return;
    }

    p.playFree = function (param1) {
        if (IS_FREE_SPIN_SOUND) {
            this._plNorm.play();
        }
        this._plFree.play();
        this._reels.spin(param1);
        return;
    }

    p.handleReelsComplete = function (event) {
        this._plNorm.stop();
        this._plFree.pause();
        this.onReelsComplete();
        return;
    }
    window.Game.Basis.Slots = Slots;

})(window);