﻿(function (window) {
    function Controller(param1) {
        //private var targ:Game;
        //private var model:MGame;
        //private var input:Game.Basis.GameInput;
        //private var screen:VScreen;
        //private var pageSlots:VPageSlots;
        //private var money:VMoneyParams;
        //private var betParam:VBetParams;
        //private var slots:VSlots;
        //private var przLine:VPrizeLine;
        //private var przScat:VPrizeScat;
        //private var przSpec:VPrizeSpecial;
        //private var przFree:VPrizeFree;
        //private var przJackpot:VPrizeJackpot;
        //private var prizes:VPrizes;
        //private var help:VHelp;
        //private var gamble:VGamble;
        //private var error:VPageError;

        this.targ = param1;
        Game.Gfx.visible = false;
        this.input = new Game.Basis.GameInput();
        ////console.log('D:\OpenServer\domains\test\src\game\mvc\Controller.js 30')
        this.input.addEventListener(Game.ModelBox.CHANGE, this.handleInputChange.bind(this));
        //this.targ.addChild(Game.Gfx);
        //this.targ.stage.addEventListener('ModelPropagationEvent', this.handleModelBoxPropagation);
        ////console.log('D:\OpenServer\domains\test\src\game\mvc\Controller.js 34')
        this.targ.addEventListener('ModelPropagationEvent', this.handleModelBoxPropagation.bind(this));

    }

    var p = Controller.prototype;

    p.init = function () {
        this.model = new Game.MVC.MGame();
        this.screen = new Game.MVC.VScreen();
        this.pageSlots = new Game.MVC.VPageSlots();
        this.money = new Game.MVC.VMoneyParams();
        this.betParam = new Game.MVC.VBetParams();
        this.slots = new Game.MVC.VSlots();
        this.przLine = new Game.MVC.VPrizeLine();
        this.przScat = new Game.MVC.VPrizeScat();
        this.przSpec = new Game.MVC.VPrizeSpecial();
        this.przFree = new Game.MVC.VPrizeFree();
        this.przJackpot = new Game.MVC.VPrizeJackpot();
        this.prizes = new Game.MVC.VPrizes();
        this.help = new Game.MVC.VHelp();
        this.gamble = new Game.MVC.VGamble();
        this.error = new Game.MVC.VPageError();
        this.screen.set_model(this.model);
        this.pageSlots.set_model(this.model);
        this.money.set_model(this.model);
        this.betParam.set_model(this.model);
        this.slots.set_model(this.model);
        this.przLine.set_model(this.model);
        this.przScat.set_model(this.model);
        this.przSpec.set_model(this.model);
        this.przFree.set_model(this.model);
        this.przJackpot.set_model(this.model);
        this.prizes.set_model(this.model);
        this.help.set_model(this.model);
        this.gamble.set_model(this.model);
        this.error.set_model(this.model);
        //console.log('D:\OpenServer\domains\test\src\game\mvc\Controller.js 64 - 80')
        this.slots.addEventListener(Game.MVC.VEvent.REELS_START, this.handleReelsStart.bind(this));
        this.slots.addEventListener(Game.MVC.VEvent.REELS_STOP, this.handleReelsStop.bind(this));
        this.przLine.addEventListener(Game.MVC.VEvent.PRIZE_SHOWN, this.handlePrizeShown.bind(this));
        this.przScat.addEventListener(Game.MVC.VEvent.PRIZE_SHOWN, this.handlePrizeShown.bind(this));
        this.przSpec.addEventListener(Game.MVC.VEvent.PRIZE_SHOWN, this.handlePrizeShown.bind(this));
        this.przJackpot.addEventListener(Game.MVC.VEvent.PRIZE_SHOWN, this.handlePrizeShown.bind(this));
        this.przJackpot.addEventListener(Game.MVC.VEvent.JACKPOT, this.handleJackpot.bind(this));
        this.przFree.addEventListener(Game.MVC.VEvent.FREE_PREPARE, this.handleFreePrepare.bind(this));
        this.przFree.addEventListener(Game.MVC.VEvent.FREE_WAIT, this.handleFreeWait.bind(this));
        this.przFree.addEventListener(Game.MVC.VEvent.FREE_SHOWN, this.handleFreeShown.bind(this));
        this.przFree.addEventListener(Game.MVC.VEvent.FREE_END, this.handleFreeEnd.bind(this));
        this.prizes.addEventListener(Game.MVC.VEvent.PRIZE_WAIT, this.handlePrizeWait.bind(this));
        this.money.addEventListener(Game.MVC.VEvent.COLLECTED, this.handleCollected.bind(this));
        this.pageSlots.addEventListener(Game.MVC.VEvent.GAME_OVER, this.handleGameOver.bind(this));
        this.gamble.addEventListener(Game.MVC.VEvent.GAMBLE_READY, this.handleGambleReady.bind(this));
        this.gamble.addEventListener(Game.MVC.VEvent.GAMBLE_RESULT, this.handleGambleResult.bind(this));
        this.run();
    }
    p.onCoin = function () {
        this.model.setNextDenom();
        return;
    }

    p.onExit = function () {
        if (Game.Cfg.urlExit) {
            navigateToURL(new URLRequest(Game.Cfg.urlExit), Game.Cfg.targetExit);
        }
        else if (ExternalInterface.available) {
            ExternalInterface.call("history.back");
        }
        return;
    }

    p.onHelp = function () {
        if (this.model.screen.get_page() != "help") {
            this.input.setMode(Game.Basis.GameInput.M_HELP);
        }
        this.model.showHelp();
        return;
    }

    p.onAuto = function () {
        var _loc_1 = this.model.busy;
        this.model.tryChangeAuto();
        if (!_loc_1 && this.model.pageSlots.get_isAuto()) {
            this.input.setMode(Game.Basis.GameInput.M_BLOCK);
        }
        return;
    }

    p.onLines = function (param1) {
        switch (param1) {
            case Game.Basis.GameInput.B_LINES1:
                {
                    this.model.setLines(1);
                    break;
                }
            case Game.Basis.GameInput.B_LINES3:
                {
                    this.model.setLines(3);
                    break;
                }
            case Game.Basis.GameInput.B_LINES5:
                {
                    this.model.setLines(5);
                    break;
                }
            case Game.Basis.GameInput.B_LINES7:
                {
                    this.model.setLines(7);
                    break;
                }
            case Game.Basis.GameInput.B_LINES9:
                {
                    this.model.setLines(9);
                    break;
                }
            default:
                {
                    break;
                }
        }
        return;
    }

    p.onBet = function () {
        this.model.setNextBet();
        return;
    }

    p.onMaxBet = function () {
        this.model.setMaxBet();
        return;
    }

    p.onGamble = function () {
        this.input.setMode(Game.Basis.GameInput.M_BLOCK);
        this.model.gambleStart();
        return;
    }

    p.onColor = function (param1) {
        this.input.setMode(Game.Basis.GameInput.M_BLOCK);
        switch (param1) {
            case Game.Basis.GameInput.B_RED:
                {
                    this.model.gambleChoice(-1);
                    break;
                }
            case Game.Basis.GameInput.B_BLACK:
                {
                    this.model.gambleChoice(1);
                    break;
                }
            default:
                {
                    break;
                }
        }
        return;
    }

    p.onTake = function () {
        this.input.setMode(Game.Basis.GameInput.M_BLOCK);
        this.model.takePrize();
        return;
    }

    p.onStart = function () {
        if (this.model.money.get_credits() < this.model.betParams.get_total()) {
            this.model.decreaseTotalBet();
        }
        if (this.model.money.get_credits() < this.model.betParams.get_total()) {
            return;
        }
        this.input.setMode(Game.Basis.GameInput.M_BLOCK);
        this.model.playSpin();
        return;
    }

    p.onFree = function () {
        this.model.prizes.free.shown();
        this.model.nextPrize();
        return;
    }

    p.onAny = function () {
        return;
    }

    p.onJP = function () {
        this.model.prizes.jackpot.skip();
        return;
    }

    p.handleInputChange = function (event) {
        switch (this.input.pressed) {
            case Game.Basis.GameInput.B_COIN:
                {
                    this.onCoin();
                    return;
                }
            case Game.Basis.GameInput.B_EXIT:
                {
                    this.onExit();
                    return;
                }
            case Game.Basis.GameInput.B_HELP:
                {
                    this.onHelp();
                    return;
                }
            case Game.Basis.GameInput.B_AUTO:
                {
                    this.onAuto();
                    return;
                }
            case Game.Basis.GameInput.B_LINES1:
            case Game.Basis.GameInput.B_LINES3:
            case Game.Basis.GameInput.B_LINES5:
            case Game.Basis.GameInput.B_LINES7:
            case Game.Basis.GameInput.B_LINES9:
                {
                    this.onLines(this.input.pressed);
                    return;
                }
            case Game.Basis.GameInput.B_BET:
                {
                    this.onBet();
                    return;
                }
            case Game.Basis.GameInput.B_MAXBET:
                {
                    this.onMaxBet();
                    return;
                }
            case Game.Basis.GameInput.B_GAMBLE:
                {
                    this.onGamble();
                    return;
                }
            case Game.Basis.GameInput.B_RED:
            case Game.Basis.GameInput.B_BLACK:
                {
                    this.onColor(this.input.pressed);
                    return;
                }
            case Game.Basis.GameInput.B_TAKE:
                {
                    this.onTake();
                    return;
                }
            case Game.Basis.GameInput.B_START:
                {
                    this.onStart();
                    return;
                }
            case Game.Basis.GameInput.B_FREE:
                {
                    this.onFree();
                    return;
                }
            case Game.Basis.GameInput.B_ANY:
                {
                    this.onAny();
                    return;
                }
            case Game.Basis.GameInput.B_JP:
                {
                    this.onJP();
                    return;
                }
            default:
                {
                    break;
                }
        }
        return;
    }

    p.handleModelBoxPropagation = function (event) {
        var _loc_2 = event.currentTarget;
        _loc_2.removeAllEventListeners('ModelPropagationEvent', this.handleModelBoxPropagation);
        this.input.set_model(event.data);
        return;
    }

    p.run = function () {
        this.model.initGame();
        return;
    }

    p.handleReelsStart = function (event) {
        this.input.setMode(Game.Basis.GameInput.M_SPIN);
        return;
    }

    p.handleReelsStop = function (event) {
        this.model.afterSpin();
        return;
    }

    p.handlePrizeShown = function (event) {
        this.input.setMode(Game.Basis.GameInput.M_SPIN);
        this.model.nextPrize();
        return;
    }

    p.handlePrizeWait = function (event) {
        this.input.setMode(Game.Basis.GameInput.M_CHOICE);
        return;
    }

    p.handleCollected = function (event) {
        this.model.afterTake();
        return;
    }

    p.handleGameOver = function (event) {
        this.input.setMode(Game.Basis.GameInput.M_GAME_OVER);
        return;
    }

    p.handleGambleReady = function (event) {
        this.model.gamble.nextGame();
        this.input.setMode(Game.Basis.GameInput.M_GAMBLE);
        return;
    }

    p.handleGambleResult = function (event) {
        if (this.model.gamble.get_factor()) {
            if (this.model.gamble.get_gamesLeft()) {
                this.input.setMode(Game.Basis.GameInput.M_GAMBLE);
                this.model.gamble.nextGame();
            }
            else {
                this.model.takePrize();
            }
        }
        else {
            this.input.setMode(Game.Basis.GameInput.M_GAME_OVER);
            this.model.gambleEnd();
        }
        return;
    }

    p.handleFreePrepare = function (event) {
        this.model.executeFree();
        return;
    }

    p.handleFreeWait = function (event) {
        this.input.setMode(Game.Basis.GameInput.M_FREE);
        return;
    }

    p.handleFreeShown = function (event) {
        this.input.setMode(Game.Basis.GameInput.M_SPIN);
        return;
    }

    p.handleFreeEnd = function (event) {
        this.model.endFree();
        this.model.takePrize();
        this.model.prizes.free.clear();
        return;
    }

    p.handleJackpot = function (event) {
        this.input.setMode(Game.Basis.GameInput.M_JP);
        return;
    }

    p.handleError = function (event) {
        this.input.setMode(Game.Basis.GameInput.M_ERROR);
        return;
    }

    window.Game.Controller = Controller;
})(window);