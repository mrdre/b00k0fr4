package game.mvc.models
{
   import game.basis.BaseObject;
   
   public class BaseModel extends BaseObject
   {
       
      public function BaseModel()
      {
         super();
      }
      
      public function report(param1:String) : void
      {
         dispatchEvent(new MEvent(param1));
      }
   }
}
