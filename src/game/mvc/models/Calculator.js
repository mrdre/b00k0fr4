﻿(function (window) {
    function Calculator(param1) {
        // this._targ:MPrizes;
        // this._reels:Vector.<Vector.<uint>>;

        // p.Calculator(param1:MPrizes)
        // {
        this._targ = param1;
        return;
    }
    var p = Calculator.prototype;

    p.findPrizes = function (param1, param2, param3, param4) {
        this._reels = [];
        this._targ.items = [];
        this.parseReels(param1);
        this.calcReels();
        this.parseJackpot(param3, param4);
        this.checkTrig();
        this.parsePrizeMap();
        return;
    }

    p.parseReels = function (param1) {
        var _loc_5 = null;
        var _loc_6 = 0;
        var _loc_7 = 0;
        var _loc_8 = 0;
        var _loc_2 = param1.length;
        var _loc_3 = _loc_2 / 3;
        this._reels.length = _loc_3;
        var _loc_4 = 0;
        while (_loc_4 < _loc_3) {

            _loc_5 = Array(3);
            _loc_6 = 0;
            while (_loc_6 < 3) {

                _loc_7 = _loc_6 + 3 * _loc_4;
                _loc_8 = param1[_loc_7];
                _loc_5[_loc_6] = _loc_8;
                _loc_6 = _loc_6 + 1;
            }
            this._reels[_loc_4] = _loc_5;
            _loc_4 = _loc_4 + 1;
        }
        return;
    }

    p.parseJackpot = function (param1, param2) {
        if (!param1) {
            return;
        }
        var _loc_3 = { type: "jackpot", kind: param2, sum: param1 };
        this._targ.items.push(_loc_3);
        return;
    }

    p.parsePrizeMap = function () {
        var _loc_4 = 0;
        var _loc_5 = 0;
        var _loc_6 = 0;
        var _loc_7 = null;
        var _loc_1 = this._reels.length;
        var _loc_2 = this._reels[0].length;
        var _loc_3 = Array(_loc_1);
        _loc_4 = _loc_1;
        while (_loc_4--) {

            _loc_3[_loc_4] = Array(_loc_2);
        }
        _loc_4 = this._targ.items.length;
        while (_loc_4--) {

            _loc_7 = this._targ.items[_loc_4];
            switch (_loc_7.type) {
                case "line":
                    {
                        _loc_5 = _loc_7.size;
                        while (_loc_5--) {

                            _loc_6 = Game.Cfg.linePoints[_loc_7.id][_loc_5]; //TODO check
                            if ((_loc_3[_loc_5][_loc_6] == undefined ? 0 : _loc_3[_loc_5][_loc_6]) < _loc_7.size) {
                                _loc_3[_loc_5][_loc_6] = _loc_7.size;
                            }
                        }
                        break;
                    }
                case "scat":
                    {
                        _loc_5 = _loc_7.points.length;
                        while (_loc_5--) {

                            _loc_6 = _loc_7.points[_loc_5];
                            if (_loc_6 == -1) {
                                continue;
                            }
                            if (_loc_3[_loc_5][_loc_6] < _loc_7.size) {
                                _loc_3[_loc_5][_loc_6] = _loc_7.size;
                            }
                        }
                        break;
                    }
                case "free":
                    {
                        if (_loc_7.type == "end") {
                            break;
                        }
                        _loc_5 = _loc_7.points.length;
                        while (_loc_5--) {

                            _loc_6 = _loc_7.points[_loc_5];
                            if (_loc_6 == -1) {
                                continue;
                            }
                            if (_loc_3[_loc_5][_loc_6] < _loc_7.size) {
                                _loc_3[_loc_5][_loc_6] = _loc_7.size;
                            }
                        }
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
        this._targ.map = _loc_3;
        return;
    }

    p.calcReels = function () {
        var _loc_1 = null;
        var _loc_4 = null;
        var _loc_2 = this._targ.lines;
        var _loc_3 = 0;
        while (_loc_3 < _loc_2) {

            _loc_1 = this.calcLine(_loc_3);
            if (_loc_1) {
                if (this._targ.free.get_isActive()) {
                    _loc_1.set_sum(_loc_1.get_sum() * this._targ.free.get_factor());
                }
                this._targ.items.push(_loc_1);
            }
            _loc_3 = _loc_3 + 1;
        }
        _loc_1 = this.calcScat();
        if (_loc_1) {
            if (this._targ.free.get_isActive()) {
                _loc_1.set_sum(_loc_1.get_sum() * this._targ.free.get_factor());
            }
            this._targ.items.push(_loc_1);
        }
        if (this._targ.free.get_game() && this._targ.free.get_games()) {
            _loc_1 = this.calcSpec();
            if (_loc_1) {
                _loc_4 = { type: "specPrepare", id: this._targ.get_idSpec(), tiles: this._reels, points: _loc_1.get_points(), size: _loc_1.get_size() };
                this._targ.items.push(_loc_4);
                _loc_2 = this._targ.get_lines();
                _loc_3 = 0;
                while (_loc_3 < _loc_2) {

                    _loc_4 = { type: "spec", points: _loc_1.get_points(), factor: _loc_1.get_factor(), sum: _loc_1.get_sum(), id: _loc_3 };
                    this._targ.items.push(_loc_4);
                    _loc_3 = _loc_3 + 1;
                }
            }
        }
        return;
    }

    p.calcLine = function (param1) {
        var _loc_2 = null;
        var _loc_5 = 0;
        var _loc_6 = null;
        var _loc_8 = 0;
        var _loc_3 = this.getLineTiles(param1);
        var _loc_4 = _loc_3[0];
        var _loc_7 = 0;
        _loc_6 = this.getStdComb(_loc_3);
        _loc_4 = _loc_6.lead;
        _loc_5 = _loc_6.size;
        _loc_8 = _loc_6.wilds.length;
        _loc_7 = Game.Cfg.pays[_loc_4][(_loc_5 - 1)];
        if (_loc_4 == Game.Cfg.idScat) {
            _loc_7 = _loc_5 == 5 ? (Game.Cfg.pays[8][4]) : (0);
        }
        if (!_loc_7) {
            return null;
        }
        return { type: "line", id: param1, lead: _loc_6.lead, size: _loc_6.size, wilds: _loc_6.wilds, factor: _loc_7, sum: _loc_7 * this._targ.bet };
    }

    p.getLineTiles = function (param1) {
        var _loc_6 = 0;
        var _loc_2 = Game.Cfg.linePoints[param1];
        var _loc_3 = _loc_2.length;
        var _loc_4 = Array(_loc_3);
        var _loc_5 = 0;
        while (_loc_5 < _loc_3) {

            _loc_6 = _loc_2[_loc_5];
            _loc_4[_loc_5] = this._reels[_loc_5][_loc_6];
            _loc_5 = _loc_5 + 1;
        }
        return _loc_4;
    }

    p.getPureComb = function (param1) {
        var _loc_6 = 0;
        var _loc_2 = param1.length;
        var _loc_3 = param1[0];
        var _loc_4 = 1;
        var _loc_5 = 1;
        while (_loc_5 < _loc_2) {

            _loc_6 = param1[_loc_5];
            if (_loc_6 != _loc_3) {
                break;
            }
            _loc_4 = _loc_5 + 1;
            _loc_5 = _loc_5 + 1;
        }
        return { lead: _loc_3, size: _loc_4 };
    }

    p.getStdComb = function (param1) {
        var _loc_7 = 0;
        var _loc_2 = param1.length;
        var _loc_3 = param1[0];
        var _loc_4 = 1;
        var _loc_5 = Game.Cfg.idWild;
        var _loc_6 = this._targ.free.get_isActive() ? (this._targ.spec.get_tile()) : (-1);
        var _loc_8 = [];
        var _loc_9 = 0;
        while (_loc_9 < _loc_2) {

            _loc_7 = param1[_loc_9];
            if (_loc_7 == _loc_5) {
            }
            else {
                _loc_3 = _loc_7;
                break;
            }
            _loc_9 = _loc_9 + 1;
        }
        if (_loc_3 == _loc_6) {
            _loc_3 = param1[0];
        }
        _loc_9 = 0;
        while (_loc_9 < _loc_2) {

            _loc_7 = param1[_loc_9];
            if (_loc_7 == _loc_3) {
                _loc_4 = _loc_9 + 1;
            }
            else if (_loc_3 != _loc_6 && _loc_7 == _loc_5) {
                _loc_4 = _loc_9 + 1;
                _loc_8.push(_loc_9);
            }
            else {
                break;
            }
            _loc_9 = _loc_9 + 1;
        }
        return { lead: _loc_3, size: _loc_4, wilds: _loc_8 };
    }

    p.calcScat = function () {
        var _loc_5 = null;
        var _loc_1 = this._reels.length;
        var _loc_2 = new Array(_loc_1);
        var _loc_3 = 0;
        var _loc_4 = 0;
        while (_loc_1--) {

            _loc_5 = this._reels[_loc_1];
            _loc_2[_loc_1] = _loc_5.indexOf(Game.Cfg.idScat);
            if (_loc_2[_loc_1] != -1) {
                _loc_3 = _loc_3 + 1;
            }
        }
        if (_loc_3) {
            _loc_4 = Game.Cfg.pays[Game.Cfg.idScat][(_loc_3 - 1)];
        }
        if (!_loc_4) {
            return null;
        }
        return { type: "scat", size: _loc_3, points: _loc_2, factor: _loc_4, sum: _loc_4 * this._targ.bet * this._targ.lines };
    }

    p.calcSpec = function () {
        var _loc_6 = null;
        var _loc_1 = this._reels.length;
        var _loc_2 = this._targ.spec.get_tile();
        var _loc_3 = new Array(_loc_1);
        var _loc_4 = 0;
        var _loc_5 = 0;
        while (_loc_1--) {

            _loc_6 = this._reels[_loc_1];
            _loc_3[_loc_1] = _loc_6.indexOf(_loc_2);
            if (_loc_3[_loc_1] != -1) {
                _loc_4 = _loc_4 + 1;
            }
        }
        if (_loc_4) {
            _loc_5 = Game.Cfg.pays[_loc_2][(_loc_4 - 1)];
        }
        if (!_loc_5) {
            return null;
        }
        return { points: _loc_3, size: _loc_4, factor: _loc_5, sum: _loc_5 * this._targ.get_bet() };
    }

    p.checkTrig = function () {
        var _loc_4 = 0;
        var _loc_6 = null;
        var _loc_1 = this._reels.length;
        var _loc_2 = new Array(_loc_1);
        var _loc_3 = 0;
        while (_loc_1--) {

            _loc_6 = this._reels[_loc_1];
            _loc_2[_loc_1] = _loc_6.indexOf(Game.Cfg.idTrig);
            if (_loc_2[_loc_1] != -1) {
                _loc_3 = _loc_3 + 1;
            }
        }
        if (_loc_3) {
            _loc_4 = Game.Cfg.freeCounts[(_loc_3 - 1)];
        }
        if (!_loc_4) {
            return;
        }
        var _loc_5 = { type: "free", idSpec: this._targ.get_idSpec(), kind: this._targ.free.get_games() ? ("again") : ("begin"), points: _loc_2, size: _loc_3, games: _loc_4 };
        this._targ.items.push(_loc_5);
        return;
    }

    window.Game.MVC.Calculator = Calculator;

})(window);