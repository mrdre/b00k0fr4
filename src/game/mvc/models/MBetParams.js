﻿(function (window) {
    function MBetParams() {
        // this._bet:uint;
        // this._lines:uint;
        // this._total:uint;
        this._shown = false;

        // p.MBetParams()
        // {
        this._bet = Game.Cfg.bets[0];
        this._lines = 9;
        this._total = this._bet * this._lines;
        return;
    }

    var p = MBetParams.prototype;
    createjs.EventDispatcher.initialize(p);

    p.get_bet = function () {
        return this._bet;
    }

    p.set_bet = function (param1) {
        this._bet = param1;
        this._total = this._bet * this._lines;
        this._shown = true;
        this.dispatchEvent(Game.MVC.MEvent.TOTAL_BET);
        return;
    }

    p.get_lines = function () {
        return this._lines;
    }

    p.set_lines = function (param1) {
        this._lines = param1;
        this._total = this._bet * this._lines;
        this._shown = true;
        this.dispatchEvent(Game.MVC.MEvent.TOTAL_BET);
        return;
    }

    p.get_total = function () {
        return this._total;
    }

    p.get_shown = function () {
        return this._shown;
    }

    p.hide = function () {
        this._shown = false;
        this.dispatchEvent(Game.MVC.MEvent.TOTAL_BET);
        return;
    }

    window.Game.MVC.MBetParams = MBetParams;

})(window);