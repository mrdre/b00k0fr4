﻿(function (window) {
    function MError() {
        // this._type:String;
        // this._message:String;

        // p.MError()
        // {
        return;
    }
    var p = MError.prototype;
    createjs.EventDispatcher.initialize(p);

    p.get_type = function () {
        return this._type;
    }

    p.get_message = function () {
        return this._message;
    }

    p.oops = function (param1, param2) {
        this._type = param1;
        this._message = param2;
        this.dispatchEvent(Game.MVC.MEvent.ERROR);
        return;
    }
    window.Game.MVC.MError = MError;


})(window);