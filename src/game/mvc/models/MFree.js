﻿(function (window) {
	function MFree() {
		// this._game:uint;
		// this._games:uint;
		// this._idSpec:int;
		// this._size:uint;
		// this._sum:uint;
		// this._points:Array;
		// this._factor:uint;
		// this._kind:String;

		// p.MFree()
		// {
		// return;

	    this._factor = Game.Cfg.freeFactor;
	}
	var p = MFree.prototype;
	createjs.EventDispatcher.initialize(p);

	p.get_game = function () {
		return this._game;
	}

	p.get_games = function () {
		return this._games;
	}

	p.get_idSpec = function () {
		return this._idSpec;
	}

	p.get_size = function () {
		return this._size;
	}

	p.get_sum = function () {
		return this._sum;
	}

	p.get_points = function () {
		return this._points;
	}

	p.get_factor = function () {
		return this._factor;
	}

	p.get_kind = function () {
		return this._kind;
	}

	p.get_isActive = function () {
		return this._games > 0;
	}

	p.get_isFinal = function () {
		return this._game == this._games && this._games > 0;
	}

	p.shown = function () {
		this.dispatchEvent(Game.MVC.MEvent.FREE_SHOWN);
		return;
	}

	p.setParams = function (param1, param2, param3, param4, param5) {
		this._kind = param1;
		this._idSpec = param2;
		this._games = this._games + param3;
		this._size = param4;
		this._points = param5;
		this.dispatchEvent(Game.MVC.MEvent.FREE_PREPARE);
		return;
	}

	p.execute = function () {
		if (this._kind == "begin") {
			this.dispatchEvent(Game.MVC.MEvent.FREE_BEGIN);
		}
		else if (this._kind == "again") {
			this.dispatchEvent(Game.MVC.MEvent.FREE_AGAIN);
		}
		return;
	}

	p.play = function () {
		var _loc_1 = this;
		var _loc_2 = this._game + 1;
		_loc_1._game = _loc_2;
		if (this._game > this._games) {
			return;
		}
		this.dispatchEvent(Game.MVC.MEvent.FREE_PLAY);
		return;
	}

	p.checkFreeEnd = function (param1) {
		if (!this.get_isFinal()) {
			return false;
		}
		this._sum = param1;
		this.dispatchEvent(Game.MVC.MEvent.FREE_END);
		return true;
	}

	p.clear = function () {
		this._game = 0;
		this._games = 0;
		this._sum = 0;
		return;
	}

	window.Game.MVC.MFree = MFree;

})(window);