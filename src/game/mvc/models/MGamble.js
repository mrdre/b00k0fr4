﻿(function(window){
		function MGamble(){
        // this._queue:Array;
        // this._factor:uint;
        // this._currPrize:uint;
        // this._gamesLeft:uint;
        // this._choice:int;
        this.busy = false;

        // p.MGamble()
        // {
            this._queue = [0, 1, 2, 3];
            while (this._queue.length < 6)
            {
                
                this._queue.push(Math.floor(Math.random() *(3)));
            }

            function shuffleStack(a) {
                var j, x, i;
                for (i = a.length; i; i -= 1) {
                    j = XMath.intRnd(i);
                    x = a[i - 1];
                    a[i - 1] = a[j];
                    a[j] = x;
                }
            }

            shuffleStack(this._queue);

            return;
        }
		var p = MGamble.prototype;
		createjs.EventDispatcher.initialize(p);

        p.get_queue= function(){
            return this._queue;
        }

        p.get_factor= function(){
            return this._factor;
        }

        p.get_currPrize= function(){
            return this._currPrize;
        }

        p.get_gamesLeft= function(){
            return this._gamesLeft;
        }

        p.get_choice= function(){
            return this._choice;
        }

        p.prepare= function(param1){
            this.busy = true;
            this._gamesLeft = Game.Cfg.gambleGames;
            this._currPrize = param1;
            this.dispatchEvent(new createjs.Event(Game.MVC.MEvent.GAMBLE_PREPARE));
            return;
        }

        p.nextGame= function(){
            if (this._gamesLeft)
            {
                var _loc_1 = this;
                var _loc_2 = this._gamesLeft - 1;
                _loc_1._gamesLeft = _loc_2;
                this.dispatchEvent(Game.MVC.MEvent.GAMBLE_ATTEMPT);
            }
            else
            {
                this.gambleEnd();
            }
            return;
        }

        p.set_choice= function(param1){
            this._choice = param1;
            if (this.get_choice() == 0)
            {
                this.gambleEnd();
            }
            else
            {
                this.dispatchEvent(Game.MVC.MEvent.GAMBLE_CHOICE);
            }
            return;
        }

        p.set_factor= function(param1){
            this._factor = param1;
            this._currPrize = this._currPrize * this.get_factor();
            var _loc_2 = this._choice == -1 ? (0) : (1);
            if (this._factor)
            {
                _loc_2 = 2 * _loc_2 + Math.floor(Math.random() *(1));
            }
            else
            {
                _loc_2 = 2 * (1 - _loc_2) + Math.floor(Math.random() *(1));
            }
            this._queue.unshift(_loc_2);
            this._queue.pop();
            this.dispatchEvent(Game.MVC.MEvent.GAMBLE_RES);
            return;
        }

        p.gambleEnd= function(){
            this.busy = false;
            this.dispatchEvent(new createjs.Event(Game.MVC.MEvent.GAMBLE_END));
            return;
        }
        window.Game.MVC.MGamble = MGamble;

    
})(window);