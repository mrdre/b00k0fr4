﻿(function (window) {
    function MGame() {
        // this.server:Server;
        // this.screen:MScreen;
        // this.pageSlots:MPageSlots;
        // this.money:MMoneyParams;
        // this.betParams:MBetParams;
        // this.slots:MSlots;
        // this.prizes:MPrizes;
        // this.gamble:MGamble;
        // this.help:MHelp;
        // this.error:MError;
        // this._prevGame:String;
        this.busy = false;


        this.server = new Game.Server(Game.Cfg.urlServer);
        this.screen = new Game.MVC.MScreen();
        this.pageSlots = new Game.MVC.MPageSlots();
        this.money = new Game.MVC.MMoneyParams();
        this.betParams = new Game.MVC.MBetParams();
        this.slots = new Game.MVC.MSlots();
        this.prizes = new Game.MVC.MPrizes();
        this.gamble = new Game.MVC.MGamble();
        this.help = new Game.MVC.MHelp();
        this.error = new Game.MVC.MError();
        this.prizes.bet = this.betParams.get_bet();
        this.prizes.lines = this.betParams.get_lines();
        this.prizes.denom = this.money.get_denom();
    }
    var p = MGame.prototype;
    createjs.EventDispatcher.initialize(p);

    p.initGame = function () {
        //console.log('D:\OpenServer\domains\test\src\game\mvc\models\MGame.js 36-40')
        this.server.addEventListener(Game.SrvEvent.INIT, this.handleServerInit.bind(this));
        this.server.addEventListener(Game.SrvEvent.SPIN, this.handleServerSpin.bind(this));
        this.server.addEventListener(Game.SrvEvent.GAMBLE, this.handleServerGamble.bind(this));
        this.server.addEventListener(Game.SrvEvent.TAKE, this.handleServerTake.bind(this));
        this.server.addEventListener(Game.SrvEvent.ERROR, this.handleServerError.bind(this));
        this.server.askInit(Game.TXT.langs.join("|"));
        return;
    }

    p.runGame = function () {
        this.gameOver();
        Game.Gfx.visible = true;
        this.screen.set_page("slots");
        this.pageSlots.set_mode("normal");
        return;
    }

    p.showHelp = function () {
        if (this.screen.get_page() != "help") {
            this.screen.set_page("help");
            this.help.set_page(0);
        }
        else {
            var _loc_1 = this.help;
            var _loc_2 = this.help.get_page() + 1;
            _loc_1.set_page(_loc_2);
            if (this.help.get_complete()) {
                this.screen.set_page("slots");
                this.gameOver();
            }
        }
        return;
    }

    p.setNextDenom = function () {
        var _loc_1 = parseInt(XMath.nextItem(this.money.get_denom(), Game.Cfg.denoms));
        this.money.set_denom(_loc_1);
        this.prizes.denom = _loc_1;
        this.prizes.jackpot.set_denom(_loc_1);
        return;
    }

    p.setLines = function (param1) {
        this.betParams.set_lines(param1);
        this.prizes.lines = param1;
        return;
    }

    p.setNextBet = function () {
        var _loc_1 = parseInt(XMath.nextItem(this.betParams.get_bet(), Game.Cfg.bets));
        this.betParams.set_bet(_loc_1);
        this.prizes.bet = _loc_1;
        return;
    }

    p.setMaxBet = function () {
        var _loc_1 = this.money.get_credits() / this.betParams.get_lines();
        var _loc_2 = Game.Cfg.bets.length;
        var _loc_3 = Game.Cfg.bets[0];
        var _loc_4 = 0;
        while (_loc_4 < _loc_2) {

            if (Game.Cfg.bets[_loc_4] > _loc_1) {
                break;
            }
            _loc_3 = Game.Cfg.bets[_loc_4];
            _loc_4 = _loc_4 + 1;
        }
        this.betParams.set_bet(_loc_3);
        this.prizes.bet = _loc_3;
        return;
    }

    p.decreaseTotalBet = function () {
        var _loc_5 = 0;
        var _loc_1 = this.money.get_credits();
        var _loc_2 = this.betParams.get_bet();
        var _loc_3 = this.betParams.get_lines();
        var _loc_4 = this.betParams.get_total();
        _loc_5 = Game.Cfg.bets.indexOf(this.betParams.get_bet());
        while (_loc_5-- > 0) {

            _loc_2 = Game.Cfg.bets[_loc_5];
            _loc_4 = _loc_2 * this.betParams.get_lines();
            if (_loc_4 <= _loc_1) {
                break;
            }
        }
        if (_loc_4 <= _loc_1) {
            this.betParams.set_bet(_loc_2);
            this.prizes.bet = _loc_2;
            return;
        }
        _loc_5 = Game.Cfg.lines.indexOf(this.betParams.get_lines());
        while (_loc_5-- > 0) {

            _loc_3 = Game.Cfg.lines[_loc_5];
            _loc_4 = _loc_2 * _loc_3;
            if (_loc_4 <= _loc_1) {
                break;
            }
        }
        if (_loc_4 <= _loc_1) {
            this.betParams.set_bet(_loc_2);
            this.betParams.set_lines(_loc_3);
            this.prizes.bet = _loc_2;
            this.prizes.lines = _loc_3;
            return;
        }
        return;
    }

    p.playSpin = function () {
        this.busy = true;
        if (this.prizes.free.get_isActive()) {
            this._prevGame = "free";
            this.slots.set_state("free");
            this.prizes.clearCurrentWin();
            this.money.clearCurrentWin();
            this.prizes.free.play();
        }
        else {
            this._prevGame = "spin";
            this.slots.set_state("norm");
            this.prizes.clearTotalWin();
            this.money.clearTotalWin();
            this.pageSlots.set_status("");
            this.money.payBet(this.betParams.get_total());
        }
        this.betParams.hide();
        this.slots.spin();
        this.server.askSpin(this.money.get_denom(), this.betParams.get_bet(), this.betParams.get_lines(), this.slots.get_activeReels());
        return;
    }

    p.afterSpin = function () {
        var _loc_1 = this.server.response;
        this.prizes.findPrizes(_loc_1.slots, _loc_1.idSpec, _loc_1.jpSum, _loc_1.jpKind);
        this.nextPrize();
        return;
    }

    p.nextPrize = function () {
        if (!this.prizes.get_isEmpty()) {
            this.prizes.nextPrize();
            switch (this.prizes.get_currWinType()) {
                case "line":
                case "scat":
                case "jackpot":
                case "spec":
                    this.money.set_prizeRaw(this.prizes.get_currWinSum());
                    break;
                case "free":
            }
        }
        else if (!this.prizes.chekFreeEnd()) {
            if (this.prizes.free.get_isActive()) {
                this.playSpin();
            }
            else if (this.pageSlots.get_isAuto()) {
                if (this.prizes.get_rawTotal()) {
                    this.takePrize();
                }
                else {
                    this.tryAuto();
                }
            }
            else if (this.prizes.get_rawTotal()) {
                this.waitPrize();
            }
            else {
                this.gameOver();
            }
        }
    }

    p.executeFree = function () {
        this.prizes.free.execute();
        this.pageSlots.set_mode("bonus");
        return;
    }

    p.endFree = function () {
        this.pageSlots.set_mode("normal");
        return;
    }

    p.takePrize = function () {
        this.server.askTake();
        if (this._prevGame == "gamble") {
            this.gambleEnd();
        }
        return;
    }

    p.afterTake = function () {
        if (this.pageSlots.get_isAuto()) {
            this.tryAuto();
        }
        else {
            this.gameOver();
        }
        return;
    }

    p.gambleStart = function () {
        this._prevGame = "gamble";
        this.screen.set_page("gamble");
        this.prizes.waitEnd();
        this.gamble.prepare(this.prizes.get_winTotal());
        return;
    }

    p.gambleChoice = function (param1) {
        this.gamble.set_choice(param1);
        this.server.askGamble();
        return;
    }

    p.gambleEnd = function () {
        this.screen.set_page("slots");
        this.gamble.set_choice(0);
        this.busy = false;
        return;
    }

    p.gameOver = function () {
        var _loc1_ = null;
        this.busy = false;
        switch (this._prevGame) {
            case "gamble":
                _loc1_ = Game.TXT.GAMBLE_OVER;
                break;
            case "free":
                _loc1_ = Game.TXT.FREE_OVER;
                break;
            case "spin":
            default:
                _loc1_ = !!this.money.get_balance() ? Game.TXT.GAME_OVER : Game.TXT.COIN_OVER;
        }
        this.pageSlots.gameOver(Game.TXT.getText(_loc1_));
    }

    p.tryChangeAuto = function () {
        if (this.pageSlots.get_isAuto()) {
            this.pageSlots.set_isAuto(false);
        }
        else if (this.money.get_credits() >= this.betParams.get_total()) {
            this.pageSlots.set_isAuto(true);
            if (!this.busy) {
                setTimeout(this.playSpin.bind(this), 500);
            }
        }
        return;
    }

    p.tryAuto = function () {
        if (this.pageSlots.get_isAuto()) {
            if (this.money.get_credits() < this.betParams.get_total()) {
                this.decreaseTotalBet();
            }
            if (this.money.get_credits() < this.betParams.get_total()) {
                this.pageSlots.set_isAuto(false);
            }
        }
        if (this.pageSlots.get_isAuto()) {
            this.playSpin();
        }
        else if (this.prizes.get_rawTotal()) {
            this.waitPrize();
        }
        else {
            this.gameOver();
        }
        return;
    }

    p.waitPrize = function () {
        this.prizes.wait();
        return;
    }

    p.handleServerInit = function (event) {

        //Game.box._targ.instance.instance.visible = false; //костыль
        //Game.box._targ.container.visible = false; //костыль

        this.server.removeAllEventListeners(Game.SrvEvent.INIT, this.handleServerInit);
        var _loc_2 = this.server.response;
        Game.Cfg.urlExit = _loc_2.exitURL;
        Game.Cfg.targetExit = _loc_2.exitTarg;
        Game.Cfg.parseDenoms(_loc_2.denoms);
        Game.TXT.lng = _loc_2.lang;
        this.money.set_balance(_loc_2.credits);
        this.money.set_currency(_loc_2.currency);
        this.money.set_denom(Game.Cfg.denoms[0]);
        this.prizes.denom = Game.Cfg.denoms[0];
        this.prizes.jackpot.set_denom(Game.Cfg.denoms[0]);
        this.prizes.jackpot.set_currency(_loc_2.currency);
        Game.Gfx.prepareJackpotComponents(_loc_2.jpComps, this.runGame.bind(this));
        return;
    }

    p.handleServerSpin = function (event) {
        var _loc_2 = this.server.response;
        this.slots.set_tilesFin(_loc_2.slots);
        if (!this.prizes.free.get_isActive()) {
            this.prizes.set_idSpec(_loc_2.idSpec);
        }
        return;
    }

    p.handleServerGamble = function (event) {
        var _loc_2 = this.server.response;
        this.gamble.set_factor(_loc_2.factorGamble);
        this.prizes.set_gambleFactor(_loc_2.factorGamble);
        this.money.set_gambleFactor(_loc_2.factorGamble);
        return;
    }

    p.handleServerTake = function (event) {
        var _loc_2 = this.server.response;
        this.prizes.waitEnd();
        this.money.set_balancePlusWin(_loc_2.credits);
        return;
    }

    p.handleServerError = function (event) {
        var _loc_2 = this.server.response;
        this.screen.set_page("error");
        this.error.oops(_loc_2.errType, _loc_2.errMsg);
        return;
    }

    //p.init= function(){
    //    this.server = new Server(Game.Cfg.urlServer);
    //    this.screen = new MScreen();
    //    this.pageSlots = new MPageSlots();
    //    this.money = new MMoneyParams();
    //    this.betParams = new MBetParams();
    //    this.slots = new MSlots();
    //    this.prizes = new MPrizes();
    //    this.gamble = new MGamble();
    //    this.help = new MHelp();
    //    this.error = new MError();
    //    this.prizes.bet = this.betParams.get_bet(;
    //    this.prizes.lines = this.betParams.lines;
    //    this.prizes.denom = this.money.denom;
    //    return;
    //}
    window.Game.MVC.MGame = MGame;


})(window);