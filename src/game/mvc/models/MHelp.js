﻿(function(window){
		function MHelp(){
        // this._pages:uint;
        // this._page:uint;

        // p.MHelp()
        // {
            this._pages = Game.Gfx.helpVecPage.length;
            return;
        }
		var p = MHelp.prototype;
		createjs.EventDispatcher.initialize(p);

        p.get_complete= function(){
            return this._page >= this._pages;
        }

        p.get_page= function(){
            return this._page;
        }

        p.set_page= function(param1){
            this._page = param1;
            if (this._page < this._pages)
            {
                this.dispatchEvent(Game.MVC.MEvent.HELP_SHOW);
            }
            return;
        }
        window.Game.MVC.MHelp = MHelp;

})(window);