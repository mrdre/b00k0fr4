﻿(function (window) {
	function MJackpot() {
		// this._currency:String;
		// this._denom:uint;
		// this._kind:String;
		// this._sum:uint;

		// p.MJackpot()
		// {
		return;
	}
	var p = MJackpot.prototype;
	createjs.EventDispatcher.initialize(p);

	p.get_currency = function () {
		return this._currency;
	}

	p.set_currency = function (param1) {
		this._currency = param1;
		return;
	}

	p.get_denom = function () {
		return this._denom;
	}

	p.set_denom = function (param1) {
		this._denom = param1;
		return;
	}

	p.get_kind = function () {
		return this._kind;
	}

	p.get_sum = function () {
		return this._sum;
	}

	p.skip = function () {
		this.dispatchEvent(Game.MVC.MEvent.JACKPOT_SKIP);
		return;
	}

	p.setParams = function (param1, param2) {
		this._kind = param1;
		this._sum = param2;
		this.dispatchEvent(Game.MVC.MEvent.JACKPOT);
		return;
	}

	p.clear = function () {
		this._kind = null;
		this._sum = 0;
		return;
	}

	window.Game.MVC.MJackpot = MJackpot;

})(window);