﻿(function(window){
		function MMoneyParams(){
        // this._currency:String;
        // this._denom:uint;
        // this._balance:uint;
        // this._income:uint;
        // this._prize:uint;
        // this._winCurrent:uint;
        // this._winTotal:uint;

        // p.MMoneyParams()
        // {
		    this._denom = Game.Cfg.denoms[0];
		    return;
        }
		var p = MMoneyParams.prototype;
		createjs.EventDispatcher.initialize(p);

        p.payBet= function(param1){
            var _loc_2 = param1 * this.get_denom();
            if (_loc_2 > this._balance)
            {
                throw new Error("Can\'t pay bet " + _loc_2);
            }
            this._balance = this._balance - _loc_2;
            this.dispatchEvent(Game.MVC.MEvent.BALANCE_OUTGO);
            return;
        }

        p.startCurrentPrizes= function(){
            this._winCurrent = 0;
            return;
        }

        p.endTakePrize= function(){
            this._winCurrent = 0;
            this._winTotal = 0;
            return;
        }

        p.get_currency= function(){
            return this._currency;
        }

        p.set_currency= function(param1){
            this._currency = param1;
            this.dispatchEvent(Game.MVC.MEvent.CURRENCY);
            return;
        }

        p.get_denom= function(){
            return this._denom;
        }

        p.set_denom= function(param1){
            this._denom = param1;
            this.dispatchEvent(Game.MVC.MEvent.DENOM);
            return;
        }

        p.get_credits= function(){
            return this._balance / this._denom;
        }

        p.get_balance= function(){
            return this._balance;
        }

        p.set_balance= function(param1){
            this._balance = param1;
            this.dispatchEvent(Game.MVC.MEvent.BALANCE);
            return;
        }

        p.get_prize= function(){
            return this._prize;
        }

        p.get_winCurrentRaw= function(){
            return this._winCurrent;
        }

        p.get_winTotalRaw= function(){
            return this._winTotal;
        }

        p.get_winCurrentCred= function(){
            return this._winCurrent / this._denom;
        }

        p.get_winTotalCred= function(){
            return this._winTotal / this._denom;
        }

        p.set_prizeCred= function(param1){
            this._prize = param1;
            this._winCurrent = this._winCurrent + this._prize * this._denom;
            this._winTotal = this._winTotal + this._prize * this._denom;
            this.dispatchEvent(Game.MVC.MEvent.INC_PRIZE);
            return;
        }

        p.set_prizeRaw= function(param1){
            this._prize = param1 / this._denom;
            this._winCurrent = this._winCurrent + param1;
            this._winTotal = this._winTotal + param1;
            this.dispatchEvent(Game.MVC.MEvent.INC_PRIZE);
            return;
        }

        p.set_gambleFactor= function(param1){
            this._winTotal = this._winTotal * param1;
            this.dispatchEvent(Game.MVC.MEvent.GAMBLE_FACTOR);
            return;
        }

        p.clearCurrentWin= function(){
            this._prize = 0;
            this._winCurrent = 0;
            return;
        }

        p.clearTotalWin= function(){
            this.clearCurrentWin();
            this._winTotal = 0;
            this.dispatchEvent(Game.MVC.MEvent.PRIZE_CLEAR);
            return;
        }

        p.get_income= function(){
            return this._income;
        }

        p.set_balancePlusWin = function (param1) {
            param1 = this._balance + this._winTotal;
            //if (this._balance + this._winTotal !== param1) //TODO uncomment on production
            //{
            //    throw new Error("oldBalance + win != newBalance.");
            //}
            this._income = (param1 - this._balance) / this._denom;
            this._balance = param1;
            this.dispatchEvent(Game.MVC.MEvent.BALANCE_INCOME);
            return;
        }

        window.Game.MVC.MMoneyParams = MMoneyParams;

})(window);