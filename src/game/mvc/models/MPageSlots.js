﻿(function (window) {
    function MPageSlots() {
        // this._mode:String;
        this._isAuto = false;
        // this._status:String;

        // p.MPageSlots()
        // {
        return;
    }
    var p = MPageSlots.prototype;
    createjs.EventDispatcher.initialize(p);

    p.get_mode = function () {
        return this._mode;
    }

    p.set_mode = function (param1) {
        this._mode = param1;
        this.dispatchEvent(Game.MVC.MEvent.PAGE_SLOTS_MODE);
        return;
    }

    p.get_isAuto = function () {
        return this._isAuto;
    }

    p.set_isAuto = function (param1) {
        this._isAuto = param1;
        this.dispatchEvent(Game.MVC.MEvent.AUTO);
        return;
    }

    p.get_status = function () {
        return this._status;
    }

    p.set_status = function (param1) {
        this._status = param1;
        this.dispatchEvent(Game.MVC.MEvent.PAGE_SLOTS_STATUS);
        return;
    }

    p.gameOver = function (param1) {
        this._status = param1;
        this.dispatchEvent(Game.MVC.MEvent.GAME_OVER);
        return;
    }

    window.Game.MVC.MPageSlots = MPageSlots;

})(window);