﻿(function (window) {
    function MPrizes() {
        // var bet:uint;
        // var lines:uint;
        // var denom:uint;
        // var items:Array;
        // this.map:Vector.<Vector.<uint>>;
        // this.line:MPrzLine;
        // this.scat:MPrzScat;
        // this.spec:MPrzSpec;
        // this.jackpot:MJackpot;
        // this.free:MFree;
        // this._calc:Calculator;
        // this._idSpec:int;
        // this._currWinSum:uint;
        // this._currWinType:String;
        // this._winCurrent:uint;
        // this._winTotal:uint;
        // this._rawTotal:uint;

        this.line = new Game.MVC.MPrzLine();
        this.scat = new Game.MVC.MPrzScat();
        this.spec = new Game.MVC.MPrzSpec();
        this.jackpot = new Game.MVC.MJackpot();
        this.free = new Game.MVC.MFree();
        this._calc = new Game.MVC.Calculator(this);
    }
    var p = MPrizes.prototype;
    createjs.EventDispatcher.initialize(p);

    p.get_idSpec = function () {
        return this._idSpec;
    }

    p.set_idSpec = function (param1) {
        this._idSpec = param1;
        return;
    }

    p.get_isEmpty = function () {
        return this.items.length == 0;
    }

    p.get_currWinSum = function () {
        return this._currWinSum;
    }

    p.get_currWinType = function () {
        return this._currWinType;
    }

    p.get_winCurrent = function () {
        return this._winCurrent;
    }

    p.get_winTotal = function () {
        return this._winTotal;
    }

    p.get_rawTotal = function () {
        return this._rawTotal;
    }

    p.set_gambleFactor = function (param1) {
        this._rawTotal = this._rawTotal * param1;
        this._winTotal = this._rawTotal / this.denom;
        return;
    }

    p.findPrizes = function (param1, param2, param3, param4) {
        this.jackpot.clear();
        this._calc.findPrizes(param1, param2, param3, param4);
        this.dispatchEvent(Game.MVC.MEvent.SPIN_PRIZES);
        return;
    }

    p.nextPrize = function () {
        var _loc1_ = null; //TODO set get
        if (this.items.length) {
            _loc1_ = this.items.shift();
            switch (_loc1_.type) {
                case "line":
                    this._currWinSum = _loc1_.sum * this.denom;
                    this._currWinType = "line";
                    this.line.setParams(_loc1_.id, _loc1_.lead, _loc1_.size, _loc1_.wilds, _loc1_.sum);
                    break;
                case "scat":
                    this._currWinSum = _loc1_.sum * this.denom;
                    this._currWinType = "scat";
                    this.scat.setParams(_loc1_.size, _loc1_.points, _loc1_.sum);
                    break;
                case "specPrepare":
                    this._currWinType = "specPrepare";
                    this._currWinSum = 0;
                    this.spec.setParamsPrepare(_loc1_.size, _loc1_.tiles, _loc1_.points);
                    break;
                case "spec":
                    this._currWinType = "spec";
                    this._currWinSum = _loc1_.sum * this.denom;
                    this.spec.setParamsPrize(_loc1_.id, _loc1_.sum, _loc1_.points);
                    break;
                case "jackpot":
                    this._currWinSum = _loc1_.sum;
                    this._currWinType = "jackpot";
                    this.jackpot.setParams(_loc1_.kind, _loc1_.sum);
                    break;
                case "free":
                    this._currWinSum = 0;
                    this._currWinType = "free";
                    this.free.setParams(_loc1_.kind, _loc1_.idSpec, _loc1_.games, _loc1_.size, _loc1_.points);
                    this.spec.set_tile(this._idSpec);
            }
        }
        else {
            this._currWinType = null;
        }
        this._winCurrent = this._winCurrent + this._currWinSum / this.denom;
        this._rawTotal = this._rawTotal + this._currWinSum;
        this._winTotal = this._rawTotal / this.denom;
    }

    p.chekFreeEnd = function () {
        return this.free.checkFreeEnd(this._winTotal);
    }

    p.wait = function () {
        this.dispatchEvent(Game.MVC.MEvent.PRIZE_WAIT);
        return;
    }

    p.waitEnd = function () {
        this.dispatchEvent(Game.MVC.MEvent.PRIZE_WAIT_END);
        return;
    }

    p.clearCurrentWin = function () {
        this._winCurrent = 0;
        this._currWinSum = 0;
        this._currWinType = null;
        this.dispatchEvent(Game.MVC.MEvent.HIDE_PRIZES);
        return;
    }

    p.clearTotalWin = function () {
        this.clearCurrentWin();
        this._winTotal = 0;
        this._rawTotal = 0;
        return;
    }
    window.Game.MVC.MPrizes = MPrizes;


})(window);