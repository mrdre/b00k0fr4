﻿(function (window) {
    function MPrzLine() {
        // this._id:uint;
        // this._lead:uint;
        // this._size:uint;
        // this._wilds:Array;
        // this._sum:uint;

        // p.MPrzLine()
        // {
        return;
    }
    var p = MPrzLine.prototype;
    createjs.EventDispatcher.initialize(p);

    p.get_id = function () {
        return this._id;
    }

    p.get_lead = function () {
        return this._lead;
    }

    p.get_size = function () {
        return this._size;
    }

    p.get_wilds = function () {
        return this._wilds;
    }

    p.get_sum = function () {
        return this._sum;
    }

    p.setParams = function (param1, param2, param3, param4, param5) {
        this._id = param1;
        this._lead = param2;
        this._size = param3;
        this._wilds = param4;
        this._sum = param5;
        this.dispatchEvent(Game.MVC.MEvent.PRZ_LINE);
        return;
    }

    window.Game.MVC.MPrzLine = MPrzLine;

})(window);