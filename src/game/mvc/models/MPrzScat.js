﻿(function (window) {
	function MPrzScat() {
		// this._size:uint;
		// this._points:Array;
		// this._sum:uint;

		// p.MPrzScat()
		// {
		return;
	}
	var p = MPrzScat.prototype;
	createjs.EventDispatcher.initialize(p);

	p.get_size = function () {
		return this._size;
	}

	p.get_points = function () {
		return this._points;
	}

	p.get_sum = function () {
		return this._sum;
	}

	p.setParams = function (param1, param2, param3) {
		this._size = param1;
		this._points = param2;
		this._sum = param3;
		this.dispatchEvent(Game.MVC.MEvent.PRZ_SCAT);
		return;
	}

	window.Game.MVC.MPrzScat = MPrzScat;

})(window);