﻿(function (window) {
	function MPrzSpec() {
		// this._tile:int;
		// this._line:uint;
		// this._size:uint;
		// this._points:Array;
		// this._sum:uint;
		// this._tiles:Vector.<Vector.<uint>>;

		// p.MPrzSpec()
		// {

		this._tile = -1;
		return;
	}
	var p = MPrzSpec.prototype;
	createjs.EventDispatcher.initialize(p);

	p.set_tile = function (param1) {
		this._tile = param1;
		return;
	}

	p.get_tile = function () {
		return this._tile;
	}

	p.get_line = function () {
		return this._line;
	}

	p.get_size = function () {
		return this._size;
	}

	p.get_points = function () {
		return this._points;
	}

	p.get_sum = function () {
		return this._sum;
	}

	p.get_tiles = function () {
		return this._tiles;
	}

	p.setParamsPrepare = function (param1, param2, param3) {
		this._size = param1;
		this._tiles = param2;
		this._points = param3;
		this.dispatchEvent(Game.MVC.MEvent.PRZ_SPEC_PREPARE);
		return;
	}

	p.setParamsPrize = function (param1, param2, param3) {
		this._line = param1;
		this._sum = param2;
		this._points = param3;
		this.dispatchEvent(Game.MVC.MEvent.PRZ_SPEC);
		return;
	}
	window.Game.MVC.MPrzSpec = MPrzSpec;


})(window);