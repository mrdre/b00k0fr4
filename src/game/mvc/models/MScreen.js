﻿(function (window) {
    function MScreen() {
        // this._page:String;

        // p.MScreen()
        // {
        return;
    }
    var p = MScreen.prototype;
    createjs.EventDispatcher.initialize(p);

    p.get_page = function () {
        return this._page;
    }

    p.set_page = function (param1) {
        this._page = param1;
        this.dispatchEvent(new createjs.Event(Game.MVC.MEvent.PAGE));
        return;
    }

    window.Game.MVC.MScreen = MScreen;

})(window);