﻿(function (window) {
    // this._activeReels:Array;
    // this._tiles:Array;
    // this._state:String;
    MSlots.STATE_NORM = "norm";
    MSlots.STATE_FREE = "free";
    MSlots.REELS_COUNT = 5;
    MSlots.REELS_SIZE = 3;
    MSlots.TILES_COUNT = 15;

    // p.MSlots()
    // {
    function MSlots() {
        this._state = MSlots.STATE_NORM;
        this._activeReels = [0, 1, 2, 3, 4];
        this._tiles = [];
        this._tiles.length = MSlots.REELS_COUNT * MSlots.REELS_SIZE;
        return;
    }
    var p = MSlots.prototype;
    createjs.EventDispatcher.initialize(p);

    p.get_activeReels = function () {
        return this._activeReels;
    }

    p.set_activeReels = function (param1) {
        this._activeReels = param1;
        this._activeReels.sort();
        return;
    }

    p.get_state = function () {
        return this._state;
    }

    p.set_state = function (param1) {
        this._state = param1;
        return;
    }

    p.get_tiles = function () {
        return this._tiles;
    }

    p.set_tiles = function (param1) {
        this._tiles = param1;
        this._tiles.length = MSlots.TILES_COUNT;
        this.dispatchEvent(Game.MVC.MEvent.SLOTS_TILES);
        return;
    }

    p.set_tilesFin = function (param1) {
        var _loc_4 = 0;
        var _loc_5 = 0;
        var _loc_2 = param1.length / MSlots.REELS_SIZE;
        var _loc_3 = _loc_2;
        while (_loc_3--) {

            _loc_4 = this._activeReels[_loc_3];
            _loc_5 = MSlots.REELS_SIZE;
            while (_loc_5--) {

                this._tiles[MSlots.REELS_SIZE * _loc_4 + _loc_5] = param1[MSlots.REELS_SIZE * _loc_3 + _loc_5];
            }
        }
        this.dispatchEvent(Game.MVC.MEvent.SLOTS_FIN);
        return;
    }

    p.spin = function () {
        this.dispatchEvent(Game.MVC.MEvent.SLOTS_SPIN);
        return;
    }

    window.Game.MVC.MSlots = MSlots;

})(window);