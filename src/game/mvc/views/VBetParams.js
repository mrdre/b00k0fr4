﻿(function (window) {
    function VBetParams() {
        Game.Basis.BetParams.call(this);
        // this._model:MGame;

        // p.VBetParams()
        // {
        return;
    }
    var p = createjs.extend(VBetParams, Game.Basis.BetParams);
    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VBetParams.js 15')
        this._model.betParams.addEventListener(Game.MVC.MEvent.TOTAL_BET, this.handleTotalBet.bind(this));
        this.set_bet(this._model.betParams.get_bet());
        this.set_lines(this._model.betParams.get_lines());
        this.set_total(this._model.betParams.get_total());
        this.set_shown(false);
        this.updatePays();
        return;
    }

    p.handleTotalBet = function (event) {
        this.set_bet(this._model.betParams.get_bet());
        this.set_lines(this._model.betParams.get_lines());
        this.set_total(this._model.betParams.get_total());
        this.set_shown(this._model.betParams.get_shown());
        this.updatePays();
        if (this._model.betParams.get_shown()) {
            this.beep();
        }
        return;
    }

    window.Game.MVC.VBetParams = VBetParams;

})(window);