﻿(function(window){
		
        VEvent.ERROR = "error";
        VEvent.REELS_START = "reelsStart";
        VEvent.REELS_STOP = "reelsStop";
        VEvent.PRIZE_SHOWN = "prizeShown";
        VEvent.PRIZE_WAIT = "prizeWait";
        VEvent.COLLECTED = "collected";
        VEvent.GAME_OVER = "gameOver";
        VEvent.GAMBLE_READY = "gambleReady";
        VEvent.GAMBLE_RESULT = "gambleResult";
        VEvent.FREE_PREPARE = "freePrepare";
        VEvent.FREE_BEGIN = "freeBegin";
        VEvent.FREE_AGAIN = "freeAgain";
        VEvent.FREE_END = "freeEnd";
        VEvent.FREE_WAIT = "freeWait";
        VEvent.FREE_SHOWN = "freeShown";
        VEvent.JACKPOT = "jackpot";
        VEvent.SCREEN_INIT = "screenInit";
        VEvent.REELS_FINITED = "reelsFinited";
        VEvent.PRIZES_OVER = "prizesOver";
        VEvent.AUTO = "auto";
        VEvent.HELP_SHOWN = "helpShown";
        VEvent.SCR_PAGE_SHOWN = "scrPageShown";

        function VEvent(){}
        window.Game.MVC.VEvent = VEvent;

    
})(window);