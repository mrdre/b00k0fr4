﻿(function(window){
		function VGamble(){
		    Game.Basis.Gamble.call(this);
		    // this._model;

        // p.VGamble()
        // {
            return;
		}
		var p = createjs.extend(VGamble, Game.Basis.Gamble);
		createjs.EventDispatcher.initialize(p);

        p.set_model= function(param1){
            this._model = param1;
            //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VGamble.js 15 - 20')
            this._model.gamble.addEventListener(Game.MVC.MEvent.GAMBLE_PREPARE, this.handlePrepare.bind(this));
            this._model.gamble.addEventListener(Game.MVC.MEvent.GAMBLE_ATTEMPT, this.handleAttempt.bind(this));
            this._model.gamble.addEventListener(Game.MVC.MEvent.GAMBLE_CHOICE, this.handleChoice.bind(this));
            this._model.gamble.addEventListener(Game.MVC.MEvent.GAMBLE_RES, this.handleRes.bind(this));
            this._model.gamble.addEventListener(Game.MVC.MEvent.GAMBLE_END, this.handleEnd.bind(this));
            return;
        }

        p.handlePrepare= function(event){
            this.prepare(this._model.gamble.get_currPrize(), this._model.gamble.get_queue());
            return;
        }

        p.handleAttempt= function(event){
            this.attempt();
            return;
        }

        p.handleChoice= function(event){
            this.set_choice(this._model.gamble.get_choice());
            return;
        }

        p.handleRes= function(event){
            this.result(this._model.gamble.get_factor(), this._model.gamble.get_queue(), this._model.gamble.get_gamesLeft());
            return;
        }

        p.handleEnd= function(event){
            this.end();
            return;
        }

        p.onPrepare= function(){
            //super.onPrepare();
            this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.GAMBLE_READY));
            return;
        }

        p.onResult= function(){
            //super.onResult();
            this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.GAMBLE_RESULT));
            return;
        }
        window.Game.MVC.VGamble = VGamble;

        
})(window);