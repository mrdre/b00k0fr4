﻿(function (window) {
    function VHelp() {
        Game.Basis.Help.call(this);
        Game.MVC.MHelp.call(this);
        this.busy = false;
        // this._model;

        // p.VHelp()
        // { 
        return;
    }
    var p = createjs.extend(VHelp, Game.Basis.Help);
    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VHelp.js 16')
        this._model.help.addEventListener(Game.MVC.MEvent.HELP_SHOW, this.handleHelpShow.bind(this));
        return;
    }

    p.handleHelpShow = function (event) {
        this.show(this._model.help.get_page());
        return;
    }
    window.Game.MVC.VHelp = VHelp;


})(window);