﻿(function (window) {
    function VMoneyParams() {
        Game.Basis.MoneyParams.call(this);
        // this._model;

        // p.VMoneyParams()
        // {
        return;
    }
    var p = createjs.extend(VMoneyParams, Game.Basis.MoneyParams);
    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VMoneyParams.js 15 - 23')
        this._model.money.addEventListener(Game.MVC.MEvent.CURRENCY, this.handleCurrency.bind(this));
        this._model.money.addEventListener(Game.MVC.MEvent.DENOM, this.handleDenom.bind(this));
        this._model.money.addEventListener(Game.MVC.MEvent.BALANCE, this.handleBalance.bind(this));
        this._model.money.addEventListener(Game.MVC.MEvent.BALANCE_OUTGO, this.handleOutgo.bind(this));
        this._model.money.addEventListener(Game.MVC.MEvent.BALANCE_INCOME, this.handleIncome.bind(this));
        this._model.money.addEventListener(Game.MVC.MEvent.INC_PRIZE, this.handleIncPrize.bind(this));
        this._model.money.addEventListener(Game.MVC.MEvent.PRIZE_CLEAR, this.handlePrizeClear.bind(this));
        this._model.money.addEventListener(Game.MVC.MEvent.GAMBLE_FACTOR, this.handleGambleFactor.bind(this));
        return;
    }

    p.handleCurrency = function (event) {
        this.set_currency(this._model.money.get_currency());
        return;
    }

    p.handleDenom = function (event) {
        this.set_denom(this._model.money.get_denom());
        this.set_credits(this._model.money.get_credits());
        return;
    }

    p.handleBalance = function (event) {
        this.set_credits(this._model.money.get_credits());
        return;
    }

    p.handleOutgo = function (event) {
        this.set_credits(this._model.money.get_credits());
        return;
    }

    p.handleIncome = function (event) {
        this.collect(this._model.money.get_income(), this._model.money.get_credits());
        return;
    }

    p.handleIncPrize = function (event) {
        this.set_prize(this._model.money.get_prize());
        this.set_winCurrent(this._model.money.get_winCurrentCred());
        this.set_winTotal(this._model.money.get_winTotalCred());
        return;
    }

    p.handlePrizeClear = function (event) {
        this.set_winTotal(0);
        return;
    }

    p.handleGambleFactor = function (event) {
        this.set_winTotal(this._model.money.get_winTotalCred());
        return;
    }

    p.onCollect = function () {
        //super.onCollect();
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.COLLECTED));
        return;
    }

    window.Game.MVC.VMoneyParams = VMoneyParams;

})(window);