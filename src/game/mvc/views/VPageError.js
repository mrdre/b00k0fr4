﻿(function(window){
		function VPageError(){
		    Game.Basis.PageError.call(this);
		    // this._model;

        // p.VPageError()
        // {
            // return;
        }

		var p = createjs.extend(VPageError, Game.Basis.PageError);
		createjs.EventDispatcher.initialize(p);

        p.set_model= function(param1){
            this._model = param1;
            //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VPageError.js 16')
            this._model.error.addEventListener(Game.MVC.MEvent.ERROR, this.handleError.bind(this));
            return;
        }

        p.handleError= function(event){
            if (this._model.error.get_type() == "invalidation")
            {
                this.set_message(Game.TXT.getText(Game.TXT.INVALID));
            }
            else
            {
                this.set_message(this._model.error.get_type() + ":\n" + this._model.error.get_message());
            }
            this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.ERROR));
            return;
        }
        window.Game.MVC.VPageError = VPageError;
        
    
})(window);