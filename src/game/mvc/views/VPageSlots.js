﻿(function (window) {
    function VPageSlots() {
        Game.Basis.PageSlots.call(this);
        // this._model;

        // p.VPageSlots()
        // {
        // return;
    }
    var p = createjs.extend(VPageSlots, Game.Basis.PageSlots);
    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VPageSlots.js 15 - 20')
        this._model.pageSlots.addEventListener(Game.MVC.MEvent.AUTO, this.handleAuto.bind(this));
        this._model.pageSlots.addEventListener(Game.MVC.MEvent.PAGE_SLOTS_MODE, this.handleMode.bind(this));
        this._model.pageSlots.addEventListener(Game.MVC.MEvent.PAGE_SLOTS_STATUS, this.handleStatus.bind(this));
        this._model.pageSlots.addEventListener(Game.MVC.MEvent.GAME_OVER, this.handleGameOver.bind(this));
        return;
    }

    p.handleAuto = function (event) {
        this._isAuto = this._model.pageSlots._isAuto;
        return;
    }

    p.handleMode = function (event) {
        this.set_mode(this._model.pageSlots.get_mode());
        return;
    }

    p.handleStatus = function (event) {
        this.set_status(this._model.pageSlots.get_status());
        return;
    }

    p.handleGameOver = function (event) {
        this.set_status(this._model.pageSlots.get_status());
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.GAME_OVER));
        return;
    }

    p.onAuto = function () {
        //super.onAuto();
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.AUTO));
        return;
    }

    p.onMode = function () {
        //super.onMode();
        this.dispatchEvent(new createjs.Event(Game.MVC.MEvent.PAGE_SLOTS_MODE));
        return;
    }

    window.Game.MVC.VPageSlots = VPageSlots;

})(window);