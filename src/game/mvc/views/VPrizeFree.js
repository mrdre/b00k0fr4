﻿(function (window) {
    function VPrizeFree() {
        Game.Basis.PrizeFree.call(this);
        // this._model:MGame;

        // p.VPrizeFree()
        // {
        // return;
    }

    var p = createjs.extend(VPrizeFree, Game.Basis.PrizeFree);
    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VPrizeFree.js 15 - 20')
        this._model.prizes.free.addEventListener(Game.MVC.MEvent.FREE_PREPARE, this.handlePrepare.bind(this));
        this._model.prizes.free.addEventListener(Game.MVC.MEvent.FREE_BEGIN, this.handleBegin.bind(this));
        this._model.prizes.free.addEventListener(Game.MVC.MEvent.FREE_AGAIN, this.handleAgain.bind(this));
        this._model.prizes.free.addEventListener(Game.MVC.MEvent.FREE_END, this.handleEnd.bind(this));
        this._model.prizes.free.addEventListener(Game.MVC.MEvent.FREE_PLAY, this.handlePlay.bind(this));
        this._model.prizes.free.addEventListener(Game.MVC.MEvent.FREE_SHOWN, this.handleShown.bind(this));
        return;
    }

    p.handlePrepare = function (event) {
        this.prepare(this._model.prizes.free.get_kind(), this._model.betParams.get_bet(), this._model.prizes.free.get_size(), this._model.prizes.free.get_points(), this._model.prizes.free.get_games(), this._model.prizes.get_idSpec());
        Game.Comps.Tile.SPEC_ID = this._model.prizes.get_idSpec();
        return;
    }

    p.handleBegin = function (event) {
        this.execute(this._model.prizes.free.get_kind(), this._model.betParams.get_bet(), this._model.prizes.free.get_size(), this._model.prizes.free.get_points(), this._model.prizes.free.get_games(), this._model.prizes.get_idSpec());
        Game.Comps.Tile.SPEC_ID = this._model.prizes.get_idSpec();
        return;
    }

    p.handleAgain = function (event) {
        this.execute(this._model.prizes.free.get_kind(), this._model.betParams.get_bet(), this._model.prizes.free.get_size(), this._model.prizes.free.get_points(), this._model.prizes.free.get_games(), this._model.prizes.get_idSpec());
        return;
    }

    p.handleEnd = function (event) {
        this.finish(this._model.prizes.free.get_games(), this._model.prizes.free.get_sum());
        Game.Comps.Tile.SPEC_ID = -1;
        return;
    }

    p.handlePlay = function (event) {
        this.play(this._model.prizes.free.get_game(), this._model.prizes.free.get_games());
        return;
    }

    p.handleShown = function (event) {
        this.shown();
        return;
    }

    p.onPrepare = function () {
        //super.onPrepare();
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.FREE_PREPARE));
        return;
    }

    p.onStart = function () {
        //super.onStart();
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.FREE_WAIT));
        return;
    }

    p.onShown = function () {
        //super.onShown();
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.FREE_SHOWN));
        return;
    }

    p.onFinish = function () {
        //super.onFinish();
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.FREE_END));
        return;
    }
    window.Game.MVC.VPrizeFree = VPrizeFree;


})(window);