﻿(function (window) {
    function VPrizeJackpot() {
        Game.Basis.PrizeJackpot.call(this);
        // this._model:MGame;

        // p.VPrizeJackpot()
        // {
        return;
    }

    var p = createjs.extend(VPrizeJackpot, Game.Basis.PrizeJackpot);
    createjs.EventDispatcher.initialize(p);
    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VPrizeJackpot.js 15 - 20')
        this._model.prizes.jackpot.addEventListener(Game.MVC.MEvent.JACKPOT, this.handleJackpot.bind(this));
        this._model.prizes.jackpot.addEventListener(Game.MVC.MEvent.JACKPOT_SKIP, this.handleJackpotSkip.bind(this));
        return;
    }

    p.handleJackpot = function (event) {
        this.set_denom(this._model.money.get_denom());
        this.set_currency(this._model.money.get_currency());
        this.execute(this._model.prizes.jackpot.get_kind(), this._model.prizes.jackpot.get_sum());
        setTimeout(this.dispatchEvent, 1000, new createjs.Event(Game.MVC.VEvent.JACKPOT));
        return;
    }

    p.handleJackpotSkip = function (event) {
        this.skip();
        return;
    }

    p.onComplete = function () {
        //super.onComplete();
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.PRIZE_SHOWN));
        return;
    }

    window.Game.MVC.VPrizeJackpot = VPrizeJackpot;

})(window);