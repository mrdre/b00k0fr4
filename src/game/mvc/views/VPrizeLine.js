﻿(function(window){
		function VPrizeLine(){
		    Game.Basis.PrizeLine.call(this);
		    // this._model;

        // p.VPrizeLine()
        // {
            // return;
        }

		var p = createjs.extend(VPrizeLine, Game.Basis.PrizeLine);
		createjs.EventDispatcher.initialize(p);
        p.set_model= function(param1){
            this._model = param1;
            //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VPrizeLine.js 15 - 20')
            this._model.prizes.line.addEventListener(Game.MVC.MEvent.PRZ_LINE, this.handlePrzLine.bind(this));
            return;
        }

        p.handlePrzLine= function(event){
            this.execute(this._model.prizes.line.get_id(), this._model.prizes.line.get_lead(), this._model.prizes.line.get_size(), this._model.prizes.line.get_sum());
            return;
        }

        p.onComplete= function(){
            this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.PRIZE_SHOWN));
            return;
        }

        window.Game.MVC.VPrizeLine = VPrizeLine;
        
})(window);