﻿(function(window){
		function VPrizeScat(){
		    Game.Basis.PrizeScat.call(this);
		    // this._model;

        // p.VPrizeScat()
        // {
            return;
		}
		var p = createjs.extend(VPrizeScat, Game.Basis.PrizeScat);
		createjs.EventDispatcher.initialize(p);
		
        p.set_model= function(param1){
            this._model = param1;
            //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VPrizeScat.js 15 - 20')
            this._model.prizes.scat.addEventListener(Game.MVC.MEvent.PRZ_SCAT, this.handleRpzScat.bind(this));
            return;
        }

        p.handleRpzScat= function(event){
            this.execute(this._model.prizes.scat.get_size(), this._model.prizes.scat.get_points(), this._model.prizes.scat.get_sum());
            return;
        }

        p.onComplete= function(){
            //super.onComplete();
            this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.PRIZE_SHOWN));
            return;
        }

        window.Game.MVC.VPrizeScat = VPrizeScat;

})(window);