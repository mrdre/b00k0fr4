﻿(function (window) {
    function VPrizeSpecial() {
        Game.Basis.PrizeSpecial.call(this);
        // this._model:MGame;

        // p.VPrizeSpecial()
        // {
        // return;
    }

    var p = createjs.extend(VPrizeSpecial, Game.Basis.PrizeSpecial);
    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VPrizeSpecial.js 15 - 20')
        this._model.prizes.spec.addEventListener(Game.MVC.MEvent.PRZ_SPEC_PREPARE, this.handlePrepare.bind(this));
        this._model.prizes.spec.addEventListener(Game.MVC.MEvent.PRZ_SPEC, this.handleSpec.bind(this));
        return;
    }

    p.handlePrepare = function (event) {
        this.prepare(this._model.prizes.spec.get_tile(), this._model.prizes.spec.get_tiles(), this._model.prizes.spec.get_size());
        return;
    }

    p.handleSpec = function (event) {
        this.execute(this._model.prizes.spec.get_line(), this._model.prizes.spec.get_points(), this._model.prizes.spec.get_sum());
        return;
    }

    p.onPrepare = function () {
        //super.onPrepare();
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.PRIZE_SHOWN));
        return;
    }

    p.onComplete = function () {
        //super.onComplete();
        if ((this._model.prizes.spec.get_line() + 1) >= this._model.betParams.get_lines()) {
            this.clear();
        }
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.PRIZE_SHOWN));
        return;
    }

    window.Game.MVC.VPrizeSpecial = VPrizeSpecial;

})(window);