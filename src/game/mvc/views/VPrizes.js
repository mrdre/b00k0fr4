﻿(function (window) {
    function VPrizes() {
        Game.Basis.Prizes.call(this);
        // this._model;
        this.afterJackpot = false;

        // p.VPrizes()
        // {
        // return;
    }

    var p = createjs.extend(VPrizes, Game.Basis.Prizes);
    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VPrizes.js 15 - 20')
        this._model.prizes.addEventListener(Game.MVC.MEvent.PRIZE_WAIT, this.handlePrizeWait.bind(this));
        this._model.prizes.addEventListener(Game.MVC.MEvent.PRIZE_WAIT_END, this.handlePrizeWaitEnd.bind(this));
        this._model.prizes.addEventListener(Game.MVC.MEvent.SPIN_PRIZES, this.handleSpinPrizes.bind(this));
        this._model.prizes.addEventListener(Game.MVC.MEvent.HIDE_PRIZES, this.handleHidePrizes.bind(this));
        return;
    }

    p.handleSpinPrizes = function (event) {
        var _loc_6 = 0;
        var _loc_7 = 0;
        var _loc_2 = this._model.prizes.map.length;
        var _loc_3 = this._model.prizes.map[0].length;
        var _loc_4 = Array(_loc_2);
        var _loc_5 = _loc_2;
        while (_loc_5--) {

            _loc_4[_loc_5] = Array(_loc_3);
            _loc_6 = _loc_3;
            while (_loc_6--) {

                _loc_7 = _loc_5 * _loc_3 + _loc_6;
                _loc_4[_loc_5][_loc_6] = this._model.slots.get_tiles()[_loc_7];
            }
        }
        this.animate(_loc_4, this._model.prizes.map);
        return;
    }

    p.handlePrizeWait = function (event) {
        this.afterJackpot = this._model.prizes.jackpot.get_sum() > 0;
        this.wait(this.afterJackpot);
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.PRIZE_WAIT));
        return;
    }

    p.handlePrizeWaitEnd = function (event) {
        this.waitEnd();
        return;
    }

    p.handleHidePrizes = function (event) {
        this.hide();
        return;
    }

    window.Game.MVC.VPrizes = VPrizes;

})(window);