﻿(function (window) {
    function VScreen() {
        Game.Basis.Screen.call(this);
        // this._model;

        // p.VScreen= function()
        // {
        return;
    }

    var p = createjs.extend(VScreen, Game.Basis.Screen);
    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        ////console.log('D:\OpenServer\domains\test\src\game\mvc\views\VScreen.js 15 - 20')
        this._model.screen.addEventListener(Game.MVC.MEvent.PAGE, this.handlePage.bind(this));
        return;
    }

    p.handlePage = function (event) {
        switch (this._model.screen.get_page()) {
            case "slots":
                {
                    this.doSlots();
                    break;
                }
            case "gamble":
                {
                    this.doGamble();
                    break;
                }
            case "help":
                {
                    this.doHelp();
                    break;
                }
            case "error":
                {
                    this.doError();
                    break;
                }
            default:
                {
                    break;
                }
        }
        return;
    }

    window.Game.MVC.VScreen = VScreen;

})(window);