﻿(function (window) {
    function VSlots() {
        Game.Basis.Slots.call(this);
        // this._model:MGame;

        // p.VSlots()
        // {
        // return;
    }
    var p = createjs.extend(VSlots, Game.Basis.Slots);

    createjs.EventDispatcher.initialize(p);

    p.set_model = function (param1) {
        this._model = param1;
        //console.log('D:\OpenServer\domains\test\src\game\mvc\views\VSlots.js 15 - 20')
        this._model.slots.addEventListener(Game.MVC.MEvent.SLOTS_TILES, this.handleTiles.bind(this));
        this._model.slots.addEventListener(Game.MVC.MEvent.SLOTS_SPIN, this.handleSpin.bind(this));
        this._model.slots.addEventListener(Game.MVC.MEvent.SLOTS_FIN, this.handleFin.bind(this));
        return;
    }

    p.parseTiles = function (param1) {
        var _loc_4 = 0;
        var _loc_5 = 0;
        var _loc_2 = Array(Game.MVC.MSlots.REELS_COUNT);
        var _loc_3 = Game.MVC.MSlots.REELS_COUNT;
        while (_loc_3--) {

            _loc_2[_loc_3] = Array(Game.MVC.MSlots.REELS_SIZE);
            _loc_4 = Game.MVC.MSlots.REELS_SIZE;
            while (_loc_4--) {

                _loc_5 = _loc_3 * Game.MVC.MSlots.REELS_SIZE + _loc_4;
                _loc_2[_loc_3][_loc_4] = param1[_loc_5];
            }
        }
        return _loc_2;
    }

    p.onReelsComplete = function () {
        //super.onReelsComplete();
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.REELS_STOP));
        return;
    }

    p.handleTiles = function (event) {
        var _loc_2 = this.parseTiles(this._model.slots.get_tiles());
        this.setTiles(_loc_2);
        return;
    }

    p.handleSpin = function (event) {
        this.spin(this._model.slots.get_state(), this._model.slots.get_activeReels());
        this.dispatchEvent(new createjs.Event(Game.MVC.VEvent.REELS_START));
        return;
    }

    p.handleFin = function (event) {
        var _loc_2 = this.parseTiles(this._model.slots.get_tiles());
        this.setFin(_loc_2);
        return;
    }

    window.Game.MVC.VSlots = VSlots;
})(window);