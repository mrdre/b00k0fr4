﻿(function (window) {
	function GameRespHash() {
		//this.credits:uint;
		//this.currency:String;
		//this.exitURL:String;
		//this.exitTarg:String;
		//this.lang:String;
		//this.jpComps:Object;
		//this.slots:Array;
		//this.idSpec:int;
		//this.jpSum:uint;
		//this.jpKind:String;
		//this.factorGamble:uint;
		//this.errType:String;
		//this.errMsg:String;
		//this.denoms:String;
		//this.validator:String;

		//p.GameRespHash= function()
		//{
		//    return;
	}

	var p = GameRespHash.prototype;

	p.parseResp = function (param1, param2, param3) {
		var _loc_4 = null;
		var _loc_5 = 0;
		var _loc_6 = null;
	    //if (param2 != 'OK') {
	    //	this.errType = param2;
	    //	this.errMsg = param3.toString();
		//	return;
		//}
		switch (param1) {
			case "init":
				{
					this.credits = parseInt(param3.credits);
					this.currency = String(param3.currency);
					this.denoms = param3.denoms ? (String(param3.denoms)) : (null);
					this.lang = String(param3.lang);
					_loc_4 = String(param3.url_exit).split("\n");
					this.exitURL = _loc_4[0];
					this.exitTarg = _loc_4[1] ? (_loc_4[1]) : ("_self");
					_loc_4 = param3.jp_comps ? (String(param3.jp_comps).split(",")) : ([]);
					this.jpComps = {};
					_loc_5 = _loc_4.length;
					while (_loc_5--) {

						_loc_6 = String(_loc_4[_loc_5]).split(":");
						this.jpComps[_loc_6[0]] = _loc_6[1];
					}
					break;
				}
			case "spin":
				{
					this.slots = String(param3.slots).split("");
					_loc_5 = this.slots.length;
					while (_loc_5--) {

						this.slots[_loc_5] = parseInt(this.slots[_loc_5], 16);
					}
					this.idSpec = parseInt(param3.superTile);
					_loc_4 = param3.payJP.split(":");
					this.jpSum = parseInt(_loc_4[0]);
					this.jpKind = String(_loc_4[1]);
					break;
				}
			case "gamble":
				{
					this.factorGamble = parseInt(param3.factor);
					break;
				}
			case "take":
				{
					this.credits = parseInt(param3.credits);
					break;
				}
			default:
			    {
			        //this.errType = param2;
			        //this.errMsg = param3.toString();
					break;
				}
		}
		this.validator = String(param3.validator);
		return;
	}

	p.clear = function () {
		this.credits = 0;
		this.currency = null;
		this.exitURL = null;
		this.exitTarg = null;
		this.lang = null;
		this.jpComps = null;
		this.slots = null;
		this.jpSum = 0;
		this.jpKind = null;
		this.factorGamble = 0;
		this.errType = null;
		this.errMsg = null;
		this.validator = null;
		return;
	}
	window.Game.GameRespHash = GameRespHash;


})(window);