﻿(function (window) {
    function Server(param1) {
        //this.response:GameRespHash;
        //this.command;
        this.CMD_INIT = "init";
        this.CMD_SPIN = "spin";
        this.CMD_GAMBLE = "gamble";
        this.CMD_TAKE = "take";

        //this.url = 'http://vulcangrand.com/games/gaminators/bookofra/ge_server.php'//param1;//TODO
        this.url = 'https://crossorigin.me/http://g5.testslot.com/games/gaminators/Book_of_Ra/game.php'//param1;//TODO
        //this.url = 'http://g5.testslot.com/games/gaminators/Book_of_Ra/game.php'//param1;//TODO
        this.response = new Game.GameRespHash();
        this.VALIDATOR = new Date().getTime().toString(16).toUpperCase();
        return;
    }

    var p = Server.prototype;
    createjs.EventDispatcher.initialize(p);

    p.askInit = function (param1) {
        this.command = this.CMD_INIT;
        this.askServer({ command: "authentication", lang: param1, validator: this.VALIDATOR });
        return;
    }

    p.askSpin = function (param1, param2, param3, param4) {
        this.command = this.CMD_SPIN;
        this.askServer({ command: "spin", denomination: param1, bet: param2, lines: param3, reels: param4.join() });
        return;
    }

    p.askGamble = function () {
        this.command = this.CMD_GAMBLE;
        this.askServer({ command: "gamble" });
        return;
    }

    p.askTake = function () {
        this.command = this.CMD_TAKE;
        this.askServer({ command: "takeWin" });
        return;
    }

    p.clbLoad = function (param1, param2) {
        this.response.parseResp(this.command, param1, param2);
        if (this.response.errType || this.response.errMsg) {
            this.dispatchEvent(new createjs.Event(Game.SrvEvent.ERROR));
            return;
        }
        //if (this.response.validator != this.VALIDATOR) {
        //    this.response.errType = "invalidation";
        //    this.response.errMsg = null;
        //    this.dispatchEvent(new createjs.Event(Game.SrvEvent.ERROR));
        //    return;
        //}
        switch (this.command) {
            case this.CMD_INIT:
                {
                    this.dispatchEvent(new createjs.Event(Game.SrvEvent.INIT));
                    break;
                }
            case this.CMD_SPIN:
                {
                    this.dispatchEvent(new createjs.Event(Game.SrvEvent.SPIN));
                    break;
                }
            case this.CMD_GAMBLE:
                {
                    this.dispatchEvent(new createjs.Event(Game.SrvEvent.GAMBLE));
                    break;
                }
            case this.CMD_TAKE:
                {
                    this.dispatchEvent(new createjs.Event(Game.SrvEvent.TAKE));
                    break;
                }
            default:
                {
                    this.dispatchEvent(new createjs.Event(Game.SrvEvent.ERROR));
                    break;
                    break;
                }
        }
        return;
    }

    p.askServer = function (cmd) {
        var me = this,
            url = '';
        for (var k in cmd) {
            url += '&' + k + '=' + cmd[k]
        }
        url = url.substring(1);
        var xhttp = new window.XMLHttpRequest()

        //xhttp.withCredentials = true;
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                var match,
                pl = /\+/g,  // Regex for replacing addition symbol with a space
                search = /([^&=]+)=?([^&]*)/g,
                decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },

                query = xhttp.responseText == 'Not enougth money to bet' ? 'slots=759097092075621&payJP=0&validator=155C0A44EC0' : xhttp.responseText;
                //query = xhttp.responseText == 'Not enougth money to bet' ? 'slots=|8|6|10|1|10|8|1|10|3|1|8|6|7|3|2|1|10|10|0|0|10|0|0|1|0&payJP=0&validator=155C0A44EC0' : xhttp.responseText;
                //query = xhttp.responseText == 'Not enougth money to bet' ? 'slots=291084539718782&payJP=0&validator=155C0A44EC0' : xhttp.responseText;
                //query = xhttp.responseText == 'Not enougth money to bet' ? 'slots=342975159645962&payJP=0&superTile=3&validator=155C0A44EC0' : xhttp.responseText;
                console.log(query)
                urlParams = {};
                while (match = search.exec(query))
                    urlParams[decode(match[1])] = decode(match[2]);

                me.clbLoad(xhttp.statusText, urlParams)
            }
        };
        xhttp.open("GET", this.url + '?' + url, true);
        xhttp.send();
    }
    //p.askServer = function (cmd) {
    //    var createCORSRequest = function (method, url) {
    //        var xhr = new XMLHttpRequest();
    //        if ("withCredentials" in xhr) {
    //            // Most browsers.
    //            xhr.open(method, url, true);
    //        } else if (typeof XDomainRequest != "undefined") {
    //            // IE8 & IE9
    //            xhr = new XDomainRequest();
    //            xhr.open(method, url);
    //        } else {
    //            // CORS not supported.
    //            xhr = null;
    //        }
    //        return xhr;
    //    };

    //    var method = 'POST',
    //        xhr = createCORSRequest(method, this.url),
    //        me = this;

    //    xhr.onload = function () {
    //        var match,
    //        pl = /\+/g,  // Regex for replacing addition symbol with a space
    //        search = /([^&=]+)=?([^&]*)/g,
    //        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },

    //        //query = xhttp.responseText == 'Not enougth money to bet' ? 'slots=792760958832728&payJP=0&validator=155C0A44EC0' : xhttp.responseText;
    //        query = xhr.responseText == 'Undefined error' ? 'slots=291084539718782&payJP=0&validator=155C0A44EC0' : xhr.responseText;
    //        //query = xhttp.responseText == 'Not enougth money to bet' ? 'slots=342975159645962&payJP=0&superTile=3&validator=155C0A44EC0' : xhttp.responseText;
    //        console.log(query)
    //        urlParams = {};
    //        while (match = search.exec(query))
    //            urlParams[decode(match[1])] = decode(match[2]);

    //        me.clbLoad(xhr.statusText, urlParams);
    //    };

    //    xhr.onerror = function () {
    //        // Error code goes here.
    //    };
    //    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //    xhr.send('action=state&min=&game=bookofra');
    //}
    window.Game.Server = Server;


})(window);