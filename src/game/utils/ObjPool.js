﻿(function (window) {

	function ObjPool() {

		if (this.INST) {
			throw new Error(this.ERR[0]);
		}
		if (!this._store) {
			this._store = {};
		}

		this.getInst = function (param1) {
			if (!param1) {
				console.log(this.ERR[1]);
			}
			if (!this._store[param1.toString()]) {
				this._store[param1] = new Array();
			}
			var _loc_2 = this._store[param1];
			return _loc_2.length ? (_loc_2.pop()) : (new param1);
		}

		this.giveInst = function (param1) {
			if (!param1) {
				console.log(this.ERR[2]);
			}
			var _loc_2 = param1["constructor"];
			if (!this._store[_loc_2]) {
				this._store[_loc_2] = new Array();
			}
			var _loc_3 = this._store[_loc_2];
			_loc_3.push(param1);
			param1 = null;
			return;
		}

		this.getInstCount = function (param1) {
			var _loc_2 = this._store[param1];
			return _loc_2 ? (_loc_2.length) : (0);
		}

		this.clearInstances = function (param1) {
			delete this._store[param1];
			return;
		}

		this.clearAll = function () {
			this._store = {};
			return;
		}
		this.ERR = ["Oops... `ObjPool` is a singleton. Use `ObjPool.inst` to access an instance.", "Argument `aClass` must be not null.", "Argument `inst` must be not null."];
	}

	ObjPool.get_inst = function () {
		if (!this.INST) {
			this.INST = new ObjPool;
		}
		return this.INST;
	}

	window.Game.Utils.ObjPool = ObjPool;

})(window);