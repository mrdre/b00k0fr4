(function (window) {
	function PlayList() {
		this.init();
	}
	var p = PlayList.prototype;
	createjs.EventDispatcher.initialize(p);
	p.get_numSounds = function () {
		return this._list.length;
	}

	p.clear = function () {
		this._list.length = 0;
	}

	p.add = function () {
		var _loc3_ = null;
		var _loc2_ = arguments.length;
		while (_loc2_--) {
			_loc3_ = arguments[_loc2_];
			if (_loc3_) {
				this._list.push(_loc3_);
			}
		}
	}

	p.play = function () {
		this._curr = 0;
		this.next();
	}

	p.init = function () {
		this._list = [];
	}

	p.next = function () {
		var _loc1_ = null;
		if (this._curr < this._list.length) {
			_loc1_ = this._list[this._curr++];
			this._chan = _loc1_.play();
			this._chan.addEventListener('complete', this.handleSoundComplete.bind(this));
		}
		else {
			this.stop();
		}
	}

	p.stop = function () {
		this._list.length = 0;
		this.dispatchEvent('Event.COMPLETE');
	}

	p.handleSoundComplete = function (param1) {
		this._chan.removeAllEventListeners('complete', this.handleSoundComplete);
		this.next();
	}
	window.PlayList = PlayList;
})(window);