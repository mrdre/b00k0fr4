(function (window) {
	function ThemePlayer(param1) {
		this._snd = param1;
	}
	var p = ThemePlayer.prototype;

	p.set_sound = function (param1) {
		this.stop();
		this._snd = param1;
	}

	p.play = function () {
		if (this._chan) {
			return;
		}
		if (this._pos + 10 > this._snd.length) {
			this._pos = 0;
		}
		this._chan = this._snd.play(this._pos);
		this._chan.addEventListener('complete', this.handleSoundCompletet.bind(this));
	}

	p.pause = function () {
		if (!this._chan) {
			return;
		}
		this._pos = this._chan.position;
		this._chan.removeAllEventListeners('complete', this.handleSoundCompletet);
		this._chan.stop();
		this._chan = null;
	}

	p.stop = function () {
		this._pos = 0;
		if (!this._chan) {
			return;
		}
		this._chan.removeAllEventListeners('complete', this.handleSoundCompletet);
		this._chan.stop();
		this._chan = null;
	}

	p.handleSoundCompletet = function (param1) {
		this._chan.removeAllEventListeners('complete', this.handleSoundCompletet);
		this._pos = 0;
		this._chan = this._snd.play(0);
		this._chan.addEventListener('complete', this.handleSoundCompletet.bind(this));
	}
	window.ThemePlayer = ThemePlayer;
})(window);
