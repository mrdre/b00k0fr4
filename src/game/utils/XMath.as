package lib.utils
{
   public class XMath
   {
       
      public function XMath()
      {
         super();
      }
      
      public static function intRnd(param1:int, param2:int = 0) : int
      {
         var _loc3_:int = 0;
         if(param1 < param2)
         {
            _loc3_ = param1;
            param1 = param2;
            param2 = param1;
         }
         return Math.round(param2 + Math.random() * (param1 - param2));
      }
      
      public static function clamp(param1:Number, param2:Number, param3:Number) : Number
      {
         var _loc4_:Number = NaN;
         if(param2 > param3)
         {
            _loc4_ = param2;
            param2 = param3;
            param3 = _loc4_;
         }
         if(param1 < param2)
         {
            param1 = param2;
         }
         if(param1 > param3)
         {
            param1 = param3;
         }
         return param1;
      }
      
      public static function arrMinID(param1:Object) : uint
      {
         if(!param1.hasOwnProperty("length"))
         {
            throw new ArgumentError("`stack` must be an array or a vector.");
         }
         var _loc2_:uint = 0;
         var _loc3_:uint = param1["length"];
         var _loc4_:uint = 1;
         while(_loc4_ < _loc3_)
         {
            if(param1[_loc4_] < param1[_loc2_])
            {
               _loc2_ = _loc4_;
            }
            _loc4_++;
         }
         return _loc2_;
      }
      
      public static function arrMaxID(param1:*) : uint
      {
         if(!param1.hasOwnProperty("length"))
         {
            throw new ArgumentError("`stack` must be an array or a vector.");
         }
         var _loc2_:uint = 0;
         var _loc3_:uint = param1["length"];
         var _loc4_:uint = 1;
         while(_loc4_ < _loc3_)
         {
            if(param1[_loc4_] > param1[_loc2_])
            {
               _loc2_ = _loc4_;
            }
            _loc4_++;
         }
         return _loc2_;
      }
      
      public static function nextItem(param1:Object, param2:Object) : Object
      {
         var _loc3_:uint = param2["length"];
         var _loc4_:uint = 1 + (param2["indexOf"] as Function).call(null,param1);
         if(_loc4_ >= _loc3_)
         {
            _loc4_ = 0;
         }
         return param2[_loc4_];
      }
   }
}
