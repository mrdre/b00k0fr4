﻿(function (window) {
    var XMath = {};

    XMath.intRnd = function (param1, param2) {
        param2 = param2 == undefined ? 0 : param2
        var _loc_3 = 0;
        if (param1 < param2) {
            _loc_3 = param1;
            param1 = param2;
            param2 = param1;
        }
        return Math.round(param2 + Math.random() * (param1 - param2));
    }

    XMath.clamp = function (param1, param2, param3) {
        var _loc_4 = NaN;
        if (param2 > param3) {
            _loc_4 = param2;
            param2 = param3;
            param3 = _loc_4;
        }
        if (param1 < param2) {
            param1 = param2;
        }
        if (param1 > param3) {
            param1 = param3;
        }
        return param1;
    }

    XMath.arrMinID = function (param1) {
        if (!param1.hasOwnProperty("length")) {
            throw new ArgumentError("`stack` must be an array or a vector.");
        }
        var _loc_2 = 0;
        var _loc_3 = param1["length"];
        var _loc_4 = 1;
        while (_loc_4 < _loc_3) {

            if (param1[_loc_4] < param1[_loc_2]) {
                _loc_2 = _loc_4;
            }
            _loc_4 = _loc_4 + 1;
        }
        return _loc_2;
    }

    XMath.arrMaxID = function (param1) {
        if (!param1.hasOwnProperty("length")) {
            throw new ArgumentError("`stack` must be an array or a vector.");
        }
        var _loc_2 = 0;
        var _loc_3 = param1["length"];
        var _loc_4 = 1;
        while (_loc_4 < _loc_3) {

            if (param1[_loc_4] > param1[_loc_2]) {
                _loc_2 = _loc_4;
            }
            _loc_4 = _loc_4 + 1;
        }
        return _loc_2;
    }

    XMath.nextItem = function (param1, param2) {
        //console.log(param1, param2)
        var _loc_3 = param2.length;
        var _loc_4 = 1 + param2.indexOf(param1);
        if (1 + param2.indexOf(param1) >= _loc_3) {
            _loc_4 = 0;
        }
        return param2[_loc_4];
    }
    window.XMath = XMath;
})(window);