﻿(function (window) {

    function BaseBox(param1) {
        param1 = (param1 == undefined) ? 12 : param1;
        this.Container_constructor();

        this._container = new createjs.Container();
        this._buttons = new Game.BaseBtn();
        var ERR = ["`BaseBox` is a template class. It is only for inheritance", "Box must be added to stage before showing content."];

        this._buttonsCount = param1;
        if (this["constructor"] == BaseBox) {
            throw new Error(ERR[0]);
        }
        if (stage) {
            this.handleAdded();
        }
        else {
            //console.log('D:\OpenServer\domains\test\src\slots_box\base\BaseBox.js 19')
            this.addEventListener(Event.ADDED_TO_STAGE, this.handleAdded.bind(this));
        }
    }

    var p = createjs.extend(BaseBox, createjs.Container);

    p.showContent = function (param1) {
        //if (!stage) {
        //    throw new Error(ERR[1]);
        //}
        //this.addChild(param1);
        event = new createjs.Event('ModelPropagationEvent');
        event.data = this._model;
        param1.dispatchEvent(event);
        //stage.dispatchEvent(new ModelPropagationEvent(this._model));
        //setTimeout(this.cleanFrame.bind(this), 10);
    }

    p.defineButton = function (param1) {
        this._buttons[param1._id] = param1;
        ////console.log('D:\OpenServer\domains\test\src\slots_box\base\BaseBox.js 40')
        param1.addEventListener(Game.ModelBox.PRESS, this.handlePress.bind(this));
    }

    p.cleanFrame = function () {
        while (this.numChildren > 1) {

            this.removeChildAt(0);
        }
    }

    p.handleAdded = function (event) {
        this.removeAllEventListeners(Event.ADDED_TO_STAGE, this.handleAdded);
        this.init();
    }

    p.handlePress = function (event) {
        var _loc_2 = event.target;
        this._model.set_pressed(_loc_2._id);
    }

    p.handleModelChange = function (event) {
        var _loc_2 = this._buttonsCount;
        while (_loc_2--) {
            this._buttons[_loc_2].set_mode(this._model.get_mode());
            this._buttons[_loc_2].set_enable(this._model.getState(_loc_2));
        }
    }

    p.init = function () {
        this._buttons = Array(this._buttonsCount);
        this._model = new Game.ModelBox(this._buttonsCount);
            ////console.log('D:\OpenServer\domains\test\src\slots_box\base\BaseBox.js 72')
        this._model.addEventListener(Game.ModelBox.CHANGE, this.handleModelChange.bind(this));
    }

    window.Game.BaseBox = createjs.promote(BaseBox, "Container");
})(window);