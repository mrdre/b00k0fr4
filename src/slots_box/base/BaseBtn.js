﻿(function (window) {
    function BaseBtn(param1) {

        this._id = param1;
        this._mode = '';
        this._enable = false;
    }

    var p = BaseBtn.prototype;

    createjs.EventDispatcher.initialize(p);

    p.get_id = function () {
        return this._id;
    }

    p.get_mode = function () {
        return this._mode;
    }

    p.set_mode = function (param1) {
        this._mode = param1;
        this.updMode();
        return;
    }

    p.get_enable = function () {
        return this._enable;
    }

    p.set_enable = function (param1) {
        this._enable = param1;
        this.updEnable();
        return;
    }

    p.updMode = function () {
        return;
    }

    p.updEnable = function () {
        return;
    }

    p.report = function () {
        this.dispatchEvent(Game.ModelBox.PRESS);
        return;
    }
    window.Game.BaseBtn = BaseBtn;
})(window);