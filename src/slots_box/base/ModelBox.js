﻿(function (window) {

    ModelBox.CHANGE = "change";
    ModelBox.PRESS = "press";

    function ModelBox(param1) {

        this._mode = '';
        this._buttonsCount = null;
        this._lastPressed = null;
        this._states = [];

        this._buttonsCount = param1;
        this.init();
    }

    var p = ModelBox.prototype;

    createjs.EventDispatcher.initialize(p);

    p.get_mode = function () {
        return this._mode;
    }

    p.set_mode = function (param1) {
        this._mode = param1;
        return;
    }

    p.get_pressed = function () {
        return this._lastPressed;
    }

    p.set_pressed = function (param1) {
        if (param1 < this._buttonsCount) {
            this._lastPressed = param1;
            this.dispatchEvent(Game.ModelBox.PRESS);
        }
        return;
    }

    p.getState = function (param1) {
        return param1 < this._buttonsCount ? (this._states[param1]) : (false);
    }

    p.setStates = function (param1) {
        var _loc_4 = 0;
        var _loc_2 = false;
        var _loc_3 = param1.length > this._buttonsCount ? (this._buttonsCount) : (param1.length);
        while (_loc_3--) {
            _loc_4 = param1[_loc_3];
            if (_loc_4 == 0 || _loc_4 == 1) {
                if (this._states[_loc_3] !== _loc_4) {
                    this._states[_loc_3] = _loc_4;
                    _loc_2 = true;
                }
            }
        }
        if (_loc_2) {
            this.dispatchEvent(Game.ModelBox.CHANGE);
        }
        return;
    }

    p.enableButtons = function (param1) {
        var _loc_3 = 0;
        if (!param1.length) {
            param1 = [parseInt(param1)];
        }
        var _loc_2 = false;
        var _loc_4 = param1["length"];
        while (_loc_4--) {

            _loc_3 = parseInt(param1[_loc_4]);
            if (_loc_3 < this._buttonsCount) {
                if (!this._states[_loc_3]) {
                    this._states[_loc_3] = true;
                    _loc_2 = true;
                }
            }
        }
        if (_loc_2) {
            this.dispatchEvent(Game.ModelBox.CHANGE);
        }
        return;
    }

    p.disableButtons = function (param1) {
        var _loc_3 = 0;
        if (!param1.length) {
            param1 = [parseInt(param1)];
        }
        var _loc_2 = false;
        var _loc_4 = param1["length"];
        while (_loc_4--) {

            _loc_3 = parseInt(param1[_loc_4]);
            if (_loc_3 < this._buttonsCount) {
                if (this._states[_loc_3]) {
                    this._states[_loc_3] = false;
                    _loc_2 = true;
                }
            }
        }
        if (_loc_2) {
            this.dispatchEvent(Game.ModelBox.CHANGE);
        }
        return;
    }

    p.init = function () {
        this._states = Array(this._buttonsCount);
        this._lastPressed = Number.MAX_VALUE;
        this._mode = "";
        return;
    }
    window.Game.ModelBox = ModelBox;
})(window);