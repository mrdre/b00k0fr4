﻿(function () {

    function Button(param1, param2) {
        Game.BaseBtn.call(this);
        this._id = param1;
        this.FRAME_BLOCK = 0;
        this.FRAME_UP = 1;
        this.FRAME_DOWN = 2;

        this._targ = param2;

        this._targ.mouseChildren = false;
        this.set_enable(false);
    }

    var p = createjs.extend(Button, Game.BaseBtn);

    //	override protected function updMode() : void
    //	{
    //		super.updMode();
    //		return;
    //	}

    p.updEnable = function () {
        var _loc_1 = this._enable ? (this.FRAME_UP) : (this.FRAME_BLOCK);
        this._targ.mouseEnabled = this._enable;
        this._targ.buttonMode = this._enable;
        this._targ.useHandCursor = this._enable;
        this._targ.gotoAndStop(_loc_1);
        if (this._enable) {
            if (!this._targ.hasEventListener('click')) {
                this._targ.addEventListener('click', this.handleMouse.bind(this));
                this._targ.addEventListener('mousedown', this.handleMouse.bind(this));
                this._targ.addEventListener('pressup', this.handleMouse.bind(this));
                this._targ.addEventListener('rollout', this.handleMouse.bind(this));
            }
        }
        else {
            this._targ.removeAllEventListeners();
        }
        return;
    }

    p.handleMouse = function (event) {
        if (!this._enable) {
            return;
        }
        switch (event.type) {
            case 'click':
                {
                    this.dispatchEvent(Game.ModelBox.PRESS);
                    break;
                }
            case 'mousedown':
                {
                    //this._targ.removeAllEventListeners('mousedown', this.handleMouse);
                    ////console.log('D:\OpenServer\domains\test\src\slots_box\gaminator\Button.js 57-58')
                    //this._targ.addEventListener('pressup', this.handleMouse.bind(this));
                    //this._targ.addEventListener('mouseout', this.handleMouse.bind(this));
                    this._targ.gotoAndStop(this.FRAME_DOWN);
                    break;
                }
            case 'pressup':
            case 'rollout':
                {
                    //        this._targ.removeAllEventListeners('pressup', this.handleMouse);
                    //        this._targ.removeAllEventListeners('mouseout', this.handleMouse);
                    ////console.log('D:\OpenServer\domains\test\src\slots_box\gaminator\Button.js 68')
                    //        this._targ.addEventListener('mousedown', this.handleMouse.bind(this));
                    this._targ.gotoAndStop(this.FRAME_UP);
                    break;
                }
            default:
                {
                    break;
                }
        }
        return;
    }
    window.Game.Button = Button
})(window);