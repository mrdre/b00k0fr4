﻿(function (window) {
    function GaminatorBox() {
        Game.BaseBox.call(this);

        this.soundMute = false;
        this.fullScreen = false;

        this.butState = new Array();
        var _loc_2 = null;
        this._targ = new but_lib.graphics();
        this._targ.width = 640;
        this._targ.height = 480;
        this._targ.size = 640 / 480;
        this._container = this._targ.container;
        //this._container.scrollRect = new createjs.Rectangle(0, 0, 640, 480);
        var _loc_1 = this._buttonsCount;
        while (_loc_1--) {
                _loc_2 = new Game.Button(_loc_1, this._targ["b" + _loc_1]);
                this.defineButton(_loc_2);
        }
        stage.addEventListener(KeyboardEvent.KEY_DOWN, this.handleKeyPress);
        stage.addEventListener(KeyboardEvent.KEY_UP, this.handleKeyPress);
        this.addChild(this._targ);
        //this._targ.on('added', this.additionalButtons.bind(this))
        this.additionalButtons();
        //stage.addEventListener(Event.RESIZE, this.updateSize);
        //this.updateSize();
        return;
    }
    var p = createjs.extend(GaminatorBox, Game.BaseBox);

    //private function updateSize(event:Event = null) : void
    //{
    //    this._targ.width = stage.stageWidth;
    //    this._targ.height = this._targ.width / this._targ.size;
    //    if (this._targ.height > stage.stageHeight)
    //    {
    //        this._targ.height = stage.stageHeight;
    //        this._targ.width = this._targ.height * this._targ.size;
    //    }
    //    this._targ.x = (stage.stageWidth - this._targ.width) / 2;
    //    return;
    //}

    p.additionalButtons = function () {
        //this.b12 = this._targ.b12;
        //this.b13 = this._targ.b13;
        //this.b12.gotoAndStop(1);
        //this.b13.gotoAndStop(1);
        //this.b12.buttonMode = true;
        //this.b13.buttonMode = true;
        //this.b12.addEventListener('click', this.handleSoundChange);
        //this.b13.addEventListener('click', this.handleScreenChange);
        //stage.addEventListener(FullScreenEvent.FULL_SCREEN, this.fullScreenRedraw);
        return;
    }

    //private function fullScreenRedraw(event:FullScreenEvent) : void
    //{
    //    if (stage.displayState == StageDisplayState.NORMAL)
    //    {
    //        this.b13.gotoAndStop(2);
    //    }
    //    else
    //    {
    //        this.b13.gotoAndStop(3);
    //    }
    //    return;
    //}

    p.handleSoundChange = function (event) {
        if (this.soundMute) {
            //SoundMixer.soundTransform = new SoundTransform(1);
            event.currentTarget.gotoAndStop(1);
            this.soundMute = false;
            createjs.Sound.volume = 1;
        }
        else {
            //SoundMixer.soundTransform = new SoundTransform(0);
            event.currentTarget.gotoAndStop(2);
            this.soundMute = true;
            createjs.Sound.volume = 0;
        }
    }

    p.handleScreenChange = function (event) {

        if (this.fullScreen) {
            event.currentTarget.gotoAndStop(2);
            this.fullScreen = false;
        }
        else {
            event.currentTarget.gotoAndStop(3);
            this.fullScreen = true;
        }

        //if (stage.displayState == StageDisplayState.NORMAL)
        //{
        //    stage.displayState = StageDisplayState.FULL_SCREEN;
        //    event.target.gotoAndStop(3);
        //}
        //else
        //{
        //    stage.displayState = StageDisplayState.NORMAL;
        //    event.target.gotoAndStop(2);
        //}
        //return;
    }

    //private function handleKeyPress(event:KeyboardEvent) : void
    //{
    //    var _loc_2:* = new Array();
    //    _loc_2[32] = "b11";
    //    _loc_2[37] = "b9";
    //    _loc_2[39] = "b10";
    //    _loc_2[49] = "b4";
    //    _loc_2[50] = "b5";
    //    _loc_2[51] = "b6";
    //    _loc_2[52] = "b7";
    //    _loc_2[53] = "b8";
    //    if (_loc_2[event.keyCode])
    //    {
    //        switch(event.type)
    //        {
    //            case "keyDown":
    //            {
    //                if (!this.butState[event.keyCode])
    //                {
    //                    this._targ.getChildByName(_loc_2[event.keyCode]).dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
    //                    this._targ.getChildByName(_loc_2[event.keyCode]).dispatchEvent(new MouseEvent(MouseEvent.CLICK));
    //                }
    //                this.butState[event.keyCode] = true;
    //                break;
    //            }
    //            case "keyUp":
    //            {
    //                this._targ.getChildByName(_loc_2[event.keyCode]).dispatchEvent(new MouseEvent(MouseEvent.MOUSE_UP));
    //                this.butState[event.keyCode] = false;
    //                break;
    //            }
    //            default:
    //            {
    //                break;
    //            }
    //        }
    //    }
    //    return;
    //}

    window.Game.GaminatorBox = GaminatorBox;
})(window);